<%-- 
    Document   : counter_create
    Created on : Oct 13, 2016, 3:18:54 PM
    Author     : Kelum Madushan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
<!DOCTYPE html>

<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>
        <script>
            var role = "<%=session.getAttribute("role")%>";
            if(role !== 'ADMIN'){
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }
    
            var username = "<%=session.getAttribute("loggedUser")%>"
            if(!<%=session.getAttribute("loggedUser")%>){
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            }else{
            }

        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/admin_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/admin_menu.jsp"/>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Application Counter Management <small>create</small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div id="container">
                        <div class="row">           
                            <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                <div class="box box-primary">
                                    <form:form role="form" id="insertForm" method="POST" modelAttribute="insertForm" action="${pageContext.request.contextPath}/admin/applicationcountermanagement/addcounter">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <form:label path="description">Description:<samp style="color: red">*</samp></form:label>
                                                <form:input path="description" required="required" type="text" id="description" class="form-control" placeholder="Enter Description"></form:input>   
                                                </div>
                                                <div class="form-group">
                                                <form:label  path="status">Status:<samp style="color: red">*</samp></form:label>
                                                <form:select class="form-control" required="required" id="status" path="status">
                                                    <form:option value="">Select Status</form:option> 
                                                    <c:forEach  items="${status}" var="status">
                                                        <form:option value="${status.statusCode}">${status.statusCode}</form:option> 
                                                    </c:forEach>
                                                </form:select>
                                            </div>

                                            <div class="box-footer btn-toolbar">
                                                <form:button type="button" class="btn btn-primary pull-right" onclick="window.history.back();">Cancel</form:button>
                                                <form:button id="createbutton" type="submit" class="btn btn-primary pull-right">Save</form:button>     
                                                </div>
                                            </div>
                                    </form:form>
                                </div>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>   
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
        <jsp:include page="../template/jsinclude.jsp"/>
    </body>
</html>
