<%-- 
    Document   : counter_create
    Created on : Oct 13, 2016, 3:18:54 PM
    Author     : Kelum Madushan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
<!DOCTYPE html>


<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>

        <spring:url value="/resources/Datepicker/bootstrap-datetimepicker.css" var="datepicker" />
        <link href="${datepicker}" rel="stylesheet" />

        <script>
            var role = "<%=session.getAttribute("role")%>";
            if (role !== 'ADMIN') {
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }

            var username = "<%=session.getAttribute("loggedUser")%>"
            if (!<%=session.getAttribute("loggedUser")%>) {
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            } else {
            }

        </script>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/admin_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/admin_menu.jsp"/>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Holiday Management  <small>create</small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div id="container">
                        <div class="row">           
                            <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                <div class="box box-primary">
                                    <form:form role="form" id="insertForm" method="POST" modelAttribute="insertForm" action="${pageContext.request.contextPath}/admin/holidaymanagement/addholiday">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <form:label path="holidaydate">Holiday:<samp style="color: red">*</samp></form:label>
                                                <form:input path="holidaydate" type="text" id="datetimepicker" class="form-control" placeholder="Select Date" readonly="true"></form:input>  
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-sm-2">
                                                    <form:label path="halfday">Half Day:</form:label>
                                                    <form:checkbox id="halfday" path="halfday" onclick="halfdayclick()"></form:checkbox>
                                                    </div>
                                                    <div class="col-sm-5">
                                                    <form:label path="startTime">Working From:</form:label>
                                                    <form:input path="startTime" disabled="true" required="required" type="text" id="datetimepickerStartTime" class="form-control" placeholder="Select Start Time" readonly="true" onchange="setEndHideTime()"></form:input>
                                                    </div>
                                                    <div class="col-sm-5">
                                                    <form:label path="endTime">Working To:</form:label>
                                                    <form:input path="endTime" disabled="true" required="required" type="text" id="datetimepickerEndTime" class="form-control" placeholder="Select End Time" readonly="true" onchange="setStartHideTime()"></form:input>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                <form:label path="holidaydescription">Description:<samp style="color: red">*</samp></form:label>
                                                <form:input path="holidaydescription" required="required" type="text" id="holidaydescription" class="form-control" placeholder="Enter Description" maxlength="32"></form:input>   
                                                </div>
                                                <div class="form-group">
                                                <form:label  path="status">Status:<samp style="color: red">*</samp></form:label>
                                                <form:select class="form-control" required="required" id="status" path="status">
                                                    <form:option value="">Select Status</form:option> 
                                                    <c:forEach  items="${status}" var="status">
                                                        <form:option value="${status.statusCode}">${status.statusDescription}</form:option> 
                                                    </c:forEach>
                                                </form:select>
                                            </div>

                                            <div class="box-footer btn-toolbar">
                                                <form:button type="button" class="btn btn-primary pull-right" onclick="window.history.back();">Cancel</form:button>
                                                <form:button id="createbutton" type="submit" class="btn btn-primary pull-right">Save</form:button>     
                                                </div>
                                            </div>
                                    </form:form>
                                </div>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>   
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
        <jsp:include page="../template/jsinclude.jsp"/>

        <!--DatePicker-->
        <spring:url value="/resources/Datepicker/bootstrap-datetimepicker.min.js" var="Datepicker" />
        <script src="${Datepicker}"></script>
        <!--DatePicker end-->

        <script>
            $('#datetimepicker').datetimepicker({
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });
            var currentDate = new Date();
            currentDate.setDate(currentDate.getDate() + 1);
            $('#datetimepicker').datetimepicker('setStartDate', currentDate);
            $('#datetimepickerStartTime').datetimepicker({
                format: 'h:ii',
                autoclose: true,
                showMeridian: true,
                startView: 1,
                maxView: 2
            });
            $('#datetimepickerEndTime').datetimepicker({
                format: 'h:ii',
                autoclose: true,
                showMeridian: true,
                startView: 1,
                maxView: 2
            });

            function halfdayclick() {
                if (document.getElementById('halfday').checked) {
                    $("#datetimepickerStartTime").removeAttr("disabled");
                    $("#datetimepickerEndTime").removeAttr("disabled");
                } else {
                    $('#datetimepickerStartTime').val("");
                    $('#datetimepickerEndTime').val("");
                    $("#datetimepickerStartTime").attr("disabled", true);
                    $("#datetimepickerEndTime").attr("disabled", true);
                }
            }

            function setEndHideTime() {
                $('#datetimepickerEndTime').datetimepicker('setStartDate', $('#datetimepickerStartTime').val());
            }
            function setStartHideTime() {
                $('#datetimepickerStartTime').datetimepicker('setEndDate', $('#datetimepickerEndTime').val());
            }

//            $('#insertForm').validate({
//                rules: {
////                    datetimepicker: {
////                        required: true
////                    },
//                    holidaydescription: {
//                        required: true
//                    },
//                    status: {
//                        required: true
//                    },
//                    holidaydate: {
//                        required: true,
//                        remote: {
//                            url: "${pageContext.servletContext.contextPath}/admin/holidaymanagement/exsist",
//                            type: "post",
//                            data: {
//                                datetimepicker: function() {
//                                    return $('#datetimepicker').val();
//                                }
//                            }
//                        }
//                    }
//                },
//                messages: {
//                    holidaydate: {
//                        remote: "Already exist holiday date!"
//                    }
//
//                }
//            });

            $('#insertForm').validate({
                rules: {
//                    datetimepicker: {
//                        required: true
//                    },
                    holidaydescription: {
                        required: true
                    },
                    status: {
                        required: true
                    },
                    holidaydate: {
                        required: true
                    }
                }, errorPlacement: function (error, element) {
                    element.parent().find('div.invalid').remove();
                    element.parent().append('<div class="invalid">' + error.text() + '</div>');
                }
            });
        </script>
        <script>
            $(".sidebar-menu li").removeClass("selected");
            $(".sidebar-menu  .holyday-mgmt").addClass("selected");

        </script>

    </body>
</html>
