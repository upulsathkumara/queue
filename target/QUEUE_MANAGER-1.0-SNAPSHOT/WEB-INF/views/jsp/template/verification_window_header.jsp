<%-- 
    Document   : user_header
    Created on : Oct 19, 2016, 11:15:42 AM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page session="true"%>
<!-- Logo -->
<div class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">FDO</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">Application Counter</span>
</div>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="pull-left pro-name">
        Reservation Details Verification 
    </div>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">


           

            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <!--                    <li class="user-header">
                                            <img src="${pageContext.servletContext.contextPath}/resources/img/user.png" class="img-circle" alt="User Image" style="border: none;">
                    
                                            <p>
                                                Test Name - Front Desk Officer
                                                <small>Member since Nov. 2012</small>
                                            </p>
                                        </li>-->
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <!--                        <div class="pull-left">
                                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                                </div>-->
                        <div class="pull-right">
                            <a href="<c:url value='/j_spring_security_logout' />" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>

<jsp:include page="../template/jsinclude.jsp"/>
<script>

    $("#notificatioinsVip").click(function (event) {
        $.ajax({
            async: true,
            type: "POST",
            url: "${pageContext.servletContext.contextPath}/notification/list",
            cache: false,
            data: {data: 1},
            success: function (res) {

                res1 = JSON.parse(res);
                var i = 0;
                $("#notificationList").empty();
                $.each(res1.object, function () {
                    $("#notificationList").append('<li>'
                            + '<a id="' + res1.object[i] + '" class="notificationValue">'
                            + 'Service Id:' + res1.object[i]
                            + '</a>'
                            + '</li>');
                    i++;
                });


            }
        });

    });


    $("#notificationList").on("click", ".notificationValue", function () {
        var id = $(this).attr("id");
        $.ajax({
            async: true,
            type: "POST",
            url: "${pageContext.servletContext.contextPath}/notification/serviceId",
            cache: false,
            data: {data: id},
            success: function (res) {
                res1 = JSON.parse(res);
                if (res1.STATUS === "SUCCESS") {
                    window.location.href = '${pageContext.servletContext.contextPath}/reservation/vip/create/notifications';
//                   
                }

            }
        });
    });
</script>

