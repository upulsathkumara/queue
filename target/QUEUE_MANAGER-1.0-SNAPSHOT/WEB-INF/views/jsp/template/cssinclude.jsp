<%-- 
    Document   : cssinclude
    Created on : Oct 12, 2016, 11:53:41 AM
    Author     : Rasika
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Ministry of Foreign Affairs | Dashboard</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<spring:url value="/resources/bootstrap/css/bootstrap.min.css" var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<spring:url value="/resources/fontawsome/css/font-awesome.min.css" var="fontawsome" />
<link href="${fontawsome}" rel="stylesheet" />
<spring:url value="/resources/ionicons/css/ionicons.min.css" var="ionicons" />
<link href="${ionicons}" rel="stylesheet" />
<spring:url value="/resources/dist/css/AdminLTE.min.css" var="AdminLTE" />
<link href="${AdminLTE}" rel="stylesheet" />
<spring:url value="/resources/dist/css/skins/_all-skins.min.css" var="skins" />
<link href="${skins}" rel="stylesheet" />
<spring:url value="/resources/plugins/iCheck/flat/blue.css" var="blue" />
<link href="${blue}" rel="stylesheet" />
<spring:url value="/resources/plugins/morris/morris.css" var="morris" />
<link href="${morris}" rel="stylesheet" />
<spring:url value="/resources/plugins/jvectormap/jquery-jvectormap-1.2.2.css" var="jvectormap" />
<link href="${jvectormap}" rel="stylesheet" />
<spring:url value="/resources/plugins/datepicker/datepicker3.css" var="datepicker3" />
<link href="${datepicker3}" rel="stylesheet" />
<spring:url value="/resources/plugins/daterangepicker/daterangepicker.css" var="daterangepicker" />
<link href="${daterangepicker}" rel="stylesheet" />
<spring:url value="/resources/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" var="bootstrap3" />
<link href="${bootstrap3}" rel="stylesheet" />
<spring:url value="/resources/css/main.css" var="main" />
<link href="${main}" rel="stylesheet" />

<spring:url value="/resources/css/animation.css" var="main" />
<link href="${fontello}" rel="stylesheet" />
<spring:url value="/resources/css/fontello-codes.css" var="main" />
<link href="${main}" rel="stylesheet" />
<spring:url value="/resources/css/fontello-embedded.css" var="main" />
<link href="${main}" rel="stylesheet" />
<spring:url value="/resources/css/fontello-ie7-codes.css" var="main" />
<link href="${main}" rel="stylesheet" />
<spring:url value="/resources/css/fontello-ie7.css" var="main" />
<link href="${main}" rel="stylesheet" />
<spring:url value="/resources/css/fontello.css" var="main" />
<link href="${main}" rel="stylesheet" />


