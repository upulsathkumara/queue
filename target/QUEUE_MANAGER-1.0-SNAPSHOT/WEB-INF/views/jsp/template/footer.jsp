<%-- 
    Document   : footer
    Created on : Oct 19, 2016, 11:20:02 AM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<div class="pull-right hidden-xs">
                    <b>Version</b> R1.02
                </div>
                Copyright &copy; 2016 Ministry of Foreign Affairs All Right Reserved. Powered by ICTA and Epic Lanka(PVT) Ltd.
