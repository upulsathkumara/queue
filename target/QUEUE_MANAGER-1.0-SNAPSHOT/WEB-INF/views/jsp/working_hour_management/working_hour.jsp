<%-- 
    Document   : reservationCreate
    Created on : Oct 12, 2016, 12:28:28 PM
    Author     : Rasika
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 


<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>
        <script>
            var role = "<%=session.getAttribute("role")%>";
            if (role !== 'ADMIN') {
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }

            var username = "<%=session.getAttribute("loggedUser")%>"
            if (!<%=session.getAttribute("loggedUser")%>) {
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            } else {
            }

        </script>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/admin_header.jsp"/>
                <spring:url value="/resources/Datepicker/bootstrap-datetimepicker.css" var="datepicker" />
                <link href="${datepicker}" rel="stylesheet" />

                <spring:url value="/resources/css/bootstrap-timepicker.min.css" var="timepicki" />
                <link href="${timepicki}" rel="stylesheet" />

            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/admin_menu.jsp"/>
            </aside>
            <div class="content-wrapper">
                <!--                <section class="content-header">
                                    <ol class="breadcrumb">
                                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                                        <li class="active">Dashboard</li>
                                    </ol>
                                </section>-->
                <section class="content-header">
                    <h1>
                        Working Hour Management <small>update</small>
                    </h1>
                    <div class="col-sm-6">
                        <c:if test="${not empty succMsg}">
                            <div class="msg">${succMsg}</div>
                        </c:if>
                        <c:if test="${not empty errMsg}">
                            <div class="err">${errMsg}</div>
                        </c:if>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3"></div>
                </section>
                <section class="content">
                    <div id="container">
                        <div class="row">           
                            <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                <div class="box box-primary">
                                    <form:form role="form" id="workingHourForm" method="POST" modelAttribute="workingHourForm" action="${pageContext.request.contextPath}/admin/workinghour/save">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <span>Start Time</span>
                                                <form:input path="startTime"    required="required" type="text" id="startTime" class="form-control" placeholder="Enter Start Time" value="${STARTTIME}" readonly="true"></form:input>   
                                                </div>
                                                <div class="form-group">
                                                    <span>End Time</span>
                                                <form:input path="endtime"  required="required" type="text" id="endtime" class="form-control" placeholder="Enter End Time" value="${ENDTIME}" readonly="true"></form:input>
                                                </div>
                                                <div class="form-group">
                                                    <span>Online Percentage</span>
                                                <form:input path="onlinePercentage" required="required" type="text" id="onlinePercentage" class="form-control" placeholder="Enter Online Percentage" value="${ONLINEPERCENTAGE}" ></form:input>
                                                </div>
                                                <div class="form-group">
                                                    <span>Walk-in Percentage</span>
                                                <form:input path="workingPercentage" required="required" type="text" id="workingPercentage" class="form-control" placeholder="Enter Walking Percentage" value="${WORKINGPERCENTAGE}" ></form:input>
                                                </div>
                                                <div class="form-group">
                                                    <span>Online Cutoff Time</span>
                                                <form:input path="onlineCutOffTime" required="required" type="text" id="onlineCutOffTime" class="form-control" placeholder="Enter Online Cutoff Time" value="${ONLINECUTOFFTIME}" readonly="true"></form:input>
                                                </div>
                                                <div class="form-group">
                                                    <span>Time Gap for Online Bookings(min)</span>
                                                <form:input path="timeGapToBook" required="required" type="number" id="timeGapToBook" class="form-control" placeholder="Enter Time gap for Online Bookings(min)" value="${TIMEGAPTOBOOK}" ></form:input>
                                                </div>

                                                <div class="form-group">
                                                    <span>Time Gap to Issue Queue Ticket - Before Start Time(min)</span>
                                                <form:input path="beforeTimeToIssueTicket" required="required" type="number" id="beforeTimeToIssueTicket" class="form-control" placeholder="Enter Time Gap to Issue Queue Ticket - Before Start Time(min)" value="${TIMEGAPBEFORE}" ></form:input>
                                                </div>

                                                <div class="form-group">
                                                    <span>Time Gap to Issue Queue Ticket - After End Time(min)</span>
                                                <form:input path="afterTimeToIssueTicket" required="required" type="number" id="afterTimeToIssueTicket" class="form-control" placeholder="Enter Time Gap to Issue Queue Ticket - After End Time(min)(min)" value="${TIMEGAPAFTER}" ></form:input>
                                                </div>

                                                <div class="form-group">
                                                    <span>Time Interval to Call Next Token(min)</span>
                                                <form:input path="callNextInterval" required="required" type="number" id="callNextInterval" class="form-control" placeholder="Enter Time Interval to Call Next Token(min)(min)" value="${CALLNEXT}" ></form:input>
                                                </div>

                                                <div class="box-footer btn-toolbar">
                                                <form:button type="button" class="btn btn-primary pull-right" onclick="window.history.back();">Cancel</form:button>
                                                <form:button id="createbutton" type="submit" class="btn btn-primary pull-right">Save</form:button>     
                                                </div>
                                            </div>
                                    </form:form>
                                </div>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>   
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>

        <jsp:include page="../template/jsinclude.jsp"/>
    </body>

</html>
<!--DatePicker-->
<spring:url value="/resources/Datepicker/bootstrap-datetimepicker.min.js" var="Datepicker" />
<script src="${Datepicker}"></script>

<spring:url value="/resources/Datepicker/timepicki.js" var="timepicki" />
<script src="${timepicki}"></script>
<!--DatePicker end-->

<spring:url value="/resources/Datepicker/bootstrap-timepicker.js" var="timepicki" />
<script src="${timepicki}"></script>

<script>
            $('#datetimepicker').datetimepicker({
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });
            $('#startTime').timepicker({
                minuteStep: 5,
                showInputs: false,
                disableFocus: true,
                showMeridian: false
            });
            $('#endtime').timepicker({
                minuteStep: 5,
                showInputs: false,
                disableFocus: true,
                showMeridian: false
            });
            $('#onlineCutOffTime').timepicker({
                minuteStep: 5,
                showInputs: false,
                disableFocus: true,
                showMeridian: false
            });

            $('#datetimepicker').datetimepicker('setStartDate', new Date());
//            $('#startTime').datetimepicker({
//                format: 'h:ii',
//                autoclose: true,
//                showMeridian: true,
//                startView: 1,
//                maxView: 2
//            });
//            $('#endtime').datetimepicker({
//                format: 'h:ii',
//                autoclose: true,
//                showMeridian: true,
//                startView: 1,
//                maxView: 2
//            });
//            $('#onlineCutOffTime').datetimepicker({
//                format: 'h:ii',
//                autoclose: true,
//                showMeridian: true,
//                startView: 1,
//                maxView: 2
//            });

</script>
<script>
    $('#workingHourForm').validate({
        rules: {
            startTime: {
                required: true
            },
            endtime: {
                required: true,
                daterange: true
            },
            onlinePercentage: {
                required: true,
                range: [1, 100],
                digits: true


            },
            workingPercentage: {
                required: true,
                range: [1, 100],
                digits: true,
                sumpercentage: true

            },
            onlineCutOffTime: {
                required: true
            },
            timeGapToBook: {
                required: true,
                maxlength: 3
            }

        }, errorPlacement: function(error, element) {
            element.parent().find('div.invalid').remove();
            element.parent().append('<div class="invalid">' + error.text() + '</div>');
        }
    });


    jQuery.validator.addMethod("sumpercentage", function(value, element) {
        var online = $('#onlinePercentage').val();
        var walkin = $('#workingPercentage').val();
        var sum = parseInt(online) + parseInt(walkin);
        if (sum === 100) {
            return true;
        } else {
            return false;
        }

    }, "Sum of Online and Walk-in percentages cannot be grater than or lower than 100%");



    jQuery.validator.addMethod("daterange", function(value, element) {
        var dt = new Date();
        return ($("#startTime").val() !== '' && $("#endtime").val() !== '') ? new Date((dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear() + " " + $('#startTime').val()).getTime() < new Date((dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear() + " " + $('#endtime').val()).getTime() : false;
    }, "End time should be greater than Start time");

</script>
<script>
    $(".sidebar-menu li").removeClass("selected");
    $(".sidebar-menu  .common-config").addClass("selected");
</script>
<style>
    .msg {
        padding: 15px;
        margin-bottom: 20px;
        margin-top: 15px;
        margin-left: -14px;
        border: 1px solid transparent;
        border-radius: 4px;
        color: #31708f;
        background-color: #d9edf7;
        border-color: #bce8f1;
    }
    .err {
        padding: 15px;
        margin-bottom: 20px;
        margin-top: 15px;
        margin-left: -14px;
        border: 1px solid transparent;
        border-radius: 4px;
        color: #a94442;
        background-color: #f2dede;
        border-color: #ebccd1;
    }
</style>