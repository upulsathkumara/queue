<%-- 
    Document   : reservationCreate
    Created on : Oct 12, 2016, 12:28:28 PM
    Author     : Rasika
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 


<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>
        <script>
            var role = "<%=session.getAttribute("role")%>";
            if (role !== 'USER') {
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }
            var username = "<%=session.getAttribute("loggedUser")%>";
            if (!<%=session.getAttribute("loggedUser")%>) {
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            } else {
            }

        </script>
        <style>
            .datepicker table tr td:first-child + td + td + td + td + td +td {
                color: red
            }

            .datepicker table tr td:first-child {
                color: red
            }
        </style>

        <script>



            function getUserConfirmation() {

                var data = {
                    'name': $('#fristName').val(),
                    'identificationNo': $('#identificationNumber').val(),
                    'applicationType': $('#applicationType').val(),
                    'email': $('#email').val(),
                    'telephoneMobile': $('#telephoneMobile').val(),
                    'preferredLanguage': $('#preferredLanguage').val(),
                    'bookingdate': $('#bookingdate').val(),
                    'addressLine1': $('#addressLine1').val(),
                    'addressLine2': $('#addressLine2').val(),
                    'addressLine3': $('#addressLine3').val(),
                    'startTimeStr': $('#startTimeStr').val(),
                    'endtimeStr': $('#endtimeStr').val()

                };
                console.log(JSON.stringify(data));
                localStorage.setItem("customer", JSON.stringify(data));



//                var r = confirm("Are you sure you want to proceed this reservation?");
//                if (r == true) {
//                    var length = $("#documentTable").children('tr').length;
//                    var timeTotal = $("#timeTotal").text();
//                    $("#timeDuration").val(timeTotal);
//                    if (parseInt(length) !== 0) {
//                        $("#documentType").rules("remove");
//                        $("#documentQuentity").rules("remove");
//                        $("#reservationForm").submit();
//                    } else {
//                        $('#checkAvilabilityInvlidMessage').html('<b>Please add at least one valid document</b>');
//                        $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});
//                    }
//                } else {
////                    txt = "You pressed Cancel!";
//                }
//                document.getElementById("demo").innerHTML = txt;
            }
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="loader"></div>
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/user_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/user_menu.jsp"/>
            </aside>
            <input type="hidden" name="ip" id="get_ip" />
            <div class="content-wrapper">
                <section class="content" style="margin-right: -50%;">
                    <form:form  method="POST"  action="${pageContext.request.contextPath}/reservation/created"  id="reservationForm" commandName="reservationForm" enctype="multipart/form-data">
                        <div>
                            <div class="col-xs-12">
                                <c:if test="${not empty errorMsg}">
                                    <div class="alert alert-error">
                                        <strong>Warning!</strong> <span id="mainerrormessage">${errorMsg}</span>
                                    </div>
                                    <br/>
                                </c:if> 
                            </div>
                        </div>
                        <span style="font-size: 16px;"><b>Walk-in Customer</b></span><br/><br/>
                        <u class="main-title">Identification Document</u>
                        <div class="row">
                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                                <div class="row">
                                    <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10" style="width: 95%">
                                        <div class="form-group">
                                            <label class="control-label labelfont" for="identificatioDocumentType"></label>
                                            <form:select value="NIC" path="identificatioDocumentType" class="form-control"  id="identificatioDocumentType">
                                                <!--                                                <option value="">--Select--</option>-->
                                                <option value="NIC" >NIC</option>
                                                <option value="PAS">Passport No</option>
                                                <option value="DLN">DL Number</option>
                                            </form:select>
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1" style="margin-left: -14px;margin-top: 15px;padding-left: 0px">
                                        <samp style="color: red">*</samp>
                                    </div>

                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="form-group" style="margin-left: -3px;margin-right: -3px;">
                                    <form:input autofocus="autofocus" type="text" style="margin-top:5px;" class="form-control" id="identificationNumber" placeholder="Enter identification number" path="identificationNumber" />
                                </div> 
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3"></div>
                                        <div class="col-md-3 col-lg-4 col-sm-4 col-xs-4">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <u class="main-title" style="margin-left: 15px;">Application Detail</u>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class="box-body form-horizontal">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">

                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="NIC">Application Type:<samp style="color: red">*</samp></label>
                                                    <div class="col-sm-4">
                                                        <form:select class="form-control" id="applicationType" path="applicationType">
                                                            <c:forEach var="stae" items="${applicationType}">
                                                                <option value="${stae.key}">${stae.value}</option>
                                                            </c:forEach>
                                                        </form:select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>

                            <u class="main-title" style="margin-left: 15px;">Personal Details</u>
                            <div class="row">

                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class="box-body form-horizontal">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="fristName">Name in Full:<samp style="color: red">*</samp></label>
                                                    <div class="col-sm-4">
                                                        <form:input type="text" class="form-control" id="fristName" placeholder="Enter full name" path="fristName" maxlength="254"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="email">Email:</label>
                                                    <div class="col-sm-4">
                                                        <form:input type="text" class="form-control" id="email" placeholder="Enter email" path="email" maxlength="32"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="telephoneMobile">Telephone Mobile:<samp style="color: red">*</samp></label>
                                                    <div class="col-sm-4">
                                                        <form:input type="text" class="form-control" id="telephoneMobile" placeholder="Enter telephone mobile" path="telephoneMobile" maxlength="16" step="1"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="preferredLanguage">Preferred Language:<samp style="color: red">*</samp></label>
                                                    <div class="col-sm-4">
                                                        <form:select path="preferredLanguage" class="form-control" id="preferredLanguage">
                                                            <option value="ENGLISH">English</option>
                                                            <option value="SINHALA" >Sinhala</option>
                                                            <option value="TAMIL">Tamil</option>
                                                        </form:select>
                                                    </div>
                                                </div>


                                                <!--                                                <div class="form-group">
                                                                                                    <label class="control-label labelfont col-sm-3" for="bookingdate">Requesting appointment date:<samp style="color: red">*</samp></label>
                                                
                                                                                                    <div class="input-group date col-sm-4" style="padding-left: 15px;padding-right:  15px;float: left;" >
                                                                                                        <div class="input-group-addon">
                                                                                                            <i class="fa fa-calendar" style=" cursor:auto"></i>
                                                                                                        </div>
                                                <%--<form:input type="text"  path="bookingdate" id="bookingdate" value="${dateNow}" class="form-control pull-right" style="height: 36px;" readonly="true"/>--%>
                                            </div>
                                             /.input group 
                                        </div>-->

                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="documentTypeList"></label>
                                                    <div class="col-sm-4">
                                                        <form:input type="hidden" class="form-control" id="documentTypeList" path="documentTypeList"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="documentVarifictionList"></label>
                                                    <div class="col-sm-4">
                                                        <form:input type="hidden" class="form-control" id="documentVarifictionList" path="documentVarifictionList"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="timeDuration"></label>
                                                    <div class="col-sm-4">
                                                        <form:input type="hidden" class="form-control" id="timeDuration" path="timeDuration"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="documentDatabaseList"></label>
                                                    <div class="col-sm-4">
                                                        <input type="hidden" class="form-control" id="documentDatabaseList" value="<c:out value="${Type}"/>"/>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                            <div class="col-xs-7" style="margin-top: -75px;">
                                                <div class="box box-default collapsed-box">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Address Information</h3>

                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label labelfont col-sm-5" for="customerId"></label>
                                                                <div class="col-sm-7">
                                                                    <form:input type="hidden" class="form-control" value="NO" id="customerId" placeholder="Enter first name" path="customerId"/>
                                                                </div>
                                                            </div>

                                                            <!--                                                            <div class="form-group">
                                                                                                                            <label class="control-label labelfont col-sm-5" for="lastName">Last Name:</label>
                                                                                                                            <div class="col-sm-7">
                                                            <form:input type="text" class="form-control" id="lastName" placeholder="Enter last name" path="lastName" maxlength="32"/>
                                                        </div>
                                                    </div>-->
                                                            <div class="form-group">
                                                                <label class="control-label labelfont col-sm-5" for="addressLine1">Address Line1:</label>
                                                                <div class="col-sm-7">
                                                                    <form:input type="text" class="form-control" id="addressLine1" placeholder="Enter address line1" path="addressLine1" maxlength="64"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label labelfont col-sm-5" for="addressLine2">Address Line2:</label>
                                                                <div class="col-sm-7">
                                                                    <form:input type="text" class="form-control" id="addressLine2" placeholder="Enter address line2" path="addressLine2" maxlength="64"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label labelfont col-sm-5" for="addressLine3">Address Line3:</label>
                                                                <div class="col-sm-7">
                                                                    <form:input type="text" class="form-control" id="addressLine3" placeholder="Enter address line3" path="addressLine3" maxlength="64"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.box-body -->
                                                </div>
                                                <!-- /.box -->
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <u class="main-title" style="margin-left: 15px;">Documents</u>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class="box-body form-horizontal">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="documentType">Select Document Type:</label>
                                                    <div class="col-sm-4 ">
                                                        <select class="form-control" id="documentType" name="documentType">
                                                            <c:forEach var="stae" items="${documentType}">
                                                                <option value="${stae.key}">${stae.value}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </div>
                                                    <!--                                                    <div class="col-sm-5 " style="background-color: red">
                                                                                                            Rasika Madushanka Solamans
                                                                                                        </div>-->
                                                </div>
                                                <div class="form-group ">
                                                    <label class="control-label labelfont col-sm-3" for="documentQuentity">Quantity:</label>
                                                    <div class="col-sm-4">
                                                        <input type="number" class="form-control"   id="documentQuentity" min="1" name="documentQuentity" placeholder="Enter quantity"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="documentQuentity"></label>
                                                    <div class="col-sm-4" >
                                                        <p style="margin-top: 8px;" id="maximumDocumentId"></p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-xs-7">
                                                        <button type="button" class="btn btn-default btn-block pull-right" id="addDocumentType">Add</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class="box-body form-horizontal">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-lg-7 col-sm-7 col-xs-7">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Document Type</th>
                                                                <th>Quantity</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="documentTable">
                                                            <c:forEach items="${documenttypeoldlist}" var="documenttypeoldlist">
                                                                <tr id="TABLECOLOUMN-${documenttypeoldlist.documentTypeId}">
                                                                    <td>${documenttypeoldlist.description}</td>
                                                                    <td>${documenttypeoldlist.quantity}</td>
                                                                    <td>
                                                                        <a class="documentActionView" id="${documenttypeoldlist.documentTypeId}">
                                                                            <i class="fa fa-eye"></i>
                                                                        </a>
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <a class="documentActionEdit" id="${documenttypeoldlist.documentTypeId}">
                                                                            <i class="fa fa-pencil"></i>
                                                                        </a>
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <a class="documentActionDelete" id="${documenttypeoldlist.documentTypeId}">
                                                                            <i class="fa fa-trash"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </c:forEach>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="control-label labelfont col-sm-3" for="documentQuentity">Total Time:</label>
                                                        <div class="col-sm-4" >
                                                            <c:choose>
                                                                <c:when test="${not empty timeduration}">
                                                                    <p style="margin-top: 8px;"><span id="timeTotal">${timeduration}</span> min</p>
                                                                </c:when>    
                                                                <c:otherwise>
                                                                    <p style="margin-top: 8px;"><span id="timeTotal">0</span> min</p>
                                                                </c:otherwise>
                                                            </c:choose>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="bookingdate">Requesting Appointment Date:<samp style="color: red">*</samp></label>

                                                <div class="input-group date col-sm-4" style="padding-left: 15px;padding-right:  15px;float: left;" >
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar" style=" cursor:auto"></i>
                                                    </div>
                                                    <form:input type="text"  path="bookingdate" id="bookingdate" value="${dateNow}" class="form-control pull-right" style="height: 36px;" readonly="true"/>
                                                </div>
                                                <!-- /.input group -->
                                            </div>
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-7">
                                                        <button type="button" class="btn btn-default btn-block pull-right" id="checkAvailability">Check Availability</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class="box-body form-horizontal">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="bookeDsdId"></label>
                                                    <div class="col-sm-4">
                                                        <form:input type="hidden" class="form-control"  id="bookeDsdId"  path="bookeDsdId"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="startTime">Start Time:</label>
                                                    <div class="col-sm-4">
                                                        <form:input type="text" class="form-control"  id="startTimeStr"  path="startTimeStr" readonly="true"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="endtime">End Time:</label>
                                                    <div class="col-sm-4">
                                                        <form:input type="text" class="form-control" id="endtimeStr" path="endtimeStr" readonly="true"/>
                                                    </div>
                                                </div>
                                                <form:input type="hidden" class="form-control" id="reschduleType" path="reschduleType" readonly="true"/>
                                                <form:input type="hidden" class="form-control" id="helpdeskreason" path="helpdeskreason" readonly="true"/>
                                                <div class="row">
                                                    <div class="col-xs-7">

                                                        <form:button id="createbutton" type="button" class="btn btn-default pull-right">
                                                            Create
                                                        </form:button>
                                                        <%--<form:button id="helpdeskbutton" type="button" class="btn btn-default btn-block pull-right">--%>
                                                        <!--Send to Help Desk-->  
                                                        <%--</form:button>--%>    


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </form:form>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
        <div id="myModal" class="modal fade" role="dialog" style="">
            <div class="modal-dialog modal-lg" style="overflow-y: scroll; max-height:85%;  margin-top: 50px; margin-bottom:50px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" style="text-align: center">Time Slot Selection</h4>
                        <br/>
                    </div>
                    <div class="modal-body">
                        <div class="box">

                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <table class="table table-striped" id="timeSlotRow">


                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="confirmTimeSlotSelection" class="modal fade"  role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                        <h4 class="modal-title modal-title-center" style="text-align: center;">Please confirm time-slot selection</h4>
                    </div>
                    <div class="modal-body">
                        <div id="conf-window-show-desc"></div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="confirmedSelection">OK</button>
                        <button type="button" class="btn btn-default" id="slotConfirmationCancel">Cancel</button>
                    </div>
                </div>

            </div>
        </div>


        <div id="mydocumentmodelview" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Document Reference View</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center">
                                <table class="table">
                                    <tbody id="referenceTableView" class="referenceTableview"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>              
                </div>

            </div>
        </div>

        <div id="mydocumentmodeledit" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Document Reference Edit</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center">
                                <div class="invalid" id="referenceEditValidationMessage"></div>
                                <table class="table">
                                    <tbody id="referenceTableEdit" class="referenceTableEdit"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="editReferenceDocument" type="button" class="btn btn-secondary">Edit</button>
                        <input type="hidden" id="referenceEditId" />
                    </div>
                </div>

            </div>
        </div>
        <div class="modal" id="mydocumentmodeldelete"> 
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #367fa9">×</span></button>
                        <h4 class="modal-title" style="color: #367fa9"><span class="glyphicon glyphicon-exclamation-sign"></span> Confirmation for Delete!</h4>
                    </div>
                    <div class="modal-body" style="color: #367fa9">
                        <p><b>Are you sure you want to delete this record?</b></p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="deleteReferenceDocument" class="btn btn-primary">Delete</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>  
                        <input type="hidden" id="referenceDeleteId" />
                    </div>
                </div>
            </div>
        </div>


        <!--documents model-->
        <div id="mydocumentmodel" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Document Reference</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center">
                                <div class="invalid" id="referenceValidationMessage">

                                </div>
                                <table class="table">
                                    <tbody id="referenceTable" class="referenceTable"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="addDocument" type="button" class="btn btn-secondary">Save</button>
                    </div>
                </div>

            </div>
        </div>


        <div class="modal" id="checkAvilabilityInvlid"> 
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #367fa9">×</span></button>
                        <h4 class="modal-title" style="color: #367fa9"><span class="glyphicon glyphicon-exclamation-sign"></span> Warning!</h4>
                    </div>
                    <div class="modal-body" style="color: #ef0746">
                        <p id="checkAvilabilityInvlidMessage"><b>Please add at least one valid document</b></p>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal" id="reservationcreateconfirmation"> 
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color:white">×</span></button>
                        <h4 class="modal-title" style="color:#3c8dbc"><span class="glyphicon glyphicon-exclamation-sign"></span> Confirmation for Create!</h4>
                    </div>
                    <div class="modal-body">
                        <p><b>Are you sure you want to proceed this reservation ?</b></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="createOk" class="btn btn-primary">OK</button>
                        <button type="button" id="createCancel" class="btn btn-primary" data-dismiss="modal">Cancel</button>  
                        <input type="hidden" id="referenceDeleteId" />
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="sendToHelpDeskReason"> 
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color:white">×</span></button>
                        <h4 class="modal-title" style="color:#3c8dbc"> Enter the Reason for Sending to the Help Desk</h4>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="helpdreason" id="helpdreason" style="width: 100%;" maxlength="300">
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="sendToHelpDeskOk" class="btn btn-primary">Add</button>
                        <button type="button" id="createCancel" class="btn btn-primary" data-dismiss="modal">Cancel</button>  
                    </div>
                </div>
            </div>
        </div>

        <script>
            var data_status = {
                'status': 'USERDETAILS'

            };
            localStorage.setItem("STATUS", JSON.stringify(data_status));

            var verificationJson = '${olddocumentverificationlist}';
            $('#documentVarifictionList').val(verificationJson);

            $('#endtime').prop('readonly', true);
            $('#startTime').prop('readonly', true);
            $('#startTimeStr').prop('readonly', true);
            $('#endtimeStr').prop('readonly', true);
            var documentjsonList = "";
            var referenceJsonList = "";
            var index = 0;
            $("#customerSerach").click(function (event) {
                event.preventDefault();
            });



            $("#addDocumentType").click(function (event) {

                $("#documentQuentity").rules('add', {
                    required: true,
                    number: true,
                    digits: true,
                    maximumCount: true
                });
                $("#documentType").rules('add', {
                    required: true
                });
                if ($('#documentType').valid() & $('#documentQuentity').valid() & $('#bookingdate').valid()) {
                    var quantity = $("#documentQuentity").val();
                    var documentTypeId = $("#documentType").val();
                    var time = $("#timeTotal").text();
                    time = parseInt(time) + parseInt(timeCalculate(documentTypeId) * quantity);
                    if (time <= 60) {
                        var refernceId = "REFERENCE-" + documentTypeId;
                        $("#referenceTable").html("");
                        $("#referenceValidationMessage").html("");
                        for (var i = 0; i < quantity; i++) {
                            var inputId = refernceId + "-" + i + "-INPUT";
                            $("#referenceTable").append('<tr>'
                                    + '<td class=col-xs-1 pull-left style=border-top-width:0px >Reference:</td>'
                                    + '<td class=col-xs-2 style=border-top-width:0px>'
                                    + '<input class=pull-left type=text id="' + inputId + '" maxlength="14" />'
                                    + '</td>'
                                    + '</tr>');
                        }
                        $('#mydocumentmodel').modal({backdrop: 'static', keyboard: false});
                    }
                }
            });

            $("#addDocument").click(function (event) {
                event.preventDefault();
                var tableColoumnId = "TABLECOLOUMN-";
                var documentTypeId = $("#documentType").val();
                var refernceId = "REFERENCE-" + documentTypeId;
                var quantity = $("#documentQuentity").val();
                var list = $("#documentVarifictionList").val();
                var documentList = $("#documentTypeList").val();
                var referenceArray = [];
                var documentTypeArray = [];
                tableColoumnId = tableColoumnId + documentTypeId;
                var values = [];
                for (var i = 0; i < quantity; i++) {
                    var inputId = "#" + refernceId + "-" + i + "-INPUT";
                    var value = $(inputId).val().trim();
                    values.push(value.toString());
                }
                if (isEmpty(values)) {
                    if (isUnique(values)) {
                        if (specialCharacter(values)) {
                            $("#documentType").rules("remove");
                            $("#documentQuentity").rules("remove");
                            var documentData = new Object();
                            var documentTypeText = $("#documentType option:selected").text();
                            $("#documentTable").append('<tr id="' + tableColoumnId + '"><td>' + documentTypeText + '</td><td>' + quantity + '</td><td><a class="documentActionView" id="' + documentTypeId + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="documentActionEdit" id="' + documentTypeId + '"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="documentActionDelete" id="' + documentTypeId + '"><i class="fa fa-trash"></i></a></td></tr>');
                            documentData.documentTypeId = documentTypeId;
                            documentData.quantity = quantity;
                            if (documentList) {
                                if (list) {
                                    res1 = JSON.parse(list);
                                    referenceArray = res1;
                                    res2 = JSON.parse(documentList);
                                    documentTypeArray = res2;
                                }
                            }
                            documentTypeArray.push(documentData);
                            for (var i = 0; i < quantity; i++) {
                                var referenceData = new Object();
                                var inputId = "#" + refernceId + "-" + i + "-INPUT";
                                var inputValue = $(inputId).val().trim();
                                referenceData.reference = inputValue;
                                referenceData.documentTypeId = documentTypeId;
                                referenceArray.push(referenceData);
                            }
                            index++;
                            $("#documentTypeList").val(JSON.stringify(documentTypeArray));
                            $("#documentVarifictionList").val(JSON.stringify(referenceArray));
                            var text1 = "#documentType option[value='";
                            var text2 = "']";
                            $(text1.concat(documentTypeId, text2)).remove();
                            $("#documentQuentity").val('');
                            $('#mydocumentmodel').modal('toggle');
                            $("#timeTotal").empty();
                            $("#timeTotal").append(Totaltime());
                            $("#endtime").val('');
                            $("#startTime").val('');
                            $("#startTimeStr").val('');
                            $("#endtimeStr").val('');
                            $("#maximumDocumentId").html('');
                        } else {
                            $("#referenceValidationMessage").empty();
                            $("#referenceValidationMessage").append("<p>Reference field value can not be type  special characters</p>");
                        }
                    } else {
                        $("#referenceValidationMessage").empty();
                        $("#referenceValidationMessage").append("<p>Reference field value can not be duplicate</p>");
                    }
                } else {
                    $("#referenceValidationMessage").empty();
                    $("#referenceValidationMessage").append("<p>Reference field value can not be empty</p>");
                }
            });
            $("#checkAvailability").click(function (event) {
                event.preventDefault();
                var totTime = $("#timeTotal").text();
                if (totTime === "0") {
                    $('#checkAvilabilityInvlidMessage').html('<b>Please add at least one valid document</b>');
                    $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});
                } else {
                    var dataObject = new Object();
                    dataObject.timeDuration = totTime;
                    dataObject.bookingDate = $("#bookingdate").val();
                    var content = JSON.stringify(dataObject);
                    var length = $("#documentTable").children('tr').length;
                    if ($('#bookingdate').valid()) {
                        if (parseInt(length) !== 0) {
                            $.ajax({
                                async: true,
                                type: "POST",
                                url: "${pageContext.servletContext.contextPath}/reservation/avilable/slot",
                                cache: false,
                                data: {data: content},
                                success: function (res) {
                                    res1 = JSON.parse(res);
                                    var data_status = {
                                        'status': 'TIMESLOAT'

                                    };
                                    localStorage.setItem("STATUS", JSON.stringify(data_status));
                                    localStorage.setItem("customer", res);
                                    $("#timeSlotRow").empty();
                                    if (res1.HOLIDAY === "NO") {
                                        $("#timeSlotRow").append('<tr >'
                                                + '<th>Time Slots</th>'
                                                + '<th>Available Slots</th>'
                                                + '<th style="width: 40px">Reserve</th>'
                                                + '</tr>');



                                        var index = 0;
                                        $.each(res1.TIMESLOAT, function () {
                                            if (res1.TIMESLOAT[index].halfday !== "YES") {
                                                if (res1.TIMESLOAT[index].exceed === "NO") {
                                                    if (res1.TIMESLOAT[index].availabelSlot > 0) {
                                                        $("#timeSlotRow").append('<tr>'
                                                                + '<td><span id="SLOT-STARTTIME-' + index + '">' + res1.TIMESLOAT[index].startTimeStr + '</span> - <span id="SLOT-ENDTIME-' + index + '">' + res1.TIMESLOAT[index].endtimeStr + '</span></td>'
                                                                + '<td style="padding-left:80px">' + res1.TIMESLOAT[index].availabelSlot + '</td>'
                                                                + '<td>'
                                                                + '<button type="button" class="btn btn-primary reserveButton" id="' + index + '">Reserve</button>'
                                                                + '</td>'
                                                                + '</tr>');
                                                    } else {
                                                        $("#timeSlotRow").append('<tr>'
                                                                + '<td><span id="SLOT-STARTTIME-' + index + '">' + res1.TIMESLOAT[index].startTimeStr + '</span> - <span id="SLOT-ENDTIME-' + index + '">' + res1.TIMESLOAT[index].endtimeStr + '</span></td>'
                                                                + '<td style="padding-left:80px">' + res1.TIMESLOAT[index].availabelSlot + '</td>'
                                                                + '<td>'
                                                                + '<p>Not Available</p>'
                                                                + '</td>'
                                                                + '</tr>');

                                                    }

                                                } else {
                                                    $("#timeSlotRow").append('<tr>'
                                                            + '<td><span id="SLOT-STARTTIME-' + index + '">' + res1.TIMESLOAT[index].startTimeStr + '</span> - <span id="SLOT-ENDTIME-' + index + '">' + res1.TIMESLOAT[index].endtimeStr + '</span></td>'
                                                            + '<td style="padding-left:80px">' + res1.TIMESLOAT[index].availabelSlot + '</td>'
                                                            + '<td>'
                                                            + '<p>Expired</p>'
                                                            + '</td>'
                                                            + '</tr>');
                                                }
                                            } else {
                                                $("#timeSlotRow").append('<tr>'
                                                        + '<td><span id="SLOT-STARTTIME-' + index + '">' + res1.TIMESLOAT[index].startTimeStr + '</span> - <span id="SLOT-ENDTIME-' + index + '">' + res1.TIMESLOAT[index].endtimeStr + '</span></td>'
                                                        + '<td style="padding-left:80px">' + res1.TIMESLOAT[index].availabelSlot + '</td>'
                                                        + '<td>'
                                                        + '<p>Not Available</p>'
                                                        + '</td>'
                                                        + '</tr>');
                                            }


                                            index++;
                                        });
                                    } else {
                                        $("#timeSlotRow").append("<p>Sorry, Selected date is a full holiday. You can't make reservations</p>");
                                    }

                                    $('#myModal').modal({backdrop: 'static', keyboard: false});
                                }
                            });
                        } else {
                            $('#checkAvilabilityInvlidMessage').html('<b>Please add at least one valid document</b>');
                            $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});
                        }
                    } else {
                        $('#checkAvilabilityInvlidMessage').html('<b>Please add valid date.</b>');
                        $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});
                    }
                }

            });
            $("#confirmedSelection").click(function (event) {
                var endtime = $("#endTimeConfirmation").text();
                var startTime = $("#StartTimeConfirmation").text();
                $("#endtimeStr").val(endtime);
                $("#startTimeStr").val(startTime);
                $('#myModal').modal('toggle');
                $('#confirmTimeSlotSelection').modal('toggle');
            });


            $("#timeSlotRow").on("click", ".reserveButton", function () {
                var id = $(this).attr("id");
                var startId = "#SLOT-STARTTIME-" + id;
                var endId = "#SLOT-ENDTIME-" + id;
                var startTime = $(startId).text();
                var endtime = $(endId).text();
                $("#conf-window-show-desc").empty();
                $('#endtime').prop('readonly', true);
                $('#startTime').prop('readonly', true);
                $("#conf-window-show-desc").append('<div class="row"><div class="col-xs-3"></div><div class="col-xs-3"></div></div><div class="row"><div class="col-xs-3"></div><div class="col-xs-3">Start Time:</div><div class="col-xs-3" id="StartTimeConfirmation">' + startTime + '</div><div class="col-xs-3"></div></div><div class="row"><div class="col-xs-3"></div><div class="col-xs-3">End Time:</div><div class="col-xs-3" id="endTimeConfirmation">' + endtime + '</div><div class="col-xs-3"></div></div>');
                $('#confirmTimeSlotSelection').modal({backdrop: 'static', keyboard: false});
            });
            $(document).ready(function () {
                $('#menu [data-accordion]').accordion();
                var height = $(".content-area").height();
                $(".menu-container").height(Math.max(height, 600));
            });

            $('#reservationForm').validate({
                rules: {
                    identificatioDocumentType: {
                        required: true
                    },
                    identificationNumber: {
                        required: true,
                        maxlength: 12
                    },
                    fristName: {
                        required: true
                    },
                    applicationType: {
                        required: true
                    },
                    telephoneMobile: {
                        required: true,
                        maxlength: 16,
                        minlength: 10,
                        number: true
                    },
                    startTimeStr: {
                        required: true
                    },
                    endtimeStr: {
                        required: true

                    },
                    bookingdate: {
                        required: true

                    },
                    email: {
                        email: true
                    },
                    preferredLanguage: {
                        required: true
                    }

                }, errorPlacement: function (error, element) {
                    element.parent().find('div.invalid').remove();
                    element.parent().append('<div class="invalid">' + error.text() + '</div>');
                },
                onfocusout: false,
                invalidHandler: function (form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                }
            });
            $('#documentTypeform').validate({
                rules: {
                    documentType: {
                        required: true
                    },
                    documentQuentity: {
                        required: true,
                        number: true
                    }
                }, errorPlacement: function (error, element) {
                    error.insertAfter(element.parent());
                }
            });
            function isUnique(values) {
                values.sort();
                // Check whether there are two equal values next to each other
                for (var k = 1; k < values.length; ++k) {
                    if (values[k].trim() === values[k - 1].trim()) {
                        return false;
                        break;
                    }
                }
                return true;
            }
            function isEmpty(values) {
//            values.sort();
                // Check whether there are two equal values next to each other
                for (var k = 0; k < values.length; ++k) {
                    if (!values[k].trim()) {
                        return false;
                        break;
                    } else {
                        return true;
                    }
                }

            }
            function isEmpty(values) {
                values.sort();
                // Check whether there are two equal values next to each other
                for (var k = 0; k < values.length; ++k) {
                    if (!values[k]) {
                        return false;
                        break;
                    }
                }
                return true;
            }

            function specialCharacter(values) {
                values.sort();
                // Check whether there are two equal values next to each other
                for (var k = 0; k < values.length; ++k) {
                    var string = values[k];
                    for (var l = 0; l < string.length; ++l) {
                        var c = string.charAt(l);
                        if ((c === '"') || (c === ';') || (c === '`') || (c === '?') || (c === '^') || (c === '~') || (c === '$') || (c === '#')
                                || (c === '@') || (c === '^') || (c === '>') || (c === '<') || (c === '%') || (c === '&') || (c === '{') || (c === '}')) {
                            return false;
                        }
                    }
                }
                return true;
            }

            $("#documentTable").on("click", ".documentActionView", function () {
                var id = $(this).attr("id");
                var list = $("#documentVarifictionList").val();
                var databaseList = $("#documentDatabaseList").val();
                res1 = JSON.parse(list);
                res2 = JSON.parse(databaseList);
                $("#referenceTableView").empty();
                console.log(res1);
                index = 0;
                $.each(res1, function () {
                    var statusId = 'REFERENCEVIEW-STATUS-' + index;
                    var chargeId = 'REFERENCEVIEW-CHARGE-' + index;
                    if (res1[index].documentTypeId === id) {
                        $("#referenceTableView").append('<tr>'
                                + '<td class=col-xs-1 pull-left style=border-top-width:0px >Reference:</td>'
                                + '<td class=col-xs-2 style=border-top-width:0px>'
                                + '<input class=pull-left type=text value="' + res1[index].reference + '" readonly/>'
                                + '</td>'
                                + '</tr>');
                    }
                    index++;
                });
                $('#mydocumentmodelview').modal({backdrop: 'static', keyboard: false});
            });
            $("#documentTable").on("click", ".documentActionEdit", function () {
                var id = $(this).attr("id");
                var list = $("#documentVarifictionList").val();
                var databaseList = $("#documentDatabaseList").val();
                res1 = JSON.parse(list);
                res2 = JSON.parse(databaseList);
                $("#referenceTableEdit").empty();
                index = 0;
                i = 0;
                $.each(res1, function () {
                    if (res1[index].documentTypeId === id) {
                        var referenceId = 'REFERENCEEDIT-REFERENCE-' + i + "-" + res1[index].documentTypeId;
                        $("#referenceTableEdit").append('<tr>'
                                + '<td class=col-xs-1 pull-left style=border-top-width:0px >Reference:</td>'
                                + '<td class=col-xs-2 style=border-top-width:0px>'
                                + '<input maxlength="14" class=pull-left type=text value="' + res1[index].reference + '" id="' + referenceId + '"/>'
                                + '</td>'
                                + '</tr>');
                        i++;
                    }
                    index++;
                });
                $("#referenceEditId").val(id);
                $('#mydocumentmodeledit').modal({backdrop: 'static', keyboard: false});
            });
            $("#editReferenceDocument").click(function (event) {
                $("#startTimeStr").val(id);
                $("#endtimeStr").val(id);
                event.preventDefault();
                var id = $("#referenceEditId").val();
                var list = $("#documentVarifictionList").val();
                var documentList = $("#documentTypeList").val();
                var quantity = "";
                res1 = JSON.parse(list);
                res2 = JSON.parse(documentList);
                for (var j = 0; j < res2.length; j++) {
                    if (res2[j].documentTypeId === id) {
                        quantity = res2[j].quantity;
                    }
                }

                var values = [];
                for (var i = 0; i < quantity; i++) {
                    var referenceId = 'REFERENCEEDIT-REFERENCE-' + i + "-" + id;
                    var reference = $('#' + referenceId).val().trim();
                    values.push(reference);
                }
                if (isEmpty(values)) {
                    if (isUnique(values)) {
                        if (specialCharacter(values)) {
                            var count = 0;
                            for (var i = 0; i < res1.length; i++) {
                                if (res1[i].documentTypeId === id) {
                                    count++;
                                }
                            }

                            for (var i = 0; i < res1.length; i++) {
                                if (res1[i].documentTypeId === id) {
                                    res1.splice(i, count);
                                }
                            }
                            var referenceArray = [];
                            referenceArray = res1;
                            for (var k = 0; k < quantity; k++) {
                                var referenceId = 'REFERENCEEDIT-REFERENCE-' + k + "-" + id;
                                var reference = $('#' + referenceId).val().trim();
                                var referenceData = new Object();
                                referenceData.reference = reference;
                                referenceData.documentTypeId = id;
                                referenceArray.push(referenceData);
                            }


                            $("#documentVarifictionList").val(JSON.stringify(referenceArray));
                            $("#timeTotal").empty();
                            $("#timeTotal").append(Totaltime());
                            $('#mydocumentmodeledit').modal('toggle');
                        } else {
                            $("#referenceEditValidationMessage").empty();
                            $("#referenceEditValidationMessage").append("<p>Reference field value can not be type  special characters</p>");
                        }
                    } else {
                        $("#referenceEditValidationMessage").empty();
                        $("#referenceEditValidationMessage").append("<p>Reference field value can not be duplicate</p>");
                    }
                } else {
                    $("#referenceEditValidationMessage").empty();
                    $("#referenceEditValidationMessage").append("<p>Reference field value can not be empty</p>");
                }
            });
            $("#documentTable").on("click", ".documentActionDelete", function () {
                var id = $(this).attr("id");
                $("#referenceDeleteId").val(id);
                $('#mydocumentmodeldelete').modal({backdrop: 'static', keyboard: false});
            });
            $("#deleteReferenceDocument").click(function (event) {
                event.preventDefault();
                var id = $("#referenceDeleteId").val();
                var tableColoumnId = "#TABLECOLOUMN-";
                tableColoumnId = tableColoumnId + id;
                $(tableColoumnId).remove();
                var list = $("#documentVarifictionList").val();
                var documentList = $("#documentTypeList").val();
                var databaseDocumnetTypeList = $("#documentDatabaseList").val();
                res1 = JSON.parse(list);
                res2 = JSON.parse(documentList);
                res3 = JSON.parse(databaseDocumnetTypeList);
                var count = 0;
                for (var j = 0; j < res1.length; j++) {
                    if (res1[j].documentTypeId === id) {
                        count++;
                    }
                }
                for (var j = 0; j < res1.length; j++) {

                    if (res1[j].documentTypeId === id) {
                        res1.splice(j, count);
                    }
                }

                for (var i = 0; i < res2.length; i++) {
                    if (res2[i].documentTypeId === id) {
                        res2.splice(i, 1);
                    }
                }

                $("#documentVarifictionList").val(JSON.stringify(res1));
                $("#documentTypeList").val(JSON.stringify(res2));
                for (var i = 0; i < res3.TYPE.length; i++) {
                    if (res3.TYPE[i].documentTypeId === id) {
                        $("#documentType").append('<option value="' + res3.TYPE[i].documentTypeId + '">' + res3.TYPE[i].description + '</option>');
                    }
                }
                $("#timeTotal").empty();
                $("#endtime").val('');
                $("#startTime").val('');
                $("#startTimeStr").val('');
                $("#endtimeStr").val('');
                $("#timeTotal").append(Totaltime());
                $('#mydocumentmodeldelete').modal('toggle');
            });
            function Totaltime() {
                var documentVarificationList = $("#documentVarifictionList").val();
                var databaseDocumnetTypeList = $("#documentDatabaseList").val();
                res1 = JSON.parse(documentVarificationList);
                res2 = JSON.parse(databaseDocumnetTypeList);
                var time = 0;
                for (var j = 0; j < res2.TYPE.length; j++) {
                    for (var i = 0; i < res1.length; i++) {
                        if (res2.TYPE[j].documentTypeId === res1[i].documentTypeId) {
                            var varifitime = res2.TYPE[j].varificationTime;
                            time = time + parseInt(varifitime);
                        }
                    }
                }
                return time;
            }


            function timeCalculate(documentTypeId) {
                var databaseDocumnetTypeList = $("#documentDatabaseList").val();
                res1 = JSON.parse(databaseDocumnetTypeList);
                var time = 0;
                for (var j = 0; j < res1.TYPE.length; j++) {
                    if (res1.TYPE[j].documentTypeId === documentTypeId) {
                        var varifitime = res1.TYPE[j].varificationTime;
                        time = time + parseInt(varifitime);

                    }
                }
                return time;
            }

            $('#ticketSerach').on('click', function (event) {
                event.preventDefault();
                var serachValue = $("#ticketSerachValue").val();
                var dataObject = new Object();
                dataObject.serachValue = serachValue;
                var content = JSON.stringify(dataObject);
                $.ajax({
                    async: true,
                    type: "POST",
                    url: "${pageContext.servletContext.contextPath}/reservation/search",
                    cache: false,
                    data: {data: content},
                    success: function (res) {
                        res1 = JSON.parse(res);
                        console.log(res1.object[0].customer.addressLine1);
                        $('#startTime').val("" + res1.object[0].customer.addressLine1);
                    }
                });
            });

            function clearLS() {
                var data = {
                    'name': "",
                    'identificationNo': "",
                    'applicationType': "",
                    'email': "",
                    'telephoneMobile': "",
                    'preferredLanguage': "",
                    'bookingdate': "",
                    'addressLine1': "",
                    'addressLine2': "",
                    'addressLine3': "",
                    'startTimeStr': "",
                    'endtimeStr': ""

                };
                localStorage.setItem("customer", JSON.stringify(data));
            }
            $(window).on('beforeunload', function (e) {
                clearLS();
            });
            $("#createOk").click(function () {

                $('#createOk').attr('disabled', 'disabled');
                $('#reservationcreateconfirmation').modal('toggle');
                $(".loader").fadeIn();

                clearLS();
                $("#reservationForm").submit();
            });
//            $("#createOk").dblclick(function () {
//                alert("Handler for .dblclick() called.");
//                clearLS();
//                $("#reservationForm").submit();
//            });
            $("#sendToHelpDeskOk").click(function () {

                $('#sendToHelpDeskOk').attr('disabled', 'disabled');
                $('#sendToHelpDeskReason').modal('toggle');
                $(".loader").fadeIn();

                var reason = $("#helpdreason").val();
                $("#helpdeskreason").val(reason);
                clearLS();
                $("#reservationForm").submit();
            });

            $("#createCancel").click(function () {
                clearLS();
            });
            $("#createbutton").click(function () {
                $('#reschduleType').val("WALKIN");
                var length = $("#documentTable").children('tr').length;
                var timeTotal = $("#timeTotal").text();
                $("#timeDuration").val(timeTotal);
                if (parseInt(length) !== 0) {
                    $("#documentType").rules("remove");
                    $("#documentQuentity").rules("remove");


                    if ($("#reservationForm").valid()) {
                        var data = {
                            'name': $('#fristName').val(),
                            'identificationNo': $('#identificationNumber').val(),
                            'applicationType': $('#applicationType').val(),
                            'email': $('#email').val(),
                            'telephoneMobile': $('#telephoneMobile').val(),
                            'preferredLanguage': $('#preferredLanguage').val(),
                            'bookingdate': $('#bookingdate').val(),
                            'addressLine1': $('#addressLine1').val(),
                            'addressLine2': $('#addressLine2').val(),
                            'addressLine3': $('#addressLine3').val(),
                            'startTimeStr': $('#startTimeStr').val(),
                            'endtimeStr': $('#endtimeStr').val()

                        };
                        var data_status = {
                            'status': 'USERDETAILS'

                        };
                        localStorage.setItem("STATUS", JSON.stringify(data_status));
                        localStorage.setItem("customer", JSON.stringify(data));
                        $('#reservationcreateconfirmation').modal({backdrop: 'static', keyboard: false});
                    }
                } else {
                    $('#checkAvilabilityInvlidMessage').html('<b>Please add at least one valid document</b>');
                    $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});

                }

            });
            $("#helpdeskbutton").click(function () {
                $('#reschduleType').val("WALKINSEHD");
                var length = $("#documentTable").children('tr').length;
                var timeTotal = $("#timeTotal").text();
                $("#timeDuration").val(timeTotal);
                if (parseInt(length) !== 0) {
                    $("#documentType").rules("remove");
                    $("#documentQuentity").rules("remove");


                    if ($("#reservationForm").valid()) {
                        var data = {
                            'name': $('#fristName').val(),
                            'identificationNo': $('#identificationNumber').val(),
                            'applicationType': $('#applicationType').val(),
                            'email': $('#email').val(),
                            'telephoneMobile': $('#telephoneMobile').val(),
                            'preferredLanguage': $('#preferredLanguage').val(),
                            'bookingdate': $('#bookingdate').val(),
                            'addressLine1': $('#addressLine1').val(),
                            'addressLine2': $('#addressLine2').val(),
                            'addressLine3': $('#addressLine3').val(),
                            'startTimeStr': $('#startTimeStr').val(),
                            'endtimeStr': $('#endtimeStr').val()

                        };
                        localStorage.setItem("customer", JSON.stringify(data));
                        $('#sendToHelpDeskReason').modal({backdrop: 'static', keyboard: false});
                    }
                } else {
                    $('#checkAvilabilityInvlidMessage').html('<b>Please add at least one valid document</b>');
                    $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});

                }

            });



            $("#slotConfirmationCancel").click(function () {
                $("#startTimeStr").val('');
                $("#endtimeStr").val('');
                $('#confirmTimeSlotSelection').modal('toggle');
            });

            $('#bookingdate').datepicker({
                format: "yyyy-mm-dd",
                autoclose: false,
//                todayBtn: true,
                startDate: new Date(),
                minuteStep: 10,
                keyboardNavigation: false
            });



            $("#documentType").change(function () {
                var documentTypeId = $("#documentType").val();
                var timeTotalTextValue = $("#timeTotal").text();
                var verificationTime = 0;
                var maximumDocument = 0;
                if (documentTypeId !== "") {
                    var databaseDocumnetTypeList = $("#documentDatabaseList").val();
                    res3 = JSON.parse(databaseDocumnetTypeList);
                    for (var i = 0; i < res3.TYPE.length; i++) {
                        if (res3.TYPE[i].documentTypeId === documentTypeId) {
                            verificationTime = res3.TYPE[i].varificationTime;
                        }
                    }

                    maximumDocument = (60 - timeTotalTextValue) / verificationTime;
                    $("#maximumDocumentId").html('Maximum document  <span id="maximumDocumentCount">' + parseInt(maximumDocument) + '</span>');
                    $("#documentQuentity").val('');
                } else {
                    $("#maximumDocumentId").html('');
                }

            });

            $("#bookingdate").change(function () {
                $("#startTimeStr").val('');
                $("#endtimeStr").val('');
            });





            $("#documentQuentity").keyup(function (event) {
                if (event.keyCode === 13) {

                    $("#documentType").rules('add', {
                        required: true
                    });
                    $("#documentQuentity").rules('add', {
                        required: true,
                        number: true,
                        maximumCount: true
                    });
                    if ($('#documentType').valid() & $('#documentQuentity').valid()) {

                    }
                }
            });

            jQuery.validator.addMethod("maximumCount", function (value, element) {
                var quentity = 0;
                quentity = $("#maximumDocumentCount").text();
                if (parseInt(quentity) >= parseInt(value)) {
                    return true;
                } else {
                    return false;
                }

            }, "Document count should be less than or equal to maximum number of documents");


//            $('#fristName').keyup(function () {
//                $('#fristName').val($('#fristName').val().toUpperCase());
//            });

            $("#identificationNumber").on('input', function () {

                var start = this.selectionStart,
                        end = this.selectionEnd;

                this.value = this.value.toUpperCase();

                this.setSelectionRange(start, end);
            });

            $("#fristName").on('input', function () {

                var start = this.selectionStart,
                        end = this.selectionEnd;

                this.value = this.value.toUpperCase();

                this.setSelectionRange(start, end);
            });



            $("#identificationNumber").keyup(function (event) {
                if (event.keyCode === 13) {
                    var serachValue = $(this).val();
                    var dataObject = new Object();
                    dataObject.serachValue = serachValue;
                    var content = JSON.stringify(dataObject);
                    var type = "";
                    $.ajax({
                        async: true,
                        type: "POST",
                        url: "${pageContext.servletContext.contextPath}/reservation/exit/customer",
                        cache: false,
                        data: {data: content},
                        success: function (res) {
                            res1 = JSON.parse(res);
                            $("#customerId").val('NO');
                            $("#fristName").val('');
                            $("#lastName").val('');
                            $("#telephoneMobile").val('');
                            $("#NIC").val('');
                            $("#addressLine1").val('');
                            $("#addressLine2").val('');
                            $("#addressLine3").val('');
                            $("#telephoneResidence").val('');
                            $("#preferredLanguage").val('ENGLISH');
                            $("#email").val('');
                            $.each(res1.CUSTOMER, function (index, value) {
                                $("#customerId").val(value.customerId);
                                $("#fristName").val(value.fristName);
                                $("#telephoneMobile").val(value.telephoneMobile);
                                if (value.NIC) {
                                    $("#NIC").val(value.NIC);
                                } else {
                                    $("#NIC").val("NIC");
                                }
                                if (value.preferredLanguage) {
                                    $("#preferredLanguage").val(value.preferredLanguage);
                                } else {
                                    $("#preferredLanguage").val("ENGLISH");
                                }
                                $("#addressLine1").val(value.addressLine1);
                                $("#addressLine2").val(value.addressLine2);
                                $("#addressLine3").val(value.addressLine3);
                                $("#telephoneResidence").val(value.telephoneResidence);
                                $("#email").val(value.email);
                                $("#identificatioDocumentType").val(value.identificatioDocumentType);
                                $("#preferredLanguage").val(value.preferredLanguage);
                                $("#documentImgae").empty();
                            });
                        }
                    });

                }
            });
            $(window).load(function () {
                $(".loader").fadeOut("slow");
            });
            $(document).bind("ajaxSend", function () {
                $(".loader").fadeIn();
            }).bind("ajaxComplete", function () {
                $(".loader").fadeOut("slow");
            });
        </script>
    </body>
</html>

