<%-- 
    Document   : reservationCreate
    Created on : Oct 12, 2016, 12:28:28 PM
    Author     : Rasika
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>
        <script>
            var role = "<%=session.getAttribute("role")%>";
            if (role !== 'USER') {
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }
            var username = "<%=session.getAttribute("loggedUser")%>"
            if (!<%=session.getAttribute("loggedUser")%>) {
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            } else {
            }

        </script>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="loader"></div>
        <div class="wrapper">
            <header class="main-header">
                <jsp:include page="../template/user_header.jsp"/>
            </header>
            <aside class="main-sidebar">
                <jsp:include page="../template/user_menu.jsp"/>
            </aside>
            <div class="content-wrapper">
                <section class="content">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="box">
                                <div  id="tiket_content" class="box-body" style="font-family: 'Times New Roman', Times, serif; font-weight: 1000">
                                    <c:if test="${ERR != 1}">
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4" style="text-align: center">
                                                <img src="${pageContext.servletContext.contextPath}/resources/img/Capture.PNG"  alt="User Image">
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <!--<div class="col-md-1"></div>-->
                                            <div class="col-md-12" style="text-align: center;font-size: 15px;margin-top: -22px;">
                                                <p><b>Ministry of Foreign Affairs Colombo 01,</b></p>
                                                <p style="margin-top: -11px;"><b>Sri Lanka</b></p>
                                            </div>
                                            <!--<div class="col-md-1"></div>-->
                                        </div> 
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4" style="text-align: center;margin-top: -33px;">
                                                <img src="data:image/png;base64,${QRIMAGEPATH}"  alt="QR Image">
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="text-align: center;font-size: 18px;margin-top: -28px;">
                                                <p>Service ID: ${SERVICEID}</p>
                                                <!--<p>Token Number: 0014</p>-->
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                        <br>

                                        <div class="row">
                                            <div class="col-md-12" style="text-align: center;margin-top: 0px;">
                                                <c:forEach var = "BOOKINGREFERENCE" items = "${BOOKINGREFERENCE}">
                                                    <p style="margin-bottom: 0px;">${BOOKINGREFERENCE.description}: ${fn:escapeXml(BOOKINGREFERENCE.reference)}</p>
                                                </c:forEach>
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: center;margin-top: -70px;">
                                                <P>${INSTRUCTION}</P>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-md-12" style="text-align: center;margin-top: -26px;">
                                                <P>${IDENTIFICATION}</P>
                                            </div>
                                        </div>
                                    </c:if>
                                    <c:if test="${ERR == 1}">
                                        <div class="alert alert-error">
                                            <strong>Warning!</strong> <span id="mainerrormessage">${errorMsg}</span>
                                        </div>
                                    </c:if>




                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <c:if test="${ERR != 1}">
                    <section class="content">
                        <div class="row">
                            <div class="col-md-5" style="text-align: right;margin-top: -45px;">
                                <button onclick="printsummary('tiket_content')"> <img src="${pageContext.servletContext.contextPath}/resources/img/print.png"  alt="User Image"></button>
                            </div>
                        </div>
                    </section>
                </c:if>

            </div>
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
        <script>
//            function printsummary() {
//                window.print();
//            }


            function printsummary(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;

                document.body.innerHTML = printContents;

                window.print();

                document.body.innerHTML = originalContents;
            }
        </script>
        <script type="text/javascript">
            $(window).load(function () {
                $(".loader").fadeOut("slow");
            });
        </script>
        <script>
            $(document).bind("ajaxSend", function () {
                $(".loader").fadeIn();
            }).bind("ajaxComplete", function () {
                $(".loader").fadeOut("slow");
            });
        </script>
    </body>
</html>

