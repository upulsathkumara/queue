<%-- 
    Document   : reservationCreate
    Created on : Oct 12, 2016, 12:28:28 PM
    Author     : Rasika
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 


<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>
        
        <script>
            var role = "<%=session.getAttribute("role")%>";
            if(role !== 'ADMIN'){
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }
    
            var username = "<%=session.getAttribute("loggedUser")%>"
            if (!<%=session.getAttribute("loggedUser")%>) {
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            } else {
            }

        </script>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/admin_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/admin_menu.jsp"/>
            </aside>
            <div class="content-wrapper">
                <section class="content">
                    <span style="font-size: 16px;"><b>Priority Request</b></span><br/><br/>
                    <form:form  method="POST"  action="${pageContext.request.contextPath}/reservation/vip/triger/created"  id="reservationForm" commandName="reservationForm" enctype="multipart/form-data"><span style="font-size: 16px;"></span><br/><br/> 
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <%-- <form:form class="form-horizontal" method="POST"  action="${pageContext.request.contextPath}/reservation/created" id="reservationForm" commandName="reservationForm">--%>
                                <div class="box-body form-horizontal">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="fristName">Name In Full:<samp style="color: red">*</samp></label>
                                                <div class="col-sm-4">
                                                    <form:input type="text" class="form-control" id="fristName" placeholder="Enter full name" path="fristName" maxlength="250"/>
                                                </div>
                                            </div>
                                            <!--                                            <div class="form-group">
                                                                                            <label class="control-label labelfont col-sm-3" for="lastName">Last Name:<samp style="color: red">*</samp></label>
                                                                                            <div class="col-sm-4">
                                            <%--<form:input type="text" class="form-control" id="lastName" placeholder="Enter last name" path="lastName" maxlength="32" />--%>
                                        </div>
                                    </div>-->
                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="fristName">Identification Document Type:<samp style="color: red">*</samp></label>
                                                <div class="col-sm-4">
                                                    <form:select value="NIC" path="identificatioDocumentType" class="form-control" id="searchTypeidentification">
                                                        <!--<option value="">--Select--</option>-->
                                                        <option value="NIC" >NIC</option>
                                                        <option value="PAS">Passport No</option>
                                                        <option value="DLN">DL Number</option>
                                                    </form:select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="identificationNumber">Identification Number:<samp style="color: red">*</samp></label>
                                                <div class="col-sm-4">
                                                    <form:input type="text" class="form-control" id="identificationNumber" placeholder="Enter Identification Number" path="identificationNumber" maxlength="12"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>

                            <div class="content" style="" id="stratEndtimePanel">
                                <div class="row">
                                    <br>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 form-horizontal">

                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-lg-5 col-sm-5 col-xs-5">
                                                </div>
                                                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
                                                    <form:button id="createbutton" type="submit" class="btn btn-default btn-block">
                                                        Create
                                                    </form:button>
                                                </div>
                                            </div>

                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
<jsp:include page="../template/jsinclude.jsp"/>
        <script>

            $('#reservationForm').validate({
                rules: {
                    identificationNumber: {
                        required: true
                    },
                    fristName: {
                        required: true
                    },
                    lastName: {
                        required: true
                    },
                    identificatioDocumentType: {
                        required: true
                    }

                }, errorPlacement: function(error, element) {
                    element.parent().find('div.invalid').remove();
                    element.parent().append('<div class="invalid">' + error.text() + '</div>');
                }
            });


            $('#fristName').keyup(function() {
                $('#fristName').val($('#fristName').val().toUpperCase());
            });
        </script>
        <script>
            $(".sidebar-menu li").removeClass( "selected" );
            $(".sidebar-menu  .priority-req").addClass( "selected" );
        </script>
    </body>
</html>

