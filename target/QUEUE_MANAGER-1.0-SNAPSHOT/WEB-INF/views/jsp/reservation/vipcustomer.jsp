<%-- 
    Document   : reservationCreate
    Created on : Oct 12, 2016, 12:28:28 PM
    Author     : Rasika
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 


<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>
        <script>
            var role = "<%=session.getAttribute("role")%>";
            if (role !== 'USER') {
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }
            var username = "<%=session.getAttribute("loggedUser")%>";
            if (!<%=session.getAttribute("loggedUser")%>) {
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            } else {
            }

        </script>
        <style>
            .datepicker table tr td:first-child + td + td + td + td + td +td {
                color: red
            }

            .datepicker table tr td:first-child {
                color: red
            }
        </style>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/user_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/user_menu.jsp"/>
            </aside>
            <div class="content-wrapper">
                <section class="content" style="margin-right: -50%;">
                    <form:form  method="POST"  action="${pageContext.request.contextPath}/reservation/vip/created"  id="reservationForm" commandName="reservationForm" enctype="multipart/form-data">
                        <br>
                        <div>
                            <div class="col-xs-12">
                                <c:if test="${not empty errorMsg}">
                                    <div class="alert alert-error">
                                        <strong>Warning!</strong> <span id="mainerrormessage">${errorMsg}</span>
                                    </div>
                                    <br/>
                                </c:if> 
                            </div>
                        </div>
                        <span style="font-size: 16px;"><b>Priority Appointment</b></span><br/><br/>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <div class="box-body form-horizontal">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="bookingId">Service Id:</label>
                                                <div class="col-sm-4">
                                                    <form:input type="text" class="form-control" id="bookingId" placeholder="Service Id" path="bookingId" maxlength="10" autocomplete="off"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <u class="main-title">Application Detail</u>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <%-- <form:form class="form-horizontal" method="POST"  action="${pageContext.request.contextPath}/reservation/created" id="reservationForm" commandName="reservationForm">--%>
                                <div class="box-body form-horizontal">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">

                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="NIC">Application Type:<samp style="color: red">*</samp></label>
                                                <div class="col-sm-4">
                                                    <form:select class="form-control" id="applicationType" path="applicationType">
                                                        <c:forEach var="stae" items="${applicationType}">
                                                            <option value="${stae.key}">${stae.value}</option>
                                                        </c:forEach>
                                                    </form:select>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>

                        <u class="main-title">Personal Details</u>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <div class="box-body form-horizontal">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="customerId"></label>
                                                <div class="col-sm-4">
                                                    <form:input type="hidden" class="form-control"  id="customerId" placeholder="Enter first name" path="customerId"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="identificationNumber">NIC Number:<samp style="color: red">*</samp></label>
                                                <div class="col-sm-4" >
                                                    <form:input type="text" class="form-control" id="identificationNumber" placeholder="Enter NIC number" path="identificationNumber" maxlength="12"/>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="fristName">Name In Full:<samp style="color: red">*</samp></label>
                                                <div class="col-sm-4">
                                                    <form:input type="text" class="form-control" id="fristName" placeholder="Enter full name" path="fristName" maxlength="128"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="email">Email:</label>
                                                <div class="col-sm-4">
                                                    <form:input type="text" class="form-control" id="email" placeholder="Enter email" path="email" maxlength="32"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="telephoneMobile">Telephone Mobile:<samp style="color: red">*</samp></label>
                                                <div class="col-sm-4">
                                                    <form:input type="text" class="form-control" id="telephoneMobile" placeholder="Enter telephone mobile" path="telephoneMobile" maxlength="16"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="preferredLanguage">Preferred Language:<samp style="color: red">*</samp></label>
                                                <div class="col-sm-4">
                                                    <form:select path="preferredLanguage" class="form-control" id="preferredLanguage">
                                                        <option value="ENGLISH">English</option>
                                                        <option value="SINHALA" >Sinhala</option>
                                                        <option value="TAMIL">Tamil</option>
                                                    </form:select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="bookingdate">Requesting Appointment Date:<samp style="color: red">*</samp></label>

                                                <div class="input-group date col-sm-4" style="padding-left: 15px;padding-right:  15px;float: left;" >
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar" style="cursor:auto"></i>
                                                    </div>
                                                    <form:input type="text"  path="bookingdate" id="bookingdate" value="${dateNow}" class="form-control pull-right" style="height: 36px;" readonly="true"/>
                                                </div>
                                                <!-- /.input group -->
                                            </div>

                                            <div class="col-xs-7" >
                                                <div class="box box-default collapsed-box">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title">Address Information</h3>

                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body">
                                                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label class="control-label labelfont col-sm-5" for="addressLine1">Address Line1:</label>
                                                                <div class="col-sm-7">
                                                                    <form:input type="text" class="form-control" id="addressLine1" placeholder="Enter address line1" path="addressLine1" maxlength="32"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label labelfont col-sm-5" for="addressLine2">Address Line2:</label>
                                                                <div class="col-sm-7">
                                                                    <form:input type="text" class="form-control" id="addressLine2" placeholder="Enter address line2" path="addressLine2" maxlength="32"/>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label labelfont col-sm-5" for="addressLine3">Address Line3:</label>
                                                                <div class="col-sm-7">
                                                                    <form:input type="text" class="form-control" id="addressLine3" placeholder="Enter address line3" path="addressLine3" maxlength="32"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.box-body -->
                                                </div>
                                                <!-- /.box -->
                                            </div>




                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="documentTypeList"></label>
                                                <div class="col-sm-4">
                                                    <form:input type="hidden" class="form-control" id="documentTypeList" path="documentTypeList"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="documentVarifictionList"></label>
                                                <div class="col-sm-4">
                                                    <form:input type="hidden" class="form-control" id="documentVarifictionList" path="documentVarifictionList"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label labelfont col-sm-3" for="documentDatabaseList"></label>
                                                <div class="col-sm-4">
                                                    <input type="hidden" class="form-control" id="documentDatabaseList" value="<c:out value="${Type}"/>"/>
                                                </div>
                                            </div>
                                            <br>

                                            <u class="main-title">Documents</u>
                                            <div class="row">
                                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="control-label labelfont col-sm-3" for="documentType">Select Document Type:</label>
                                                            <div class="col-sm-4">
                                                                <select class="form-control" id="documentType" name="documentType">
                                                                    <c:forEach var="stae" items="${documentType}">
                                                                        <option value="${stae.key}">${stae.value}</option>
                                                                    </c:forEach>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label labelfont col-sm-3" for="documentQuentity">Quantity:</label>
                                                            <div class="col-sm-4">
                                                                <input type="text" class="form-control" id="documentQuentity" name="documentQuentity" placeholder="Enter quantity" maxlength="2"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-7">
                                                                <button type="button" class="btn btn-default btn-block pull-right" id="addDocumentType">Add</button>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row labelfont" >
                                                <div class="col-md-7 col-lg-7 col-sm-7 col-xs-7">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Document</th>
                                                                <th>Quantity</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="documentTable">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                            <form:input type="hidden" class="form-control" id="reschduleType" path="reschduleType" readonly="true"/>
                            <form:input type="hidden" class="form-control" id="helpdeskreason" path="helpdeskreason" readonly="true"/>
                            <div class="content" style="" id="stratEndtimePanel">
                                <div class="row">
                                    <br>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="row">

                                            <div class="row">
                                                <div class="col-xs-7">
                                                    <form:button id="createbutton" type="button" class="btn btn-default pull-right">
                                                        Create
                                                    </form:button>
                                                    <%--<form:button id="helpdeskbutton" type="button" class="btn btn-default btn-block pull-right">--%>
                                                    <!--Send to Help Desk-->  
                                                    <%--</form:button>--%>  
                                                </div>
                                            </div>

                                        </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
        <div id="myModal" class="modal fade" role="dialog" style="">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" style="text-align: center">Time Slot Selection</h4>
                        <br/>
                        <div class="raw">
                            <div class="col-xs-3"></div>
                            <div class="col-xs-2"><div style="width: 15px;height: 15px;background: #0AC21F;-moz-border-radius: 50px;-webkit-border-radius: 50px;border-radius: 50px;margin-right: 5px;float: left;"></div>Available</div>
                            <div class="col-xs-2"><div style="width: 15px;height: 15px;background: #e20b1d;-moz-border-radius: 50px;-webkit-border-radius: 50px;border-radius: 50px;margin-right: 5px;float: left;"></div>Allocated</div>
                            <div class="col-xs-2"><div style="width: 15px;height: 15px;background: #eae4e4;-moz-border-radius: 50px;-webkit-border-radius: 50px;border-radius: 50px;margin-right: 5px;float: left;"></div>Insufficient</div>
                            <div class="col-xs-3"></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center" id="slotBarList">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="confirmTimeSlotSelection" class="modal fade" role="dialog" style="">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title modal-title-center" style="text-align: center;">You are selecting</h4>
                    </div>
                    <div class="modal-body">
                        <div id="conf-window-show-desc"></div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="confirmedSelection">OK</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
        <!--documents model-->
        <div id="mydocumentmodel" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Document Reference</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center">
                                <div class="invalid" id="referenceValidationMessage">

                                </div>
                                <table class="table">
                                    <tbody id="referenceTable" class="referenceTable"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="addDocument" type="button" class="btn btn-secondary">Save</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="mydocumentmodelview" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Document Reference View</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center">
                                <table class="table">
                                    <tbody id="referenceTableView" class="referenceTableview"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>              
                </div>

            </div>
        </div>

        <div id="mydocumentmodeledit" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Document Reference Edit</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center">
                                <div class="invalid" id="referenceEditValidationMessage"></div>
                                <table class="table">
                                    <tbody id="referenceTableEdit" class="referenceTableEdit"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="editReferenceDocument" type="button" class="btn btn-secondary">Edit</button>
                        <input type="hidden" id="referenceEditId" />
                    </div>
                </div>

            </div>
        </div>
        <div id="mydocumentmodeldelete" class="modal fade" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Documents</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center">
                                <p class="refereceDelete">Are you sure you want to delete this record?</p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="deleteReferenceDocument" type="button" class="btn btn-secondary">Delete</button>
                        <input type="hidden" id="referenceDeleteId" />
                    </div>
                </div>

            </div>
        </div>

        <!--        <div id="checkAvilabilityInvlid" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Warning</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: center">
                                        <p class="invalid">Please Add Document</p>
                                    </div>
                                </div>
                            </div>
                        </div>
        
                    </div>
                </div>-->

        <div class="modal" id="checkAvilabilityInvlid"> 
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #367fa9">×</span></button>
                        <h4 class="modal-title" style="color: #367fa9"><span class="glyphicon glyphicon-exclamation-sign"></span> Warning!</h4>
                    </div>
                    <div class="modal-body" style="color: #ef0746">
                        <p id="checkAvilabilityInvlidMessage"><b>Please add at least one valid document</b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="reservationcreateconfirmation"> 
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #367fa9">×</span></button>
                        <h4 class="modal-title" style="color: #367fa9"><span class="glyphicon glyphicon-exclamation-sign"></span> Confirmation for Create!</h4>
                    </div>
                    <div class="modal-body" style="color: #367fa9">
                        <p><b>Are you sure you want to proceed this reservation?</b></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="createOk" class="btn btn-primary">Ok</button>
                        <button type="button" id="createCancel" class="btn btn-primary" data-dismiss="modal">Cancel</button>  
                        <input type="hidden" id="referenceDeleteId" />
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="sendToHelpDeskReason"> 
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color:white">×</span></button>
                        <h4 class="modal-title" style="color:#3c8dbc"> Enter the Reason for Sending to the Help Desk</h4>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="helpdreason" id="helpdreason" style="width: 100%;" maxlength="300">
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="sendToHelpDeskOk" class="btn btn-primary">Add</button>
                        <button type="button" id="createCancel" class="btn btn-primary" data-dismiss="modal">Cancel</button>  
                    </div>
                </div>
            </div>
        </div>
        <script>
            $('#documentVarifictionList').val('');
            var serviceId =${SERVICEID};
            if (serviceId !== 0) {
                var dataObject = new Object();
                dataObject.serachValue = serviceId.toString();
                var content = JSON.stringify(dataObject);
                var type = "";
                $.ajax({
                    async: true,
                    type: "POST",
                    url: "${pageContext.servletContext.contextPath}/reservation/exit/customer/vip",
                    cache: false,
                    data: {data: content},
                    success: function (res) {
                        res1 = JSON.parse(res);
                        $("#identificationNumber").val('');
                        $("#customerId").val('');
                        $("#fristName").val('');
                        $("#lastName").val('');
                        $("#bookingId").val('');
                        $.each(res1.CUSTOMER, function (index, value) {
                            $("#identificatioDocumentType").val(value.identificatioDocumentType);
                            $("#identificationNumber").val(value.identificationNumber);
                            $("#customerId").val(value.customerId);
                            $("#fristName").val(value.fristName);
                            $("#lastName").val(value.lastName);
                            $("#bookingId").val(serviceId);
                            $("#imageChoose").empty();
                            if (value.identificatioDocumentType !== 'NIC') {
                                $("#imageChoose").append('<div class="input-group form-group">'
                                        + '<input type="file" id="file" name="files[0]" class="form-control"  onchange="this.parentNode.nextSibling.value = this.value"/>'
                                        + '<span class="input-group-btn">'
                                        + '<button class="btn btn-secondary" type="button" id="customerSerach">Scan</button>'
                                        + '</span>'
                                        + '</div>');
                            } else {
                                $("#imageChoose").append('<div class="input-group form-group">'
                                        + '<input type="file" id="file" name="files[0]" class="form-control"  onchange="this.parentNode.nextSibling.value = this.value"/>'
                                        + '<span class="input-group-btn">'
                                        + '<button class="btn btn-secondary" type="button" id="customerSerach">Scan</button>'
                                        + '</span>'
                                        + '</div>'
                                        + '<div class="input-group form-group">'
                                        + '<input type="file" id="file" name="files[1]" class="form-control" onchange="this.parentNode.nextSibling.value = this.value">'
                                        + '<span class="input-group-btn">'
                                        + '<button class="btn btn-secondary" type="button" id="customerSerach">Scan</button>'
                                        + '</span>'
                                        + '</div>');
                            }

                        });
                    }
                });
            }


            $('#endtime').prop('readonly', true);
            $('#startTime').prop('readonly', true);
            var documentjsonList = "";
            var referenceJsonList = "";
            var index = 0;
            $("#customerSerach").click(function (event) {
                event.preventDefault();

            });


            $("#bookingId").keyup(function (event) {
                if (event.keyCode === 13) {
                    if ($('#bookingId').valid()) {
                        var serachValue = $(this).val();
                        var dataObject = new Object();
                        dataObject.serachValue = serachValue;
                        var content = JSON.stringify(dataObject);
                        var type = "";
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "${pageContext.servletContext.contextPath}/reservation/exit/customer/vip",
                            cache: false,
                            data: {data: content},
                            success: function (res) {
                                res1 = JSON.parse(res);

                                if (res1.CUSTOMER.length > 0) {
                                    $("#identificationNumber").val('');
                                    $("#customerId").val('');
                                    $("#fristName").val('');
                                    $("#lastName").val('');
                                    $.each(res1.CUSTOMER, function (index, value) {
                                        $("#identificationNumber").val(value.identificationNumber);
                                        $("#customerId").val(value.customerId);
                                        $("#fristName").val(value.fristName);
                                        $("#lastName").val(value.lastName);
                                    });
                                } else {
                                    $('#checkAvilabilityInvlidMessage').html('<b>Not a valid Priority service ID or already processed one</b>');
                                    $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});
                                }

                            }
                        });
                    }

                }
            });

//            $("#addDocumentType").click(function (event) {
//                $("#documentQuentity").rules('add', {
//                    required: true,
//                    number: true
//                });
//                $("#documentType").rules('add', {
//                    required: true
//                });
//                if ($('#documentType').valid() & $('#documentQuentity').valid()) {
//                    var quantity = $("#documentQuentity").val();
//                    var chargeList = $("#documentDatabaseList").val();
//                    var documentTypeId = $("#documentType").val();
//                    var refernceId = "REFERENCE-" + documentTypeId;
//                    $("#referenceTable").html("");
//                    $("#referenceValidationMessage").html("");
//                    res3 = JSON.parse(chargeList);
////                    $("#chargeType").html("");
//
//
//                    for (var i = 0; i < quantity; i++) {
//                        var inputId = refernceId + "-" + i + "-INPUT";
//                        var selectId = refernceId + "-" + i + "-SELECT";
//                        var selectChargeCode = refernceId + "-" + i + "-SELECTCHRG";
//                        $("#referenceTable").append('<tr>'
//                                + '<td class=col-xs-1 pull-left style=border-top-width:0px >Reference:</td>'
//                                + '<td class=col-xs-2 style=border-top-width:0px>'
//                                + '<input class=pull-left maxlength="20" type=text id="' + inputId + '"/>'
//                                + '</td>'
//                                + '<td class=col-xs-3 style=border-top-width:0px>'
//                                + '<select id="' + selectId + '">'
//                                + '<option value="ACCP">Accept</option>'
//                                + '<option value="REJT">Reject</option>'
//                                + '</select>'
//                                + '</td>'
//                                + '<td class=col-xs-6 style=border-top-width:0px; >'
//                                + '<select id="' + selectChargeCode + '" style="width:160px;">'
//                                + '</select>'
//                                + '</td>'
//                                + '</tr>');
//                        //add charge type dropdown
//                        for (var j = 0; j < res3.charge.length; j++) {
//                            $('#' + selectChargeCode + '').append('<option value="' + res3.charge[j].documentChargeCode + '">' + res3.charge[j].description + '</option>');
//                        }
//                    }
//
//
//                    $('#mydocumentmodel').modal({backdrop: 'static', keyboard: false});
//                }
//            });


//            $("#bookingId").focusout(function () {
//                var serachValue = $(this).val();
//                var dataObject = new Object();
//                dataObject.serachValue = serachValue;
//                var content = JSON.stringify(dataObject);
//                $.ajax({
//                    async: true,
//                    type: "POST",
//                    url: "${pageContext.servletContext.contextPath}/reservation/exit/customer/vip",
//                    cache: false,
//                    data: {data: content},
//                    success: function (res) {
//                        res1 = JSON.parse(res);
//                        $("#identificationNumber").val('');
//                        $("#customerId").val('');
//                        $("#fristName").val('');
//                        $("#lastName").val('');
//                        $.each(res1.CUSTOMER, function (index, value) {
//                            $("#identificatioDocumentType").val(value.identificatioDocumentType);
//                            $("#identificationNumber").val(value.identificationNumber);
//                            $("#customerId").val(value.customerId);
//                            $("#fristName").val(value.fristName);
//                            $("#lastName").val(value.lastName);
//                        });
//                    }
//                });
//            });

//            $("#addDocumentType").click(function (event) {
//                $("#documentQuentity").rules('add', {
//                    required: true,
//                    number: true
//                });
//                $("#documentType").rules('add', {
//                    required: true
//                });
//                if ($('#documentType').valid() & $('#documentQuentity').valid()) {
//                    var quantity = $("#documentQuentity").val();
//                    var chargeList = $("#documentDatabaseList").val();
//                    var documentTypeId = $("#documentType").val();
//                    var refernceId = "REFERENCE-" + documentTypeId;
//                    $("#referenceTable").html("");
//                    $("#referenceValidationMessage").html("");
//                    res3 = JSON.parse(chargeList);
//                    //                    $("#chargeType").html("");
//
//
//                    for (var i = 0; i < quantity; i++) {
//                        var inputId = refernceId + "-" + i + "-INPUT";
//                        var selectId = refernceId + "-" + i + "-SELECT";
//                        var selectChargeCode = refernceId + "-" + i + "-SELECTCHRG";
//                        $("#referenceTable").append('<tr>'
//                                + '<td class=col-xs-1 pull-left style=border-top-width:0px >Reference:</td>'
//                                + '<td class=col-xs-2 style=border-top-width:0px>'
//                                + '<input class=pull-left type=text id="' + inputId + '" maxlength="20" />'
//                                + '</td>'
//                                + '<td class=col-xs-3 style=border-top-width:0px>'
//                                + '<select id="' + selectId + '">'
//                                + '<option value="ACCP">Accept</option>'
//                                + '<option value="REJT">Reject</option>'
//                                + '</select>'
//                                + '</td>'
//                                + '<td class=col-xs-6 style=border-top-width:0px; >'
//                                + '<select id="' + selectChargeCode + '" style="width:160px;">'
//                                + '</select>'
//                                + '</td>'
//                                + '</tr>');
//                        //add charge type dropdown
//                        for (var j = 0; j < res3.charge.length; j++) {
//                            $('#' + selectChargeCode + '').append('<option value="' + res3.charge[j].documentChargeCode + '">' + res3.charge[j].description + '</option>');
//                        }
//                    }
//
//
//                    $('#mydocumentmodel').modal({backdrop: 'static', keyboard: false});
//                }
//            });
//            $("#addDocument").click(function (event) {
//                event.preventDefault();
//                var tableColoumnId = "TABLECOLOUMN-";
//                var documentTypeId = $("#documentType").val();
//                var refernceId = "REFERENCE-" + documentTypeId;
//                var quantity = $("#documentQuentity").val();
//                tableColoumnId = tableColoumnId + documentTypeId;
//                var values = [];
//                for (var i = 0; i < quantity; i++) {
//                    var inputId = "#" + refernceId + "-" + i + "-INPUT";
//                    var value = $(inputId).val();
//                    values.push(value.toString());
//                }
//                if (isEmpty(values)) {
//                    if (isUnique(values)) {
//                        $("#documentType").rules("remove");
//                        $("#documentQuentity").rules("remove");
//                        var documentData = new Object();
//                        var documentTypeText = $("#documentType option:selected").text();
//                        $("#documentTable").append('<tr id="' + tableColoumnId + '"><td>' + documentTypeText + '</td><td>' + quantity + '</td><td><a class="documentActionView" id="' + documentTypeId + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="documentActionEdit" id="' + documentTypeId + '"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="documentActionDelete" id="' + documentTypeId + '"><i class="fa fa-trash"></i></a></td></tr>');
//                        documentData.documentTypeId = documentTypeId;
//                        documentData.quantity = quantity;
//                        var content = JSON.stringify(documentData);
//                        var referenceJson = "";
//                        for (var i = 0; i < quantity; i++) {
//                            var referenceData = new Object();
//                            var inputId = "#" + refernceId + "-" + i + "-INPUT";
//                            var selectId = "#" + refernceId + "-" + i + "-SELECT";
//                            var selectChargeCode = "#" + refernceId + "-" + i + "-SELECTCHRG";
//                            var inputValue = $(inputId).val();
//                            var selectValue = $(selectId).val();
//                            var charg = $(selectChargeCode).val();
//                            referenceData.reference = inputValue;
//                            referenceData.status = selectValue;
//                            referenceData.documentTypeId = documentTypeId;
//                            referenceData.charge = charg;
//                            var content_1 = JSON.stringify(referenceData);
//                            if (i === 0) {
//                                referenceJson = content_1;
//                            } else {
//                                referenceJson = referenceJson + "," + content_1;
//                            }
//
//                        }
//                        if (index === 0) {
//                            documentjsonList = content;
//                            referenceJsonList = referenceJson;
//                            index++;
//                        } else {
//                            documentjsonList = documentjsonList + "," + content;
//                            referenceJsonList = referenceJsonList + "," + referenceJson;
//                            index++;
//                        }
//                        var jsonString1 = "[" + documentjsonList + "]";
//                        var jsonString2 = "[" + referenceJsonList + "]";
//                        $("#documentTypeList").val(jsonString1);
//                        $("#documentVarifictionList").val(jsonString2);
//                        var text1 = "#documentType option[value='";
//                        var text2 = "']";
//                        $(text1.concat(documentTypeId, text2)).remove();
//                        $("#documentQuentity").val('');
//                        $('#mydocumentmodel').modal('toggle');
//                        $("#timeTotal").val(Totaltime());
//                    } else {
//                        $("#referenceValidationMessage").empty();
//                        $("#referenceValidationMessage").append("<p>Reference field value can not be duplicate</p>");
//                    }
//                } else {
//                    $("#referenceValidationMessage").empty();
//                    $("#referenceValidationMessage").append("<p>Reference field value can not be empty</p>");
//                }
//            });
//
//            $("#confirmedSelection").click(function (event) {
//                $('#myModal').modal('toggle');
//                $('#confirmTimeSlotSelection').modal('toggle');
//            });
//            $("#slotBarList").on("click", ".get-slot-id", function () {
//
////                $('#myModal').modal('toggle');
//                var id = $(this).attr("id");
//                var stratTimeId = "#" + id + "-strat-time";
//                var endTimeId = "#" + id + "-end-time";
//                var dekNoId = "#" + id + "-dek-no";
//                var startTime = $(stratTimeId).val();
//                var endtime = $(endTimeId).val();
//                var dekNo = $(dekNoId).val();
//                $('#confirmTimeSlotSelection').modal({backdrop: 'static', keyboard: false});
//                $("#conf-window-show-desc").empty();
//                $("#stratEndtimePanel").show();
//                $("#endtime").val(endtime);
//                $("#startTime").val(startTime);
//                $("#bookeDsdId").val(dekNo);
//                $('#endtime').prop('readonly', true);
//                $('#startTime').prop('readonly', true);
//                $("#conf-window-show-desc").append('<div class="row"><div class="col-xs-3"></div><div class="col-xs-3">Desk Number:</div><div class="col-xs-3">' + dekNo + '</div><div class="col-xs-3"></div></div><div class="row"><div class="col-xs-3"></div><div class="col-xs-3">Start Time:</div><div class="col-xs-3">' + startTime + '</div><div class="col-xs-3"></div></div><div class="row"><div class="col-xs-3"></div><div class="col-xs-3">End Time:</div><div class="col-xs-3">' + endtime + '</div><div class="col-xs-3"></div></div>');
////                $('#myModal').modal('toggle');
////                $('#confirmTimeSlotSelection').modal({backdrop: 'static', keyboard: false});
//            });
//            $(document).ready(function () {
//                $('#menu [data-accordion]').accordion();
//                var height = $(".content-area").height();
//                $(".menu-container").height(Math.max(height, 600));
//            });
//            function hideFileChooser() {
//                $("#imageChoose").empty();
//                if ($("#identificatioDocumentType").val() !== 'NIC') {
//                    $("#imageChoose").append('<div class="input-group form-group">'
//                            + '<input type="file" id="file" name="files[0]" class="form-control"  onchange="this.parentNode.nextSibling.value = this.value"/>'
//                            + '<span class="input-group-btn">'
//                            + '<button class="btn btn-secondary" type="button" id="customerSerach">Scan</button>'
//                            + '</span>'
//                            + '</div>');
//                } else {
//                    $("#imageChoose").append('<div class="input-group form-group">'
//                            + '<input type="file" id="file" name="files[0]" class="form-control"  onchange="this.parentNode.nextSibling.value = this.value"/>'
//                            + '<span class="input-group-btn">'
//                            + '<button class="btn btn-secondary" type="button" id="customerSerach">Scan</button>'
//                            + '</span>'
//                            + '</div>'
//                            + '<div class="input-group form-group">'
//                            + '<input type="file" id="file" name="files[1]" class="form-control" onchange="this.parentNode.nextSibling.value = this.value">'
//                            + '<span class="input-group-btn">'
//                            + '<button class="btn btn-secondary" type="button" id="customerSerach">Scan</button>'
//                            + '</span>'
//                            + '</div>');
//                }
//            }
//
//
//            $('#reservationForm').validate({
//                rules: {
//                    identificatioDocumentType: {
//                        required: true
//                    },
//                    identificationNumber: {
//                        required: true
//                    },
//                    applicationType: {
//                        required: true
//                    },
//                    fristName: {
//                        required: true
//                    },
//                    lastName: {
//                        required: true
//                    },
//                    email: {
//                        email: true
//                    },
//                    telephoneMobile: {
//                        required: true,
//                        number: true,
//                        maxlength: 10,
//                        minlength: 10
//                    },
//                    preferredLanguage: {
//                        required: true
//                    }
//
//                }, errorPlacement: function (error, element) {
//                    error.insertAfter(element.parent());
//                }
//            });
//            $('#documentTypeform').validate({
//                rules: {
//                    documentType: {
//                        required: true
//                    },
//                    documentQuentity: {
//                        required: true,
//                        number: true
//                    }
//                }, errorPlacement: function (error, element) {
//                    error.insertAfter(element.parent());
//                }
//            });
//            function isUnique(values) {
//                values.sort();
//                // Check whether there are two equal values next to each other
//                for (var k = 1; k < values.length; ++k) {
//                    if (values[k] === values[k - 1]) {
//                        return false;
//                        break;
//                    }
//                }
//                return true;
//            }
//            function isEmpty(values) {
//                values.sort();
//                // Check whether there are two equal values next to each other
//                for (var k = 0; k < values.length; ++k) {
//                    if (!values[k]) {
//                        return false;
//                        break;
//                    }
//                }
//                return true;
//            }
//
//            $("#documentTable").on("click", ".documentActionView", function () {
//                var id = $(this).attr("id");
//                var list = $("#documentVarifictionList").val();
//                var databaseList = $("#documentDatabaseList").val();
//                res1 = JSON.parse(list);
//                res2 = JSON.parse(databaseList);
//                $("#referenceTableView").empty();
//                console.log(res1);
//                index = 0;
//                $.each(res1, function () {
//                    var statusId = 'REFERENCEVIEW-STATUS-' + index;
//                    var chargeId = 'REFERENCEVIEW-CHARGE-' + index;
//                    if (res1[index].documentTypeId === id) {
//                        console.log(res1[index]);
//                        console.log(res1[index].reference);
//                        console.log(res1[index].status);
//                        $("#referenceTableView").append('<tr>'
//                                + '<td class=col-xs-1 pull-left style=border-top-width:0px >Reference:</td>'
//                                + '<td class=col-xs-2 style=border-top-width:0px>'
//                                + '<input class=pull-left type=text value="' + res1[index].reference + '" readonly/>'
//                                + '</td>'
//                                + '<td class=col-xs-3 style=border-top-width:0px>'
//                                + '<select id="' + statusId + '" disabled="true">'
//                                + '<option value="ACCP">Accept</option>'
//                                + '<option value="REJT">Reject</option>'
//                                + '</select>'
//                                + '</td>'
//                                + '<td class=col-xs-6 style=border-top-width:0px; >'
//                                + '<select id="' + chargeId + '" style="width:160px" disabled="true">'
//                                + '</select>'
//                                + '</td>'
//                                + '</tr>');
//                        $('#' + statusId).val(res1[index].status);
//                        for (var j = 0; j < res2.charge.length; j++) {
//                            $('#' + chargeId).append('<option value="' + res2.charge[j].documentChargeCode + '">' + res2.charge[j].description + '</option>');
//                        }
//                        $('#' + chargeId).val(res1[index].charge);
//                    }
//                    index++;
//                });
//                $('#mydocumentmodelview').modal({backdrop: 'static', keyboard: false});
//            });
//            $("#documentTable").on("click", ".documentActionEdit", function () {
//                var id = $(this).attr("id");
//                var list = $("#documentVarifictionList").val();
//                var databaseList = $("#documentDatabaseList").val();
//                res1 = JSON.parse(list);
//                res2 = JSON.parse(databaseList);
//                $("#referenceTableEdit").empty();
//                index = 0;
//                i = 0;
//                $.each(res1, function () {
//                    if (res1[index].documentTypeId === id) {
//                        var referenceId = 'REFERENCEEDIT-REFERENCE-' + i + "-" + res1[index].documentTypeId;
//                        var statusId = 'REFERENCEEDIT-STATUS-' + i + "-" + res1[index].documentTypeId;
//                        var chargeId = 'REFERENCEEDIT-CHARGE-' + i + "-" + res1[index].documentTypeId;
//                        $("#referenceTableEdit").append('<tr>'
//                                + '<td class=col-xs-1 pull-left style=border-top-width:0px >Reference:</td>'
//                                + '<td class=col-xs-2 style=border-top-width:0px>'
//                                + '<input class=pull-left maxlength="20" type=text value="' + res1[index].reference + '" id="' + referenceId + '"/>'
//                                + '</td>'
//                                + '<td class=col-xs-3 style=border-top-width:0px>'
//                                + '<select id="' + statusId + '">'
//                                + '<option value="ACCP">Accept</option>'
//                                + '<option value="REJT">Reject</option>'
//                                + '</select>'
//                                + '</td>'
//                                + '<td class=col-xs-6 style=border-top-width:0px; >'
//                                + '<select id="' + chargeId + '" style="width:160px">'
//                                + '</select>'
//                                + '</td>'
//                                + '</tr>');
//                        $('#' + statusId).val(res1[index].status);
//                        for (var j = 0; j < res2.charge.length; j++) {
//                            $('#' + chargeId).append('<option value="' + res2.charge[j].documentChargeCode + '">' + res2.charge[j].description + '</option>');
//                        }
//                        $('#' + chargeId).val(res1[index].charge);
//                        i++;
//                    }
//                    index++;
//                });
//                $("#referenceEditId").val(id);
//                $('#mydocumentmodeledit').modal({backdrop: 'static', keyboard: false});
//            });
//            $("#editReferenceDocument").click(function (event) {
//                event.preventDefault();
//                var id = $("#referenceEditId").val();
//                var list = $("#documentVarifictionList").val();
//                var documentList = $("#documentTypeList").val();
//                var quantity = "";
//                res1 = JSON.parse(list);
//                res2 = JSON.parse(documentList);
//                for (var j = 0; j < res2.length; j++) {
//                    if (res2[j].documentTypeId === id) {
//                        quantity = res2[j].quantity;
//                    }
//                }
//
//
//                for (var i = 0; i < res1.length; i++) {
//                    if (res1[i].documentTypeId === id) {
//                        res1.splice(i);
//                    }
//                }
//                var length = res1.length;
//                for (var k = 0; k < quantity; k++) {
//                    var referenceId = 'REFERENCEEDIT-REFERENCE-' + k + "-" + id;
//                    var statusId = 'REFERENCEEDIT-STATUS-' + k + "-" + id;
//                    var chargeId = 'REFERENCEEDIT-CHARGE-' + k + "-" + id;
//                    var reference = $('#' + referenceId).val();
//                    var status = $('#' + statusId).val();
//                    var charge = $('#' + chargeId).val();
//                    res1[length] = {documentTypeId: id, reference: reference, status: status, charge: charge};
//                    length++;
//                }
//
//                $("#documentVarifictionList").val(JSON.stringify(res1));
//                $('#mydocumentmodeledit').modal('toggle');
//            });
//            $("#documentTable").on("click", ".documentActionDelete", function () {
//                var id = $(this).attr("id");
//                $("#referenceDeleteId").val(id);
//                $('#mydocumentmodeldelete').modal({backdrop: 'static', keyboard: false});
//            });
//            $("#deleteReferenceDocument").click(function (event) {
//                event.preventDefault();
//                var id = $("#referenceDeleteId").val();
//                var tableColoumnId = "#TABLECOLOUMN-";
//                tableColoumnId = tableColoumnId + id;
//                $(tableColoumnId).remove();
//                var list = $("#documentVarifictionList").val();
//                var documentList = $("#documentTypeList").val();
//                var databaseDocumnetTypeList = $("#documentDatabaseList").val();
//                res1 = JSON.parse(list);
//                res2 = JSON.parse(documentList);
//                res3 = JSON.parse(databaseDocumnetTypeList);
//                for (var j = 0; j < res2.length; j++) {
//                    if (res2[j].documentTypeId === id) {
//                        res2.splice(j);
//                    }
//                }
//                for (var i = 0; i < res1.length; i++) {
//                    if (res1[i].documentTypeId === parseInt(id)) {
//                        res1.splice(i);
//                    }
//                }
//                $("#documentVarifictionList").val(JSON.stringify(res1));
//                $("#documentTypeList").val(JSON.stringify(res2));
//                for (var i = 0; i < res3.TYPE.length; i++) {
//                    if (res3.TYPE[i].documentTypeId === id) {
//                        $("#documentType").append('<option value="' + res3.TYPE[i].documentTypeId + '">' + res3.TYPE[i].description + '</option>');
//                    }
//                }
//                $("#timeTotal").val(Totaltime());
//                $('#mydocumentmodeldelete').modal('toggle');
//            });
//            function Totaltime() {
//                var documentList = $("#documentTypeList").val();
//                var databaseDocumnetTypeList = $("#documentDatabaseList").val();
//                res1 = JSON.parse(documentList);
//                res2 = JSON.parse(databaseDocumnetTypeList);
//                var time = 0;
//                for (var j = 0; j < res2.TYPE.length; j++) {
//                    for (var i = 0; i < res1.length; i++) {
//                        if (res2.TYPE[j].documentTypeId === res1[i].documentTypeId) {
//                            var quantity = res1[i].quantity;
//                            var varifitime = res2.TYPE[j].varificationTime;
//                            time = time + parseInt(quantity) * parseInt(varifitime);
//                        }
//                    }
//                }
//                return time + " min";
//            }
//
//            $('#ticketSerach').on('click', function (event) {
//                event.preventDefault();
//                var serachValue = $("#ticketSerachValue").val();
//                var dataObject = new Object();
//                dataObject.serachValue = serachValue;
//                var content = JSON.stringify(dataObject);
//                $.ajax({
//                    async: true,
//                    type: "POST",
//                    url: "${pageContext.servletContext.contextPath}/reservation/search",
//                    cache: false,
//                    data: {data: content},
//                    success: function (res) {
//                        res1 = JSON.parse(res);
//                        console.log(res1.object[0].customer.addressLine1);
//                        $('#startTime').val("" + res1.object[0].customer.addressLine1)
//                    }
//                });
//            });









            $("#addDocumentType").click(function (event) {
                $("#documentQuentity").rules('add', {
                    required: true,
                    number: true
                });
                $("#documentType").rules('add', {
                    required: true
                });
                if ($('#documentType').valid() & $('#documentQuentity').valid()) {
                    var quantity = $("#documentQuentity").val();
                    var chargeList = $("#documentDatabaseList").val();
                    var documentTypeId = $("#documentType").val();
                    var refernceId = "REFERENCE-" + documentTypeId;
                    $("#referenceTable").html("");
                    $("#referenceValidationMessage").html("");
                    res3 = JSON.parse(chargeList);
                    //                    $("#chargeType").html("");


                    for (var i = 0; i < quantity; i++) {
                        var inputId = refernceId + "-" + i + "-INPUT";
                        $("#referenceTable").append('<tr>'
                                + '<td class=col-xs-1 pull-left style=border-top-width:0px >Reference:</td>'
                                + '<td class=col-xs-2 style=border-top-width:0px>'
                                + '<input class=pull-left type=text id="' + inputId + '" maxlength="14" />'
                                + '</td>'
                                + '</tr>');

                    }


                    $('#mydocumentmodel').modal({backdrop: 'static', keyboard: false});
                }
            });
            $("#addDocument").click(function (event) {
                event.preventDefault();
                var tableColoumnId = "TABLECOLOUMN-";
                var documentTypeId = $("#documentType").val();
                var refernceId = "REFERENCE-" + documentTypeId;
                var quantity = $("#documentQuentity").val();
                var list = $("#documentVarifictionList").val();
                var documentList = $("#documentTypeList").val();
                var referenceArray = [];
                var documentTypeArray = [];
                tableColoumnId = tableColoumnId + documentTypeId;
                var values = [];
                for (var i = 0; i < quantity; i++) {
                    var inputId = "#" + refernceId + "-" + i + "-INPUT";
                    var value = $(inputId).val();
                    values.push(value.toString());
                }


                if (isEmpty(values)) {
                    if (isUnique(values)) {
                        if (specialCharacter(values)) {
                            $("#documentType").rules("remove");
                            $("#documentQuentity").rules("remove");
                            var documentData = new Object();
                            var documentTypeText = $("#documentType option:selected").text();
                            $("#documentTable").append('<tr id="' + tableColoumnId + '"><td>' + documentTypeText + '</td><td>' + quantity + '</td><td><a class="documentActionView" id="' + documentTypeId + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="documentActionEdit" id="' + documentTypeId + '"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="documentActionDelete" id="' + documentTypeId + '"><i class="fa fa-trash"></i></a></td></tr>');
                            documentData.documentTypeId = documentTypeId;
                            documentData.quantity = quantity;
                            if (index !== 0) {
                                res1 = JSON.parse(list);
                                referenceArray = res1;
                                res2 = JSON.parse(documentList);
                                documentTypeArray = res2;
                            }
                            documentTypeArray.push(documentData);
                            for (var i = 0; i < quantity; i++) {
                                var referenceData = new Object();
                                var inputId = "#" + refernceId + "-" + i + "-INPUT";
                                var inputValue = $(inputId).val();
                                referenceData.reference = inputValue;
                                referenceData.documentTypeId = documentTypeId;
                                referenceArray.push(referenceData);
                            }
                            index++;
                            $("#documentTypeList").val(JSON.stringify(documentTypeArray));
                            $("#documentVarifictionList").val(JSON.stringify(referenceArray));
                            var text1 = "#documentType option[value='";
                            var text2 = "']";
                            $(text1.concat(documentTypeId, text2)).remove();
                            $("#documentQuentity").val('');
                            $('#mydocumentmodel').modal('toggle');
                            $("#endtime").val('');
                            $("#startTime").val('');
                            $("#startTimeStr").val('');
                            $("#endtimeStr").val('');

                        } else {
                            $("#referenceValidationMessage").empty();
                            $("#referenceValidationMessage").append("<p>Reference field value can not be type  special characters</p>");
                        }
                    } else {
                        $("#referenceValidationMessage").empty();
                        $("#referenceValidationMessage").append("<p>Reference field value can not be duplicate</p>");
                    }
                } else {
                    $("#referenceValidationMessage").empty();
                    $("#referenceValidationMessage").append("<p>Please insert the reference number with out any special characters</p>");
                }
            });
            $("#checkAvailability").click(function (event) {
                event.preventDefault();
                var totTime = $("#timeTotal").text();
                var length = $("#documentTable").children('tr').length;
                if (parseInt(length) !== 0) {
                    $.ajax({
                        async: true,
                        type: "POST",
                        url: "${pageContext.servletContext.contextPath}/reservation/avilable/slot",
                        cache: false,
                        //                        data: {data: documentlist},
                        data: {data: totTime},
                        success: function (res) {
                            res1 = JSON.parse(res);
                            var i = 0;
                            var str = "slotBarId";
                            var slotBarId = "";
                            $("#slotBarList").empty();
                            $("#slotBarList").append('<div class="vcenter" style="margin-left: 5px;margin-right: 5px;">'
                                    + '<div style="height:' + res1.DayTimeDiffrence + '"><span style="font-size: 23px;color: red;margin-left: -10px;">' + res1.StartTime
                                    + '</span><p style="margin-top: ' + res1.MiddleTimeDiffrence + 'px;font-size: 23px;color: red;margin-left: -10px;">' + res1.MiddleTime + '</p></div><div><span style="font-size: 23px;color: red;margin-left: -10px;">' + res1.EndTime + '</span></div>');
                            var countSrviceDesk = 0;
                            $.each(res1.ServiceDeskList, function () {
                                countSrviceDesk++;
                            });
                            var width = 78 / countSrviceDesk;
                            var marginRight = 6 / countSrviceDesk;
                            $.each(res1.ServiceDeskList, function () {
                                slotBarId = str + "-" + i;
                                $("#slotBarList").append('<div class="vcenter" style="margin-right: ' + marginRight + '%;  width: ' + width + '%;">'
                                        + '<div style="height:' + res1.DayTimeDiffrence + ';border:1px solid #000" id="' + slotBarId + '">'
                                        + '</div>'
                                        + '</div>');
                                var j = 0;
                                $.each(res1.ServiceDeskList[i].slots, function () {
                                    var slot = "#" + slotBarId;
                                    var slotid = slotBarId + "-" + j;
                                    if (res1.ServiceDeskList[i].slots[j].availability === 0) {
                                        $(slot).append('<div id="' + slotid + '" style="height:' + res1.ServiceDeskList[i].slots[j].height + 'px; background-color: white; border-bottom: 1px;border-top: 1px;border-top-style: solid; border-bottom-style: solid;" data-toggle="tooltip" title="Start Time:' + res1.ServiceDeskList[i].slots[j].startTime + '  End Time:' + res1.ServiceDeskList[i].slots[j].endTime + '"></div>');
                                    } else if (res1.ServiceDeskList[i].slots[j].availability === 1) {
                                        $(slot).append('<div class="get-slot-id" id="' + slotid + '" style="height:' + res1.ServiceDeskList[i].slots[j].height + 'px; background-color: greenyellow;border-bottom: 1px;border-top: 1px;border-top-style: solid; border-bottom-style: solid;" data-toggle="tooltip" title="Start Time:' + res1.ServiceDeskList[i].slots[j].startTime + '  End Time:' + res1.ServiceDeskList[i].slots[j].endTime + '">'
                                                + '<input type="hidden" class="form-control" id="' + slotid + '-strat-time" value ="' + res1.ServiceDeskList[i].slots[j].startTime + '"/>'
                                                + '<input type="hidden" class="form-control" id="' + slotid + '-end-time" value ="' + res1.ServiceDeskList[i].slots[j].endTime + '"/>'
                                                + '<input type="hidden" class="form-control" id="' + slotid + '-dek-no" value ="' + res1.ServiceDeskList[i].deskNo + '"/>'
                                                + '</div>');
                                    } else if (res1.ServiceDeskList[i].slots[j].availability === 2) {
                                        $(slot).append('<div id="' + slotid + '" style="height:' + res1.ServiceDeskList[i].slots[j].height + 'px; background-color: red;border-bottom: 1px;border-top: 1px;border-top-style: solid; border-bottom-style: solid;" data-toggle="tooltip" title="Start Time:' + res1.ServiceDeskList[i].slots[j].startTime + '  End Time:' + res1.ServiceDeskList[i].slots[j].endTime + '"></div>');
                                    }

                                    j++;
                                });
                                i++;
                            });
                            $('#myModal').modal({backdrop: 'static', keyboard: false});
                        }
                    });
                } else {
                    $('#checkAvilabilityInvlidMessage').html('<b>Please add at least one valid document</b>');
                    $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});
                }

            });
            $("#confirmedSelection").click(function (event) {
                var endtime = $("#endTimeConfirmation").text();
                var startTime = $("#StartTimeConfirmation").text();
                $("#endtimeStr").val(endtime);
                $("#startTimeStr").val(startTime);
                $('#myModal').modal('toggle');
                $('#confirmTimeSlotSelection').modal('toggle');
            });
            $("#slotBarList").on("click", ".get-slot-id", function () {

                //                $('#myModal').modal('toggle');
                var id = $(this).attr("id");
                var stratTimeId = "#" + id + "-strat-time";
                var endTimeId = "#" + id + "-end-time";
                var dekNoId = "#" + id + "-dek-no";
                var startTime = $(stratTimeId).val();
                var endtime = $(endTimeId).val();
                var dekNo = $(dekNoId).val();
                $('#confirmTimeSlotSelection').modal({backdrop: 'static', keyboard: false});
                $("#conf-window-show-desc").empty();
                $('#endtime').prop('readonly', true);
                $("#bookeDsdId").val(dekNo);
                $('#startTime').prop('readonly', true);
                $("#conf-window-show-desc").append('<div class="row"><div class="col-xs-3"></div><div class="col-xs-3"></div></div><div class="row"><div class="col-xs-3"></div><div class="col-xs-3">Start Time:</div><div class="col-xs-3" id="StartTimeConfirmation">' + startTime + '</div><div class="col-xs-3"></div></div><div class="row"><div class="col-xs-3"></div><div class="col-xs-3">End Time:</div><div class="col-xs-3" id="endTimeConfirmation">' + endtime + '</div><div class="col-xs-3"></div></div>');
                //                $('#myModal').modal('toggle');
                //                $('#confirmTimeSlotSelection').modal({backdrop: 'static', keyboard: false});
            });
            $(document).ready(function () {
                $('#menu [data-accordion]').accordion();
                var height = $(".content-area").height();
                $(".menu-container").height(Math.max(height, 600));
            });
            $("#identificatioDocumentType").change(function () {
                $("#documentImgae").empty();
                if ($(this).val() !== 'NIC') {
                    $("#documentImgae").append('<div class="input-group form-group" style="margin-left: 5px">'
                            + '<input type="file" id="file" name="files[0]" class="form-control"  onchange="this.parentNode.nextSibling.value = this.value"/>'
                            + '<span class="input-group-btn">'
                            + '<button class="btn btn-secondary" type="button" id="customerSerach">Scan</button>'
                            + '</span>'
                            + '</div>');
                } else {
                    $("#documentImgae").append('<div class="input-group form-group" style="margin-left: 5px">'
                            + '<input type="file" id="file" name="files[0]" class="form-control"  onchange="this.parentNode.nextSibling.value = this.value"/>'
                            + '<span class="input-group-btn">'
                            + '<button class="btn btn-secondary" type="button" id="customerSerach">Scan</button>'
                            + '</span>'
                            + '</div>'
                            + '<div class="input-group form-group" style="margin-left: 5px">'
                            + '<input type="file" id="file" name="files[1]" class="form-control" onchange="this.parentNode.nextSibling.value = this.value">'
                            + '<span class="input-group-btn">'
                            + '<button class="btn btn-secondary" type="button" id="customerSerach">Scan</button>'
                            + '</span>'
                            + '</div>');
                }
            });
            $('#reservationForm').validate({
                rules: {
                    identificatioDocumentType: {
                        required: true
                    },
                    identificationNumber: {
                        required: true
                    },
                    applicationType: {
                        required: true
                    },
                    fristName: {
                        required: true
                    },
                    lastName: {
                        required: true
                    },
                    telephoneMobile: {
                        digits: true,
                        required: true,
                        maxlength: 16,
                        minlength: 10
                    },
                    startTimeStr: {
                        required: true
                    },
                    endtimeStr: {
                        required: true
                    },
                    email: {
                        email: true
                    },
                    preferredLanguage: {
                        required: true
                    },
                    bookingId: {
                        required: true,
                        digits: true,
                        number: true
                    }

                }, errorPlacement: function (error, element) {
                    element.parent().find('div.invalid').remove();
                    element.parent().append('<div class="invalid">' + error.text() + '</div>');
                },
                onfocusout: false,
                invalidHandler: function (form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                }
            });
            $('#documentTypeform').validate({
                rules: {
                    documentType: {
                        required: true
                    },
                    documentQuentity: {
                        required: true,
                        digits: true
                    }
                }, errorPlacement: function (error, element) {
                    error.insertAfter(element.parent());
                }
            });
            function isUnique(values) {
                values.sort();
                // Check whether there are two equal values next to each other
                for (var k = 1; k < values.length; ++k) {
                    if (values[k].trim() === values[k - 1].trim()) {
                        return false;
                        break;
                    }
                }
                return true;
            }
            function isEmpty(values) {
//            values.sort();
                // Check whether there are two equal values next to each other
                for (var k = 0; k < values.length; ++k) {
                    if (!values[k].trim()) {
                        return false;
                        break;
                    } else {
                        return true;
                    }
                }

            }

            function specialCharacter(values) {
                values.sort();
                // Check whether there are two equal values next to each other
                for (var k = 0; k < values.length; ++k) {
                    var string = values[k];
                    for (var l = 0; l < string.length; ++l) {
                        var c = string.charAt(l);
                        if ((c === '"') || (c === ';') || (c === '`') || (c === '?') || (c === '^') || (c === '~') || (c === '$') || (c === '#')
                                || (c === '@') || (c === '^') || (c === '>') || (c === '<') || (c === '%') || (c === '&') || (c === '{') || (c === '}')) {
                            return false;
                        }
                    }
                }
                return true;
            }
            $("#documentTable").on("click", ".documentActionView", function () {
                var id = $(this).attr("id");
                var list = $("#documentVarifictionList").val();
                var databaseList = $("#documentDatabaseList").val();
                res1 = JSON.parse(list);
                res2 = JSON.parse(databaseList);
                $("#referenceTableView").empty();
                index = 0;
                $.each(res1, function () {
                    if (res1[index].documentTypeId === id) {
                        $("#referenceTableView").append('<tr>'
                                + '<td class=col-xs-1 pull-left style=border-top-width:0px >Reference:</td>'
                                + '<td class=col-xs-2 style=border-top-width:0px>'
                                + '<input class=pull-left type=text value="' + res1[index].reference + '" readonly/>'
                                + '</td>'
                                + '</tr>');
                    }
                    index++;
                });
                $('#mydocumentmodelview').modal({backdrop: 'static', keyboard: false});
            });
            $("#documentTable").on("click", ".documentActionEdit", function () {
                var id = $(this).attr("id");
                var list = $("#documentVarifictionList").val();
                var databaseList = $("#documentDatabaseList").val();
                res1 = JSON.parse(list);
                res2 = JSON.parse(databaseList);
                $("#referenceTableEdit").empty();
                index = 0;
                i = 0;
                $.each(res1, function () {
                    if (res1[index].documentTypeId === id) {
                        var referenceId = 'REFERENCEEDIT-REFERENCE-' + i + "-" + res1[index].documentTypeId;
                        $("#referenceTableEdit").append('<tr>'
                                + '<td class=col-xs-1 pull-left style=border-top-width:0px >Reference:</td>'
                                + '<td class=col-xs-2 style=border-top-width:0px>'
                                + '<input maxlength="14" class=pull-left type=text value="' + res1[index].reference + '" id="' + referenceId + '"/>'
                                + '</td>'
                                + '</tr>');
                        i++;
                    }
                    index++;
                });
                $("#referenceEditId").val(id);
                $('#mydocumentmodeledit').modal({backdrop: 'static', keyboard: false});
            });
            $("#editReferenceDocument").click(function (event) {
                $("#startTimeStr").val(id);
                $("#endtimeStr").val(id);
                event.preventDefault();
                var id = $("#referenceEditId").val();
                var list = $("#documentVarifictionList").val();
                var documentList = $("#documentTypeList").val();
                var quantity = "";
                res1 = JSON.parse(list);
                res2 = JSON.parse(documentList);
                for (var j = 0; j < res2.length; j++) {
                    if (res2[j].documentTypeId === id) {
                        quantity = res2[j].quantity;
                    }
                }

                var values = [];
                for (var i = 0; i < quantity; i++) {
                    var referenceId = 'REFERENCEEDIT-REFERENCE-' + i + "-" + id;
                    var reference = $('#' + referenceId).val();
                    values.push(reference);
                }
                console.log(values);
                if (isEmpty(values)) {
                    if (isUnique(values)) {
                        if (specialCharacter(values)) {
                            var count = 0;
                            for (var i = 0; i < res1.length; i++) {
                                if (res1[i].documentTypeId === id) {
                                    count++;
                                }
                            }

                            for (var i = 0; i < res1.length; i++) {
                                if (res1[i].documentTypeId === id) {
                                    res1.splice(i, count);
                                }
                            }
                            var referenceArray = [];
                            referenceArray = res1;
                            for (var k = 0; k < quantity; k++) {
                                var referenceId = 'REFERENCEEDIT-REFERENCE-' + k + "-" + id;
                                var reference = $('#' + referenceId).val();
                                var referenceData = new Object();
                                referenceData.reference = reference;
                                referenceData.documentTypeId = id;
                                referenceArray.push(referenceData);
                            }


                            $("#documentVarifictionList").val(JSON.stringify(referenceArray));
                            $('#mydocumentmodeledit').modal('toggle');
                        } else {
                            $("#referenceEditValidationMessage").empty();
                            $("#referenceEditValidationMessage").append("<p>Reference field value can not be type  special characters</p>");
                        }
                    } else {
                        $("#referenceEditValidationMessage").empty();
                        $("#referenceEditValidationMessage").append("<p>Reference field value can not be duplicate</p>");
                    }
                } else {
                    $("#referenceEditValidationMessage").empty();
                    $("#referenceEditValidationMessage").append("<p>Please insert the reference number with out any special characters</p>");
                }
            });
            $("#documentTable").on("click", ".documentActionDelete", function () {
                var id = $(this).attr("id");
                $("#referenceDeleteId").val(id);
                $('#mydocumentmodeldelete').modal({backdrop: 'static', keyboard: false});
            });
            $("#deleteReferenceDocument").click(function (event) {
                event.preventDefault();
                var id = $("#referenceDeleteId").val();
                var tableColoumnId = "#TABLECOLOUMN-";
                tableColoumnId = tableColoumnId + id;
                $(tableColoumnId).remove();
                var list = $("#documentVarifictionList").val();
                var documentList = $("#documentTypeList").val();
                var databaseDocumnetTypeList = $("#documentDatabaseList").val();
                res1 = JSON.parse(list);
                res2 = JSON.parse(documentList);
                res3 = JSON.parse(databaseDocumnetTypeList);
                var count = 0;
                for (var j = 0; j < res1.length; j++) {
                    if (res1[j].documentTypeId === id) {
                        count++;
                    }
                }
                for (var j = 0; j < res1.length; j++) {

                    if (res1[j].documentTypeId === id) {
                        res1.splice(j, count);
                    }
                }

                for (var i = 0; i < res2.length; i++) {
                    if (res2[i].documentTypeId === id) {
                        res2.splice(i, 1);
                    }
                }

                $("#documentVarifictionList").val(JSON.stringify(res1));
                $("#documentTypeList").val(JSON.stringify(res2));
                for (var i = 0; i < res3.TYPE.length; i++) {
                    if (res3.TYPE[i].documentTypeId === id) {
                        $("#documentType").append('<option value="' + res3.TYPE[i].documentTypeId + '">' + res3.TYPE[i].description + '</option>');
                    }
                }

                $("#endtime").val('');
                $("#startTime").val('');
                $("#startTimeStr").val('');
                $("#endtimeStr").val('');
                $('#mydocumentmodeldelete').modal('toggle');
            });
            function Totaltime() {
                var documentVarificationList = $("#documentVarifictionList").val();
                var databaseDocumnetTypeList = $("#documentDatabaseList").val();
                res1 = JSON.parse(documentVarificationList);
                res2 = JSON.parse(databaseDocumnetTypeList);
                var time = 0;
                for (var j = 0; j < res2.TYPE.length; j++) {
                    for (var i = 0; i < res1.length; i++) {
                        if (res2.TYPE[j].documentTypeId === res1[i].documentTypeId) {
                            var varifitime = res2.TYPE[j].varificationTime;
                            time = time + parseInt(varifitime);
                        }
                    }
                }
                return time;
            }

            $('#ticketSerach').on('click', function (event) {
                event.preventDefault();
                var serachValue = $("#ticketSerachValue").val();
                var dataObject = new Object();
                dataObject.serachValue = serachValue;
                var content = JSON.stringify(dataObject);
                $.ajax({
                    async: true,
                    type: "POST",
                    url: "${pageContext.servletContext.contextPath}/reservation/search",
                    cache: false,
                    data: {data: content},
                    success: function (res) {
                        res1 = JSON.parse(res);
                        console.log(res1.object[0].customer.addressLine1);
                        $('#startTime').val("" + res1.object[0].customer.addressLine1);
                    }
                });
            });
            function clearLS() {
                var data = {
                    'name': "",
                    'identificationNo': "",
                    'applicationType': "",
                    'email': "",
                    'telephoneMobile': "",
                    'preferredLanguage': "",
                    'bookingdate': "",
                    'addressLine1': "",
                    'addressLine2': "",
                    'addressLine3': "",
                    'startTimeStr': "",
                    'endtimeStr': ""

                };
                localStorage.setItem("customer", JSON.stringify(data));
            }
            $(window).on('beforeunload', function (e) {
                clearLS();
            });
            $("#createOk").click(function () {
                $('#createOk').attr('disabled', 'disabled');
                $('#reservationcreateconfirmation').modal('toggle');
                $(".loader").fadeIn();
                clearLS();
                $("#reservationForm").submit();
            });
            $("#sendToHelpDeskOk").click(function () {

                $('#sendToHelpDeskOk').attr('disabled', 'disabled');
                $('#sendToHelpDeskReason').modal('toggle');
                $(".loader").fadeIn();
                var reason = $("#helpdreason").val();
                $("#helpdeskreason").val(reason);
                clearLS();
                $("#reservationForm").submit();
            });
            $("#createCancel").click(function () {
                clearLS();
            });
            $("#createbutton").click(function (event) {
                var length = $("#documentTable").children('tr').length;
                $('#reschduleType').val("VIP");
                if (parseInt(length) !== 0) {
                    $("#documentType").rules("remove");
                    $("#documentQuentity").rules("remove");
                    if ($("#reservationForm").valid()) {

                        var serachValue = $("#bookingId").val();
                        var dataObject = new Object();
                        dataObject.serachValue = serachValue;
                        var content = JSON.stringify(dataObject);
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "${pageContext.servletContext.contextPath}/reservation/exit/customer/vip",
                            cache: false,
                            data: {data: content},
                            success: function (res) {
                                res1 = JSON.parse(res);
                                if (res1.CUSTOMER.length > 0) {
                                    var data = {
                                        'name': $('#fristName').val(),
                                        'identificationNo': $('#identificationNumber').val(),
                                        'applicationType': $('#applicationType').val(),
                                        'email': $('#email').val(),
                                        'telephoneMobile': $('#telephoneMobile').val(),
                                        'preferredLanguage': $('#preferredLanguage').val(),
                                        'bookingdate': $('#bookingdate').val(),
                                        'addressLine1': $('#addressLine1').val(),
                                        'addressLine2': $('#addressLine2').val(),
                                        'addressLine3': $('#addressLine3').val(),
                                        'startTimeStr': $('#startTimeStr').val(),
                                        'endtimeStr': $('#endtimeStr').val()

                                    };
                                    var data_status = {
                                        'status': 'USERDETAILS'

                                    };
                                    localStorage.setItem("STATUS", JSON.stringify(data_status));
                                    localStorage.setItem("customer", JSON.stringify(data));
                                    $('#reservationcreateconfirmation').modal({backdrop: 'static', keyboard: false});
                                } else {
                                    $('#checkAvilabilityInvlidMessage').html('<b>Not a valid Priority service ID or already processed one</b>');
                                    $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});
                                }

                            }
                        });
                    }
                } else {
                    $('#checkAvilabilityInvlidMessage').html('<b>Please add at least one valid document</b>');
                    $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});
                }



            });
            $("#helpdeskbutton").click(function (event) {
                var length = $("#documentTable").children('tr').length;
                $('#reschduleType').val("VIPSEHD");
                if (parseInt(length) !== 0) {
                    $("#documentType").rules("remove");
                    $("#documentQuentity").rules("remove");
                    if ($("#reservationForm").valid()) {

                        var serachValue = $("#bookingId").val();
                        var dataObject = new Object();
                        dataObject.serachValue = serachValue;
                        var content = JSON.stringify(dataObject);
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "${pageContext.servletContext.contextPath}/reservation/exit/customer/vip",
                            cache: false,
                            data: {data: content},
                            success: function (res) {
                                res1 = JSON.parse(res);
                                if (res1.CUSTOMER.length > 0) {
                                    var data = {
                                        'name': $('#fristName').val(),
                                        'identificationNo': $('#identificationNumber').val(),
                                        'applicationType': $('#applicationType').val(),
                                        'email': $('#email').val(),
                                        'telephoneMobile': $('#telephoneMobile').val(),
                                        'preferredLanguage': $('#preferredLanguage').val(),
                                        'bookingdate': $('#bookingdate').val(),
                                        'addressLine1': $('#addressLine1').val(),
                                        'addressLine2': $('#addressLine2').val(),
                                        'addressLine3': $('#addressLine3').val(),
                                        'startTimeStr': $('#startTimeStr').val(),
                                        'endtimeStr': $('#endtimeStr').val()

                                    };
                                    localStorage.setItem("customer", JSON.stringify(data));
                                    $('#sendToHelpDeskReason').modal({backdrop: 'static', keyboard: false});
                                } else {
                                    $('#checkAvilabilityInvlidMessage').html('<b>Not a valid Priority service ID or already processed one</b>');
                                    $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});
                                }

                            }
                        });
                    }
                } else {
                    $('#checkAvilabilityInvlidMessage').html('<b>Please add at least one valid document</b>');
                    $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});
                }



            });
            $("#slotConfirmationCancel").click(function () {
                $("#startTimeStr").val('');
                $("#endtimeStr").val('');
                $('#confirmTimeSlotSelection').modal('toggle');
            });
            $('#bookingdate').datepicker({
                format: "yyyy-mm-dd",
                autoclose: true,
                startDate: new Date(),
                minuteStep: 10
            });
//            $('#fristName').keyup(function () {
//                $('#fristName').val($('#fristName').val().toUpperCase());
//            });

            $("#identificationNumber").on('input', function () {

                var start = this.selectionStart,
                        end = this.selectionEnd;
                this.value = this.value.toUpperCase();
                this.setSelectionRange(start, end);
            });
            $("#fristName").on('input', function () {

                var start = this.selectionStart,
                        end = this.selectionEnd;
                this.value = this.value.toUpperCase();
                this.setSelectionRange(start, end);
            });
        </script>
        <script>
            $(".sidebar-menu li").removeClass("selected");
            $(".sidebar-menu  .priority").addClass("selected");
            $(window).load(function () {
                $(".loader").fadeOut("slow");
            });
            $(document).bind("ajaxSend", function () {
                $(".loader").fadeIn();
            }).bind("ajaxComplete", function () {
                $(".loader").fadeOut("slow");
            });
        </script>
    </body>
</html>

