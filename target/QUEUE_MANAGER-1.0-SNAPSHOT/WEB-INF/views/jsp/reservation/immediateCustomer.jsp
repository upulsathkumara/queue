<%-- 
    Document   : reservationCreate
    Created on : Oct 12, 2016, 12:28:28 PM
    Author     : Rasika
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 


<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>
        <script>
            var role = "<%=session.getAttribute("role")%>";
            if (role !== 'USER') {
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }
            var username = "<%=session.getAttribute("loggedUser")%>"
            if (!<%=session.getAttribute("loggedUser")%>) {
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            } else {
            }

        </script>
        <style>
            .datepicker table tr td:first-child + td + td + td + td + td +td {
                color: red
            }

            .datepicker table tr td:first-child {
                color: red
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="loader"></div>
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/user_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/user_menu.jsp"/>
            </aside>
            <div class="content-wrapper">
                <section class="content" style="margin-right: -50%;">
                    <span style="font-size: 16px;"><b>Appointment Reschedule</b></span><br/><br/>
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <div class="box-body form-horizontal">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">

                                        <div class="form-group">
                                            <label class="control-label labelfont col-sm-3" for="NIC" style="padding-right: 22px;">Service Id:</label>
                                            <div class="col-sm-4">
                                                <input type="text" class="form-control" id="onlineserviceId" placeholder="Enter Service Id" maxlength="32"/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                    <form:form  method="POST"  action="${pageContext.request.contextPath}/reservation/online/updated"  id="reservationForm" commandName="reservationForm" enctype="multipart/form-data">
                        <div>
                            <div class="col-xs-12">
                                <c:if test="${not empty errorMsg}">
                                    <div class="alert alert-warning">
                                        <strong>Warning!</strong> ${errorMsg}
                                    </div>
                                    <br/>
                                </c:if> 
                            </div>
                        </div>
                        <div class="row">
                            <u class="main-title" style="margin-left: 15px;">Application Detail</u>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class="box-body form-horizontal">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">

                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="NIC">Application Type:<samp style="color: red">*</samp></label>
                                                    <div class="col-sm-4">
                                                        <form:select class="form-control" id="applicationType" path="applicationType">
                                                            <c:forEach var="stae" items="${applicationType}">
                                                                <option value="${stae.key}">${stae.value}</option>
                                                            </c:forEach>
                                                        </form:select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>

                            <u class="main-title" style="margin-left: 15px;">Personal Details</u>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class="box-body form-horizontal">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="bookingId"></label>
                                                    <div class="col-sm-4">
                                                        <form:input type="hidden" class="form-control" value="NO" id="bookingId" placeholder="Enter first name" path="bookingId"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="identificationNumber">Identification Type:<samp style="color: red">*</samp></label>
                                                    <div class="col-sm-4" >
                                                        <form:select value="NIC" path="identificatioDocumentType" class="form-control"  id="identificatioDocumentType">
                                                            <option value="NIC" >NIC</option>
                                                            <option value="PAS">Passport No</option>
                                                            <option value="DLN">DL Number</option>
                                                        </form:select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="identificationNumber">Identification Number:<samp style="color: red">*</samp></label>
                                                    <div class="col-sm-4" >
                                                        <form:input type="text" class="form-control" id="identificationNumber" placeholder="Enter NIC number" path="identificationNumber" maxlength="12"/>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="fristName">Name In Full:<samp style="color: red">*</samp></label>
                                                    <div class="col-sm-4">
                                                        <form:input type="text" class="form-control" id="fristName" placeholder="Enter full name" path="fristName" maxlength="32"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="email">Email:</label>
                                                    <div class="col-sm-4">
                                                        <form:input type="text" class="form-control" id="email" placeholder="Enter email" path="email" maxlength="32"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="telephoneMobile">Telephone Mobile:<samp style="color: red">*</samp></label>
                                                    <div class="col-sm-4">
                                                        <form:input type="text" class="form-control" id="telephoneMobile" placeholder="Enter telephone mobile" path="telephoneMobile" maxlength="16"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="preferredLanguage">Preferred Language:<samp style="color: red">*</samp></label>
                                                    <div class="col-sm-4">
                                                        <form:select path="preferredLanguage" class="form-control" id="preferredLanguage">
                                                            <option value="ENGLISH">English</option>
                                                            <option value="SINHALA" >Sinhala</option>
                                                            <option value="TAMIL">Tamil</option>
                                                        </form:select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="bookingdate">Requesting Appointment Date:<samp style="color: red">*</samp></label>

                                                    <div class="input-group date col-sm-4" style="padding-left: 15px;padding-right:  15px;float: left;" >
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"  style=" cursor:auto"></i>
                                                        </div>
                                                        <form:input type="text"  path="bookingdate" id="bookingdate" value="${dateNow}" class="form-control pull-right" style="height: 36px;" readonly="true"/>
                                                    </div>
                                                    <!-- /.input group -->
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="reason">Reason:<samp style="color: red">*</samp></label>
                                                    <div class="col-sm-4">
                                                        <form:input type="text" class="form-control" id="reason" placeholder="Enter reschedule reason" path="reason" maxlength="512"/>
                                                    </div>
                                                </div>

                                                <div class="col-xs-7" >
                                                    <div class="box box-default collapsed-box">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">Address Information</h3>

                                                            <div class="box-tools pull-right">
                                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                            <!-- /.box-tools -->
                                                        </div>
                                                        <!-- /.box-header -->
                                                        <div class="box-body">
                                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label class="control-label labelfont col-sm-5" for="addressLine1">Address Line1:</label>
                                                                    <div class="col-sm-7">
                                                                        <form:input type="text" class="form-control" id="addressLine1" placeholder="Enter address line1" path="addressLine1" maxlength="64"/>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label labelfont col-sm-5" for="addressLine2">Address Line2:</label>
                                                                    <div class="col-sm-7">
                                                                        <form:input type="text" class="form-control" id="addressLine2" placeholder="Enter address line2" path="addressLine2" maxlength="64"/>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="control-label labelfont col-sm-5" for="addressLine3">Address Line3:</label>
                                                                    <div class="col-sm-7">
                                                                        <form:input type="text" class="form-control" id="addressLine3" placeholder="Enter address line3" path="addressLine3" maxlength="64"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.box-body -->
                                                    </div>
                                                    <!-- /.box -->
                                                </div>


                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="documentTypeList"></label>
                                                    <div class="col-sm-4">
                                                        <form:input type="hidden" class="form-control" id="documentTypeList" path="documentTypeList"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="documentVarifictionList"></label>
                                                    <div class="col-sm-4">
                                                        <form:input type="hidden" class="form-control" id="documentVarifictionList" path="documentVarifictionList"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="documentDatabaseList"></label>
                                                    <div class="col-sm-4">
                                                        <input type="hidden" class="form-control" id="documentDatabaseList" value="<c:out value="${Type}"/>"/>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class="box-body form-horizontal">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                <div class="col-md-7 col-lg-7 col-sm-7 col-xs-7">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Document Type</th>
                                                                <th>Quantity</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="documentTable">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="form-group"> </div>
                                                </div>
                                            </div> </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                    <div class="box-body form-horizontal">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="bookeDsdId"></label>
                                                    <div class="col-sm-4">
                                                        <form:input type="hidden" class="form-control"  id="bookeDsdId"  path="bookeDsdId"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="startTime">Start Time:</label>
                                                    <div class="col-sm-4">
                                                        <form:input type="text" class="form-control"  id="startTimeStr"  path="startTimeStr" readonly="true"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label labelfont col-sm-3" for="endtime">End Time:</label>
                                                    <div class="col-sm-4">
                                                        <form:input type="text" class="form-control" id="endtimeStr" path="endtimeStr" readonly="true"/>
                                                    </div>
                                                    <form:input type="hidden" class="form-control" id="appoinmentType" path="appoinmentType" readonly="true"/>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-7">
                                                        <form:button id="immediatebutton" type="button" class="btn btn-default pull-right">
                                                            Immediate Appointment
                                                        </form:button>
                                                        <form:button id="createbutton" type="button" class="btn btn-default btn-block pull-right">
                                                            Create
                                                        </form:button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </form:form>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
        <div id="myModal" class="modal fade" role="dialog" style="">
            <div class="modal-dialog modal-lg" style="overflow-y: scroll; max-height:85%;  margin-top: 50px; margin-bottom:50px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" style="text-align: center">Time Slot Selection</h4>
                        <br/>
                        <div class="raw">
                            <div class="col-xs-3"></div>
                            <div class="col-xs-2"><div style="width: 15px;height: 15px;background: #0AC21F;-moz-border-radius: 50px;-webkit-border-radius: 50px;border-radius: 50px;margin-right: 5px;float: left;"></div>Available</div>
                            <div class="col-xs-2"><div style="width: 15px;height: 15px;background: #e20b1d;-moz-border-radius: 50px;-webkit-border-radius: 50px;border-radius: 50px;margin-right: 5px;float: left;"></div>Allocated</div>
                            <div class="col-xs-2"><div style="width: 15px;height: 15px;background: #eae4e4;-moz-border-radius: 50px;-webkit-border-radius: 50px;border-radius: 50px;margin-right: 5px;float: left;"></div>Insufficient</div>
                            <div class="col-xs-3"></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center" id="slotBarList">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="confirmTimeSlotSelection" class="modal fade"  role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                        <h4 class="modal-title modal-title-center" style="text-align: center;">Please confirm time-slot selection</h4>
                    </div>
                    <div class="modal-body">
                        <div id="conf-window-show-desc"></div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="confirmedSelection">OK</button>
                        <button type="button" class="btn btn-default" id="slotConfirmationCancel">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
        <!--documents model-->
        <div id="mydocumentmodel" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Document Reference</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center">
                                <div class="invalid" id="referenceValidationMessage">

                                </div>
                                <table class="table">
                                    <tbody id="referenceTable" class="referenceTable"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="addDocument" type="button" class="btn btn-secondary">Save</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="mydocumentmodelview" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Document Reference View</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center">
                                <table class="table">
                                    <tbody id="referenceTableView" class="referenceTableview"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>              
                </div>

            </div>
        </div>

        <div id="mydocumentmodeledit" class="modal fade" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Document Reference Edit</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center">
                                <div class="invalid" id="referenceEditValidationMessage"></div>
                                <table class="table">
                                    <tbody id="referenceTableEdit" class="referenceTableEdit"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="editReferenceDocument" type="button" class="btn btn-secondary">Edit</button>
                        <input type="hidden" id="referenceEditId" />
                    </div>
                </div>

            </div>
        </div>
        <!--        <div id="mydocumentmodeldelete" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Delete Documents</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: center">
                                        <p class="refereceDelete">Are you want to delete</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="deleteReferenceDocument" type="button" class="btn btn-secondary">Delete</button>
                                <input type="hidden" id="referenceDeleteId" />
                            </div>
                        </div>
        
                    </div>
                </div>-->
        <div class="modal" id="mydocumentmodeldelete"> 
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #367fa9">×</span></button>
                        <h4 class="modal-title" style="color: #367fa9"><span class="glyphicon glyphicon-exclamation-sign"></span> Confirmation for Delete!</h4>
                    </div>
                    <div class="modal-body" style="color: #367fa9">
                        <p><b>Are you sure you want to delete this record?</b></p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="deleteReferenceDocument" class="btn btn-primary">Delete</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>  
                        <input type="hidden" id="referenceDeleteId" />
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" id="checkAvilabilityInvlid"> 
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #367fa9">×</span></button>
                        <h4 class="modal-title" style="color: #367fa9"><span class="glyphicon glyphicon-exclamation-sign"></span> Warning!</h4>
                    </div>
                    <div class="modal-body" style="color: #ef0746">
                        <p id="checkAvilabilityInvlidMessage"><b>There should be at least one valid document</b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="reservationcreateconfirmation"> 
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color: #367fa9">×</span></button>
                        <h4 class="modal-title" style="color: #367fa9"><span class="glyphicon glyphicon-exclamation-sign"></span> Confirmation for Create!</h4>
                    </div>
                    <div class="modal-body" style="color: #367fa9">
                        <p><b>Are you sure you want to proceed this reservation?</b></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="createOk" class="btn btn-primary">Ok</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>  
                        <input type="hidden" id="referenceDeleteId" />
                    </div>
                </div>
            </div>
        </div>
        <script>
            $('#documentVarifictionList').val('');
            $('#endtime').prop('readonly', true);
            $('#startTime').prop('readonly', true);

            $('#startTimeStr').prop('readonly', true);
            $('#endtimeStr').prop('readonly', true);


            var documentjsonList = "";
            var referenceJsonList = "";
            var index = 0;
            $("#customerSerach").click(function (event) {
                event.preventDefault();

            });




            $("#addDocumentType").click(function (event) {
                $("#documentQuentity").rules('add', {
                    required: true,
                    number: true
                });
                $("#documentType").rules('add', {
                    required: true
                });
                if ($('#documentType').valid() & $('#documentQuentity').valid()) {
                    var quantity = $("#documentQuentity").val();
                    var chargeList = $("#documentDatabaseList").val();
                    var documentTypeId = $("#documentType").val();
                    var refernceId = "REFERENCE-" + documentTypeId;
                    $("#referenceTable").html("");
                    $("#referenceValidationMessage").html("");
                    res3 = JSON.parse(chargeList);
                    //                    $("#chargeType").html("");


                    for (var i = 0; i < quantity; i++) {
                        var inputId = refernceId + "-" + i + "-INPUT";
                        var selectId = refernceId + "-" + i + "-SELECT";
                        var selectChargeCode = refernceId + "-" + i + "-SELECTCHRG";
                        $("#referenceTable").append('<tr>'
                                + '<td class=col-xs-1 pull-left style=border-top-width:0px >Reference:</td>'
                                + '<td class=col-xs-2 style=border-top-width:0px>'
                                + '<input class=pull-left type=text id="' + inputId + '" maxlength="14" />'
                                + '</td>'
                                + '<td class=col-xs-3 style=border-top-width:0px>'
                                + '<select id="' + selectId + '">'
                                + '<option value="ACCP">Accept</option>'
                                + '<option value="REJT">Reject</option>'
                                + '</select>'
                                + '</td>'
                                + '<td class=col-xs-6 style=border-top-width:0px; >'
                                + '<select id="' + selectChargeCode + '" style="width:160px;">'
                                + '</select>'
                                + '</td>'
                                + '</tr>');
                        //add charge type dropdown
                        for (var j = 0; j < res3.charge.length; j++) {
                            $('#' + selectChargeCode + '').append('<option value="' + res3.charge[j].documentChargeCode + '">' + res3.charge[j].description + '</option>');
                        }
                    }


                    $('#mydocumentmodel').modal({backdrop: 'static', keyboard: false});
                }
            });
            $("#addDocument").click(function (event) {
                event.preventDefault();
                var tableColoumnId = "TABLECOLOUMN-";
                var documentTypeId = $("#documentType").val();
                var refernceId = "REFERENCE-" + documentTypeId;
                var quantity = $("#documentQuentity").val();
                var list = $("#documentVarifictionList").val();
                var documentList = $("#documentTypeList").val();
                var referenceArray = [];
                var documentTypeArray = [];
                tableColoumnId = tableColoumnId + documentTypeId;
                var values = [];
                for (var i = 0; i < quantity; i++) {
                    var inputId = "#" + refernceId + "-" + i + "-INPUT";
                    var value = $(inputId).val().trim();
                    values.push(value.toString());
                }
                if (isEmpty(values)) {
                    if (isUnique(values)) {
                        if (specialCharacter(values)) {
                            $("#documentType").rules("remove");
                            $("#documentQuentity").rules("remove");
                            var documentData = new Object();
                            var documentTypeText = $("#documentType option:selected").text();
                            $("#documentTable").html('<tr id="' + tableColoumnId + '"><td>' + documentTypeText + '</td><td>' + quantity + '</td><td><a class="documentActionView" id="' + documentTypeId + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="documentActionEdit" id="' + documentTypeId + '"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="documentActionDelete" id="' + documentTypeId + '"><i class="fa fa-trash"></i></a></td></tr>');
                            documentData.documentTypeId = documentTypeId;
                            documentData.quantity = quantity;
                            if (index !== 0) {
                                res1 = JSON.parse(list);
                                referenceArray = res1;
                                res2 = JSON.parse(documentList);
                                documentTypeArray = res2;
                            }
                            documentTypeArray.push(documentData);
                            for (var i = 0; i < quantity; i++) {
                                var referenceData = new Object();
                                var inputId = "#" + refernceId + "-" + i + "-INPUT";
                                var selectId = "#" + refernceId + "-" + i + "-SELECT";
                                var selectChargeCode = "#" + refernceId + "-" + i + "-SELECTCHRG";
                                var inputValue = $(inputId).val().trim();
                                var selectValue = $(selectId).val();
                                var charg = $(selectChargeCode).val();
                                referenceData.reference = inputValue;
                                referenceData.status = selectValue;
                                referenceData.documentTypeId = documentTypeId;
                                referenceData.charge = charg;
                                referenceArray.push(referenceData);
                            }
                            index++;
                            $("#documentTypeList").val(JSON.stringify(documentTypeArray));
                            $("#documentVarifictionList").val(JSON.stringify(referenceArray));
                            var text1 = "#documentType option[value='";
                            var text2 = "']";
                            $(text1.concat(documentTypeId, text2)).remove();
                            $("#documentQuentity").val('');
                            $('#mydocumentmodel').modal('toggle');
                            $("#timeTotal").empty();
                            $("#timeTotal").append(Totaltime());
                            $("#endtime").val('');
                            $("#startTime").val('');
                            $("#startTimeStr").val('');
                            $("#endtimeStr").val('');
                        } else {
                            $("#referenceValidationMessage").empty();
                            $("#referenceValidationMessage").append("<p>Reference field value can not be type  special characters</p>");
                        }
                    } else {
                        $("#referenceValidationMessage").empty();
                        $("#referenceValidationMessage").append("<p>Reference field value can not be duplicate</p>");
                    }
                } else {
                    $("#referenceValidationMessage").empty();
                    $("#referenceValidationMessage").append("<p>Reference field value can not be empty</p>");
                }
            });
            $("#checkAvailability").click(function (event) {
                event.preventDefault();
                var totTime = $("#timeTotal").text();
                if (totTime == 0) {
                    $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});
                } else {
                    var length = $("#documentTable").children('tr').length;
                    if (parseInt(length) !== 0) {
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "${pageContext.servletContext.contextPath}/reservation/avilable/slot",
                            cache: false,
                            //                        data: {data: documentlist},
                            data: {data: totTime},
                            success: function (res) {
                                res1 = JSON.parse(res);
                                var i = 0;
                                var str = "slotBarId";
                                var slotBarId = "";
                                $("#slotBarList").empty();

                                if (res1.isFullHoliday == 1) {
                                    $("#slotBarList").append('<div class="vcenter" style="margin-left: 5px;margin-right: 5px;"><p>Today is a full holiday. You cannot create reservations</p></div>');

                                } else {
                                    $("#slotBarList").append('<div class="vcenter" style="margin-left: 5px;margin-right: 5px;">'
                                            + '<div style="height:' + res1.DayTimeDiffrence + '"><span style="font-size: 23px;color: red;margin-left: -10px;">' + res1.StartTime
                                            + '</span><p style="margin-top: ' + res1.MiddleTimeDiffrence + 'px;font-size: 23px;color: red;margin-left: -10px;">' + res1.MiddleTime + '</p></div><div><span style="font-size: 23px;color: red;margin-left: -10px;">' + res1.EndTime + '</span></div>');

                                }


                                var countSrviceDesk = 0;
                                $.each(res1.ServiceDeskList, function () {
                                    countSrviceDesk++;
                                });

                                var width = 78 / countSrviceDesk;
                                var marginRight = 6 / countSrviceDesk;

                                $.each(res1.ServiceDeskList, function () {
                                    slotBarId = str + "-" + i;
                                    $("#slotBarList").append('<div class="vcenter" style="margin-right: ' + marginRight + '%;  width: ' + width + '%;">'
                                            + '<div style="height:' + res1.DayTimeDiffrence + ';border:1px solid #000" id="' + slotBarId + '">'
                                            + '</div>'
                                            + '</div>');
                                    var j = 0;
                                    $.each(res1.ServiceDeskList[i].slots, function () {
                                        var slot = "#" + slotBarId;
                                        var slotid = slotBarId + "-" + j;
                                        if (res1.ServiceDeskList[i].slots[j].availability === 0) {
                                            $(slot).append('<div id="' + slotid + '" style="height:' + res1.ServiceDeskList[i].slots[j].height + 'px; background-color: white; border-bottom: 1px;border-top: 1px;border-top-style: solid; border-bottom-style: solid;" data-toggle="tooltip" title="Start Time:' + res1.ServiceDeskList[i].slots[j].startTime + '  End Time:' + res1.ServiceDeskList[i].slots[j].endTime + '"></div>');
                                        } else if (res1.ServiceDeskList[i].slots[j].availability === 1) {
                                            $(slot).append('<div class="get-slot-id" id="' + slotid + '" style="height:' + res1.ServiceDeskList[i].slots[j].height + 'px; background-color: greenyellow;border-bottom: 1px;border-top: 1px;border-top-style: solid; border-bottom-style: solid;" data-toggle="tooltip" title="Start Time:' + res1.ServiceDeskList[i].slots[j].startTime + '  End Time:' + res1.ServiceDeskList[i].slots[j].endTime + '">'
                                                    + '<input type="hidden" class="form-control" id="' + slotid + '-strat-time" value ="' + res1.ServiceDeskList[i].slots[j].startTime + '"/>'
                                                    + '<input type="hidden" class="form-control" id="' + slotid + '-end-time" value ="' + res1.ServiceDeskList[i].slots[j].endTime + '"/>'
                                                    + '<input type="hidden" class="form-control" id="' + slotid + '-dek-no" value ="' + res1.ServiceDeskList[i].deskNo + '"/>'
                                                    + '</div>');
                                        } else if (res1.ServiceDeskList[i].slots[j].availability === 2) {
                                            $(slot).append('<div id="' + slotid + '" style="height:' + res1.ServiceDeskList[i].slots[j].height + 'px; background-color: red;border-bottom: 1px;border-top: 1px;border-top-style: solid; border-bottom-style: solid;" data-toggle="tooltip" title="Start Time:' + res1.ServiceDeskList[i].slots[j].startTime + '  End Time:' + res1.ServiceDeskList[i].slots[j].endTime + '"></div>');
                                        }

                                        j++;
                                    });
                                    i++;
                                });
                                $('#myModal').modal({backdrop: 'static', keyboard: false});
                            }
                        });
                    } else {
                        $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});
                    }
                }

            });
            $("#confirmedSelection").click(function (event) {
                var endtime = $("#endTimeConfirmation").text();
                var startTime = $("#StartTimeConfirmation").text();
                $("#endtimeStr").val(endtime);
                $("#startTimeStr").val(startTime);

                $('#myModal').modal('toggle');
                $('#confirmTimeSlotSelection').modal('toggle');
            });
            $("#slotBarList").on("click", ".get-slot-id", function () {

                //                $('#myModal').modal('toggle');
                var id = $(this).attr("id");
                var stratTimeId = "#" + id + "-strat-time";
                var endTimeId = "#" + id + "-end-time";
                var dekNoId = "#" + id + "-dek-no";
                var startTime = $(stratTimeId).val();
                var endtime = $(endTimeId).val();
                var dekNo = $(dekNoId).val();
                $('#confirmTimeSlotSelection').modal({backdrop: 'static', keyboard: false});
                $("#conf-window-show-desc").empty();
                $('#endtime').prop('readonly', true);
                $("#bookeDsdId").val(dekNo);
                $('#startTime').prop('readonly', true);
                $("#conf-window-show-desc").append('<div class="row"><div class="col-xs-3"></div><div class="col-xs-3"></div></div><div class="row"><div class="col-xs-3"></div><div class="col-xs-3">Start Time:</div><div class="col-xs-3" id="StartTimeConfirmation">' + startTime + '</div><div class="col-xs-3"></div></div><div class="row"><div class="col-xs-3"></div><div class="col-xs-3">End Time:</div><div class="col-xs-3" id="endTimeConfirmation">' + endtime + '</div><div class="col-xs-3"></div></div>');
                //                $('#myModal').modal('toggle');
                //                $('#confirmTimeSlotSelection').modal({backdrop: 'static', keyboard: false});
            });
            $(document).ready(function () {
                $('#menu [data-accordion]').accordion();
                var height = $(".content-area").height();
                $(".menu-container").height(Math.max(height, 600));
            });

            $("#identificatioDocumentType").change(function () {
                $("#documentImgae").empty();
                if ($(this).val() !== 'NIC') {
                    $("#documentImgae").append('<div class="input-group form-group" style="margin-left: 5px">'
                            + '<input type="file" id="file" name="files[0]" class="form-control"  onchange="this.parentNode.nextSibling.value = this.value"/>'
                            + '<span class="input-group-btn">'
                            + '<button class="btn btn-secondary" type="button" id="customerSerach">Scan</button>'
                            + '</span>'
                            + '</div>');
                } else {
                    $("#documentImgae").append('<div class="input-group form-group" style="margin-left: 5px">'
                            + '<input type="file" id="file" name="files[0]" class="form-control"  onchange="this.parentNode.nextSibling.value = this.value"/>'
                            + '<span class="input-group-btn">'
                            + '<button class="btn btn-secondary" type="button" id="customerSerach">Scan</button>'
                            + '</span>'
                            + '</div>'
                            + '<div class="input-group form-group" style="margin-left: 5px">'
                            + '<input type="file" id="file" name="files[1]" class="form-control" onchange="this.parentNode.nextSibling.value = this.value">'
                            + '<span class="input-group-btn">'
                            + '<button class="btn btn-secondary" type="button" id="customerSerach">Scan</button>'
                            + '</span>'
                            + '</div>');
                }
            });

            $('#reservationForm').validate({
                rules: {
                    identificatioDocumentType: {
                        required: true
                    },
                    identificationNumber: {
                        required: true
                    },
                    applicationType: {
                        required: true
                    },
                    fristName: {
                        required: true
                    },
                    lastName: {
                        required: true
                    },
                    telephoneMobile: {
                        required: true,
                        maxlength: 16,
                        digits: true,
                        minlength: 10
                    },
                    startTimeStr: {
                        required: true
                    },
                    endtimeStr: {
                        required: true
                    },
                    email: {
                        email: true
                    },
                    preferredLanguage: {
                        required: true
                    },
                    reason: {
                        required: true
                    }

                }, errorPlacement: function (error, element) {
                    element.parent().find('div.invalid').remove();
                    element.parent().append('<div class="invalid">' + error.text() + '</div>');
                }
            });
            $('#documentTypeform').validate({
                rules: {
                    documentType: {
                        required: true
                    },
                    documentQuentity: {
                        required: true,
                        number: true
                    }
                }, errorPlacement: function (error, element) {
                    error.insertAfter(element.parent());
                }
            });
            function isUnique(values) {
                values.sort();
                // Check whether there are two equal values next to each other
                for (var k = 1; k < values.length; ++k) {
                    if (values[k].trim() === values[k - 1].trim()) {
                        return false;
                        break;
                    }
                }
                return true;
            }
            function isEmpty(values) {
//            values.sort();
                // Check whether there are two equal values next to each other
                for (var k = 0; k < values.length; ++k) {
                    if (!values[k].trim()) {
                        return false;
                        break;
                    }else{
                      return true;  
                    }
                }
                
            }

            function specialCharacter(values) {
                values.sort();
                // Check whether there are two equal values next to each other
                for (var k = 0; k < values.length; ++k) {
                    var string = values[k];
                    for (var l = 0; l < string.length; ++l) {
                        var c = string.charAt(l);
                        if ((c === '"') || (c === ';') || (c === '`') || (c === '?') || (c === '^') || (c === '~') || (c === '$') || (c === '#')
                                || (c === '@') || (c === '^') || (c === '>') || (c === '<') || (c === '%') || (c === '&') || (c === '{') || (c === '}')) {
                            return false;
                        }
                    }
                }
                return true;
            }

            function addDelete(elem, docId) {
                $(elem).parent().parent().parent().remove();
            }

            function confirmDelete(elem, docId) {
                $(elem).hide();
                $(elem).next().show();
            }

            function cancelDelete(elem, docId) {
                $(elem).parent().hide();
                $(elem).parent().prev().show();
            }

            $("#documentTable").on("click", ".documentActionView", function () {
                var id = $(this).attr("id");
                var list = $("#documentVarifictionList").val();
                var databaseList = $("#documentDatabaseList").val();
                res1 = JSON.parse(list);
                res2 = JSON.parse(databaseList);
                $("#referenceTableView").empty();
                console.log(res1);
                index = 0;
                $.each(res1, function () {
                    if (res1[index].documentTypeId === id) {
                        $("#referenceTableView").append('<tr>'
                                + '<td class=col-xs-1 pull-left style=border-top-width:0px >Reference:</td>'
                                + '<td class=col-xs-2 style=border-top-width:0px>'
                                + '<input class=pull-left type=text value="' + res1[index].reference + '" readonly/>'
                                + '</td>'
                                + '</tr>');

                    }
                    index++;
                });
                $('#mydocumentmodelview').modal({backdrop: 'static', keyboard: false});
            });
            $("#documentTable").on("click", ".documentActionEdit", function () {
                var id = $(this).attr("id");
                var list = $("#documentVarifictionList").val();
                var databaseList = $("#documentDatabaseList").val();
                res1 = JSON.parse(list);
                res2 = JSON.parse(databaseList);
                $("#referenceTableEdit").empty();
                index = 0;
                i = 0;
                $.each(res1, function () {
                    if (res1[index].documentTypeId === id) {
                        var referenceId = 'REFERENCEEDIT-REFERENCE-' + i + "-" + res1[index].documentTypeId;
                        $("#referenceTableEdit").append('<tr>'
                                + '<td class=col-xs-1 pull-left style=border-top-width:0px >Reference:</td>'
                                + '<td class=col-xs-2 style=border-top-width:0px>'
                                + '<input maxlength="14" class=pull-left type=text value="' + res1[index].reference + '" id="' + referenceId + '"/>'
                                + '</td>'
                                + '<td class=col-xs-2 style=border-top-width:0px>'
                                + '<button class="btn btn-primary" onclick="confirmDelete( this, ' + res1[index].bookingVerificationId + ')"><i class="glyphicon glyphicon-remove" /></button>'
                                + '<div id="del_conf_' + res1[index].bookingVerificationId + '" style="display: none;">'
                                + '<label>Are you sure you want to delete this record?</label>'
                                + '<button class="btn btn-danger pull-right" onclick="addDelete( this, ' + res1[index].bookingVerificationId + ')"><i class="glyphicon glyphicon-ok" /></button>'
                                + '<button class="btn btn-success pull-right" style="margin-right: 5px;" onclick="cancelDelete( this, ' + res1[index].bookingVerificationId + ')"><i class="glyphicon glyphicon-remove" /></button>'
                                + '</div>'
                                + '</td>'
                                + '</tr>');
                    }
                    index++;
                });
                $("#referenceEditId").val(id);
                $('#mydocumentmodeledit').modal({backdrop: 'static', keyboard: false});
            });
            $("#editReferenceDocument").click(function (event) {
                event.preventDefault();
                var id = $("#referenceEditId").val();
                var list = $("#documentVarifictionList").val();
                var documentList = $("#documentTypeList").val();
                var quantity = "";
                res1 = JSON.parse(list);
                res2 = JSON.parse(documentList);
                for (var j = 0; j < res2.length; j++) {
                    if (res2[j].documentTypeId === id) {
                        quantity = res2[j].quantity;
                    }
                }

                var values = [];
//                for (var i = 0; i < quantity; i++) {
//                    var referenceId = 'REFERENCEEDIT-REFERENCE-' + i + "-" + id;
//                    var reference = $('#' + referenceId).val();
//                    values.push(reference);
//                }
//                for (var i = 0; i < quantity; i++) {
//                    var inputId = "#" + refernceId + "-" + i + "-INPUT";
//                    var value = $(inputId).val();
//                    values.push(value.toString());
//                }
                var i = 0;
                $("#referenceTableEdit").find("input").each(function (k, v) {
                    //var inputId = "#" + refernceId + "-" + i + "-INPUT";
                    var value = $(v).val().trim();
                    values.push(value.toString());
                    i += 1;
                });
                try {
                    if (i > 0) {
                        $($("#TABLECOLOUMN-" + id + " td")[1]).text(i);
                    } else {
                        $("#TABLECOLOUMN-" + id + " td").remove();
                    }
                } catch (e) {
                }
                if (isEmpty(values)) {
                    if (isUnique(values)) {
                        if (specialCharacter(values)) {
                            var count = 0;
                            for (var i = 0; i < res1.length; i++) {
                                if (res1[i].documentTypeId === id) {
                                    count++;
                                }
                            }

                            for (var i = 0; i < res1.length; i++) {
                                if (res1[i].documentTypeId === id) {
                                    res1.splice(i, count);
                                }
                            }
                            var referenceArray = [];
                            referenceArray = res1;
  
                            $("#referenceTableEdit").find("input").each(function (k, v) {
                                var referenceData = new Object();
                                referenceData.reference = $(v).val().trim();
                                referenceData.documentTypeId = id;
                                referenceArray.push(referenceData);
                            });
//                        for (var k = 0; k < quantity; k++) {
//                            var referenceId = 'REFERENCEEDIT-REFERENCE-' + k + "-" + id;
//                            if ( $('#' + referenceId).length > 0 ){
//                                var reference = $('#' + referenceId).val();
//                                var referenceData = new Object();
//                                referenceData.reference = reference;
//                                referenceData.documentTypeId = id;
//                                referenceArray.push(referenceData);
//                            }
//                        }


                            $("#documentVarifictionList").val(JSON.stringify(referenceArray));
                            $("#timeTotal").append(Totaltime());
                            $('#mydocumentmodeledit').modal('toggle');
                        } else {
                            $("#referenceEditValidationMessage").empty();
                            $("#referenceEditValidationMessage").append("<p>Reference field value can not be type  special characters</p>");
                        }
                    } else {
                        $("#referenceEditValidationMessage").empty();
                        $("#referenceEditValidationMessage").append("<p>Reference field value can not be duplicate</p>");
                    }
                } else {
                    $("#referenceEditValidationMessage").empty();
                    $("#referenceEditValidationMessage").append("<p>Reference field value can not be empty</p>");
                }
            });
            function Totaltime() {
                var documentVarificationList = $("#documentVarifictionList").val();
                var databaseDocumnetTypeList = $("#documentDatabaseList").val();
                res1 = JSON.parse(documentVarificationList);
                res2 = JSON.parse(databaseDocumnetTypeList);
                var time = 0;
                for (var j = 0; j < res2.TYPE.length; j++) {
                    for (var i = 0; i < res1.length; i++) {
                        if (res2.TYPE[j].documentTypeId === res1[i].documentTypeId) {
                            var varifitime = res2.TYPE[j].varificationTime;
                            if (res1[i].status === "ACCP") {
                                time = time + parseInt(varifitime);
                            }
                        }
                    }
                }
                return time;
            }

            $('#ticketSerach').on('click', function (event) {
                event.preventDefault();
                var serachValue = $("#ticketSerachValue").val();
                var dataObject = new Object();
                dataObject.serachValue = serachValue;
                var content = JSON.stringify(dataObject);
                $.ajax({
                    async: true,
                    type: "POST",
                    url: "${pageContext.servletContext.contextPath}/reservation/search",
                    cache: false,
                    data: {data: content},
                    success: function (res) {
                        res1 = JSON.parse(res);
                        console.log(res1.object[0].customer.addressLine1);
                        $('#startTime').val("" + res1.object[0].customer.addressLine1);
                    }
                });
            });
            $("#createOk").click(function () {
                var data = {
                    'name': '',
                    'identificationNo': '',
                    'applicationType': '',
                    'email': '',
                    'telephoneMobile': '',
                    'preferredLanguage': '',
                    'bookingdate': '',
                    'addressLine1': '',
                    'addressLine2': '',
                    'addressLine3': '',
                    'startTimeStr': '',
                    'endtimeStr': ''

                };

                localStorage.setItem("customer", JSON.stringify(data));
                $("#reservationForm").submit();
            });
            $("#createbutton").click(function () {
                $('#appoinmentType').val("NORMAL");
                var length = $("#documentTable").children('tr').length;
                if (parseInt(length) !== 0) {

                    if ($("#reservationForm").valid()) {
                        var data = {
                            'name': $('#fristName').val(),
                            'identificationNo': $('#identificationNumber').val(),
                            'applicationType': $('#applicationType').val(),
                            'email': $('#email').val(),
                            'telephoneMobile': $('#telephoneMobile').val(),
                            'preferredLanguage': $('#preferredLanguage').val(),
                            'bookingdate': $('#bookingdate').val(),
                            'addressLine1': $('#addressLine1').val(),
                            'addressLine2': $('#addressLine2').val(),
                            'addressLine3': $('#addressLine3').val(),
                            'startTimeStr': $('#startTimeStr').val(),
                            'endtimeStr': $('#endtimeStr').val()

                        };
                        var data_status = {
                            'status': 'USERDETAILS'

                        };
                        localStorage.setItem("STATUS", JSON.stringify(data_status));
                        localStorage.setItem("customer", JSON.stringify(data));
                        $('#reservationcreateconfirmation').modal({backdrop: 'static', keyboard: false});
                    }
                } else {
                    $('#checkAvilabilityInvlidMessage').html('<b>Please add at least one valid document</b>');
                    $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});

                }

            });
            $("#immediatebutton").click(function () {
                $('#appoinmentType').val("IMMEDIATE");
                var length = $("#documentTable").children('tr').length;
                if (parseInt(length) !== 0) {

                    if ($("#reservationForm").valid()) {
                        var data = {
                            'name': $('#fristName').val(),
                            'identificationNo': $('#identificationNumber').val(),
                            'applicationType': $('#applicationType').val(),
                            'email': $('#email').val(),
                            'telephoneMobile': $('#telephoneMobile').val(),
                            'preferredLanguage': $('#preferredLanguage').val(),
                            'bookingdate': $('#bookingdate').val(),
                            'addressLine1': $('#addressLine1').val(),
                            'addressLine2': $('#addressLine2').val(),
                            'addressLine3': $('#addressLine3').val(),
                            'startTimeStr': $('#startTimeStr').val(),
                            'endtimeStr': $('#endtimeStr').val()

                        };
                        localStorage.setItem("customer", JSON.stringify(data));
                        $('#reservationcreateconfirmation').modal({backdrop: 'static', keyboard: false});
                    }
                } else {
                    $('#checkAvilabilityInvlidMessage').html('<b>Please add at least one valid document</b>');
                    $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});

                }

            });

            $("#slotConfirmationCancel").click(function () {
                $("#startTimeStr").val('');
                $("#endtimeStr").val('');
                $('#confirmTimeSlotSelection').modal('toggle');

            });

            $("#onlineserviceId").keyup(function (event) {
                if (event.keyCode === 13) {
                    var serachValue = $(this).val();
                    if (serachValue !== "") {
                        $("#documentTable").empty();
                        var dataObject = new Object();
                        dataObject.serachValue = serachValue;
                        var content = JSON.stringify(dataObject);
                        var type = "";
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "${pageContext.servletContext.contextPath}/reservation/immediate/customer",
                            cache: false,
                            data: {data: content},
                            success: function (res) {
                                res1 = JSON.parse(res);
                                if (res1.CUSTOMER.length > 0) {
                                    $.each(res1.CUSTOMER, function (index, value) {
                                        $("#bookingId").val(value.bookingId);
                                        $("#identificatioDocumentType").val(value.identificatioDocumentType);
                                        $("#identificationNumber").val(value.identificationNumber);
                                        $("#applicationType").val(value.applicationType);
                                        $("#fristName").val(value.fristName);
                                        $("#lastName").val(value.lastName);
                                        $("#addressLine1").val(value.addressLine1);
                                        $("#addressLine2").val(value.addressLine2);
                                        $("#addressLine3").val(value.addressLine3);
                                        $("#telephoneMobile").val(value.telephoneMobile);
                                        $("#preferredLanguage").val(value.preferredLanguage);
                                        $("#email").val(value.email);
                                        $("#startTimeStr").val(value.startTimeStr);
                                        $("#endtimeStr").val(value.endtimeStr);
                                    });
                                    $("#documentTypeList").val(JSON.stringify(res1.DOCUMENT));
                                    $("#documentVarifictionList").val(JSON.stringify(res1.VERFICATIONLIST));
                                    var docs = [];
                                    $.each(res1.VERFICATIONLIST, function (k, v) {
                                        if (typeof docs[v.documentTypeId] === "undefined") {
                                            docs[v.documentTypeId] = 0;
                                        }
                                        docs[v.documentTypeId] += 1;
                                    });
                                    console.log(res1.VERFICATIONLIST, docs);
                                    $.each(res1.DOCUMENTDESCRIPTION, function (index, value) {
                                        console.log(value.documentTypeId + "    " + value.quantity + "    " + value.description);
                                        var tableColoumnId = "TABLECOLOUMN-";
                                        var documentTypeId = value.documentTypeId;
                                        tableColoumnId = tableColoumnId + documentTypeId;
                                        var documentTypeText = value.description;
                                        if (typeof docs[documentTypeId] !== "undefined") {
                                            $("#documentTable").append('<tr id="' + tableColoumnId + '"><td>' + documentTypeText + '</td><td>' + docs[documentTypeId] + '</td><td><a class="documentActionView" id="' + documentTypeId + '"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="documentActionEdit" id="' + documentTypeId + '"><i class="fa fa-pencil"></i></a></td></tr>');
                                        }
                                    });
                                } else {
                                    $('#checkAvilabilityInvlidMessage').html('<b>Not a valid service ID or already processed one</b>');
                                    $('#checkAvilabilityInvlid').modal({backdrop: 'static', keyboard: false});

                                }


                            }
                        });
                    }

                }
            });
            $('#button1').click(function () {
                $('#formId').attr('action', 'page1');
            });


            $('#button2').click(function () {
                $('#formId').attr('action', 'page2');
            });
//            $('#bookingdate').datepicker({
//                format: "yyyy-mm-dd",
//                autoclose: false,
//                todayBtn: true,
//                startDate: new Date(),
//                minuteStep: 10
//            });

//            $('#fristName').keyup(function () {
//                $('#fristName').val($('#fristName').val().toUpperCase());
//            });

            $("#identificationNumber").on('input', function () {

                var start = this.selectionStart,
                        end = this.selectionEnd;

                this.value = this.value.toUpperCase();

                this.setSelectionRange(start, end);
            });

            $("#fristName").on('input', function () {

                var start = this.selectionStart,
                        end = this.selectionEnd;

                this.value = this.value.toUpperCase();

                this.setSelectionRange(start, end);
            });


        </script>
        <script type="text/javascript">
            $(window).load(function () {
                $(".loader").fadeOut("slow");
            });
        </script>
        <script>
            $(document).bind("ajaxSend", function () {
                $(".loader").fadeIn();
            }).bind("ajaxComplete", function () {
                $(".loader").fadeOut("slow");
            });
        </script>
        <script>
            $(".sidebar-menu li").removeClass("selected");
            $(".sidebar-menu  .reshedule-appoinments").addClass("selected");
        </script>
    </body>
</html>

