<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Ministry of Foreign Affairs | Login</title>
        <spring:url value="/resources/core/css/hello.css" var="coreCss" />
        <spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
        <spring:url value="/resources/core/css/site.min.css" var="siteCss" />
        <spring:url value="/resources/core/css/template.css" var="templateCss" />
        <spring:url value="/resources/core/css/jquery-ui.min.css" var="jqueryuiminCss" />
        <spring:url value="/resources/core/css/style.css" var="styleCss" />
        <spring:url value="/resources/core/css/home.css" var="homeCss" />
        <link href="${coreCss}" rel="stylesheet" />
        <link href="${bootstrapCss}" rel="stylesheet" />
        <link href="${siteCss}" rel="stylesheet" />
        <link href="${templateCss}" rel="stylesheet" />
        <link href="${jqueryuiminCss}" rel="stylesheet" />
        <link href="${styleCss}" rel="stylesheet" />
        <link href="${homeCss}" rel="stylesheet" />
        <spring:url value="/resources/core/js/jquery.min.js" var="jqueryminJs" />
        <spring:url value="/resources/core/js/hello.js" var="coreJs" />
        <spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
        <spring:url value="/resources/core/js/site.min.js" var="siteJs" />
        <script src="${jqueryminJs}"></script>
        <script src="${coreJs}"></script>
        <script src="${bootstrapJs}"></script>
        <script src="${siteJs}"></script>

        <spring:url value="/resources/core/js/jquery.blockUI.js" var="uiBlock" />
        <script src="${uiBlock}"></script>

        <script type="text/javascript">

            $(document).ready(function () {
                $(document).ajaxStart(function () {
                    $.blockUI({css: {
                            border: 'transparent',
                            backgroundColor: 'transparent'
                        },
                        message: '<img height="100" width="100" src="${pageContext.request.contextPath}/src/main/webapp/resources/img/loading.gif" />',
                        baseZ: 2000
                    });

                });
                $(document).ajaxStop(function () {
                    $.unblockUI();
                });

            });


        </script>

        <script>
            setInterval(function () {

                var data = JSON.parse(localStorage.getItem("formdataonline"));
//                document.getElementById('fullName').value=data.name;
                $('#fullName').html(data.name);
                $('#identificationNo').html(data.identificationNo);
//                $('#applicationType').val(data.applicationType);
                $('#email').html(data.email);
                $('#telephoneMobile').html(data.telephoneMobile);
                $('#preferredLanguage').html(data.preferredLanguage);
                $('#addressLine1').html(data.addressLine1);
                $('#addressLine2').html(data.addressLine2);
                $('#addressLine3').html(data.addressLine3);
                $('#bookingdate').html(data.bookingdate);
                $('#startTimeStr').html(data.startTimeStr);
                $('#endtimeStr').html(data.endtimeStr);
            }, 1000);

        </script>

    </head>
    <body>
        <div class="container-fluid">
            <div class="row login-header">
                <div class="col-md-2 col-xs-3 title">
                    <img class="login-logo" src="${pageContext.servletContext.contextPath}/resources/img/Emblem_of_Sri_Lanka.png">
                    <!--<img src="${pageContext.servletContext.contextPath}/resources/img/user.png" class="img-circle" alt="User Image" style="border: none;">-->
                </div>
                <div class="col-md-4 col-xs-9 login-header-text">
                    <p>Electronic Document Attestation System</p>
                </div>	
            </div>
            <style>
                .error {
                    padding: 15px;
                    margin-bottom: 20px;
                    border: 1px solid transparent;
                    border-radius: 4px;
                    color: #a94442;
                    background-color: #f2dede;
                    border-color: #ebccd1;
                }

                .msg {
                    padding: 15px;
                    margin-bottom: 20px;
                    border: 1px solid transparent;
                    border-radius: 4px;
                    color: #31708f;
                    background-color: #d9edf7;
                    border-color: #bce8f1;
                }


            </style>

        </div>
        <div class="container"  style="height: 100%;font-size: 30px;">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row form-horizontal">
                        <section class="content" style="margin-top: 20px;">
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Name in Full:</label>
                                <div class="col-sm-8" id="fullName" style="margin-top: 12px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Identification Number:</label>
                                <div class="col-sm-8" id="identificationNo"style="margin-top: 12px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">email:</label>
                                <div class="col-sm-8" id="email" style="margin-top: 12px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Telephone:</label>
                                <div class="col-sm-8" id="telephoneMobile" style="margin-top: 12px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Preferred Language:</label>
                                <div class="col-sm-8" id="preferredLanguage" style="margin-top: 12px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Address 1:</label>
                                <div class="col-sm-8" id="addressLine1" style="margin-top: 12px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Address 2:</label>
                                <div class="col-sm-8" id="addressLine2" style="margin-top: 12px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Address 3:</label>
                                <div class="col-sm-8" id="addressLine3" style="margin-top: 12px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Booking Date:</label>
                                <div class="col-sm-8" id="bookingdate" style="margin-top: 12px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Start Time:</label>
                                <div class="col-sm-8" id="startTimeStr" style="margin-top: 12px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">End Time:</label>
                                <div class="col-sm-8" id="endtimeStr" style="margin-top: 12px;font-size: 30px;">
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>


    </body>
</html>
