<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Ministry of Foreign Affairs | Login</title>
        <spring:url value="/resources/core/css/hello.css" var="coreCss" />
        <spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
        <spring:url value="/resources/core/css/site.min.css" var="siteCss" />
        <spring:url value="/resources/core/css/template.css" var="templateCss" />
        <spring:url value="/resources/core/css/jquery-ui.min.css" var="jqueryuiminCss" />
        <spring:url value="/resources/core/css/style.css" var="styleCss" />
        <spring:url value="/resources/core/css/home.css" var="homeCss" />
        <spring:url value="/resources/core/css/confirm.css" var="confirmCss" />
        <link href="${coreCss}" rel="stylesheet" />
        <link href="${bootstrapCss}" rel="stylesheet" />
        <link href="${siteCss}" rel="stylesheet" />
        <link href="${templateCss}" rel="stylesheet" />
        <link href="${jqueryuiminCss}" rel="stylesheet" />
        <link href="${styleCss}" rel="stylesheet" />
        <link href="${homeCss}" rel="stylesheet" />
        <link href="${confirmCss}" rel="stylesheet" />
        <spring:url value="/resources/core/js/jquery.min.js" var="jqueryminJs" />
        <spring:url value="/resources/core/js/hello.js" var="coreJs" />
        <spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
        <spring:url value="/resources/core/js/site.min.js" var="siteJs" />
        <script src="${jqueryminJs}"></script>
        <script src="${coreJs}"></script>
        <script src="${bootstrapJs}"></script>
        <script src="${siteJs}"></script>

        <spring:url value="/resources/core/js/jquery.blockUI.js" var="uiBlock" />
        <script src="${uiBlock}"></script>

        <script type="text/javascript">

            $(document).ready(function () {
                $(document).ajaxStart(function () {
                    $.blockUI({css: {
                            border: 'transparent',
                            backgroundColor: 'transparent'
                        },
                        message: '<img height="100" width="100" src="${pageContext.request.contextPath}/src/main/webapp/resources/img/loading.gif" />',
                        baseZ: 2000
                    });

                });
                $(document).ajaxStop(function () {
                    $.unblockUI();
                });

            });


        </script>

        <script>
            function tConvert (time) {
                // Check correct time format and split into components
                time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

                if (time.length > 1) { // If time format correct
                  time = time.slice (1);  // Remove full string match value
                  time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
                  time[0] = +time[0] % 12 || 12; // Adjust hours
                }
                return time.join (''); // return adjusted time or original string
            }

            setInterval(function () {
                var data = JSON.parse(localStorage.getItem("customer"));
                var data_status = JSON.parse(localStorage.getItem("STATUS"));

                if (data_status.status === 'USERDETAILS') {
                    $("#userdetails").show();
                    $("#timesloat").hide();
                    if (data !== null) {
                        $('#fullName').html(data.name);
                        $('#identificationNo').html(data.identificationNo);
                        //                $('#applicationType').val(data.applicationType);

                        if (data.email !== "") {
                            $('#email').parent().show();
                            $('#email').html(data.email);
                        } else {
                            $('#email').parent().hide();
                        }
                        $('#telephoneMobile').html(data.telephoneMobile);
                        $('#preferredLanguage').html(data.preferredLanguage);
                        if (data.addressLine1 !== "") {
                            $('#addressLine1').parent().show();
                            $('#addressLine1').html(data.addressLine1);
                        } else {
                            $('#addressLine1').parent().hide();
                        }
                        if (data.addressLine2 !== "") {
                            $('#addressLine2').parent().show();
                            $('#addressLine2').html(data.addressLine2);
                        } else {
                            $('#addressLine2').parent().hide();
                        }
                        if (data.addressLine3 !== "") {
                            $('#addressLine3').parent().show();
                            $('#addressLine3').html(data.addressLine3);
                        } else {
                            $('#addressLine3').parent().hide();
                        }
                        $('#bookingdate').html(data.bookingdate);
                        $('#startTimeStr').html(data.startTimeStr);
                        $('#endtimeStr').html(data.endtimeStr);
                    }
                } else if (data_status.status === 'TIMESLOAT') {
                    $("#timesloat").show();
                    $("#userdetails").hide();
                    res1 = data;
                    $("#timeSlotRow").empty();
                    if (res1.HOLIDAY === "NO") {
                        
                        $("#timeSlotRow").append("<div style='margin-bottom: 20px;text-align: center;width: 100%;'><span style='border-left: 35px solid #f00; padding-left: 5px;'>Not available.</span> <span style='border-left: 35px solid #17a000; padding-left: 5px;'>Available.</span></div>");
//                        $("#timeSlotRow").append('<tr >'
//                                + '<th>Time Slots</th>'
//                                + '<th>Available Slots</th>'
//                                + '<th style="width: 40px">Availability</th>'
//                                + '</tr>');



                        var index = 0;
                        $.each(res1.TIMESLOAT, function () {
                            if (res1.TIMESLOAT[index].halfday !== "YES") {
                                if (res1.TIMESLOAT[index].exceed === "NO") {
                                    if (res1.TIMESLOAT[index].availabelSlot > 0) {
//                                        $("#timeSlotRow").append('<tr>'
//                                                + '<td><span id="SLOT-STARTTIME-' + index + '">' + res1.TIMESLOAT[index].startTimeStr + '</span> - <span id="SLOT-ENDTIME-' + index + '">' + res1.TIMESLOAT[index].endtimeStr + '</span></td>'
//                                                + '<td style="padding-left:80px">' + res1.TIMESLOAT[index].availabelSlot + '</td>'
//                                                + '<td>'
//                                               + '<p style="color: #35d642">Available</p>'
//                                                + '</td>'
//                                                + '</tr>');
                                            $("#timeSlotRow").append( "<div class='slot success'><div class='time'><h3>" 
                                                    + tConvert( res1.TIMESLOAT[index].startTimeStr ) + "</h3><h4>to</h4><h3>" 
                                                    + tConvert( res1.TIMESLOAT[index].endtimeStr ) + "</h3></div><div class='slots'>" 
                                                    + res1.TIMESLOAT[index].availabelSlot + " slots available</div></div>" );
                                    } else {
//                                        $("#timeSlotRow").append('<tr>'
//                                                + '<td><span id="SLOT-STARTTIME-' + index + '">' + res1.TIMESLOAT[index].startTimeStr + '</span> - <span id="SLOT-ENDTIME-' + index + '">' + res1.TIMESLOAT[index].endtimeStr + '</span></td>'
//                                                + '<td style="padding-left:80px">' + res1.TIMESLOAT[index].availabelSlot + '</td>'
//                                                + '<td>'
//                                                + '<p>Not Available</p>'
//                                                + '</td>'
//                                                + '</tr>');
                                            $("#timeSlotRow").append( "<div class='slot danger'><div class='time'><h3>" 
                                                    + tConvert( res1.TIMESLOAT[index].startTimeStr ) + "</h3><h4>to</h4><h3>" 
                                                    + tConvert( res1.TIMESLOAT[index].endtimeStr ) + "</h3></div></div>" );

                                    }

                                } else {
//                                    $("#timeSlotRow").append('<tr>'
//                                            + '<td><span id="SLOT-STARTTIME-' + index + '">' + res1.TIMESLOAT[index].startTimeStr + '</span> - <span id="SLOT-ENDTIME-' + index + '">' + res1.TIMESLOAT[index].endtimeStr + '</span></td>'
//                                            + '<td style="padding-left:80px">' + res1.TIMESLOAT[index].availabelSlot + '</td>'
//                                            + '<td>'
//                                            + '<p>Expired</p>'
//                                            + '</td>'
//                                            + '</tr>');
                                            $("#timeSlotRow").append( "<div class='slot danger'><div class='time'><h3>" 
                                                    + tConvert( res1.TIMESLOAT[index].startTimeStr ) + "</h3><h4>to</h4><h3>" 
                                                    + tConvert( res1.TIMESLOAT[index].endtimeStr ) + "</h3></div></div>" );
                                }
                            } else {
//                                $("#timeSlotRow").append('<tr>'
//                                        + '<td><span id="SLOT-STARTTIME-' + index + '">' + res1.TIMESLOAT[index].startTimeStr + '</span> - <span id="SLOT-ENDTIME-' + index + '">' + res1.TIMESLOAT[index].endtimeStr + '</span></td>'
//                                        + '<td style="padding-left:80px">' + res1.TIMESLOAT[index].availabelSlot + '</td>'
//                                        + '<td>'
//                                        + '<p>Not Available</p>'
//                                        + '</td>'
//                                        + '</tr>');
                                            $("#timeSlotRow").append( "<div class='slot danger'><div class='time'><h3>" 
                                                    + tConvert( res1.TIMESLOAT[index].startTimeStr ) + "</h3><h4>to</h4><h3>" 
                                                    + tConvert( res1.TIMESLOAT[index].endtimeStr ) + "</h3></div></div>" );
                            }


                            index++;
                        });
                    } else {
                        $("#timeSlotRow").empty();
                        $("#timeSlotRow").append("<p>Sorry, Selected date is a full holiday. You can't make reservations</p>");
                    }
                }


            }, 1000);

        </script>

    </head>
    <body>
        <input type="hidden" name="ip" id="get_ip" />
        <div class="container-fluid">
            <div class="row login-header">
                <div class="col-md-2 col-xs-3 title">
                    <img class="login-logo" src="${pageContext.servletContext.contextPath}/resources/img/Emblem_of_Sri_Lanka.png">
                    <!--<img src="${pageContext.servletContext.contextPath}/resources/img/user.png" class="img-circle" alt="User Image" style="border: none;">-->
                </div>
                <div class="col-md-10 col-xs-9 login-header-text">
                    <p>Electronic Document Attestation System</p>
                </div>	
            </div>
            <style>
                .error {
                    padding: 15px;
                    margin-bottom: 20px;
                    border: 1px solid transparent;
                    border-radius: 4px;
                    color: #a94442;
                    background-color: #f2dede;
                    border-color: #ebccd1;
                }

                .msg {
                    padding: 15px;
                    margin-bottom: 20px;
                    border: 1px solid transparent;
                    border-radius: 4px;
                    color: #31708f;
                    background-color: #d9edf7;
                    border-color: #bce8f1;
                }
                
                .slot {
                    padding: 10px;
                    width: 24%;
                    color: white;
                    text-align: center;
                    border: 1px solid rgba(255, 255, 255, 0.1);
                    box-shadow: inset 0px 0px 30px 0px rgba(0, 0, 0, 0.4);
                    float: left;
                    margin-bottom: 5px;
                    margin-right: 1%;
                }
                
                .slot.success {
                    background: #17a000;
                }

                .slot.danger {
                    background: #a00000;
                    opacity: 0.3;
                }
                
                .slot .slots {
                    font-size: 1.5vw;
                    display: none;
                }
                
                .slot .time {
                    font-size: 1.7vw;
                }
                
                .slot .time > h3 {
                    margin: 5px 0px;
                }
                
                .slot .time > h4 {
                    margin: 5px 0px;
                }

            </style>

        </div>
        <div class="container"  style="height: 100%;font-size: 30px;">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row form-horizontal" id="userdetails">
                        <section class="content gnuolane" style="margin-top: 20px;">
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Name in Full</label>
                                <div class="col-sm-8" id="fullName" style="margin-top: 7px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Identification Number</label>
                                <div class="col-sm-8" id="identificationNo"style="margin-top: 7px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Email</label>
                                <div class="col-sm-8" id="email" style="margin-top: 7px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Telephone</label>
                                <div class="col-sm-8" id="telephoneMobile" style="margin-top: 7px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Preferred Language</label>
                                <div class="col-sm-8" id="preferredLanguage" style="margin-top: 7px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Address 1</label>
                                <div class="col-sm-8" id="addressLine1" style="margin-top: 7px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Address 2</label>
                                <div class="col-sm-8" id="addressLine2" style="margin-top: 7px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Address 3</label>
                                <div class="col-sm-8" id="addressLine3" style="margin-top: 7px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Booking Date</label>
                                <div class="col-sm-8" id="bookingdate" style="margin-top: 7px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">Start Time</label>
                                <div class="col-sm-8" id="startTimeStr" style="margin-top: 7px;font-size: 30px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label labelfont col-sm-4" for="email">End Time</label>
                                <div class="col-sm-8" id="endtimeStr" style="margin-top: 7px;font-size: 30px;">
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="row form-horizontal" id="timesloat" style="display: none">
                        <div class="box-body no-padding">
                            <div class="table" id="timeSlotRow" style="margin-top: 25px;">


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">

//            function getLocalIPs() {
//                var e = window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
//                e && function() {
//                    function n(e) {
//                        e.split("\r\n").forEach(function(e) {
//                            if (~e.indexOf("a=candidate")) {
//                                var n = e.split(" "),
//                                    i = n[4],
//                                    a = n[7];
//                                if ("host" === a) var t = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
//                                t.test(i) && $("#get_ip").val(i)
//                            } else if (~e.indexOf("c=")) var n = e.split(" "),
//                                i = n[2]
//                        })
//                    }
//                    var i = new e({
//                        iceServers: []
//                    });
//                    i.createDataChannel("", {
//                        reliable: !1
//                    }), i.onicecandidate = function(e) {
//                        e.candidate && n("a=" + e.candidate.candidate)
//                    }, i.createOffer(function(e) {
//                        n(e.sdp), i.setLocalDescription(e)
//                    }, function(e) {
//                        console.warn("offer failed", e);
//                    });
//                    try {
//                        addrs["0.0.0.0"] = !1
//                    } catch (a) {}
//                }()
//            }
//            // Reconnecting WebSocket
//            !function(a,b){"function"==typeof define&&define.amd?define([],b):"undefined"!=typeof module&&module.exports?module.exports=b():a.ReconnectingWebSocket=b()}(this,function(){function a(b,c,d){function l(a,b){var c=document.createEvent("CustomEvent");return c.initCustomEvent(a,!1,!1,b),c}var e={debug:!1,automaticOpen:!0,reconnectInterval:1e3,maxReconnectInterval:3e4,reconnectDecay:1.5,timeoutInterval:2e3};d||(d={});for(var f in e)this[f]="undefined"!=typeof d[f]?d[f]:e[f];this.url=b,this.reconnectAttempts=0,this.readyState=WebSocket.CONNECTING,this.protocol=null;var h,g=this,i=!1,j=!1,k=document.createElement("div");k.addEventListener("open",function(a){g.onopen(a)}),k.addEventListener("close",function(a){g.onclose(a)}),k.addEventListener("connecting",function(a){g.onconnecting(a)}),k.addEventListener("message",function(a){g.onmessage(a)}),k.addEventListener("error",function(a){g.onerror(a)}),this.addEventListener=k.addEventListener.bind(k),this.removeEventListener=k.removeEventListener.bind(k),this.dispatchEvent=k.dispatchEvent.bind(k),this.open=function(b){h=new WebSocket(g.url,c||[]),b||k.dispatchEvent(l("connecting")),(g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","attempt-connect",g.url);var d=h,e=setTimeout(function(){(g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","connection-timeout",g.url),j=!0,d.close(),j=!1},g.timeoutInterval);h.onopen=function(){clearTimeout(e),(g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","onopen",g.url),g.protocol=h.protocol,g.readyState=WebSocket.OPEN,g.reconnectAttempts=0;var d=l("open");d.isReconnect=b,b=!1,k.dispatchEvent(d)},h.onclose=function(c){if(clearTimeout(e),h=null,i)g.readyState=WebSocket.CLOSED,k.dispatchEvent(l("close"));else{g.readyState=WebSocket.CONNECTING;var d=l("connecting");d.code=c.code,d.reason=c.reason,d.wasClean=c.wasClean,k.dispatchEvent(d),b||j||((g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","onclose",g.url),k.dispatchEvent(l("close")));var e=g.reconnectInterval*Math.pow(g.reconnectDecay,g.reconnectAttempts);setTimeout(function(){g.reconnectAttempts++,g.open(!0)},e>g.maxReconnectInterval?g.maxReconnectInterval:e)}},h.onmessage=function(b){(g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","onmessage",g.url,b.data);var c=l("message");c.data=b.data,k.dispatchEvent(c)},h.onerror=function(b){(g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","onerror",g.url,b),k.dispatchEvent(l("error"))}},1==this.automaticOpen&&this.open(!1),this.send=function(b){if(h)return(g.debug||a.debugAll)&&console.debug("ReconnectingWebSocket","send",g.url,b),h.send(b);throw"INVALID_STATE_ERR : Pausing to reconnect websocket"},this.close=function(a,b){"undefined"==typeof a&&(a=1e3),i=!0,h&&h.close(a,b)},this.refresh=function(){h&&h.close()}}return a.prototype.onopen=function(){},a.prototype.onclose=function(){},a.prototype.onconnecting=function(){},a.prototype.onmessage=function(){},a.prototype.onerror=function(){},a.debugAll=!1,a.CONNECTING=WebSocket.CONNECTING,a.OPEN=WebSocket.OPEN,a.CLOSING=WebSocket.CLOSING,a.CLOSED=WebSocket.CLOSED,a});
//            
//            // Init
//            getLocalIPs();
//            function init(){
//                if ( $("#get_ip").val() == "" ){
//                    console.log('no ip');
//                    setTimeout( function(){ init() }, 1000 );
//                } else {
//                    var socket = new ReconnectingWebSocket("ws://localhost:3000");
//                    socket.onopen = function(e){
//                        socket.send( JSON.stringify( { type: "listen", data: { ip: $("#get_ip").val() } } ) );
//                    }
//                    socket.onmessage = function(m){
//                        console.log(m.data);
//                        try{ 
//                            let obj = JSON.parse( m.data );
//                            for (var k in obj){
//                                //console.log(k, obj[k]);
//                                //$("#" + k).val();
//                                document.getElementById(k).innerText = obj[k];
//                            }
//                        } catch(e){
//                            
//                        }
//                    }
//                }
//            }
//            
//            init();
//            
        </script>
    </body>
</html>
