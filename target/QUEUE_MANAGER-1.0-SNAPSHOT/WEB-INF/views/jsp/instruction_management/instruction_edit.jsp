<%-- 
    Document   : instruction_edit
    Created on : Oct 25, 2016, 11:35:31 AM
    Author     : Kelum Madushan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
<!DOCTYPE html>

<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>
        <script>
            var role = "<%=session.getAttribute("role")%>";
            if (role !== 'ADMIN') {
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }

            var username = "<%=session.getAttribute("loggedUser")%>"
            if (!<%=session.getAttribute("loggedUser")%>) {
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            } else {
            }

        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/admin_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/admin_menu.jsp"/>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Instruction Management <small>Edit</small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div id="container">
                        <div class="row">           
                            <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                <div class="box box-primary">
                                    <form:form role="form" id="editForm" method="POST" modelAttribute="editForm" action="${pageContext.request.contextPath}/admin/instructionmanagement/edittedinstruction">
                                        <div class="box-body">
                                            <form:input path="instructionid" id="instructionid" value="${instructionList[0].instructionid}" type="hidden"></form:input>
                                            </div>                                        
                                            <div class="box-body">
                                                <div class="form-group">
                                                <form:label  path="status">Instruction Type:<samp style="color: red">*</samp></form:label>
                                                <form:select class="form-control" required="required" id="status" path="status">
                                                    <form:option value="">Select Status</form:option> 
                                                    <c:forEach  items="${status}" var="status">
                                                        <c:choose>
                                                            <c:when test="${status.statusCode eq instructionList[0].status}">
                                                                <form:option value="${status.statusCode}" selected="selected">${status.statusDescription}</form:option> 
                                                            </c:when>
                                                            <c:otherwise>
                                                                <%--<form:option value="${status.statusCode}" disabled="disabled" readonly="true">${status.statusDescription}</form:option>--%>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </form:select>
                                            </div>
                                            <div class="form-group">
                                                <form:label  path="status">Sinhala:</form:label>
                                                <textarea class="form-control" required="required" id="sinhala" name="sinhala" maxlength="2000">${instructionList[0].sinhala}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <form:label  path="status">English:</form:label>
                                                <textarea class="form-control" required="required" id="english" name="english" maxlength="2000">${instructionList[0].english}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <form:label  path="status">Tamil:</form:label>
                                                <textarea class="form-control" required="required" id="tamil" name="tamil" maxlength="2000">${instructionList[0].tamil}</textarea>
                                            </div>
                                            <div class="box-footer btn-toolbar">
                                                <form:button type="button" class="btn btn-primary pull-right" onclick="window.history.back();">Cancel</form:button>
                                                <form:button id="createbutton" type="submit" class="btn btn-primary pull-right">Save</form:button>     
                                                </div>
                                            </div>
                                    </form:form>
                                </div>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>   
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
        <jsp:include page="../template/jsinclude.jsp"/>
        <script>
            $(".sidebar-menu li").removeClass("selected");
            $(".sidebar-menu  .ins-mgmt").addClass("selected");
        </script>
    </body>
</html>


