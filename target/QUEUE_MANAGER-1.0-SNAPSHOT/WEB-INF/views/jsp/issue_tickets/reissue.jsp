<%-- 
    Document   : reservationCreate
    Created on : Oct 12, 2016, 12:28:28 PM
    Author     : Rasika
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 


<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>
        <script>
            var role = "<%=session.getAttribute("role")%>";
            if (role !== 'USER') {
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }

            var username = "<%=session.getAttribute("loggedUser")%>";
            if (!<%=session.getAttribute("loggedUser")%>) {
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            } else {
            }

        </script>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="loader"></div>
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/user_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/user_menu.jsp"/>
            </aside>
            <div class="content-wrapper">
                <section class="content" style="min-height: 130px">
                    <span style="font-size: 16px;"><b>Issue Queue Tickets</b></span><br/><br/>
                    <div class="form-group">
                        <label class="control-label labelfont col-sm-3" for="telephoneMobile">Service Id:<samp style="color: red">*</samp></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="serviceid"  placeholder="Enter service id" autofocus/>
                            <div class="invalid" id="numberValidated"></div>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-default btn-block" id="reprintButton">Reprint</button>
                        </div>

                    </div>
                </section>
                <div id="serviceinfo" style="display: none">
                    <section class="content" id="printArea">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="box">
                                    <div  id="tiket_content" class="box-body" style="font-family: 'Times New Roman', Times, serif; font-weight: 1000">
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4" style="text-align: center">
                                                <img src="${pageContext.servletContext.contextPath}/resources/img/Capture.PNG"  alt="User Image">
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-10" style="text-align: center;font-size: 15px;">
                                                <p><b>Ministry of Foreign Affairs Colombo 01,</b></p>
                                                <p><b>Sri Lanka</b></p>
                                            </div>
                                            <div class="col-md-1"></div>
                                        </div> 
                                        <br>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="text-align: center;font-size: 20px;margin-bottom: 35px;">
                                                <p id="tokenlabel1"></p>
                                                <p id="token1" style="font-size: 36px;"></p>
                                                <p id="serviceId1"></p>

                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6" style="text-align: center;font-size: 20px;margin-bottom: 35px;">
                                                <p>------------------------------</p>
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="content">
                        <div class="row">
                            <div class="col-md-5" style="text-align: right;margin-top: -45px;">
                                <button onclick="printsummary('tiket_content')"> <img src="${pageContext.servletContext.contextPath}/resources/img/print.png"  alt="User Image"></button>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
    </body>
    <script>

        $("#reprintButton").click(function () {
            $("#serviceinfo").hide();
            var serviceId = $("#serviceid").val();
            var reg = /^\d+$/;
            var myRegEx = new RegExp('/[0-9 -()+]+$/'); //make sure the var is a number

            if (reg.test(serviceId)) {
                $("#numberValidated").html("");
                var dataObject = new Object();
                dataObject.serviceId = serviceId;
                var content = JSON.stringify(dataObject);
                $.ajax({
                    async: true,
                    type: "POST",
                    url: "${pageContext.servletContext.contextPath}/issuetickets/reprint",
                    cache: false,
                    data: {data: content},
                    success: function (res) {
                        res1 = JSON.parse(res);
                        $("#serviceinfo").show();
                        $("#serviceid1").val('');
                        $("#serviceId1").text(res1.serviceId);
                        $("#token1").text(res1.token);
                        $("#tokenlabel1").text(res1.tokenlabel);
                    }
                });
            } else {
                $("#numberValidated").html("Please enter a valid number.");
            }



        });






        function printsummary(divName) {
            $("#serviceinfo").hide();

            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
            window.location.href = "${pageContext.servletContext.contextPath}/issuetickets/reissue";
        }


    </script>
    <script>
        $(".sidebar-menu li").removeClass("selected");
        $(".sidebar-menu  .re-issue").addClass("selected");
    </script>
    <script>
        $(window).load(function () {
            $(".loader").fadeOut("slow");
        });
        $(document).bind("ajaxSend", function () {
            $(".loader").fadeIn();
        }).bind("ajaxComplete", function () {
            $(".loader").fadeOut("slow");
        });
    </script>
</html>

