<%-- 
    Document   : counter_list
    Created on : Oct 13, 2016, 11:00:55 AM
    Author     : Kelum Madushan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
<!DOCTYPE html>

<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>

        <!--Data Table-->
        <spring:url value="/resources/DataTables/media/js/jquery-1.12.0.min.js" var="jqueryJs" />
        <script src="${jqueryJs}"></script>
        <spring:url value="/resources/DataTables/media/js/jquery.dataTables.min.js" var="jqueryJs1" />
        <script src="${jqueryJs1}"></script>
        <spring:url value="/resources/DataTables/media/css/jquery.dataTables.min.css" var="datatableCss" />
        <link href="${datatableCss}" rel="stylesheet" />
        <!--Data Table ends-->

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/admin_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/admin_menu.jsp"/>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        System User <small>List</small>
                    </h1>
                    <div class="col-sm-6">
                        <c:if test="${not empty msg}">
                            <div class="msg">${msg}</div>
                        </c:if>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3"></div>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div id="container">
                        <a href="${pageContext.servletContext.contextPath}/admin/systemuser/adduser" class="btn btn-primary pull-right" role="button" style="margin-bottom:10px;">New User</a>
                        <table id="listbusinessinfo" class="display table table-bordered table-striped dataTable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><div align="center">User Name</div></th>
                            <th><div align="center">First Name</div></th>
                            <th><div align="center">Last Name</div></th>
                            <th><div align="center">User Role</div></th>
                            <th><div align="center">Mobile</div></th>
                            <th><div align="center">Email</div></th>
                            <th><div align="center">NIC</div></th>
                            <th><div align="center">Edit</div></th>
                            <th><div align="center">Delete</div></th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${userList}" var="userList">
                                    <tr>
                                        <td>${userList.username}</td>
                                        <td>${userList.firstname}</td>
                                        <td>${userList.lastname}</td>
                                        <td>${userList.userRoleDes}</td>
                                        <td>${userList.mobile}</td>
                                        <td>${userList.email}</td>
                                        <td>${userList.nic}</td>
                                        <td>
                                            <c:set var="count" value="${count+1}" scope="session"/>
                                            <form id="${count}" name="updateForm" method="POST" action ="${pageContext.servletContext.contextPath}/admin/systemuser/edituser" commandName="updateForm">
                                                <input type="hidden" name="username" value="${userList.username}">
                                                <div align="center"><a href="javascript:{}" id="updateRow" onclick="document.getElementById(${count}).submit();
                                                            return false;"><span class="glyphicon glyphicon-edit"></span></a>
                                                </div>
                                            </form>
                                        </td>
                                        <td>
                                            <c:set var="count" value="${count+1}" scope="session"/>
                                            <form id="${count}" name="deleteForm" method="POST" action = "${pageContext.servletContext.contextPath}/admin/systemuser/deletecounter" commandName="deleteForm" >
                                                <input type="hidden" name="username" value="${userList.username}">
                                                <div align="center"><a href="#" id="smart-mod-eg1" onclick="myFunction${count}();"><span class="glyphicon glyphicon-trash"></span></a></div>
                                                <script>
                                                        function myFunction${count}() {

                                                            $('#myModal').modal('show');
                                                            $('[id="deletebtn"]').click(function() {
                                                                document.getElementById(<c:out value="${count}"/>).submit();
                                                                $('#myModal').modal('hide');

                                                            });

                                                        }
                                                </script>
                                            </form>
                                        </td>
                                    </tr>
                                </c:forEach>
                            <div class="modal" id="myModal"> 
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" style="color: #367fa9">×</span></button>
                                            <h4 class="modal-title" style="color: #367fa9"><span class="glyphicon glyphicon-exclamation-sign"></span> Confirmation for Delete!</h4>
                                        </div>
                                        <div class="modal-body" style="color: #367fa9">
                                            <p><b>Are you sure you want to delete this record?</b></p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary" id="deletebtn">Delete</button>
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </tbody>
                        </table>
                        <script>
                            $('#listbusinessinfo').dataTable();
                        </script> 
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
        <jsp:include page="../template/jsinclude.jsp"/>
    </body>
</html>
<style>
    .msg {
        padding: 15px;
        margin-bottom: 20px;
        margin-top: 15px;
        margin-left: -14px;
        border: 1px solid transparent;
        border-radius: 4px;
        color: #31708f;
        background-color: #d9edf7;
        border-color: #bce8f1;
    }
</style>