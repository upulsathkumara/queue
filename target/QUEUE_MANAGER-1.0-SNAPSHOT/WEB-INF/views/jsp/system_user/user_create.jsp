<%-- 
    Document   : counter_create
    Created on : Oct 13, 2016, 3:18:54 PM
    Author     : Kelum Madushan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
<!DOCTYPE html>

<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/admin_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/admin_menu.jsp"/>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        System User <small>create</small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div id="container">
                        <div class="row">           
                            <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                <div class="box box-primary">
                                    <form:form role="form" id="insertForm" method="POST" modelAttribute="insertForm" action="${pageContext.request.contextPath}/admin/systemuser/adduser">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <form:label path="username">Username:<samp style="color: red">*</samp></form:label>
                                                <form:input path="username" type="text" id="username" class="form-control" placeholder="Enter Username" onblur="check_availability()" maxlength="32"></form:input>
                                                    <div id="status"></div>   
                                                </div>
                                                <div class="form-group">
                                                    <label for="spassword">Password:<samp style="color: red">*</samp></label>
                                                    <input name="spassword" id="spassword" type="password" class="form-control" placeholder="Enter Password"/>
                                                </div>
                                                <div class="form-group">
                                                <form:label path="password">Confirm Password:<samp style="color: red">*</samp></form:label>
                                                <form:input path="password" type="password" id="password" class="form-control" placeholder="Retype Password"></form:input> 
                                                </div>
                                                <div class="form-group">
                                                <form:label  path="userrole">User Role:<samp style="color: red">*</samp></form:label>
                                                <form:select class="form-control" required="required" id="userrole" path="userrole">
                                                    <form:option value="">Select user Role</form:option> 
                                                    <c:forEach  items="${role}" var="role">
                                                        <form:option value="${role.userroleid}">${role.description}</form:option> 
                                                    </c:forEach>
                                                </form:select>
                                            </div>
                                            <div class="form-group">
                                                <form:label path="firstname">First Name:<samp style="color: red">*</samp></form:label>
                                                <form:input path="firstname" type="text" id="firstname" class="form-control" placeholder="Enter First Name" maxlength="32"></form:input>
                                                </div>
                                                <div class="form-group">
                                                <form:label path="lastname">Last Name:<samp style="color: red">*</samp></form:label>
                                                <form:input path="lastname" type="text" id="lastname" class="form-control" placeholder="Enter Lastname" maxlength="32"></form:input>
                                                </div>
                                                <div class="form-group">
                                                <form:label path="mobile">Mobile:<samp style="color: red">*</samp></form:label>
                                                <form:input class="form-control" path="mobile" type="text" id="mobile" placeholder="Enter Mobile" maxlength="10"></form:input>
                                                </div>
                                                <div class="form-group">
                                                <form:label path="email">Email:</form:label>
                                                <form:input class="form-control" path="email" type="text" id="email" placeholder="Enter Email" maxlength="32"></form:input>
                                                </div>
                                                <div class="form-group">
                                                <form:label path="nic">NIC:<samp style="color: red">*</samp></form:label>
                                                <form:input class="form-control" path="nic" type="text" id="nic" placeholder="Enter NIC" maxlength="12"></form:input>
                                                </div>
                                                <div class="box-footer btn-toolbar">
                                                <form:button type="button" class="btn btn-primary pull-right" onclick="window.history.back();">Cancel</form:button>
                                                <form:button id="createbutton" type="submit" class="btn btn-primary pull-right">Save</form:button>    
                                                </div>
                                            </div>
                                    </form:form>
                                </div>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>   
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
        <jsp:include page="../template/jsinclude.jsp"/>
    </body>
</html>
<script>

    $.validator.addMethod('NICNumber', function (value) {
        return  /^[0-9]{9}[vVxX]$/.test(value);
    }, 'Please enter a valid National Identity Card Number.');


    $("#insertForm").validate({
        rules: {
//            username: "required",
            spassword: "required",
            password: {
                equalTo: "#spassword"
            },
            firstname: "required",
            lastname: "required",
            email: {
                required: true,
                email: true
            },
            mobile: {
                required: true,
                number: true
            },
            nic: {
                required: true,
                NICNumber: true,
                remote: {
                    url: "${pageContext.servletContext.contextPath}/admin/systemuser/exsistnic",
                    type: "post",
                    data: {
                        nic: function () {
                            return $('#nic').val();
                        }
                    }
                }
            },
            username: {
                required: true,
//                checkExists: true,
                remote: {
                    url: "${pageContext.servletContext.contextPath}/admin/systemuser/exsist",
                    type: "post",
                    data: {
                        username: function () {
                            return $('#username').val();
                        }
                    }
                }
            }
        },
        messages: {
            username: {
                remote: "Username already in use!"
            },
            nic: {
                remote: "NIC already taken"
            }

        }, errorPlacement: function (error, element) {
            element.parent().find('div.invalid').remove();
            element.parent().append('<div class="invalid">' + error.text() + '</div>');
        }
    });
</script>
<script>


</script>
