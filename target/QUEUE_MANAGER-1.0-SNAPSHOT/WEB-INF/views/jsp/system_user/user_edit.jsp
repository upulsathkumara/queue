<%-- 
    Document   : counter_edit
    Created on : Oct 16, 2016, 10:44:48 AM
    Author     : Kelum Madushan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
<!DOCTYPE html>

<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/admin_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/admin_menu.jsp"/>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        System User <small>edit</small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">          
                    <div id="container">
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                            <div class="box box-primary">
                                <form:form role="form" id="editForm" method="POST" modelAttribute="editForm" action="${pageContext.request.contextPath}/admin/systemuser/editeduser">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <form:label path="username">Username:</form:label>
                                            <form:input path="username" readonly="true" type="text" id="username" class="form-control" placeholder="Enter Username" onblur="check_availability()" value="${user[0].username}"></form:input>
                                                <div id="status"></div>   
                                            </div>
                                            <div class="form-group">
                                            <form:label  path="userrole">User Role:<samp style="color: red">*</samp></form:label>
                                            <form:select class="form-control" required="required" id="userrole" path="userrole">
                                                <form:option value="">Select user Role</form:option> 
                                                <c:forEach  items="${role}" var="role">
                                                    <c:choose>
                                                        <c:when test="${role.userroleid eq user[0].userrole}">
                                                            <form:option value="${role.userroleid}" selected="selected">${role.description}</form:option> 
                                                        </c:when>
                                                        <c:otherwise>
                                                            <form:option value="${role.userroleid}">${role.description}</form:option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                            </form:select>
                                        </div>
                                        <div class="form-group">
                                            <form:label path="firstname">First Name:<samp style="color: red">*</samp></form:label>
                                            <form:input path="firstname" value="${user[0].firstname}" type="text" id="firstname" class="form-control" placeholder="Enter First Name" maxlength="32"></form:input>
                                            </div>
                                            <div class="form-group">
                                            <form:label path="lastname">Last Name:<samp style="color: red">*</samp></form:label>
                                            <form:input path="lastname" value="${user[0].lastname}" type="text" id="lastname" class="form-control" placeholder="Enter Lastname" maxlength="32"></form:input>
                                            </div>
                                            <div class="form-group">
                                            <form:label path="mobile">Mobile:<samp style="color: red">*</samp></form:label>
                                            <form:input class="form-control" value="${user[0].mobile}" path="mobile" type="text" id="mobile" placeholder="Enter Mobile" maxlength="10"></form:input>
                                            </div>
                                            <div class="form-group">
                                            <form:label path="email">Email:</form:label>
                                            <form:input class="form-control" value="${user[0].email}" path="email" type="text" id="email" placeholder="Enter Email" maxlength="32"></form:input>
                                            </div>
                                            <div class="form-group">
                                            <form:label path="nic">NIC:<samp style="color: red">*</samp></form:label>
                                            <form:input class="form-control" value="${user[0].nic}" path="nic" type="text" id="nic" placeholder="Enter NIC" maxlength="12"></form:input>
                                            </div>
                                            <div class="box-footer btn-toolbar">
                                            <form:button type="button" class="btn btn-primary pull-right" onclick="window.history.back();">Cancel</form:button>
                                            <form:button id="createbutton" type="submit" class="btn btn-primary pull-right">Save</form:button>    
                                            </div>
                                        </div>
                                </form:form>
                            </div>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
        <jsp:include page="../template/jsinclude.jsp"/>
    </body>
</html>
<script>
$("#editForm").validate({
        rules: {
            firstname: "required",
            lastname: "required",
            userrole: "required",
            email: {
                required: true,
                email: true
            },
//            nic: "required",
            mobile:{
                required: true,
                number: true
            },
            
            mobile: "required",
            nic: {
                required: true,
                NICNumber: true
            }


        }, errorPlacement: function(error, element) {
                    element.parent().find('div.invalid').remove();
                    element.parent().append('<div class="invalid">' + error.text() + '</div>');
                }
    });
</script>
