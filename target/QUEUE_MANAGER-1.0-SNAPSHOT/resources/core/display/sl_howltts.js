window.slMfaPlay = {
    sounds: [],
    tn: null,
    pctc: null,
    playSequance: [],
    sequanceDelay: 500,
    dodebug: false,
    playing: null,
    docRoot: "",
    busy: false,
    onSequanceEnd: null,
    onSequanceStart: null,
    
    init: function( docRoot ){
        this.docRoot = docRoot;
        for (i = 0; i <= 20; i++) {
            var clip = new Howl({ src: [docRoot + i + ".mp3"], volume: 1 });
            this.sounds[i] = clip;
        }
        this.tn = new Howl({ src: [docRoot + "Token_Number.mp3"], volume: 1 });
        this.pctc = new Howl({ src: [docRoot + "Please_Come_To_Counter.mp3"], volume: 1 });
    },
    doSequance: function( no ){
        try {
            this.busy = true;
            no = ( no == null ) ? 0 : no;
            if ( no === 0 ){
                if ( slMfaPlay.onSequanceStart !== null ){ slMfaPlay.debug("On Sequance Start"); slMfaPlay.onSequanceStart(); }
            }
            if ( this.playing === no || no === 0 ){
                this.playSequance[no].on( "end", function(self){
                    slMfaPlay.debug( "End of item " + no );
                    slMfaPlay.playSequance[no].off( "end" );
                    if ( typeof slMfaPlay.playSequance[no+1] === "object" ){
                        slMfaPlay.playing = no + 1;
                        setTimeout( function(){ slMfaPlay.doSequance( no + 1 ); }, this.sequanceDelay );
                    } else {
                        slMfaPlay.busy = false;
                        slMfaPlay.cleanUp();
                        if ( slMfaPlay.onSequanceEnd !== null ){ slMfaPlay.debug("On Sequance End"); slMfaPlay.onSequanceEnd(); }
                    }
                } );
                slMfaPlay.debug( "Start of item " + no );
                this.playSequance[no].play();
            }
        } catch(e) {
            this.cleanUp();
            this.busy = false;
            this.onSequanceEnd();
        }
    },
    cleanUp: function(){
        this.playSequance = [];
    },
    playToken: function(){
        this.playSequance.push( this.tn );
    },
    playCounter: function(){
        this.playSequance.push( this.pctc );
    },
    playNumberToken: function( number ){
        try {
            number = parseInt(number, 10).toString();
            if ( number == parseInt(number, 10) ){
                //console.log(number);
                for (i = 0; i < number.length; i++) {
                    var no = parseInt( number.substr( i, 1 ) );
                    if ( no <= 20 ){
                        this.playSequance.push( this.sounds[no] );
                    }
                }
            } else {
                console.error( "Please enter a valid number." );
            }
        } catch(e) {
            console.error(e);
        }
    },
    playNumberCounter: function( number ){
        try {
            if ( number == parseInt(number, 10) ){
                //for (i = 0; i < number.length; i++) {
                    var no = number;
                    if ( no < 30 ){
                        //console.log(no);
                        if ( no > 20 ){
                            this.playSequance.push( this.sounds[20] );
                        } else {
                            this.playSequance.push( this.sounds[no] );
                        }
                    }
                    if ( no < 30 && no > 20 ){
                        this.playSequance.push( this.sounds[no - 20] );
                    }
                //}
            } else {
                console.error( "Please enter a valid number." );
            }
        } catch(e) {
            console.error(e);
        }
    },
    debug: function( message ){
        if ( this.dodebug ){
            console.info( message );
        }
    }
}