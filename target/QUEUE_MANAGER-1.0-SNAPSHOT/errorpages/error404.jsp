<%-- 
    Document   : reservationCreate
    Created on : Oct 12, 2016, 12:28:28 PM
    Author     : Rasika
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 


<html>
    <head>
        <jsp:include page="/WEB-INF/views/jsp/template/cssinclude.jsp"/>
        <jsp:include page="/WEB-INF/views/jsp/template/jsinclude.jsp"/>
        <script>
            var role = "<%=session.getAttribute("role")%>";
            if (role !== 'USER') {
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }
            var username = "<%=session.getAttribute("loggedUser")%>";
            if (!<%=session.getAttribute("loggedUser")%>) {
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            } else {
            }

        </script>

        
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="/WEB-INF/views/jsp/template/admin_header.jsp"/>

            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="/WEB-INF/views/jsp/template/error_menu.jsp"/>
            </aside>
            <!--            <div class="content-wrapper">
                            <section class="content" style="margin-right: -50%;">
                                <p>fffffffff</p>
                            </section>
                             /.content 
                        </div>-->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <!--                <section class="content-header">
                                    <h1 >404</h1>
                                    <h1 > File not found,sorry.</h1>
                                </section>-->

                <!-- Main content -->
                <section class="content">
                    <div class="error-page">
                        <h2 class="headline text-yellow"> 404</h2>

                        <div class="error-content">
                            <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

                            <p>
                                The Linked you followed may be broken, or 
                                the page may have been removed , either contact your 
                                Administrator or try again. Use your browsers Back button 
                                to navigate to the page you have previously come from 
                            </p>
                        </div>
                        <div class="col-sm-12" style="text-align: center">
                            <ul class="list-inline">
<!--                                <li>
                                    &nbsp;<a href="/AVN-CCL/login"><h3>Home</h3></a>&nbsp &nbsp;
                                </li>-->

                                <li>
                                    &nbsp;<a href="${pageContext.servletContext.contextPath}/"><h3>Login</h3></a>&nbsp &nbsp;
                                </li>

                                <li>
                                    &nbsp;<a href="" onclick="goBack()"><h3>Go Back </h3> </a>&nbsp &nbsp;
                                    <script>
                                        function goBack() {
                                            window.history.back();
                                        }
                                    </script>
                                </li>


                            </ul>
                        </div>
                        <!-- /.error-content -->
                    </div>
                    <!-- /.error-page -->
                </section>
                <!-- /.content -->

                
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="/WEB-INF/views/jsp/template/footer.jsp"/>
            </footer>
        </div>



    </body>
</html>

