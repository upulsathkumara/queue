
/**
 * GovSMSRoutingFaultException0.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

package lk.icta.schemas.xsd.kannel.handler.v1;

public class GovSMSRoutingFaultException0 extends java.lang.Exception{
    
    private lk.icta.schemas.xsd.kannel.handler.v1.MTSmsHandlerServiceStub.GovSMSRoutingFault faultMessage;
    
    public GovSMSRoutingFaultException0() {
        super("GovSMSRoutingFaultException0");
    }
           
    public GovSMSRoutingFaultException0(java.lang.String s) {
       super(s);
    }
    
    public GovSMSRoutingFaultException0(java.lang.String s, java.lang.Throwable ex) {
      super(s, ex);
    }
    
    public void setFaultMessage(lk.icta.schemas.xsd.kannel.handler.v1.MTSmsHandlerServiceStub.GovSMSRoutingFault msg){
       faultMessage = msg;
    }
    
    public lk.icta.schemas.xsd.kannel.handler.v1.MTSmsHandlerServiceStub.GovSMSRoutingFault getFaultMessage(){
       return faultMessage;
    }
}
    