

var tokens = m.prop([]);

var beeph = null;

function testToken(id, counter) {
    var token = { counterNo : "Counter " + counter, id: "465", tokenNo: "00" + id };
    playing_token(token);
    tokens().splice(0, 0, token);
    if(tokens().length > 7){
            tokens().pop();
    }
    m.redraw();
    playSound( token.tokenNo, token.counterNo.replace( "Counter ", "" ) );
}
//var test = 1249;
//var testc = 1;
var playing_token = m.prop(0);
var id = 1;
var times = 0;
var beep = true;
var pl = [];
var timeoutv = null;
function getToken() {
//    test += 1;
//    if ( testc < 29 ){
//        testc += 1;
//    } else {
//        testc = 0;
//    }
//    testToken(test, testc);
    var last_id = tokens().length===0 ? 0 : tokens()[0].id;
	$.ajax({
		url: window.location.pathname.replace("/getDisplay", "/getToken/")+last_id,
		dataType: "json",
		type: 'GET',
		success: function(token){ 
			if(token) {
				//console.log(token);
				playing_token(token);
				tokens().splice(0, 0, token);
				if(tokens().length > 7){
					tokens().pop();
				}
				m.redraw();
                                //console.log(token);
                                try {
                                    playSound( token.tokenNo, token.counterNo.replace( "Counter ", "" ) );
                                    timeout();
                                }catch(e){
                                    timeout();
                                }
               //beep();
			} else {
				//console.log('Failed to get token.');	
                                timeout();
			}
                    
		},
		error: function(data) {
			//console.log('Connection Err.');
			timeout();
		}
	});
}

function animateBlink(){
    $(".token-list").each( function(k, v){
        $( $(v).find(".token-container .token")[0] ).animate( { backgroundColor: "#ffcd00" }, 750, function(){
            $( $(v).find(".token-container .token")[0] ).animate( { backgroundColor: "#3c8dbc" }, 750 );
        } );
    } );
	if ( times < 3 ){
		times += 1;
		setTimeout( function(){ animateBlink(); }, 1500 );
    } else {
		times = 0;
	}
}

function timeout(){
//    if ( timeoutv !== null ){
//        clearTimeout( timeoutv );
//    }
    timeoutv = setTimeout(function(){
        getToken(); 
    }, 4000);
}

function playSound( token, counter ){
//    if ( timeoutv !== null ){
//        clearTimeout( timeoutv );
//    }
  animateBlink();
  if (beep) {
    beeph.play();
  } else {
    pl.push( { token: token, counter: counter } );
  }
}

//function doTTS(  ){
//    try{
//        if ( typeof pl[0] === "object" && slMfaPlay.busy === false ){
//            var item = pl[0];
//            pl.shift();
//            slMfaPlay.playToken();
//            slMfaPlay.playNumberToken(item.token);
//            slMfaPlay.playCounter();
//            slMfaPlay.playNumberCounter(item.counter);
//            slMfaPlay.doSequance();
//        }
//    } catch(e) {
//        console.error(e);
//    }
//}
//
//setInterval( function(){ doTTS(); }, 100 );

function pad(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length-size);
}

//function beep() {
//  var oscillator = audioCtx.createOscillator();
//  var gainNode = audioCtx.createGain();
//
//  oscillator.connect(gainNode);
//  gainNode.connect(audioCtx.destination);
//
//  gainNode.gain.value = 100;
//  oscillator.frequency.value = 60;
//  oscillator.type = type;
//
//  oscillator.start();
//
//  setTimeout(
//    function() {
//      oscillator.stop();
//    },
//    duration
//  );
//};

//$(document).ready(function(){
//    slMfaPlay.init( window.location.pathname.replace("/getDisplay", "/resources/core/display/sounds/") );
    beeph = new Howl({
        src: [window.location.pathname.replace("/getDisplay", "/resources/core/display/success.mp3")]
    });
//    beeph.on("end", function(){ timeout(); });
//    slMfaPlay.onSequanceEnd = function(){
//        getToken();
//    }
//});

var display = {};

display.controller = function(){
  this.tokens = tokens;
  this.playing_token = playing_token;
  getToken();
}

display.view = function(ctrl){
  return m('.display',
    m('.token-head.tokens' , m(".s", m("img", { src: window.location.pathname.replace("/getDisplay", "/resources/img/tn_s.PNG") })), m(".t", m("img", { src: window.location.pathname.replace("/getDisplay", "/resources/img/tn_t.PNG") }) ), m(".e", "Token Number") ),
    m('.token-head.counters' , m(".s", m("img", { src: window.location.pathname.replace("/getDisplay", "/resources/img/c_s.PNG") })), m(".t", m("img", { src: window.location.pathname.replace("/getDisplay", "/resources/img/c_t.PNG") }) ), m(".e", "Counter") ),
    m('.token-list.counters',
      ctrl.tokens().map(function(token){
        return m('.token-container', {key: token.id+'-'+token.tokenNo} ,
            m('.token', 
                m('.token-no', pad( token.counterNo.replace( "Counter ", "" ), 2) )
            )         
        )
      })
    ),
    m('.token-list.tokens',
      ctrl.tokens().map(function(token){
        return m('.token-container', {key: token.id+'-'+token.tokenNo} ,
            m('.token', 
                m('.token-no', pad( token.tokenNo, 4 ))/*,
                m('.counter-no', 'COUNTER '+token.counterNo)*/
            )         
        )
      })
    ),
    m('.logos',
        m('.gov', m("img", { src: window.location.pathname.replace("/getDisplay", "/resources/img/rsz_1govlogo.png") } )),
        m('.text', m("h3", "Ministry of Foreign Affairs, Sri Lanka."), m("h4", "Consular Affaris Division")),
        m('.icta', m("img", { src: window.location.pathname.replace("/getDisplay", "/resources/img/icta logo.png") } ))
    )
    /*,
    m('.main-board', 
        ctrl.playing_token()!=0 ? 
        m('.playing-token', 
            m('.token-no', 'Token No : '+ctrl.playing_token().tokenNo), 
            m('.counter-no', 'Counter No : '+ctrl.playing_token().counterNo)
        ) : 
        m('.screen', '')
        
    )*/
  );
}

m.mount(document.body, display);