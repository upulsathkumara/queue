<%-- 
    Document   : counter_edit
    Created on : Oct 16, 2016, 10:44:48 AM
    Author     : Kelum Madushan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
<!DOCTYPE html>

<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>

        <spring:url value="/resources/Datepicker/bootstrap-datetimepicker.css" var="datepicker" />
        <link href="${datepicker}" rel="stylesheet" />

        <script>
            var role = "<%=session.getAttribute("role")%>";
            if (role !== 'ADMIN') {
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }

            var username = "<%=session.getAttribute("loggedUser")%>"
            if (!<%=session.getAttribute("loggedUser")%>) {
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            } else {
            }

        </script>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/admin_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/admin_menu.jsp"/>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Holiday Management <small>edit</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div id="container">
                        <div class="row">           
                            <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                <div class="box box-primary">
                                    <form:form role="form" id="editForm" method="POST" modelAttribute="editForm" action="${pageContext.request.contextPath}/admin/holidaymanagement/editedholiday">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <form:input path="holidayid" type="hidden" id="holidayid" class="form-control" placeholder="Enter Description" value="${holiday[0].holidayid}"></form:input>   
                                                </div>
                                                <div class="form-group">
                                                <form:label path="holidaydate">Holiday:</form:label>
                                                <form:input path="holidaydate" required="required" type="text" id="datetimepicker" class="form-control" value="${holiday[0].holidaydate}" placeholder="Select Date" readonly="true" disabled="true"></form:input>  
                                                </div>
                                            <c:choose>
                                                <c:when test="${holiday[0].halfday == '0'}">
                                                    <div class="form-group row">
                                                        <div class="col-sm-2">
                                                            <form:label path="halfday">Half day:</form:label>
                                                            <form:checkbox id="halfday" onclick="halfdayclick()" path="halfday"></form:checkbox>
                                                            </div>
                                                            <div class="col-sm-5">
                                                            <form:label path="startTime">Start Time:</form:label>
                                                            <form:input path="startTime" disabled="true" required="required" type="text" id="datetimepickerStartTime" class="form-control" placeholder="Select Start Time" onchange="setEndHideTime()"></form:input>
                                                            </div>
                                                            <div class="col-sm-5">
                                                            <form:label path="endTime">End Time:</form:label>
                                                            <form:input path="endTime" disabled="true" required="required" type="text" id="datetimepickerEndTime" class="form-control" placeholder="Select End Time" onchange="setStartHideTime()"></form:input>
                                                            </div>
                                                        </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="form-group row">
                                                        <div class="col-sm-2">
                                                            <form:label path="halfday">Half day:</form:label>
                                                            <form:checkbox id="halfday" onclick="halfdayclick()"checked="true" value="true" path="halfday"></form:checkbox>
                                                            </div>
                                                            <div class="col-sm-5">
                                                            <form:label path="startTime">Start Time:</form:label>
                                                            <form:input path="startTime" value="${holiday[0].startTime}" required="required" type="text" id="datetimepickerStartTime" class="form-control" placeholder="Select Start Time" readonly="true"></form:input>
                                                            </div>
                                                            <div class="col-sm-5">
                                                            <form:label path="endTime">End Time:</form:label>
                                                            <form:input path="endTime" required="required" value="${holiday[0].endTime}" type="text" id="datetimepickerEndTime" class="form-control" placeholder="Select End Time" readonly="true"></form:input>
                                                            </div>
                                                        </div>
                                                </c:otherwise>
                                            </c:choose>
                                            <div class="form-group">
                                                <form:label path="holidaydescription">Description:</form:label>
                                                <form:input path="holidaydescription" required="required" value="${holiday[0].holidaydescription}" type="text" id="holidaydescription" class="form-control" placeholder="Enter Description"maxlength="32"></form:input>   
                                                </div>
                                                <div class="form-group">
                                                <form:label  path="status">Status:<samp style="color: red">*</samp></form:label>
                                                <form:select class="form-control" required="required" id="status" path="status">
                                                    <form:option value="">Select Status</form:option> 
                                                    <c:forEach  items="${status}" var="status">
                                                        <c:choose>
                                                            <c:when test="${status.statusCode eq holiday[0].status}">
                                                                <form:option value="${status.statusCode}" selected="selected">${status.statusDescription}</form:option> 
                                                            </c:when>
                                                            <c:otherwise>
                                                                <form:option value="${status.statusCode}">${status.statusDescription}</form:option>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </form:select>
                                            </div>

                                            <div class="box-footer btn-toolbar">
                                                <form:button type="button" class="btn btn-primary pull-right" onclick="window.history.back();">Cancel</form:button>
                                                <form:button id="createbutton" type="submit" class="btn btn-primary pull-right">Save</form:button>     
                                                </div>
                                            </div>
                                    </form:form>
                                </div>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>   
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>

        <jsp:include page="../template/jsinclude.jsp"/>

        <!--DatePicker-->
        <spring:url value="/resources/Datepicker/bootstrap-datetimepicker.min.js" var="Datepicker" />
        <script src="${Datepicker}"></script>
        <!--DatePicker end-->

        <script>
            $('#datetimepicker').datetimepicker({
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });

            $('#datetimepickerStartTime').datetimepicker({
                format: 'h:ii',
                autoclose: true,
                showMeridian: true,
                startView: 1,
                maxView: 2
            });

            $('#datetimepickerEndTime').datetimepicker({
                format: 'h:ii',
                autoclose: true,
                showMeridian: true,
                startView: 1,
                maxView: 2
            });

            function halfdayclick() {
                if (document.getElementById('halfday').checked) {
                    $("#datetimepickerStartTime").removeAttr("disabled");
                    $("#datetimepickerEndTime").removeAttr("disabled");
                } else {
                    $('#datetimepickerStartTime').val("");
                    $('#datetimepickerEndTime').val("");
                    $("#datetimepickerStartTime").attr("disabled", true);
                    $("#datetimepickerEndTime").attr("disabled", true);
                }
            }

            function setEndHideTime() {
                $('#datetimepickerEndTime').datetimepicker('setStartDate', $('#datetimepickerStartTime').val());
            }
            function setStartHideTime() {
                $('#datetimepickerStartTime').datetimepicker('setEndDate', $('#datetimepickerEndTime').val());
            }
        </script>
        <script>
            $('#editForm').validate({
                rules: {
                    holidaydescription: {
                        required: true
                    },
                    status: {
                        required: true
                    }
                }, errorPlacement: function(error, element) {
                    element.parent().find('div.invalid').remove();
                    element.parent().append('<div class="invalid">' + error.text() + '</div>');
                }
            });
        </script>
        <script>
            $(".sidebar-menu li").removeClass("selected");
            $(".sidebar-menu  .holyday-mgmt").addClass("selected");

        </script>
    </body>
</html>
