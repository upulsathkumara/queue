<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
    <head>
        <jsp:include page="template/cssinclude.jsp"/>
        <script>
            var role = "<%=session.getAttribute("role")%>";
            if(role !== 'ADMIN'){
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }
    
            var username = "<%=session.getAttribute("loggedUser")%>"
            if(!<%=session.getAttribute("loggedUser")%>){
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            }else{
            }

        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="template/admin_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="template/admin_menu.jsp"/>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="template/footer.jsp"/>
            </footer>
        </div>
        <jsp:include page="template/jsinclude.jsp"/>
    </body>
</html>
