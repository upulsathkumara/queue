<%-- 
    Document   : counter_list
    Created on : Oct 13, 2016, 11:00:55 AM
    Author     : Kelum Madushan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
<!DOCTYPE html>

<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>

        <!--Data Table-->
        <spring:url value="/resources/DataTables/media/js/jquery-1.12.0.min.js" var="jqueryJs" />
        <script src="${jqueryJs}"></script>
        <spring:url value="/resources/DataTables/media/js/jquery.dataTables.min.js" var="jqueryJs1" />
        <script src="${jqueryJs1}"></script>
        <spring:url value="/resources/DataTables/media/css/jquery.dataTables.min.css" var="datatableCss" />
        <link href="${datatableCss}" rel="stylesheet" />
        <!--Data Table ends-->
        <script>
            var role = "<%=session.getAttribute("role")%>";
            if(role !== 'ADMIN'){
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }
    
            var username = "<%=session.getAttribute("loggedUser")%>"
            if(!<%=session.getAttribute("loggedUser")%>){
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            }else{
            }

        </script>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/admin_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/admin_menu.jsp"/>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Service Counter Management <small>List</small>
                    </h1>
                    <div class="col-sm-6">
                        <c:if test="${not empty msg}">
                            <div class="msg">${msg}</div>
                        </c:if>
                    </div>
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3"></div>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div id="container">
                        <a href="${pageContext.servletContext.contextPath}/admin/servicecountermanagement/addcounter" class="btn btn-primary pull-right" role="button" style="margin-bottom:10px;">New Counter</a>
                        <table id="list" class="display table table-bordered table-striped dataTable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><div align="center">Service Desk ID</div></th>
                            <th><div align="center">Description</div></th>
                            <th><div align="center">Status</div></th>
                            <th><div align="center">Edit</div></th>
                            <th><div align="center">Delete</div></th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${counterList}" var="counterList">
                                    <tr>
                                        <td>${counterList.sdid}</td>
                                        <td>${counterList.description}</td>
                                        <td>${counterList.statusDes}</td>
                                        <td>
                                            <c:set var="count" value="${count+1}" scope="session"/>
                                            <form id="${count}" name="updateForm" method="POST" action ="${pageContext.servletContext.contextPath}/admin/servicecountermanagement/editcounter" commandName="updateForm">
                                                <input type="hidden" name="sdid" value="${counterList.sdid}">
                                                <div align="center"><a href="javascript:{}" id="updateRow" onclick="document.getElementById(${count}).submit();
                                                        return false;"><span class="glyphicon glyphicon-edit"></span></a>
                                                </div>
                                            </form>
                                        </td>
                                        <td>
                                            <c:set var="count" value="${count+1}" scope="session"/>
                                            <form id="${count}" name="deleteForm" method="POST" action = "${pageContext.servletContext.contextPath}/admin/servicecountermanagement/deletecounter" commandName="deleteForm" >
                                                <input type="hidden" name="sdid" value="${counterList.sdid}">
                                                <div align="center"><a href="#" id="smart-mod-eg1" onclick="myFunction${count}();"><span class="glyphicon glyphicon-trash"></span></a></div>
                                                <script>
                                                    function myFunction${count}() {

                                                        $('#myModal').modal('show');
                                                        $('[id="deletebtn"]').click(function() {
                                                            document.getElementById(<c:out value="${count}"/>).submit();
                                                            $('#myModal').modal('hide');

                                                        });

                                                    }
                                                </script>
                                            </form>
                                        </td>
                                    </tr>
                                </c:forEach>
                            <div class="modal" id="myModal"> 
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" style="color: #367fa9">×</span></button>
                                            <h4 class="modal-title" style="color: #367fa9"><span class="glyphicon glyphicon-exclamation-sign"></span> Confirmation for Delete!</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p style="color: #367fa9" ><b>Are you sure you want to delete this record?</b></p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary" id="deletebtn">Delete</button>
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </tbody>
                        </table>
                        <script>
                            $('#list').dataTable();
                        </script> 
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
        <jsp:include page="../template/jsinclude.jsp"/>
    </body>
</html>
<style>
    .msg {
        padding: 15px;
        margin-bottom: 20px;
        margin-top: 15px;
        margin-left: -14px;
        border: 1px solid transparent;
        border-radius: 4px;
        color: #31708f;
        background-color: #d9edf7;
        border-color: #bce8f1;
    }
</style>
