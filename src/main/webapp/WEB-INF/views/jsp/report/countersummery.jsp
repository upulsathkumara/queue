<%-- 
    Document   : counter_create
    Created on : Oct 13, 2016, 3:18:54 PM
    Author     : Kelum Madushan
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%> 
<!DOCTYPE html>


<html>
    <head>
        <jsp:include page="../template/cssinclude.jsp"/>

        <spring:url value="/resources/Datepicker/bootstrap-datetimepicker.css" var="datepicker" />
        <link href="${datepicker}" rel="stylesheet" />
        <script>
            var role = "<%=session.getAttribute("role")%>";
            if(role !== 'ADMIN'){
                window.location.href = "${pageContext.servletContext.contextPath}/unotherized";
            }
    
            var username = "<%=session.getAttribute("loggedUser")%>"
            if(!<%=session.getAttribute("loggedUser")%>){
                window.location.href = "${pageContext.servletContext.contextPath}/sessionout";
            }else{
            }

        </script>

    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <jsp:include page="../template/admin_header.jsp"/>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <jsp:include page="../template/admin_menu.jsp"/>
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Report Management  <small>Counter Summary</small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    <div id="container">
                        <div class="row">           
                            <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                <div class="box box-primary">
                                    <form:form role="form" id="insertForm" method="POST" modelAttribute="insertForm" action="${pageContext.request.contextPath}/counterreport/pdf">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label>Counter: </label>
                                                <form:select  class="form-control" id="countername" path="countername" items="${counterList}"/>
                                            </div>
                                            <div class="form-group">
                                                <label>From Date </label>
                                                <form:input path="fromdate" required="required" type="text" id="datetimepicker" class="form-control" placeholder="Select Date" readonly="true"></form:input>  
                                                </div>
                                                <div class="form-group">
                                                    <label>To Date </label>
                                                <form:input path="todate" required="required" type="text" id="datetimepicker2" class="form-control" placeholder="Select Date" readonly="true"></form:input>  
                                                </div>
                                                <div class="box-footer btn-toolbar">
                                                <form:button id="createbutton" type="submit" class="btn btn-primary pull-right">Generate Report</form:button>    
                                                </div>
                                            </div>
                                    </form:form>
                                </div>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>   
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <jsp:include page="../template/footer.jsp"/>
            </footer>
        </div>
        <jsp:include page="../template/jsinclude.jsp"/>

        <!--DatePicker-->
        <spring:url value="/resources/Datepicker/bootstrap-datetimepicker.min.js" var="Datepicker" />
        <script src="${Datepicker}"></script>
        <!--DatePicker end-->

        <script>
            $('#datetimepicker').datetimepicker({
                weekStart: 0,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });
            $('#datetimepicker2').datetimepicker({
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: "yyyy-mm-dd"
            });
            var dates = new Date();
            var yy = dates.getFullYear();
            var mm = dates.getMonth();
            var dd = dates.getDate();
            $('#datetimepicker').datetimepicker('setEndDate', new Date(yy, mm, dd));
            $('#datetimepicker').datetimepicker('setDate', new Date(yy, mm, dd));
            $('#datetimepicker2').datetimepicker('setDate', new Date(yy, mm, dd));
            $('#datetimepicker').datetimepicker().on('changeDate', function (ev) {
                loadToDate();
            });

            function loadToDate() {
                var date = new Date($('#datetimepicker').val());
                var yy = date.getFullYear();
                var mm = date.getMonth();
                var dd = date.getDate();
//                $('#datetimepicker2').datetimepicker('setDate', new Date(yy, mm, dd));
                $('#datetimepicker2').datetimepicker('setStartDate', new Date(yy, mm, dd));
                $('#datetimepicker2').datetimepicker('setEndDate', new Date());
            }

            $('#datetimepickerStartTime').datetimepicker({
                format: 'h:ii',
                autoclose: true,
                showMeridian: true,
                startView: 1,
                maxView: 2
            });
            $('#datetimepickerEndTime').datetimepicker({
                format: 'h:ii',
                autoclose: true,
                showMeridian: true,
                startView: 1,
                maxView: 2
            });

//            $('#insertForm').validate({
//                onkeyup: function (element) {
//                    $(element).valid();
//                },
//                onfocusout: function (element) {
//                    $(element).valid();
//                },
//                rules: {
//                    fromdate: {
//                        required: true
//                    },
//                    todate: {
//                        required: true
//                    },
//                    countername: {
//                        required: true
//                    }
//                }
//            });

        </script>
        <script>
            $(".sidebar-menu li").removeClass("selected");
            $(".sidebar-menu  .report-mgmt").addClass("selected");
            $(".sidebar-menu  .report-mgmt .counter-summery").addClass("selected");
        </script>

    </body>
</html>
