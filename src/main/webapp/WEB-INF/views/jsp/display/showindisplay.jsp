<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<title>Token Display</title>
<spring:url value="/resources/core/display/mithril.min.js" var="mithril" />
<spring:url value="/resources/core/display/jquery-3.1.1.min.js" var="jquery" />
<spring:url value="/resources/core/display/jquery.animate-colors-min.js" var="jqueryColor" />
<spring:url value="/resources/core/display/style.css" var="disstyles" />
<spring:url value="/resources/core/display/howler.core.js" var="howler" />
<!--spring:url value="/resources/core/display/sl_howltts.js" var="slMfaPlay" /-->
<link href="${disstyles}" rel="stylesheet" />
<script src="${mithril}"></script>
<script src="${jquery}"></script>
<script src="${jqueryColor}"></script>
<script src="${howler}"></script>
<!--script src="${slMfaPlay}"></script-->
<body>
<spring:url value="/resources/core/display/app.js" var="app" />
<script src="${app}"></script>

<!--spring:url value="/resources/sounds/beep.mp3" var="app2" />
<p>Test</p>
<audio id="myAudio">
  <source src="${app2}" type="audio/mpeg">
  Your browser does not support the audio element.
</audio-->

</body>
