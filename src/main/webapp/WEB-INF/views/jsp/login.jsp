<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Ministry of Foreign Affairs | Login</title>
        <spring:url value="/resources/core/css/hello.css" var="coreCss" />
        <spring:url value="/resources/core/css/bootstrap.min.css" var="bootstrapCss" />
        <spring:url value="/resources/core/css/site.min.css" var="siteCss" />
        <spring:url value="/resources/core/css/template.css" var="templateCss" />
        <spring:url value="/resources/core/css/jquery-ui.min.css" var="jqueryuiminCss" />
        <spring:url value="/resources/core/css/style.css" var="styleCss" />
        <spring:url value="/resources/core/css/home.css" var="homeCss" />
        <link href="${coreCss}" rel="stylesheet" />
        <link href="${bootstrapCss}" rel="stylesheet" />
        <link href="${siteCss}" rel="stylesheet" />
        <link href="${templateCss}" rel="stylesheet" />
        <link href="${jqueryuiminCss}" rel="stylesheet" />
        <link href="${styleCss}" rel="stylesheet" />
        <link href="${homeCss}" rel="stylesheet" />
        <spring:url value="/resources/core/js/site.min.js" var="siteJs" />
        <spring:url value="/resources/core/js/jquery.min.js" var="jqueryminJs" />
        <spring:url value="/resources/core/js/hello.js" var="coreJs" />
        <spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
        <spring:url value="/resources/core/js/bootstrap-notify.min.js" var="bootstrapJsNotify" />
        <spring:url value="/resources/core/js/scanner.js" var="scanner" />
        <script src="${jqueryminJs}"></script>
        <script src="${coreJs}"></script>
        <script src="${bootstrapJs}"></script>
        <!--script src="${bootstrapJsNotify}"></script-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
        <script src="${siteJs}"></script>
        <script src="${scanner}"></script>
        <spring:url value="/resources/core/js/jquery.blockUI.js" var="uiBlock" />
        <script src="${uiBlock}"></script>

        <script type="text/javascript">

            function getLocalIP(successful, mesg, response) {
                if (!successful) { // On error
                    $("#login-form .error").remove();
                    $("#login-form").prepend("<div class=\"error\">" + mesg + "</div>");
                    return;
                }
                if (successful && mesg !== null) {
                    var response = $.parseJSON(mesg);
                    if (response.STATUS) {
                        $("#get_ip").val(response.param.ip);
                        $("#login-form button[type=submit]").removeAttr("disabled");
                    } else {
                        $("#login-form .error").remove();
                        $("#login-form").prepend("<div class=\"error\">" + response.MSG + "</div>");
                    }
                    return;
                }
            }

            $(document).ready(function () {
                $(document).ajaxStart(function () {
                    $.blockUI({css: {
                            border: 'transparent',
                            backgroundColor: 'transparent'
                        },
                        message: '<img height="100" width="100" src="${pageContext.request.contextPath}/src/main/webapp/resources/img/loading.gif" />',
                        baseZ: 2000
                    });

                });
                $(document).ajaxStop(function () {
                    $.unblockUI();
                });

            });


        </script>

        <script>
            $(document).ready(function () {
                $('.login-menu a').click(function (e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var href = $(this).attr('href');
                    href = href + '?name=' + id;
                    if (href === '#') {
                        scrollToID(id, 1000);
                    } else {
                        window.location.href = href;
                    }
                });
                
            });
        </script>

    </head>
    <body>
        <div class="container-fluid">
            <div class="row login-header">
                <div class="col-md-2 col-xs-3 title">
                    <img class="login-logo" src="${pageContext.servletContext.contextPath}/resources/img/Emblem_of_Sri_Lanka.png">
                    <!--<img src="${pageContext.servletContext.contextPath}/resources/img/user.png" class="img-circle" alt="User Image" style="border: none;">-->
                </div>
                <div class="col-md-4 col-xs-9 login-header-text">
                    <p>Electronic Document Attestation System</p>
                </div>	
            </div>
            <script>
                $('#org_button').click(function () {
                    window.location.href = "";
                });
                $('#user_button').click(function () {
                    window.location.href = "";
                });
            </script>
            <style>
                .error {
                    padding: 15px;
                    margin-bottom: 20px;
                    border: 1px solid transparent;
                    border-radius: 4px;
                    color: #a94442;
                    background-color: #f2dede;
                    border-color: #ebccd1;
                }

                .msg {
                    padding: 15px;
                    margin-bottom: 20px;
                    border: 1px solid transparent;
                    border-radius: 4px;
                    color: #31708f;
                    background-color: #d9edf7;
                    border-color: #bce8f1;
                }


            </style>

            <div class="row">
                <div class="col-md-12">

                    <div class="col-md-3"></div>
                    <div class="col-md-6 signin-blk">
                        <div class="signin-title">Sign In</div>
                        <form name='loginForm' action="${pageContext.request.contextPath}/loginAuth" method='POST' id="login-form" class="smart-form client-form">
                            <c:if test="${not empty error}">
                                <div class="error">${error}</div>
                            </c:if>
                            <c:if test="${not empty msg}">
                                <div class="msg">${msg}</div>
                            </c:if>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email">Username:</label>
                                    <input type="text" class="form-control" name="username">
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Password:</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                                <input type="hidden" name="ip" id="get_ip" />
                            </div>

                            <div class="col-md-12" style="text-align: center">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-default btn-block" >Submit</button>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>

                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-10" >
                                        <br/>

                                        <label for="" style="font-size: 10px;">R1.02</label></div>

                                    <div class="col-md-1"></div>


                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
    </body>
</html>
