<%-- 
    Document   : admin_menu
    Created on : Oct 12, 2016, 3:34:20 PM
    Author     : Kelum Madushan
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="${pageContext.servletContext.contextPath}/resources/img/Emblem_of_Sri_Lanka.png"  alt="User Image">
        </div>
        <div class="pull-left info">
            <p style="font-size: 17px;">Ministry of Foreign</p>
            <p style="font-size: 17px;">Affairs</p>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->

</section>
<!-- /.sidebar -->
<style>
    .selected { background-color: #0a1013}
</style>

