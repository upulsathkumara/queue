<%-- 
    Document   : menu
    Created on : Oct 12, 2016, 12:11:09 PM
    Author     : Rasika
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="${pageContext.servletContext.contextPath}/resources/img/Emblem_of_Sri_Lanka.png"  alt="User Image">
        </div>
        <div class="pull-left info">
            <p style="font-size: 17px;">Ministry of Foreign</p>
            <p style="font-size: 17px;">Affairs</p>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul id="sidemenu" class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>

        <li class="treeview selected walking">
            <a href="${pageContext.servletContext.contextPath}/reservation/create">
                <i class="icon-blind"></i> <span>Walk-in Customer</span>
            </a>

        </li>
        <li class="treeview priority">
            <a href="${pageContext.servletContext.contextPath}/reservation/vip/create">
                <i class="icon-crown"></i> <span>Priority Appointment</span>
            </a>

        </li>

        <li class="treeview issue-tikcet">
            <a href="${pageContext.servletContext.contextPath}/issuetickets/create">
                <i class="icon-qrcode"></i> <span>Issue Queue Tickets</span>
            </a>

        </li>
        <li class="treeview re-issue">
            <a href="${pageContext.servletContext.contextPath}/issuetickets/reissue">
                <i class="icon-qrcode"></i> <span>Reissue Tokens</span>
            </a>

        </li>

        <li class="treeview online-customer">
            <a href="${pageContext.servletContext.contextPath}/reservation/online">
                <i class="icon-toggle-on"></i> <span>Online Customer</span>
            </a>

        </li>
        
        <c:if test="${sessionScope.token != null}">

            <li class="treeview reshedule-appoinments">
                <a href="${pageContext.servletContext.contextPath}/reservation/immediate">
                    <i class="icon-stopwatch"></i> <span>Reschedule Appointments</span>
                </a>

            </li>
        </c:if>
        <c:if test="${sessionScope.token != null}">
            <li class="treeview">

                <form name="my_form" id="my_form" action="http://localhost:8080/QUEUE_MANAGER/loginToQms" method="POST">

                    <input type="text" hidden="true" name="username" value="<%= session.getAttribute("loggedUser")%>">
                    <input type="text" hidden="true" name="token" value="<%= session.getAttribute("token")%>">
                    <a href="javascript:{}" onclick="document.getElementById('my_form').submit();
                            return false;">
                        <i class="icon-toggle-on"></i> <span>Back to Help Desk</span>
                    </a>
                </form>

            </li>
        </c:if>

    </ul>
</section>
<!-- /.sidebar -->
<style>
    .selected { background-color: #0a1013}
</style>
