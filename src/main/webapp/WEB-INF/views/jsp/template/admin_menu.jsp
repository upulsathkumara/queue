<%-- 
    Document   : admin_menu
    Created on : Oct 12, 2016, 3:34:20 PM
    Author     : Kelum Madushan
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="${pageContext.servletContext.contextPath}/resources/img/Emblem_of_Sri_Lanka.png"  alt="User Image">
        </div>
        <div class="pull-left info">
            <p style="font-size: 17px;">Ministry of Foreign</p>
            <p style="font-size: 17px;">Affairs</p>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul id="sidemenu" class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <!--        <li id="dashboard" class="treeview">
                    <a href="#">
                        <i class="glyphicon glyphicon-folder-open"></i> <span>Dashboard</span>
                    </a>
                </li>-->
        <!--        <li class="treeview">
                    <a href="${pageContext.servletContext.contextPath}/admin/servicecountermanagement/getcounterlist">
                        <i class="glyphicon glyphicon-folder-open"></i> <span>Service Desk Management</span>
                    </a>
        
                </li>-->
        <!--        <li class="treeview">
                    <a href="${pageContext.servletContext.contextPath}/admin/applicationcountermanagement/getcounterlist">
                        <i class="glyphicon glyphicon-folder-open"></i> <span>Front Desk Management</span>
                    </a>
        
                </li>-->
        <li class="treeview priority-req">
            <a href="${pageContext.servletContext.contextPath}/reservation/vip/triger/create">
                <i class="icon-address-card"></i> <span>Priority Request</span>
            </a>

        </li>
        <li class="treeview holyday-mgmt">
            <a href="${pageContext.servletContext.contextPath}/admin/holidaymanagement/getholidaylist">
                <i class="icon-calendar"></i> <span>Holiday Management</span>
            </a>

        </li>
        <!--        <li class="treeview">
                    <a href="${pageContext.servletContext.contextPath}/admin/systemuser/getsystemuserlist">
                        <i class="glyphicon glyphicon-folder-open"></i> <span>System User Management</span>
                    </a>
        
                </li>-->
        <li class="treeview ins-mgmt">
            <a href="${pageContext.servletContext.contextPath}/admin/instructionmanagement/getinsttructionlist">
                <i class="icon-info-circled-alt"></i> <span>Instruction Management</span>
            </a>

        </li>
        <li class="treeview common-config">
            <a href="${pageContext.servletContext.contextPath}/admin/workinghour/create">
                <i class="icon-clock"></i> <span>Common Configuration</span>
            </a>

        </li>
        <li class="treeview report-mgmt">
            <a href="#">
                <i class="icon-doc-text"></i> <span>Report Management</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul id="report_menu" class="treeview-menu">
                <li id="applicationdetails" class="counter-summery"><a href="${pageContext.servletContext.contextPath}/admin/report/sample" ><i class="glyphicon glyphicon-triangle-right"></i>Counter Summary Reports</a></li>
                 <li id="applicationdetails" class="applicant-info"><a href="${pageContext.servletContext.contextPath}/admin/report/countersummarybyhours" ><i class="glyphicon glyphicon-triangle-right"></i>Counter Summary By Hours</a></li>
                <li id="applicationdetails" class="applicant-summery"><a href="${pageContext.servletContext.contextPath}/admin/report/applicationsummery" ><i class="glyphicon glyphicon-triangle-right"></i>Applicant summary report</a></li>
                <li id="applicationdetails" class="applicant-info"><a href="${pageContext.servletContext.contextPath}/admin/report/applicantdetails" ><i class="glyphicon glyphicon-triangle-right"></i>Applicant Details Reports</a></li>
            </ul>
            <!--            <ul id="report_menu" class="treeview-menu">
                            <li id="applicationdetails" ><a href="${pageContext.servletContext.contextPath}/admin/report/applicationsummery" ><i class="glyphicon glyphicon-triangle-right"></i>Applicant summary report</a></li>
                        </ul>-->
            <!--            <ul id="report_menu" class="treeview-menu">
                            <li id="applicationdetails" ><a href="${pageContext.servletContext.contextPath}/admin/report/applicantdetails" ><i class="glyphicon glyphicon-triangle-right"></i>Applicant Details Reports</a></li>
                        </ul>
                        <ul id="report_menu" class="treeview-menu">
                            <li id="applicationdetails" ><a href="${pageContext.servletContext.contextPath}/admin/report/userdurationreports" ><i class="glyphicon glyphicon-triangle-right"></i>User duration details</a></li>
                        </ul>-->
        </li>
        <li class="treeview booking-cancel">
            <a href="${pageContext.servletContext.contextPath}/admin/bookinglist/getlist">
                <i class="icon-clock"></i> <span>Cancel Reservations</span>
            </a>
        </li>
        
        <li class="treeview reshedule-appoinments">
                <a href="${pageContext.servletContext.contextPath}/reservation/reschdule/approval">
                    <i class="icon-stopwatch"></i> <span>Approval Appointments</span>
                </a>

            </li>

        <!--        <li class="treeview">
                    <a href="${pageContext.servletContext.contextPath}/admin/report/sample">
                        <i class="glyphicon glyphicon-folder-open"></i> <span>Counter Summery Reports</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="${pageContext.servletContext.contextPath}/admin/report/applicationsummery">
                        <i class="glyphicon glyphicon-folder-open"></i> <span>Applicant summery report</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="${pageContext.servletContext.contextPath}/admin/report/applicantdetails">
                        <i class="glyphicon glyphicon-folder-open"></i> <span>Applicant Details Reports</span>
                    </a>
                </li>-->
        <!--        <li class="treeview">
                    <a href="${pageContext.servletContext.contextPath}/admin/report/userdurationreports">
                        <i class="glyphicon glyphicon-folder-open"></i> <span>User duration details</span>
                    </a>
                </li>-->
    </ul>
</section>
<!-- /.sidebar -->
<style>
    .selected { background-color: #0a1013}
</style>

