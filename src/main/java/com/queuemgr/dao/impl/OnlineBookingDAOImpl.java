/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.dao.impl;

import com.queuemgr.modals.BookingVerification;
import com.queuemgr.modals.Customer;
import com.queuemgr.modals.DocumentType;
import com.queuemgr.util.LogFileCreator;
import com.queuemgr.util.StatusVarList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author user
 */
public class OnlineBookingDAOImpl {

    private DataSource dataSource;
//    private Connection connection;
//    private PreparedStatement statement;
//    private ResultSet resultSet;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<DocumentType> getOnlineBookinDocument(int bookingId) throws Exception {
        List<DocumentType> documentList = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {

            String sql = "SELECT "
                    + "     documenttype.DESCRIPTION,"
                    + "     bookingdocuments.DOCUMENTTYPEID,"
                    + "     bookingdocuments.QUANTITY "
                    + " FROM "
                    + "     documenttype "
                    + " INNER JOIN "
                    + "     bookingdocuments "
                    + " ON "
                    + "     bookingdocuments.DOCUMENTTYPEID=documenttype.DOCUMENTTYPEID "
                    + " WHERE "
                    + "     bookingdocuments.BOOKINGID=?";

            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            resultSet = statement.executeQuery();
            documentList = new ArrayList();
            DocumentType doc;
            while (resultSet.next()) {
                doc = new DocumentType();
                doc.setDescription(resultSet.getString("documenttype.DESCRIPTION"));
                doc.setDocumentTypeId(resultSet.getString("bookingdocuments.DOCUMENTTYPEID"));
                doc.setQuantity(resultSet.getString("bookingdocuments.QUANTITY"));
                documentList.add(doc);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return documentList;
    }

    public List<DocumentType> getOnlineBookinDocuments(int bookingId) throws Exception {
        List<DocumentType> documentList = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {

            String sql = "SELECT "
                    + "DOCUMENTTYPEID, "
                    + "QUANTITY "
                    + "FROM "
                    + "bookingdocuments "
                    + "WHERE "
                    + "bookingdocuments.BOOKINGID=?";

            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            resultSet = statement.executeQuery();
            documentList = new ArrayList();
            DocumentType doc;
            while (resultSet.next()) {
                doc = new DocumentType();
                doc.setDocumentTypeId(resultSet.getString("DOCUMENTTYPEID"));
                doc.setQuantity(resultSet.getString("QUANTITY"));
                documentList.add(doc);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return documentList;
    }

    public List<BookingVerification> getOnlineBookinDocumentVerification(int bookingId) throws Exception {
        List<BookingVerification> verificationtList = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {

            String sql = "SELECT "
                    + "BOOKINGVERIFICATIONDOCID,"
                    + "DOCUMENTTYPE,"
                    + "REFERENCE,"
                    + "STATUS,"
                    + "DOCUMENTCHARECODE "
                    + "FROM "
                    + "bookingverificationdocs "
                    + "WHERE "
                    + "BOOKINGID=?";

            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            resultSet = statement.executeQuery();
            verificationtList = new ArrayList();
            BookingVerification verification;
            while (resultSet.next()) {
                verification = new BookingVerification();
                verification.setBookingVerificationId(resultSet.getInt("BOOKINGVERIFICATIONDOCID"));
                verification.setDocumentTypeId(resultSet.getString("DOCUMENTTYPE"));
                verification.setReference(resultSet.getString("REFERENCE"));
                verification.setStatus(resultSet.getString("STATUS"));
                verification.setCharge(resultSet.getString("DOCUMENTCHARECODE"));
                verificationtList.add(verification);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return verificationtList;
    }

    public synchronized void BookinDocumentVerificationUpdate(Customer customer, String loggedUser, List<BookingVerification> bookingVerification, int bookingId, String loggedCounter) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        try {

            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            String sql = "UPDATE "
                    + "     bookings "
                    + " SET "
                    + "     BOOKINGSTATUS = ? ,"
                    + "     APPLICATIONCOUNTER  = ? ,"
                    + "     LASTUPDATEDDATETIME  = NOW(), "
                    + "     APPLICATIONTYPEID=?,"
                    + "     BOOKINGSERVICESTATUS=?,"
                    + "     LASTUPDATEDUSER=?"
                    + " WHERE "
                    + "     BOOKINGID =? ";
            statement = connection.prepareStatement(sql);
            statement.setString(1, StatusVarList.BOOKING_CONFIRMED);
            statement.setString(2, loggedCounter);
            statement.setInt(3, Integer.parseInt(customer.getApplicationType()));
            if (StatusVarList.APPOINMENT_TYPE_IMMEDIATE.equals(customer.getAppoinmentType())) {
                statement.setString(4, StatusVarList.FRONT_DESK_AWAITING_APPROVAL);
            } else if (StatusVarList.APPOINMENT_TYPE_NORMAL.equals(customer.getAppoinmentType())) {
                statement.setString(4, StatusVarList.FRONT_DESK_NORMAL);
            } else {
                statement.setString(4, StatusVarList.PENDING);
            }
            statement.setString(5, loggedUser);
            statement.setInt(6, bookingId);

            statement.execute();
            sql = "DELETE "
                    + "FROM "
                    + "     bookingverificationdocs "
                    + "WHERE "
                    + "     BOOKINGID=?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            statement.execute();
            for (int j = 0; j < bookingVerification.size(); j++) {
                sql = "INSERT "
                        + "INTO "
                        + "     bookingverificationdocs "
                        + "    ( "
                        + "        BOOKINGID,"
                        + "        DOCUMENTTYPE, "
                        + "        REFERENCE "
                        + "    ) "
                        + "    VALUES "
                        + "    ( ?, ?, ?)";
                statement = connection.prepareStatement(sql);
                statement.setInt(1, bookingId);
                statement.setString(2, bookingVerification.get(j).getDocumentTypeId().trim());
                statement.setString(3, bookingVerification.get(j).getReference().trim());
                statement.execute();
            }

            sql = "UPDATE customer "
                    + "   JOIN "
                    + "     bookings "
                    + "   ON "
                    + "     customer.CUSTOMERID = bookings.CUSTOMERID "
                    + "   SET "
                    + "     FIRSTNAME=?,"
                    + "     IDENTIFICATIONNUMBER=?, "
                    + "     EMAIL=?, "
                    + "     MOBILE=?, "
                    + "     ADDRESSLINE1=?, "
                    + "     ADDRESSLINE2=?, "
                    + "     ADDRESSLINE3=?, "
                    + "     PREFFEREDLANG=?,"
                    + "     customer.LASTUPDATEDUSER=?,"
                    + "     customer.LASTUPDATEDDATETIME=NOW(),"
                    + "     IDENTIFICATIONTYPECODE=? "
                    + "   WHERE "
                    + "     bookings.BOOKINGID=?";

            statement = connection.prepareStatement(sql);
            statement.setString(1, customer.getFristName().trim());
            statement.setString(2, customer.getIdentificationNumber().trim());
            statement.setString(3, customer.getEmail().trim());
            statement.setString(4, customer.getTelephoneMobile().trim());
            statement.setString(5, customer.getAddressLine1().trim());
            statement.setString(6, customer.getAddressLine2().trim());
            statement.setString(7, customer.getAddressLine3().trim());
            statement.setString(8, customer.getPreferredLanguage().trim());
            statement.setString(9, loggedUser);
            statement.setString(10, customer.getIdentificatioDocumentType().trim());
            statement.setInt(11, Integer.parseInt(customer.getBookingId()));
            statement.execute();

            if (StatusVarList.APPOINMENT_TYPE_IMMEDIATE.equals(customer.getAppoinmentType())) {
                sql = "INSERT "
                        + "INTO "
                        + " reschedulingreasons "
                        + "  ("
                        + "     BOOKINGID, "
                        + "     REASON, "
                        + "     CREATEDDATETIME, "
                        + "     CREATEDUSER"
                        + "   ) "
                        + " VALUES "
                        + " ("
                        + "   ?,?,NOW(),?  "
                        + " )";
                statement = connection.prepareStatement(sql);
                statement.setString(1, customer.getBookingId().trim());
                statement.setString(2, customer.getReason().trim());
                statement.setString(3, loggedUser);
                statement.execute();
            }

            connection.commit();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            connection.rollback();
            throw ex;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
    }

}
