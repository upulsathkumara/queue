/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.dao.impl;

import com.queuemgr.modals.AllBookings;
import com.queuemgr.modals.BookingTimes;
import com.queuemgr.modals.BookingVerification;
import com.queuemgr.modals.Bookings;
import com.queuemgr.modals.CommonConfigurations;
import com.queuemgr.modals.Customer;
import com.queuemgr.modals.DocumentCharges;
import com.queuemgr.modals.DocumentType;
import com.queuemgr.modals.Holiday;
import com.queuemgr.modals.IdentificationDocument;
import com.queuemgr.modals.Instruction;
import com.queuemgr.modals.ReservationFullObject;
import com.queuemgr.modals.TmeGaps;
import com.queuemgr.util.Common;
import com.queuemgr.util.LogFileCreator;
import com.queuemgr.util.StatusVarList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author Maheshm
 */
public class ReservationDaoImpl {

    private DataSource dataSource;
//    private Connection connection;
//    private PreparedStatement statement;
//    private ResultSet resultSet;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public synchronized String bookingStatusAndTokenUpdate(int bookingId, String loggedUser) throws Exception {
        int token;
        String tokenStr;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            int lastSeq = 0;
            String sqlSelect = "SELECT MAX(SEQUENCENUMBER) AS COUNT FROM tokensequence WHERE DATE = current_date()";
            statement = connection.prepareStatement(sqlSelect);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                lastSeq = resultSet.getInt("COUNT");
            }

            token = lastSeq + 1;
            tokenStr = String.format("%04d", token);
            String sqlUpdate = "UPDATE bookings SET BOOKINGSERVICESTATUS=?,TOKEN=?,LASTUPDATEDDATETIME=NOW() WHERE BOOKINGID=?";

            statement = connection.prepareStatement(sqlUpdate);
            statement.setString(1, "RDTQ");
            statement.setString(2, tokenStr);
            statement.setInt(3, bookingId);
            statement.execute();

            String sqlInsert = "INSERT INTO tokensequence(DATE,SEQUENCENUMBER,CREATEDUSER) VALUES (current_date(),?,?)";
            statement = connection.prepareStatement(sqlInsert);
            statement.setInt(1, lastSeq + 1);
            statement.setString(2, loggedUser);
            statement.execute();

            connection.commit();
            return tokenStr;
        } catch (SQLException sQLException) {
            LogFileCreator.writeErrorToLog(sQLException);
            connection.rollback();
            throw sQLException;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
    }

    public synchronized int createBooking(Customer customer, List<BookingVerification> bookingVerification, List<DocumentType> documentType, String loggedUser, IdentificationDocument document, String loggedCounter) throws SQLException, Exception {
        int bookingId = 0;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {

            int customerId = 0;
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            if ("NO".equals(customer.getCustomerId())) {
                String sql_insert = "INSERT "
                        + "INTO "
                        + "    customer "
                        + "    ( "
                        + "        FIRSTNAME,"
                        + "        IDENTIFICATIONNUMBER, "
                        + "        EMAIL, "
                        + "        MOBILE, "
                        + "        ADDRESSLINE1, "
                        + "        ADDRESSLINE2, "
                        + "        ADDRESSLINE3, "
                        + "        PREFFEREDLANG,"
                        + "        CREATEDUSER, "
                        + "        IDENTIFICATIONTYPECODE, "
                        + "        LASTUPDATEDDATETIME, "
                        + "        CREATEDDATETIME"
                        + "    ) "
                        + "    VALUES "
                        + "    ( ?, ?, ?, ?, ?, ?, ?, ?,?,?,NOW(),NOW())";
                String[] generatedColumns = {"CUSTOMERID"};
                statement = connection.prepareStatement(sql_insert, generatedColumns);
                statement.setString(1, customer.getFristName().trim());
                statement.setString(2, customer.getIdentificationNumber().trim());
                statement.setString(3, customer.getEmail().trim());
                statement.setString(4, customer.getTelephoneMobile().trim());
                statement.setString(5, customer.getAddressLine1().trim());
                statement.setString(6, customer.getAddressLine2().trim());
                statement.setString(7, customer.getAddressLine3().trim());
                statement.setString(8, customer.getPreferredLanguage().trim());
                statement.setString(9, loggedUser);
                statement.setString(10, customer.getIdentificatioDocumentType().trim());
                statement.executeUpdate();
                resultSet = statement.getGeneratedKeys();
                while (resultSet.next()) {
                    customerId = resultSet.getInt(1);
                }

            } else {
                customerId = Integer.parseInt(customer.getCustomerId());
                String sql = "UPDATE "
                        + "    customer "
                        + "    SET "
                        + "        FIRSTNAME=?,"
                        + "        IDENTIFICATIONNUMBER=?, "
                        + "        EMAIL=?, "
                        + "        MOBILE=?, "
                        + "        ADDRESSLINE1=?, "
                        + "        ADDRESSLINE2=?, "
                        + "        ADDRESSLINE3=?, "
                        + "        PREFFEREDLANG=?,"
                        + "        LASTUPDATEDUSER=?,"
                        + "        LASTUPDATEDDATETIME=NOW(),"
                        + "        IDENTIFICATIONTYPECODE=? "
                        + " WHERE "
                        + "    CUSTOMERID = ? ";
                statement = connection.prepareStatement(sql);
                statement.setString(1, customer.getFristName().trim());
                statement.setString(2, customer.getIdentificationNumber().trim());
                statement.setString(3, customer.getEmail().trim());
                statement.setString(4, customer.getTelephoneMobile().trim());
                statement.setString(5, customer.getAddressLine1().trim());
                statement.setString(6, customer.getAddressLine2().trim());
                statement.setString(7, customer.getAddressLine3().trim());
                statement.setString(8, customer.getPreferredLanguage().trim());
                statement.setString(9, loggedUser);
                statement.setString(10, customer.getIdentificatioDocumentType().trim());
                statement.setInt(11, customerId);
                statement.executeUpdate();
            }
            String sql_insert = "INSERT "
                    + "INTO "
                    + "    bookings "
                    + "    ( "
                    + "        CUSTOMERID,"
                    + "        BOOKEDSDID, "
                    + "        BOOKEDDATE, "
                    + "        STARTTIME, "
                    + "        BOOKINGSTATUS, "
                    + "        ISONLINE, "
                    + "        ISVIP, "
                    + "        BOOKINGSERVICESTATUS, "
                    + "        ENDTIME,"
                    + "        CREATEDUSER, "
                    + "        CREATEDDATETIME, "
                    + "        LASTUPDATEDDATETIME, "
                    + "        APPLICATIONCOUNTER,"
                    + "        TIMEDURATION,"
                    + "        APPLICATIONTYPEID"
                    + "    ) "
                    + "    VALUES "
                    + "    ( ?, ?, ?, ?,?,?,?,? ,?,?,NOW(),NOW(),?,?,?)";
            String[] generatedColumns = {"BOOKINGID"};
            statement = connection.prepareStatement(sql_insert, generatedColumns);
            statement.setInt(1, customerId);
            statement.setString(2, customer.getBookeDsdId().trim());
            statement.setString(3, customer.getBookingdate());
            statement.setTimestamp(4, Common.getTimestampFromDateAndTime(customer.getBookingdate() + " " + customer.getStartTimeStr()));
            statement.setString(5, StatusVarList.BOOKING_CONFIRMED);
            statement.setInt(6, 0);
            statement.setInt(7, 0);
            if (StatusVarList.APPOINMENT_WALKIN.equals(customer.getReschduleType())) {
                statement.setString(8, StatusVarList.PENDING);
            } else if (StatusVarList.APPOINMENT_WALKIN_SEND_TO_HELP_DESK.equals(customer.getReschduleType())) {
                statement.setString(8, StatusVarList.SENT_TO_HELP_DESK);
            }

            statement.setTimestamp(9, Common.getTimestampFromDateAndTime(customer.getBookingdate() + " " + customer.getEndtimeStr()));
            statement.setString(10, loggedUser);
            statement.setString(11, loggedCounter);
            statement.setInt(12, customer.getTimeDuration());
            statement.setInt(13, Integer.parseInt(customer.getApplicationType()));

            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            while (resultSet.next()) {
                bookingId = resultSet.getInt(1);
            }

            for (int j = 0; j < documentType.size(); j++) {
                sql_insert = "INSERT "
                        + "INTO "
                        + "     bookingdocuments "
                        + "    ( "
                        + "        BOOKINGID,"
                        + "        DOCUMENTTYPEID, "
                        + "        QUANTITY"
                        + "    ) "
                        + "    VALUES "
                        + "    ( ?, ?, ?)";
                statement = connection.prepareStatement(sql_insert);
                statement.setInt(1, bookingId);
                statement.setString(2, documentType.get(j).getDocumentTypeId().trim());
                statement.setInt(3, Integer.parseInt(documentType.get(j).getQuantity().trim()));
                statement.executeUpdate();
            }

            for (int j = 0; j < bookingVerification.size(); j++) {
                sql_insert = "INSERT "
                        + "INTO "
                        + "     bookingverificationdocs "
                        + "    ( "
                        + "        BOOKINGID,"
                        + "        DOCUMENTTYPE, "
                        + "        REFERENCE "
                        + "    ) "
                        + "    VALUES "
                        + "    ( ?, ?, ?)";
                statement = connection.prepareStatement(sql_insert);
                statement.setInt(1, bookingId);
                statement.setString(2, bookingVerification.get(j).getDocumentTypeId().trim());
                statement.setString(3, bookingVerification.get(j).getReference().trim());
                statement.executeUpdate();
            }

            sql_insert = "INSERT "
                    + "INTO "
                    + "     bookingidentificationdocument "
                    + "    ( "
                    + "        DOCTYPE,"
                    + "        DOCUMENTPATH1, "
                    + "        CREATEDUSER, "
                    + "        BOOKINGID, "
                    + "        DOCUMENTREFERENCENO, "
                    + "        CREATEDDATETIME, "
                    + "        DOCUMENTPATH2"
                    + "    ) "
                    + "    VALUES "
                    + "    ( ?, ?, ? , ? , ? ,NOW(),?)";
            statement = connection.prepareStatement(sql_insert);
            statement.setString(1, "NIC");
            statement.setString(2, "-");
            statement.setString(3, loggedUser);
            statement.setInt(4, bookingId);
            statement.setString(5, "-");
            statement.setString(6, "-");
            statement.executeUpdate();

            if (StatusVarList.APPOINMENT_WALKIN_SEND_TO_HELP_DESK.equals(customer.getReschduleType())) {
                String reasonQuery = "INSERT INTO reschedulingreasons(BOOKINGID,REASON,CREATEDDATETIME,CREATEDUSER) VALUES(?,?,NOW(),?)";
                statement = connection.prepareStatement(reasonQuery);
                statement.setInt(1, bookingId);
                statement.setString(2, customer.getHelpdeskreason());
                statement.setString(3, loggedUser);

                statement.executeUpdate();
            }

            connection.commit();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            connection.rollback();
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return bookingId;
    }

    public Instruction getInstructionwalk(int instructionId, String lang) throws Exception {
        Instruction instruction = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT " + lang + " FROM instruction WHERE STATUS=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, instructionId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                instruction = new Instruction();
                instruction.setLang(resultSet.getString(lang));
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return instruction;
    }

    public Instruction getInstruction(int instructionId) throws Exception {
        Instruction instruction = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT SINHALA,ENGLISH,TAMIL FROM instruction WHERE INSTRUCTIONID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, instructionId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                instruction = new Instruction();
                instruction.setSinhala(resultSet.getString("SINHALA"));
                instruction.setEnglish(resultSet.getString("ENGLISH"));
                instruction.setTamil(resultSet.getString("TAMIL"));
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return instruction;
    }

    public String getServiceid(String loggedUser) throws Exception {
        String serviceid = "";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT MAX(BOOKINGID) AS SID FROM bookings WHERE CREATEDUSER=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, loggedUser);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                serviceid = resultSet.getString("SID");
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return serviceid;
    }

    public List<ReservationFullObject> getCustomerFulldetails(String bookingid) throws SQLException, Exception {
        List<ReservationFullObject> fullobject = null;
        List<Customer> customer = null;
        List<Bookings> bookings = null;
        List<DocumentType> doctype = null;
        ReservationFullObject res = null;
        Customer cus = null;
        Bookings booking = null;
        DocumentType doc = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            fullobject = new ArrayList();
            customer = new ArrayList();
            String sql = "SELECT applicationtype.description as des,customer.FIRSTNAME,customer.LASTNAME,customer.ADDRESSLINE1,customer.ADDRESSLINE2,customer.ADDRESSLINE3,customer.EMAIL,customer.MOBILE FROM bookings JOIN customer on bookings.customerid=customer.customerid join applicationtype on bookings.applicationtypeid=applicationtype.appicationtypeid where bookingid=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, bookingid);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                booking = new Bookings();
                doc = new DocumentType();
                cus = new Customer();
                res = new ReservationFullObject();

                cus.setFristName(resultSet.getString("FIRSTNAME"));
                cus.setLastName(resultSet.getString("LASTNAME"));
                cus.setTelephoneMobile(resultSet.getString("MOBILE"));
                cus.setEmail(resultSet.getString("EMAIL"));
                cus.setAddressLine3(resultSet.getString("ADDRESSLINE3"));
                cus.setAddressLine2(resultSet.getString("ADDRESSLINE2"));
                cus.setAddressLine1(resultSet.getString("ADDRESSLINE1"));

                res.setCustomer(cus);
                fullobject.add(res);
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return fullobject;
    }

    public List<DocumentCharges> getDocumentChargers() throws Exception {
        DocumentCharges doc = null;
        List<DocumentCharges> list = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT docchargecode,description FROM documentchargers ORDER BY SORTKEY ASC";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                doc = new DocumentCharges();
                doc.setDocumentChargeCode(resultSet.getString("docchargecode"));
                doc.setDescription(resultSet.getString("description"));
                list.add(doc);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }

    public synchronized int createBooking_vip(Customer customer, List<BookingVerification> bookingVerification, List<DocumentType> documentType, String loggedUser, IdentificationDocument document, String loggedCounter) throws SQLException, Exception {
        int bookingId = Integer.parseInt(customer.getBookingId());
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);

            String sql = "UPDATE "
                    + "    bookings "
                    + "    SET "
                    + "    BOOKEDDATE           = CURDATE(),"
                    + "    BOOKINGSTATUS        = ?, "
                    + "    ISONLINE             = ?, "
                    + "    BOOKINGSERVICESTATUS = ?, "
                    + "    LASTUPDATEDUSER          = ?,"
                    + "    LASTUPDATEDDATETIME  = NOW(), "
                    + "    APPLICATIONTYPEID  = ? ,"
                    + "    APPLICATIONCOUNTER  = ? "
                    + "WHERE "
                    + "    BOOKINGID = ? ";
            statement = connection.prepareStatement(sql);
            statement.setString(1, StatusVarList.BOOKING_CONFIRMED);
            statement.setInt(2, 0);
            if (StatusVarList.APPOINMENT_VIP.equals(customer.getReschduleType())) {
                statement.setString(3, StatusVarList.PENDING);
            } else if (StatusVarList.APPOINMENT_VIP_SEND_TO_HELP_DESK.equals(customer.getReschduleType())) {
                statement.setString(3, StatusVarList.SENT_TO_HELP_DESK);
            }

            statement.setString(4, loggedUser);
            statement.setString(5, customer.getApplicationType().trim());
            statement.setString(6, loggedCounter);
            statement.setInt(7, bookingId);
            statement.executeUpdate();

            int customerId = 0;
            sql = "SELECT CUSTOMERID FROM bookings WHERE BOOKINGID=?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                customerId = resultSet.getInt("CUSTOMERID");
            }

            sql = "UPDATE "
                    + "    customer "
                    + "    SET "
                    + "    FIRSTNAME           = ?,"
                    + "    IDENTIFICATIONNUMBER           = ?,"
                    + "    EMAIL           = ?,"
                    + "    MOBILE        = ?, "
                    + "    ADDRESSLINE1             = ?, "
                    + "    ADDRESSLINE2             = ?, "
                    + "    ADDRESSLINE3             = ?, "
                    + "    PREFFEREDLANG             = ?, "
                    + "    LASTUPDATEDUSER          = ?,"
                    + "    LASTUPDATEDDATETIME = NOW()"
                    + "WHERE "
                    + "    CUSTOMERID = ? ";
            statement = connection.prepareStatement(sql);
            statement.setString(1, customer.getFristName().trim());
            statement.setString(2, customer.getIdentificationNumber().trim());
            statement.setString(3, customer.getEmail().trim());
            statement.setString(4, customer.getTelephoneMobile().trim());
            statement.setString(5, customer.getAddressLine1().trim());
            statement.setString(6, customer.getAddressLine2().trim());
            statement.setString(7, customer.getAddressLine3().trim());
            statement.setString(8, customer.getPreferredLanguage().trim());
            statement.setString(9, loggedUser);
            statement.setInt(10, customerId);
            statement.executeUpdate();

            for (int j = 0; j < documentType.size(); j++) {
                String sql_insert = "INSERT "
                        + "INTO "
                        + "     bookingdocuments "
                        + "    ( "
                        + "        BOOKINGID,"
                        + "        DOCUMENTTYPEID, "
                        + "        QUANTITY"
                        + "    ) "
                        + "    VALUES "
                        + "    ( ?, ?, ?)";
                statement = connection.prepareStatement(sql_insert);
                statement.setInt(1, bookingId);
                statement.setString(2, documentType.get(j).getDocumentTypeId().trim());
                statement.setInt(3, Integer.parseInt(documentType.get(j).getQuantity().trim()));
                statement.executeUpdate();
            }
            for (int j = 0; j < bookingVerification.size(); j++) {
                String sql_insert = "INSERT "
                        + "INTO "
                        + "     bookingverificationdocs "
                        + "    ( "
                        + "        BOOKINGID,"
                        + "        DOCUMENTTYPE, "
                        + "        REFERENCE "
                        + "    ) "
                        + "    VALUES "
                        + "    ( ?, ?, ?)";
                statement = connection.prepareStatement(sql_insert);
                statement.setInt(1, bookingId);
                statement.setString(2, bookingVerification.get(j).getDocumentTypeId().trim());
                statement.setString(3, bookingVerification.get(j).getReference().trim());
                statement.executeUpdate();
            }
            sql = "INSERT "
                    + "INTO "
                    + "     bookingidentificationdocument "
                    + "    ( "
                    + "        DOCTYPE,"
                    + "        DOCUMENTPATH1, "
                    + "        CREATEDUSER, "
                    + "        BOOKINGID, "
                    + "        DOCUMENTREFERENCENO, "
                    + "        DOCUMENTPATH2"
                    + "    ) "
                    + "    VALUES "
                    + "    ( ?, ?, ? , ? , ? ,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, "NIC");
            statement.setString(2, "-");
            statement.setString(3, loggedUser);
            statement.setInt(4, bookingId);
            statement.setString(5, "-");
            statement.setString(6, "-");

            statement.executeUpdate();

            if (StatusVarList.APPOINMENT_VIP_SEND_TO_HELP_DESK.equals(customer.getReschduleType())) {
                String reasonQuery = "INSERT INTO reschedulingreasons(BOOKINGID,REASON,CREATEDDATETIME,CREATEDUSER) VALUES(?,?,NOW(),?)";
                statement = connection.prepareStatement(reasonQuery);
                statement.setInt(1, bookingId);
                statement.setString(2, customer.getHelpdeskreason());
                statement.setString(3, loggedUser);

                statement.executeUpdate();
            }

            connection.commit();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            connection.rollback();
            throw ex;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return bookingId;
    }

    public synchronized int createBooking_vip_triger(Customer customer, String loggedUser) throws SQLException, Exception {
        int bookingId = 0;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {

            int customerId = 0;
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            String sql_insert1 = "INSERT "
                    + "INTO "
                    + "    customer "
                    + "    ( "
                    + "        FIRSTNAME,"
                    + "        IDENTIFICATIONNUMBER, "
                    + "        IDENTIFICATIONTYPECODE, "
                    + "        CREATEDUSER,"
                    + "        CREATEDDATETIME,"
                    + "        LASTUPDATEDDATETIME"
                    + "    ) "
                    + "    VALUES "
                    + "    ( ?, ?, ?, ?, NOW(),NOW())";
            String[] generatedColumns1 = {"CUSTOMERID"};
            statement = connection.prepareStatement(sql_insert1, generatedColumns1);
            statement.setString(1, customer.getFristName().trim());
            statement.setString(2, customer.getIdentificationNumber().trim());
            statement.setString(3, customer.getIdentificatioDocumentType().trim());
            statement.setString(4, loggedUser);

            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            while (resultSet.next()) {
                customerId = resultSet.getInt(1);
            }

            String sql_insert = "INSERT "
                    + "INTO "
                    + "    bookings "
                    + "    ( "
                    + "        CUSTOMERID,"
                    + "        ISVIP,"
                    + "        CREATEDUSER,"
                    + "        CREATEDDATETIME,"
                    + "        BOOKINGSTATUS"
                    + "    ) "
                    + "    VALUES "
                    + "    ( ?, ?,  ?,NOW(),?)";
            String[] generatedColumns = {"BOOKINGID"};
            statement = connection.prepareStatement(sql_insert, generatedColumns);
            statement.setInt(1, customerId);
            statement.setInt(2, 1);
            statement.setString(3, loggedUser);
            statement.setString(4, "INIT");
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            while (resultSet.next()) {
                bookingId = resultSet.getInt(1);
            }

            connection.commit();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            connection.rollback();
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return bookingId;
    }

    public CommonConfigurations getTime() throws Exception {
        CommonConfigurations common;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT "
                    + "     SERVICEDESKSTARTTIME,"
                    + "     SERVICEDESKENDTIME,"
                    + "     MIDDLETIME"
                    + " FROM "
                    + "     commonconfigurations";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            common = new CommonConfigurations();
            while (resultSet.next()) {
                common.setServiceDeskStartTime(resultSet.getString("SERVICEDESKSTARTTIME"));
                common.setServiceDeskEndTime(resultSet.getString("SERVICEDESKENDTIME"));
                common.setMiddleTime(resultSet.getString("MIDDLETIME"));
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return common;
    }

    public boolean isToday(int bookingId) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        boolean isToday = false;
        int count = 0;
        try {
            String sql = "select COUNT(*) AS COUNT FROM bookings WHERE BOOKINGID=? AND BOOKEDDATE = current_date()";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                count = resultSet.getInt("COUNT");

                if (count > 0) {
                    isToday = true;
                } else {
                    isToday = false;
                }
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return isToday;
    }

    public boolean isInCorrectStatus(int bookingId) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        boolean isToday = false;
        int count = 0;
        try {
            String sql = "select COUNT(*) AS COUNT FROM bookings "
                    + "WHERE BOOKINGSTATUS =? AND (BOOKINGSERVICESTATUS= ? OR BOOKINGSERVICESTATUS=?) AND BOOKINGID= ?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, StatusVarList.BOOKING_CONFIRMED);
            statement.setString(2, StatusVarList.PENDING);
            statement.setString(3, StatusVarList.QUEUE_ABSENT);
            statement.setInt(4, bookingId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                count = resultSet.getInt("COUNT");

                if (count > 0) {
                    isToday = true;
                } else {
                    isToday = false;
                }
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return isToday;
    }

    public String getServiceIdStatus(String serviceId) throws SQLException, Exception {
        String status = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT BOOKINGSERVICESTATUS FROM bookings WHERE BOOKINGID = ?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, serviceId);

            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                status = resultSet.getString("BOOKINGSERVICESTATUS");
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } catch (Exception e) {
            LogFileCreator.writeErrorToLog(e);
            throw e;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return status;
    }

    public boolean isInCorrectStatusReprint(int bookingId) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        boolean isToday = false;
        int count = 0;
        try {
            String sql = "select COUNT(*) AS COUNT FROM bookings WHERE BOOKINGSTATUS =? AND BOOKINGSERVICESTATUS<>? AND BOOKINGSERVICESTATUS<>? AND BOOKINGSERVICESTATUS<>? AND BOOKINGID= ?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, StatusVarList.BOOKING_CONFIRMED);
            statement.setString(2, StatusVarList.QUEUE_ABSENT);
            statement.setString(3, StatusVarList.PENDING);
            statement.setString(4, StatusVarList.SENT_TO_HELP_DESK);
            statement.setInt(5, bookingId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                count = resultSet.getInt("COUNT");

                if (count > 0) {
                    isToday = true;
                } else {
                    isToday = false;
                }
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return isToday;
    }

    public int getWalkInPercentage() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        int percentage = 0;
        try {
            String sql = "SELECT "
                    + "     WALKINPERCENTAGE"
                    + " FROM "
                    + "     commonconfigurations";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                percentage = resultSet.getInt("WALKINPERCENTAGE");
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return percentage;
    }

    public String getOnlineCutoffTime() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        String time = "";
        try {
            String sql = "SELECT "
                    + "     ONLINECUTOFFTIME"
                    + " FROM "
                    + "     commonconfigurations";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                time = resultSet.getString("ONLINECUTOFFTIME");
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return time;
    }

    public List<Holiday> getHolidayTime(String date) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        CommonConfigurations common = this.getTime();
        String startTime = common.getServiceDeskStartTime();
        String endTime = common.getServiceDeskEndTime();
        List<Holiday> holidays = new ArrayList<>();
        Holiday holiday = new Holiday();
        holiday.setStartTime(startTime);
        holiday.setEndTime(startTime);
        holidays.add(holiday);
        try {
            String sql = "SELECT "
                    + "     STARTTIME,"
                    + "     ENDTIME "
                    + " FROM "
                    + "     holiday "
                    + " WHERE "
                    + "     STATUS='ACTV' "
                    + " AND "
                    + "     HOLIDAYDATE='" + date + "'"
                    + " AND "
                    + "     HALFDAY=1 ORDER BY STARTTIME ASC";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                holiday = new Holiday();
                holiday.setStartTime(resultSet.getString("STARTTIME"));
                holiday.setEndTime(resultSet.getString("ENDTIME"));
                holidays.add(holiday);
            }
            holiday = new Holiday();
            holiday.setStartTime(endTime);
            holiday.setEndTime(endTime);
            holidays.add(holiday);

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return holidays;
    }

    public boolean isFullHoliday(String date) throws Exception {
        boolean holiday = false;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT "
                    + "     HALFDAY"
                    + " FROM "
                    + "     holiday "
                    + " WHERE "
                    + "     STATUS='ACTV' "
                    + " AND "
                    + "     HOLIDAYDATE='" + date + "'"
                    + " AND "
                    + "     HALFDAY=0";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                holiday = true;
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return holiday;
    }

    public int getVipNotificationCount() throws Exception {
        int notificationCount = 0;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT "
                    + "     COUNT(BOOKINGID) "
                    + " AS "
                    + "     NOTIFICATIONCOUNT "
                    + " FROM "
                    + "     bookings "
                    + " WHERE "
                    + "     ISVIP=? "
                    + " AND "
                    + "     BOOKINGSTATUS=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, 1);
            statement.setString(2, "INIT");
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                notificationCount = resultSet.getInt("NOTIFICATIONCOUNT");
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return notificationCount;
    }

    public List<String> getVipNotificationList() throws Exception {
        List<String> list = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT "
                    + "     BOOKINGID "
                    + " FROM "
                    + "     bookings "
                    + " WHERE "
                    + "     ISVIP=? "
                    + " AND "
                    + "     BOOKINGSTATUS=?  ORDER BY BOOKINGID ASC";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, 1);
            statement.setString(2, "INIT");
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(Integer.toString(resultSet.getInt("BOOKINGID")));
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return list;
    }

    public Customer getRecevationSuccesData(int bookingId) throws Exception {
        Customer customer;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT "
                    + "     C.FIRSTNAME,"
                    + "     C.LASTNAME,"
                    + "     C.MOBILE,"
                    + "     C.EMAIL,"
                    + "     C.PREFFEREDLANG,"
                    + "     C.IDENTIFICATIONTYPECODE,"
                    + "     C.IDENTIFICATIONNUMBER,"
                    + "     B.STARTTIME,"
                    + "     B.ENDTIME,"
                    + "     B.BOOKEDDATE "
                    + " FROM "
                    + "     bookings B "
                    + " INNER JOIN "
                    + "     customer C "
                    + " ON "
                    + "     C.CUSTOMERID=B.CUSTOMERID"
                    + " WHERE "
                    + "     B.BOOKINGID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            resultSet = statement.executeQuery();
            customer = new Customer();
            while (resultSet.next()) {
                customer.setFristName((resultSet.getString("C.FIRSTNAME")));
                customer.setLastName(resultSet.getString("C.LASTNAME"));
                customer.setTelephoneMobile(resultSet.getString("C.MOBILE"));
                customer.setEmail(resultSet.getString("C.EMAIL"));
                customer.setPreferredLanguage(resultSet.getString("C.PREFFEREDLANG"));
                customer.setBookingdate(resultSet.getString("B.BOOKEDDATE"));
                customer.setStartTimeStr(Common.getTimeFromTimestamp(resultSet.getTimestamp("B.STARTTIME")));
                customer.setEndtimeStr(Common.getTimeFromTimestamp(resultSet.getTimestamp("B.ENDTIME")));
                customer.setIdentificatioDocumentType((resultSet.getString("C.IDENTIFICATIONTYPECODE")));
                customer.setIdentificationNumber((resultSet.getString("C.IDENTIFICATIONNUMBER")));
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return customer;
    }

    public Customer getRecevationVipCustomerSuccesData(int bookingId) throws Exception {
        Customer customer;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT "
                    + "     C.FIRSTNAME,"
                    + "     C.LASTNAME,"
                    + "     C.MOBILE,"
                    + "     C.EMAIL,"
                    + "     C.IDENTIFICATIONTYPECODE,"
                    + "     C.IDENTIFICATIONNUMBER,"
                    + "     C.PREFFEREDLANG"
                    + " FROM "
                    + "     bookings B "
                    + " INNER JOIN "
                    + "     customer C "
                    + " ON "
                    + "     C.CUSTOMERID=B.CUSTOMERID"
                    + " WHERE "
                    + "     B.BOOKINGID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            resultSet = statement.executeQuery();
            customer = new Customer();
            while (resultSet.next()) {
                customer.setFristName((resultSet.getString("C.FIRSTNAME")));
                customer.setLastName(resultSet.getString("C.LASTNAME"));
                customer.setTelephoneMobile(resultSet.getString("C.MOBILE"));
                customer.setEmail(resultSet.getString("C.EMAIL"));
                customer.setPreferredLanguage(resultSet.getString("C.PREFFEREDLANG"));
                customer.setIdentificatioDocumentType(resultSet.getString("C.IDENTIFICATIONTYPECODE"));
                customer.setIdentificationNumber(resultSet.getString("C.IDENTIFICATIONNUMBER"));
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return customer;
    }

    public Customer getRecevationVipTriggerSuccesData(int bookingId) throws Exception {
        Customer customer;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT "
                    + "     C.FIRSTNAME"
                    + " FROM "
                    + "     bookings B "
                    + " INNER JOIN "
                    + "     customer C "
                    + " ON "
                    + "     C.CUSTOMERID=B.CUSTOMERID"
                    + " WHERE "
                    + "     B.BOOKINGID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            resultSet = statement.executeQuery();
            customer = new Customer();
            while (resultSet.next()) {
                customer.setFristName((resultSet.getString("C.FIRSTNAME")));
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return customer;
    }

    public boolean validateToken(String username, String token) throws Exception {
        boolean status = false;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT COUNT(*) AS COUNT FROM usertoken WHERE USERNAME = ? AND TOKEN = ?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, username);
            statement.setString(2, token);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int count = resultSet.getInt("COUNT");
                if (count > 0) {
                    status = true;
                } else {
                    status = false;
                }
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return status;
    }

    public List<BookingVerification> getverificationDoc(int bookingid) throws Exception {
        List<BookingVerification> list = new ArrayList<>();
        BookingVerification doc = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT DOCUMENTTYPE,REFERENCE FROM bookingverificationdocs WHERE BOOKINGID=?";
            String sql1 = "SELECT "
                    + "     D.DESCRIPTION,"
                    + "     B.REFERENCE "
                    + " FROM "
                    + "     bookingverificationdocs B "
                    + " INNER JOIN "
                    + "     documenttype D "
                    + " ON "
                    + "     D.DOCUMENTTYPEID=B.DOCUMENTTYPE"
                    + " WHERE "
                    + "     B.BOOKINGID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql1);
            statement.setInt(1, bookingid);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                doc = new BookingVerification();
                doc.setDescription(resultSet.getString("D.DESCRIPTION"));
                doc.setReference(resultSet.getString("B.REFERENCE"));
                list.add(doc);
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return list;
    }

    public List<Customer> getSumDuration(String date, String where) throws Exception {
        List<Customer> customerList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT STARTTIME, SUM(TIMEDURATION) FROM bookings WHERE BOOKEDDATE='" + date + "' :where AND ISVIP=0 GROUP BY STARTTIME ORDER BY STARTTIME ASC";
            sql = sql.replace(":where", where);
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                Customer cus = new Customer();
                cus.setStartTimeStr(Common.getTimeFromTimestamp(resultSet.getTimestamp("STARTTIME")));
                cus.setSumTimeDuration(resultSet.getInt("SUM(TIMEDURATION)"));
                customerList.add(cus);
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return customerList;
    }

    public boolean thisTimeSloatSameCustomer(Timestamp startTime, Timestamp endTime, String identificationNumber) throws Exception {
        int count = 0;
        boolean isTrue = false;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT COUNT(*) AS COUNT FROM bookings INNER JOIN  customer  ON customer.CUSTOMERID=bookings.CUSTOMERID WHERE bookings.STARTTIME='" + startTime + "' AND bookings.ENDTIME='" + endTime + "' AND customer.IDENTIFICATIONNUMBER='" + identificationNumber + "' ";

            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
//            statement.setTimestamp(1, startTime);
//            statement.setTimestamp(2, endTime);
//            statement.setString(1, identificationNumber);
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                count = resultSet.getInt("COUNT");
            }
            if (count > 0) {
                isTrue = true;
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return isTrue;
    }

    public int getSumDurationTimeBlock(Timestamp date, String where) throws Exception {
        int sumTime = 0;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT SUM(TIMEDURATION) FROM bookings WHERE STARTTIME='" + date + "' :where AND ISVIP=0";
            sql = sql.replace(":where", where);
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                sumTime = resultSet.getInt("SUM(TIMEDURATION)");
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return sumTime;
    }

    public int getCountServiceDesk() throws Exception {
        int count = 0;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT COUNT(SDID) FROM servicedesks WHERE STATUS='ACTV'";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                count = resultSet.getInt("COUNT(SDID)");
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return count;
    }

    public BookingTimes getBookingTime(int serviceId) throws Exception {
        BookingTimes bt = new BookingTimes();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            String sqlSelect = "SELECT TIME(STARTTIME) as sttime,TIME(ENDTIME) as entime FROM bookings WHERE BOOKINGID = ?";
            statement = connection.prepareStatement(sqlSelect);
            statement.setInt(1, serviceId);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                bt.setStartTime(resultSet.getTime("sttime").toString());
                bt.setEndTime(resultSet.getTime("entime").toString());
            }

            connection.commit();
            return bt;
        } catch (SQLException sQLException) {
            LogFileCreator.writeErrorToLog(sQLException);
            connection.rollback();
            throw sQLException;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
    }

    public TmeGaps getTimeGapIssueTicket() throws Exception {
        TmeGaps tg = new TmeGaps();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            String sqlSelect = "SELECT TIMEGAPTOISSUETICKET,TIMEGAPTOEXPIRETICKET FROM commonconfigurations";
            statement = connection.prepareStatement(sqlSelect);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                tg.setGapBefore(resultSet.getDouble("TIMEGAPTOISSUETICKET"));
                tg.setGapAfter(resultSet.getDouble("TIMEGAPTOEXPIRETICKET"));
            }

            connection.commit();
            return tg;
        } catch (SQLException sQLException) {
            LogFileCreator.writeErrorToLog(sQLException);
            connection.rollback();
            throw sQLException;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
    }

    public String getTokenReprint(int bookingId) throws Exception {
        String token = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "select TOKEN FROM bookings WHERE BOOKINGID= ?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                token = resultSet.getString("TOKEN");
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return token;
    }

    public List<AllBookings> getAllBookings() throws SQLException {
        List<AllBookings> bookingList = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT bookings.BOOKINGID,bookings.BOOKEDDATE,bookings.STARTTIME,bookings.ENDTIME,customer.FIRSTNAME FROM bookings JOIN customer ON bookings.CUSTOMERID = customer.CUSTOMERID WHERE bookings.BOOKINGSERVICESTATUS = ? AND bookings.ISVIP != ?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, StatusVarList.PENDING);
            statement.setInt(2, 1);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                AllBookings bookings = new AllBookings();
                bookings.setServiseId(resultSet.getString("BOOKINGID"));
                bookings.setCustomerName(resultSet.getString("FIRSTNAME"));
                bookings.setBookingDate(resultSet.getString("BOOKEDDATE"));
                bookings.setStringStartTime(resultSet.getString("STARTTIME"));
                bookings.setStringEndTime(resultSet.getString("ENDTIME"));
                bookingList.add(bookings);
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return bookingList;
    }

    public void deleteBooking(int bookingId) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "DELETE FROM bookings WHERE bookingid=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            statement.execute();
//            connection.commit();

        } catch (SQLException sQLException) {
            LogFileCreator.writeErrorToLog(sQLException);
            throw sQLException;
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception exception) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception exception) {
                }
            }
        }
    }

    public String getWalkInSMSMessage(String id) throws Exception {
        String message = "";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT MESSAGEDESCRIPTION  FROM smsmessage WHERE MESSAGEID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                message = resultSet.getString("MESSAGEDESCRIPTION");
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return message;
    }

    public int getTimeGapToIssueTicket() throws Exception {
        int time = 0;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT "
                    + "     TIMEGAPTOISSUETICKET"
                    + " FROM "
                    + "     commonconfigurations";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                time = resultSet.getInt("TIMEGAPTOISSUETICKET");
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return time;
    }

    public int getApoinmentType(int bookingId) throws Exception {
        int staus = 0;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT ISVIP,ISONLINE FROM bookings WHERE BOOKINGID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getInt("ISVIP") == 1) {
                    staus = 2;
                } else if (resultSet.getInt("ISONLINE") == 1) {
                    staus = 1;
                } else {
                    staus = 0;
                }
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return staus;
    }
}
