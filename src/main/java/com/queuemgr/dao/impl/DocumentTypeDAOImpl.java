/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.dao.impl;

import com.queuemgr.modals.DocumentType;
import com.queuemgr.util.LogFileCreator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author Rasika
 */
public class DocumentTypeDAOImpl {

    private DataSource dataSource;
//    private Connection connection;
//    private PreparedStatement statement;
//    private ResultSet resultSet;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Map<String, String> getDocumentTypeList() throws Exception {
        Map<String, String> documentType = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT  DOCUMENTTYPEID,DESCRIPTION FROM documenttype ORDER BY DESCRIPTION ASC";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            documentType = new LinkedHashMap();
            documentType.put("", "-- Select --");
            while (resultSet.next()) {
                documentType.put(resultSet.getString("DOCUMENTTYPEID"), resultSet.getString("DESCRIPTION"));
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return documentType;
    }

    public Map<String, String> getApplicationTypeList(int isVip) throws Exception {
        Map<String, String> documentType = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT  APPICATIONTYPEID,DESCRIPTION FROM applicationtype WHERE ISVIP = ?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, isVip);
            resultSet = statement.executeQuery();
            documentType = new LinkedHashMap();
            while (resultSet.next()) {
                documentType.put(resultSet.getString("APPICATIONTYPEID"), resultSet.getString("DESCRIPTION"));
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return documentType;
    }

    public Map<String, String> getVipApplicationTypeList(int isVip) throws Exception {
        Map<String, String> documentType = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT  APPICATIONTYPEID,DESCRIPTION FROM applicationtype WHERE ISVIP = ?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, isVip);
            resultSet = statement.executeQuery();
            documentType = new LinkedHashMap();
            while (resultSet.next()) {
                documentType.put(resultSet.getString("APPICATIONTYPEID"), resultSet.getString("DESCRIPTION"));
            }
            sql = "SELECT  APPICATIONTYPEID,DESCRIPTION FROM applicationtype WHERE ISVIP = ?";

            statement = connection.prepareStatement(sql);
            statement.setInt(1, 0);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                documentType.put(resultSet.getString("APPICATIONTYPEID"), resultSet.getString("DESCRIPTION"));
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return documentType;
    }

    public List<DocumentType> getDocumentTypeBeanList() throws Exception {
        List<DocumentType> documentTypeList = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT DOCUMENTTYPEID,VERIFICATIONTIME,DESCRIPTION FROM documenttype";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            documentTypeList = new ArrayList();
            DocumentType docType;
            while (resultSet.next()) {
                docType = new DocumentType();
                docType.setDocumentTypeId(resultSet.getString("DOCUMENTTYPEID"));
                docType.setVarificationTime(resultSet.getString("VERIFICATIONTIME"));
                docType.setDescription(resultSet.getString("DESCRIPTION"));
                documentTypeList.add(docType);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return documentTypeList;
    }

    public List<DocumentType> getReminderDocumentTypeBeanListAddDescription(List<DocumentType> list) throws Exception {
        List<DocumentType> documentTypeList = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT DOCUMENTTYPEID,VERIFICATIONTIME,DESCRIPTION FROM documenttype";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            documentTypeList = new ArrayList();
            DocumentType docType;
            while (resultSet.next()) {
                docType = new DocumentType();
                for (int i = 0; i < list.size(); i++) {
                    if (resultSet.getString("DOCUMENTTYPEID").equals(list.get(i).getDocumentTypeId())) {
                        docType.setDocumentTypeId(resultSet.getString("DOCUMENTTYPEID"));
                        docType.setQuantity(list.get(i).getQuantity());
                        docType.setDescription(resultSet.getString("DESCRIPTION"));
                        documentTypeList.add(docType);
                    }

                }

            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return documentTypeList;
    }

    public Map<String, String> getDocumentTypeOldList(String where) throws Exception {
        Map<String, String> documentType = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT  DOCUMENTTYPEID,DESCRIPTION FROM documenttype :where";
            sql = sql.replace(":where", where);
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            documentType = new LinkedHashMap();
            documentType.put("", "-- Select --");
            while (resultSet.next()) {
                documentType.put(resultSet.getString("DOCUMENTTYPEID"), resultSet.getString("DESCRIPTION"));
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return documentType;
    }

}
