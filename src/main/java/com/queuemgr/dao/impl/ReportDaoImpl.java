/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.dao.impl;

import com.queuemgr.modals.AppicationSummeryReport;
import com.queuemgr.modals.ApplicantDetailSummeryReport;
import com.queuemgr.modals.CounterSummeryReport;
import com.queuemgr.modals.Report;
import com.queuemgr.util.Common;
import com.queuemgr.util.LogFileCreator;
import com.queuemgr.util.StatusVarList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class ReportDaoImpl {

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    //2.Counter Summery report
    public List<CounterSummeryReport> CounterSummeryReport(Report rpt) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<CounterSummeryReport> csr = new ArrayList<>();
        try {
//            "BOOKEDDATE<='2016-11-25' AND BOOKEDDATE>='2016-11-20' AND BOOKEDSDID='SCO11'"
            connection = dataSource.getConnection();

            JSONArray array = new JSONArray();

            String sql = "SELECT COUNT(B.BOOKINGID) AS APPLICANTCOUNT, AC.DESCRIPTION, APPLICATIONCOUNTERID  FROM bookings B inner join applicationcounter AC on B.APPLICATIONCOUNTER=AC.APPLICATIONCOUNTERID  WHERE 1=1 ";
            if (rpt.getFromdate() != null && rpt.getTodate() != null) {
                sql += "AND B.CREATEDDATETIME BETWEEN '" + rpt.getFromdate() + " 0:00:00 AM'  AND '" + rpt.getTodate() + " 23:59:00 PM' ";
            }
            if (!rpt.getCountername().contentEquals("")) {
                sql += "AND APPLICATIONCOUNTER='" + rpt.getCountername() + "' ";
            }

            sql += " group by APPLICATIONCOUNTER ";
            sql += " order by B.CREATEDDATETIME ";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("APPLICANTCOUNT", resultSet.getString("APPLICANTCOUNT"));
                jsonObj.put("APPLICATIONCOUNTERID", resultSet.getString("APPLICATIONCOUNTERID"));
                jsonObj.put("DESCRIPTION", resultSet.getString("DESCRIPTION"));
                CounterSummeryReport cussum = new CounterSummeryReport();
                cussum.setCounterid(resultSet.getString("APPLICATIONCOUNTERID"));
                cussum.setCountername(resultSet.getString("DESCRIPTION"));
                cussum.setNumberOfApplicant(Integer.valueOf(resultSet.getString("APPLICANTCOUNT")));
//                csr.add(cussum);
                array.put(jsonObj);
            }

            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                String sql2 = "SELECT (SELECT COUNT(BV.BOOKINGVERIFICATIONDOCID) AS COUNTSTATUS "
                        + "        FROM   bookingverificationdocs BV INNER JOIN bookings BO ON BV.BOOKINGID = BO.BOOKINGID "
                        + "        WHERE   APPLICATIONCOUNTER=BO.APPLICATIONCOUNTER :date1) "
                        + "         AS ACCP "
                        + "FROM   bookings BO "
                        + "WHERE  1 = 1 AND BO.APPLICATIONCOUNTER = '" + obj.getString("APPLICATIONCOUNTERID") + "' "
                        + "GROUP BY BO.APPLICATIONCOUNTER";

                if (rpt.getFromdate() != null && rpt.getTodate() != null) {
                    sql2 = sql2.replace(":date1", "AND BO.CREATEDDATETIME BETWEEN '" + rpt.getFromdate() + " 0:00:00 AM'  AND '" + rpt.getTodate() + " 23:59:00 PM' ");
                } else {
                    sql2 = sql2.replace(":date1", "");
                }

                statement = connection.prepareStatement(sql2);
                resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    CounterSummeryReport cussum = new CounterSummeryReport();
                    cussum.setCounterid(obj.getString("APPLICATIONCOUNTERID"));
                    cussum.setCountername(obj.getString("DESCRIPTION"));
                    cussum.setNumberOfApplicant(Integer.valueOf(obj.getString("APPLICANTCOUNT")));
                    cussum.setNoOfDocs(resultSet.getInt("ACCP"));
                    csr.add(cussum);
                }

            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return csr;
    }

    public List<CounterSummeryReport> CounterSummeryByHoursReport(Report rpt) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<CounterSummeryReport> csr = new ArrayList<>();
        try {
//            "BOOKEDDATE<='2016-11-25' AND BOOKEDDATE>='2016-11-20' AND BOOKEDSDID='SCO11'"
            connection = dataSource.getConnection();

            JSONArray array = new JSONArray();

            String sql = "SELECT DATE_FORMAT(BO.CREATEDDATETIME, '%Y-%m-%d') AS CREATEDDATETIME, COUNT(BO.BOOKINGID) AS APPLICANTCOUNT, BO.ISVIP, "
                    + "       BO.APPLICATIONCOUNTER AS APPLICATIONCOUNTERID,AC.DESCRIPTION, CASE BO.ISVIP  WHEN '1' THEN '--' WHEN '0' THEN CONCAT(hour(BO.STARTTIME), ':00-',hour(BO.STARTTIME) + 1, ':00') END  AS HOURS "
                    + "FROM   bookings BO JOIN applicationcounter AC ON BO.APPLICATIONCOUNTER = AC.APPLICATIONCOUNTERID "
                    + "WHERE  1 = 1 ";
            if (rpt.getFromdate() != null && rpt.getTodate() != null) {
                sql += " AND BO.CREATEDDATETIME BETWEEN '" + rpt.getFromdate() + " 0:00:00 AM'  AND '" + rpt.getTodate() + " 23:59:00 PM' ";
            }
            if (!rpt.getCountername().contentEquals("")) {
                sql += "AND AC.APPLICATIONCOUNTERID='" + rpt.getCountername() + "' ";
            }

            sql += "GROUP BY Hours ORDER BY      Hours ASC ,BO.CREATEDDATETIME asc";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("APPLICANTCOUNT", resultSet.getString("APPLICANTCOUNT"));
                jsonObj.put("APPLICATIONCOUNTERID", resultSet.getString("APPLICATIONCOUNTERID"));
                jsonObj.put("DESCRIPTION", resultSet.getString("DESCRIPTION"));
                jsonObj.put("CREATEDDATETIME", resultSet.getString("CREATEDDATETIME"));
                jsonObj.put("HOURS", resultSet.getString("HOURS"));
                array.put(jsonObj);
            }

            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                String sql2 = "SELECT DATE_FORMAT( BO.CREATEDDATETIME, '%Y-%m-%d') as CREATEDDATETIME,  AC.DESCRIPTION,  BO.APPLICATIONCOUNTER, COUNT(BV.BOOKINGVERIFICATIONDOCID) AS DOCCOUNT,COUNT(BO.BOOKINGID) as APPLICANTCOUNT, BO.ISVIP,  "
                        + "                                   CASE BO.ISVIP  WHEN '1' THEN '--' WHEN '0' THEN CONCAT(hour(BO.STARTTIME), ':00-',hour(BO.STARTTIME) + 1, ':00') END  AS Hours   "
                        + "                         FROM      bookings BO  join applicationcounter AC on BO.APPLICATIONCOUNTER=AC.APPLICATIONCOUNTERID LEFT OUTER JOIN bookingverificationdocs BV ON BV.BOOKINGID = BO.BOOKINGID    "
                        + "                         WHERE     1 = 1  :date1";
                sql2 = sql2.replace(":date1", "AND BO.CREATEDDATETIME BETWEEN '" + obj.getString("CREATEDDATETIME") + " 0:00:00 AM'  AND '" + obj.getString("CREATEDDATETIME") + " 23:59:00 PM' ");

                sql2 += " AND AC.APPLICATIONCOUNTERID='" + obj.getString("APPLICATIONCOUNTERID") + "' ";

                sql2 += "	GROUP BY Hours ORDER BY     BO.CREATEDDATETIME asc, Hours ASC";
                statement = connection.prepareStatement(sql2);
                resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    if (obj.getString("APPLICATIONCOUNTERID").contentEquals(resultSet.getString("APPLICATIONCOUNTER"))) {
                        if (obj.getString("HOURS").contentEquals(resultSet.getString("Hours"))) {
                            CounterSummeryReport cussum = new CounterSummeryReport();
                            cussum.setCounterid(obj.getString("APPLICATIONCOUNTERID"));
                            cussum.setCountername(obj.getString("DESCRIPTION"));
                            cussum.setNumberOfApplicant(obj.getInt("APPLICANTCOUNT"));
                            cussum.setNoOfDocs(resultSet.getInt("DOCCOUNT"));
                            cussum.setDatetime(obj.getString("CREATEDDATETIME"));
                            cussum.setTimeslout(resultSet.getString("Hours"));
                            csr.add(cussum);
                        }

                    }

                }

            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return csr;
    }

    public int getNumberOfACCEPTANDREJECT(String where) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        int Count = 0;
        try {
            String sql = "SELECT COUNT(BOOKINGID) AS APPLICANTCOUNT FROM bookings WHERE :where";
            sql = sql.replace(":where", where);
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Count = resultSet.getInt("APPLICANTCOUNT");
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return Count;
    }

    public Map<String, String> getCounterDropdownList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Map<String, String> counterList = null;

        try {
            String sql = "SELECT SDID, DESCRIPTION FROM servicedesks WHERE STATUS=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, StatusVarList.ACTIVE);
            resultSet = statement.executeQuery();
            counterList = new LinkedHashMap<>();
            counterList.put("", "-- Select --");
            while (resultSet.next()) {
                counterList.put(resultSet.getString("SDID"), resultSet.getString("DESCRIPTION"));
            }
        } catch (SQLException exception) {
            LogFileCreator.writeErrorToLog(exception);
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                }
            }
        }
        return counterList;
    }

    public Map<String, String> getDocChargesDropdownList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Map<String, String> mapList = null;

        try {
            String sql = "SELECT DOCCHARGECODE, DESCRIPTION FROM documentchargers";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            mapList = new LinkedHashMap<>();
            mapList.put("", "-- Select --");
            while (resultSet.next()) {
                mapList.put(resultSet.getString("DOCCHARGECODE"), resultSet.getString("DESCRIPTION"));
            }
        } catch (SQLException exception) {
            LogFileCreator.writeErrorToLog(exception);
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                }
            }
        }
        return mapList;
    }

    public Map<String, String> getAcceptedOrRejectStatusDropdownList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Map<String, String> mapList = null;

        try {
            String sql = "SELECT STATUSCODE, STATUSDESCRIPTION FROM status WHERE CATEGORYID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, StatusVarList.QM_CODE_STATUS_CATEGORY_DOCUMENT_VERIFICATION);
            resultSet = statement.executeQuery();
            mapList = new LinkedHashMap<>();
            mapList.put("", "-- Select --");
            while (resultSet.next()) {
                mapList.put(resultSet.getString("STATUSCODE"), resultSet.getString("STATUSDESCRIPTION"));
            }
        } catch (SQLException exception) {
            LogFileCreator.writeErrorToLog(exception);
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                }
            }
        }
        return mapList;
    }

    public Map<String, String> getApplicationTypeDropdownList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Map<String, String> mapList = null;

        try {
            String sql = "SELECT APPICATIONTYPEID, DESCRIPTION FROM applicationtype";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            mapList = new LinkedHashMap<>();
            mapList.put("", "-- Select --");
            while (resultSet.next()) {
                mapList.put(resultSet.getString("APPICATIONTYPEID"), resultSet.getString("DESCRIPTION"));
            }
        } catch (SQLException exception) {
            LogFileCreator.writeErrorToLog(exception);
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                }
            }
        }
        return mapList;
    }

    public Map<String, String> getDocumentTypeDropdownList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Map<String, String> mapList = null;

        try {
            String sql = "SELECT DOCUMENTTYPEID, DESCRIPTION FROM documenttype";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            mapList = new LinkedHashMap<>();
            mapList.put("", "-- Select --");
            while (resultSet.next()) {
                mapList.put(resultSet.getString("DOCUMENTTYPEID"), resultSet.getString("DESCRIPTION"));
            }
        } catch (SQLException exception) {
            LogFileCreator.writeErrorToLog(exception);
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                }
            }
        }
        return mapList;
    }

    public List<AppicationSummeryReport> ApplicantSummery(Report rpt) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<AppicationSummeryReport> listData = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            String sql = "SELECT DISTINCT B.BOOKINGID, DATE_FORMAT(B.BOOKEDDATE, '%Y-%m-%d') AS BOOKEDDATE, CU.CUSTOMERID,  "
                    + "                               CONCAT(CU.FIRSTNAME, ' ',  IFNULL(CU.LASTNAME,'')) AS CUSTOMERNAME, IFNULL(CU.MOBILE, '') AS MOBILE, AT.DESCRIPTION AS ATDES, IFNULL( CONCAT(CU.IDENTIFICATIONTYPECODE,' - ', CU.IDENTIFICATIONNUMBER ),'-') AS IDENTIFICATIONNUMBER,count( BV. BOOKINGVERIFICATIONDOCID) as DCOUNT , DATE_FORMAT(B.STARTTIME, '%l:%i %p') as STARTTIME , DATE_FORMAT(B.ENDTIME, '%l:%i %p') as ENDTIME, AC.DESCRIPTION, DATE_FORMAT(B.CREATEDDATETIME,'%Y-%m-%d %l:%i %p') as  CREATEDDATETIME"
                    + "                   FROM   bookings B  "
                    + "                          INNER JOIN customer CU ON B.CUSTOMERID = CU.CUSTOMERID  "
                    + "                          INNER JOIN applicationtype AT ON B.APPLICATIONTYPEID = AT.APPICATIONTYPEID  "
                    + "                          INNER JOIN bookingverificationdocs BV ON B.BOOKINGID = BV.BOOKINGID "
                    + "                          INNER JOIN applicationcounter AC ON B.APPLICATIONCOUNTER= AC.APPLICATIONCOUNTERID "
                    + "WHERE  1 = 1 ";
            if (!rpt.getCustomername().contentEquals("")) {
                sql += " AND CU.FIRSTNAME LIKE '%" + rpt.getCustomername() + "%'";
            }
            if (!rpt.getApplicationtype().contentEquals("")) {
                sql += " AND B.APPLICATIONTYPEID='" + rpt.getApplicationtype() + "'";
            }
            if (!rpt.getIdentyficationnumber().contentEquals("")) {
                sql += " AND CU.IDENTIFICATIONNUMBER='" + rpt.getIdentyficationnumber() + "'";
            }
//            if (rpt.getForingorloacal() != null || !rpt.getForingorloacal().contentEquals("")) {
//                sql += " AND BV.DOCUMENTCHARECODE='" + rpt.getForingorloacal() + "'";
//            }
            if (rpt.getFromdate() != null && rpt.getTodate() != null) {
                sql += " AND B.CREATEDDATETIME BETWEEN '" + rpt.getFromdate() + " 00:00:00'  AND '" + rpt.getTodate() + " 23:59:00'";
            }

            sql += " group by B.BOOKINGID ";
            sql += " order by B.CREATEDDATETIME ";
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            JSONArray arry = new JSONArray();
            while (resultSet.next()) {
                JSONObject obj = new JSONObject();
                obj.put("BOOKINGID", resultSet.getString("BOOKINGID"));
                obj.put("CUSTOMERID", resultSet.getString("CUSTOMERID"));
                obj.put("CUSTOMERNAME", resultSet.getString("CUSTOMERNAME"));
                obj.put("MOBILE", resultSet.getString("MOBILE"));
                obj.put("ATDES", resultSet.getString("ATDES"));
                obj.put("BOOKEDDATE", resultSet.getString("BOOKEDDATE"));
                obj.put("IDENTIFICATIONNUMBER", resultSet.getString("IDENTIFICATIONNUMBER"));
                obj.put("DCOUNT", resultSet.getString("DCOUNT"));
                obj.put("STARTTIME", resultSet.getString("STARTTIME"));
                obj.put("ENDTIME", resultSet.getString("ENDTIME"));
                obj.put("DESCRIPTION", resultSet.getString("DESCRIPTION"));
                obj.put("CREATEDDATETIME", resultSet.getString("CREATEDDATETIME"));
                arry.put(obj);
            }
            for (int i = 0; i < arry.length(); i++) {
                JSONObject jsonobj = arry.getJSONObject(i);
                String rejdocumettype = "";
                String accpdocumettype = "";

                AppicationSummeryReport rptdata = new AppicationSummeryReport();
                rptdata.setBookingid(jsonobj.getString("BOOKINGID"));
                rptdata.setBookingdate(jsonobj.getString("BOOKEDDATE"));
                rptdata.setCustomerid(jsonobj.getString("CUSTOMERID"));
                rptdata.setCustomername(jsonobj.getString("CUSTOMERNAME"));
                rptdata.setMobilenumber(jsonobj.getString("MOBILE"));
                rptdata.setApplicationtype(jsonobj.getString("ATDES"));
                rptdata.setIdentyficationnumber(jsonobj.getString("IDENTIFICATIONNUMBER"));
                rptdata.setDocumentcount(jsonobj.getString("DCOUNT"));
                rptdata.setStarttime(jsonobj.getString("STARTTIME"));
                rptdata.setEndtime(jsonobj.getString("ENDTIME"));
                rptdata.setCountername(jsonobj.getString("DESCRIPTION"));
                rptdata.setCreatdate(jsonobj.getString("CREATEDDATETIME"));

                listData.add(rptdata);
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return listData;
    }

    public List<ApplicantDetailSummeryReport> ApplicantDetailReport(Report rpt) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<ApplicantDetailSummeryReport> listData = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            String sql = "SELECT C.CUSTOMERID, CONCAT(IFNULL(C.FIRSTNAME,''), ' ',IFNULL( C.LASTNAME,'')) AS Name, "
                    + "       concat(C.IDENTIFICATIONNUMBER, ' - ', DT.DESCRIPTION) AS IDENTI, IFNULL( C.EMAIL,'--') AS EMAIL, "
                    + "     IFNULL(  C.MOBILE,'--') AS MOBILE , concat(IFNULL(C.ADDRESSLINE1,''), ' ', IFNULL(C.ADDRESSLINE2,''), ' ',  IFNULL(C.ADDRESSLINE3,'') ) as ADDRESS  "
                    + " FROM   customer C INNER JOIN documenttype DT ON C.IDENTIFICATIONTYPECODE = DT.DOCUMENTTYPEID INNER JOIN bookings B ON B.CUSTOMERID=C.CUSTOMERID"
                    + " WHERE  1 = 1 ";
            if (rpt.getFromdate() != null && rpt.getTodate() != null) {
                sql += " AND B.CREATEDDATETIME BETWEEN '" + rpt.getFromdate() + " 0:00:00'  AND '" + rpt.getTodate() + " 23:59:00' ";
            }
            if (!rpt.getIdentyficationnumber().contentEquals("")) {
                sql += " AND C.IDENTIFICATIONNUMBER='" + rpt.getIdentyficationnumber() + "'";
            }
            if (!rpt.getApplicationtype().contentEquals("")) {
                sql += " AND B.APPLICATIONTYPEID='" + rpt.getApplicationtype() + "'";
            }
            if (!rpt.getCustomername().contentEquals("")) {
                sql += " AND C.FIRSTNAME LIKE '%" + rpt.getCustomername() + "%'";
            }
            sql += " order by B.CREATEDDATETIME";
            JSONArray arry = new JSONArray();
            JSONArray arry2 = new JSONArray();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                JSONObject obj = new JSONObject();
//                ApplicantDetailSummeryReport rptdata = new ApplicantDetailSummeryReport();
//               
                obj.put("CUSTOMERID", resultSet.getString("CUSTOMERID"));
                obj.put("NAME", resultSet.getString("NAME"));
                obj.put("IDENTI", resultSet.getString("IDENTI"));
                obj.put("EMAIL", resultSet.getString("EMAIL"));
                obj.put("MOBILE", resultSet.getString("MOBILE"));
                obj.put("ADDRESS", resultSet.getString("ADDRESS"));
                arry.put(obj);
            }
            for (int i = 0; i < arry.length(); i++) {
                JSONObject objjason = new JSONObject();
                objjason = arry.getJSONObject(i);
                sql = "select B.BOOKINGID, DATE_FORMAT( B.BOOKEDDATE, '%Y-%m-%d') as BOOKEDDATE, DATE_FORMAT( B.CREATEDDATETIME, '%Y-%m-%d %l:%i %p') as CREATEDDATETIME, DATE_FORMAT( B.STARTTIME, '%l:%i %p') as STARTTIME, DATE_FORMAT( B.ENDTIME, '%l:%i %p') as ENDTIME, B.APPLICATIONCOUNTER,  AC.DESCRIPTION from bookings  B inner join applicationcounter AC on B.APPLICATIONCOUNTER=AC.APPLICATIONCOUNTERID  where B.CUSTOMERID=?";
                statement = connection.prepareStatement(sql);
                statement.setString(1, objjason.getString("CUSTOMERID"));
                resultSet = statement.executeQuery();
                while (resultSet.next()) {

                    JSONObject obj = new JSONObject();
//                    rptdata.setBookingid(resultSet.getString("BOOKINGID"));
//                    rptdata.setBookingdate(resultSet.getString("BOOKEDDATE"));
//                    rptdata.setStarttime(resultSet.getString("STARTTIME"));
//                    rptdata.setEndtime(resultSet.getString("ENDTIME"));
//                    rptdata.setCounterid(resultSet.getString("APPLICATIONCOUNTER"));
//                    rptdata.setCountername(resultSet.getString("DESCRIPTION"));
                    obj.put("BOOKINGID", resultSet.getString("BOOKINGID"));
                    obj.put("BOOKEDDATE", resultSet.getString("BOOKEDDATE"));
                    obj.put("STARTTIME", resultSet.getString("STARTTIME"));
                    obj.put("ENDTIME", resultSet.getString("ENDTIME"));
                    obj.put("APPLICATIONCOUNTER", resultSet.getString("APPLICATIONCOUNTER"));
                    obj.put("DESCRIPTION", resultSet.getString("DESCRIPTION"));
                    obj.put("CUSTOMERID", (objjason.getString("CUSTOMERID")));
                    if (objjason.getString("NAME") != null) {
                        obj.put("NAME", objjason.getString("NAME"));
                    } else {
                        obj.put("NAME", "--");
                    }
                    if (objjason.getString("IDENTI") != null) {
                        obj.put("IDENTI", objjason.getString("IDENTI"));
                    } else {
                        obj.put("IDENTI", "--");
                    }
                    if (objjason.getString("EMAIL") != null) {
                        obj.put("EMAIL", objjason.getString("EMAIL"));
                    } else {
                        obj.put("EMAIL", "--");
                    }
                    if (objjason.getString("MOBILE") != null) {
                        obj.put("MOBILE", objjason.getString("MOBILE"));
                    } else {
                        obj.put("MOBILE", "--");
                    }

                    if (objjason.getString("ADDRESS") != null) {
                        obj.put("ADDRESS", objjason.getString("ADDRESS"));
                    } else {
                        obj.put("ADDRESS", "--");
                    }
                    obj.put("CREATEDDATETIME", resultSet.getString("CREATEDDATETIME"));
                    arry2.put(obj);

                }
            }
            for (int i = 0; i < arry2.length(); i++) {
                JSONObject objjason = new JSONObject();
                objjason = arry2.getJSONObject(i);
                String sql2 = "SELECT CONCAT(DT.DESCRIPTION, ' - ',  BV.REFERENCE ) as DOCUMNETTYPE FROM bookingverificationdocs BV INNER JOIN bookings B on BV.BOOKINGID=B.BOOKINGID INNER JOIN documenttype DT on DT.DOCUMENTTYPEID=BV.DOCUMENTTYPE WHERE B.BOOKINGID=?";
                statement = connection.prepareStatement(sql2);
                statement.setString(1, objjason.getString("BOOKINGID"));
                resultSet = statement.executeQuery();
                String documentfr = "";
                while (resultSet.next()) {

                    documentfr += "<br/>" + resultSet.getString("DOCUMNETTYPE") + "<br/>";
                }
                ApplicantDetailSummeryReport rptdata = new ApplicantDetailSummeryReport();

                rptdata.setBookingid(objjason.getString("BOOKINGID"));
                rptdata.setBookingdate(objjason.getString("BOOKEDDATE"));
                rptdata.setStarttime(objjason.getString("STARTTIME"));
                rptdata.setEndtime(objjason.getString("ENDTIME"));
                rptdata.setCounterid(objjason.getString("APPLICATIONCOUNTER"));
                rptdata.setCountername(objjason.getString("DESCRIPTION"));
                rptdata.setDocumentdetails(documentfr);
                if (objjason.getString("NAME") != null) {
                    rptdata.setCustomername(objjason.getString("NAME"));
                } else {
                    rptdata.setCustomername("--");
                }
                if (objjason.getString("IDENTI") != null) {
                    rptdata.setIdentyficationnumber(objjason.getString("IDENTI"));
                } else {
                    rptdata.setIdentyficationnumber("--");
                }
                if (objjason.getString("EMAIL") != null) {
                    rptdata.setEmail(objjason.getString("EMAIL"));
                } else {
                    rptdata.setEmail("--");
                }
                if (objjason.getString("MOBILE") != null) {
                    rptdata.setMobile(objjason.getString("MOBILE"));
                } else {
                    rptdata.setMobile("--");
                }

                if (objjason.getString("ADDRESS") != null) {
                    rptdata.setAddress(objjason.getString("ADDRESS"));
                } else {
                    rptdata.setAddress("--");
                }
                rptdata.setCreateddatetime(objjason.getString("CREATEDDATETIME"));

                listData.add(rptdata);
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return listData;
    }

    public Map<String, String> getCounteList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Map<String, String> counterList = null;

        try {
            String sql = "SELECT APPLICATIONCOUNTERID, DESCRIPTION FROM applicationcounter WHERE STATUS=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, StatusVarList.ACTIVE);
            resultSet = statement.executeQuery();
            counterList = new LinkedHashMap<>();
            counterList.put("", "-- Select --");
            while (resultSet.next()) {
                counterList.put(resultSet.getString("APPLICATIONCOUNTERID"), resultSet.getString("DESCRIPTION"));
            }
        } catch (SQLException exception) {
            LogFileCreator.writeErrorToLog(exception);
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                }
            }
        }
        return counterList;
    }

    public Map<String, String> getUserList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Map<String, String> counterList = null;

        try {
            String sql = "SELECT USERNAME, CONCAT(FIRSTNAME,' ',LASTNAME) AS NAME FROM systemuser";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            counterList = new LinkedHashMap<>();
            counterList.put("", "-- Select --");
            while (resultSet.next()) {
                counterList.put(resultSet.getString("USERNAME"), resultSet.getString("NAME"));
            }
        } catch (SQLException exception) {
            LogFileCreator.writeErrorToLog(exception);
            throw exception;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                }
            }
        }
        return counterList;
    }

    public List<CounterSummeryReport> UserDeurationReport(Report rpt) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<CounterSummeryReport> csr = new ArrayList<>();
        try {
            connection = dataSource.getConnection();

            JSONArray array = new JSONArray();

            String sql = "SELECT COUNT(B.BOOKINGID) AS APPLICANTCOUNT, AC.DESCRIPTION, APPLICATIONCOUNTERID  FROM bookings B inner join applicationcounter AC on B.APPLICATIONCOUNTER=AC.APPLICATIONCOUNTERID   WHERE 1=1 ";
            if (rpt.getFromdate() != null && rpt.getTodate() != null) {
                sql += "AND BOOKEDDATE BETWEEN '" + rpt.getFromdate() + "'  AND '" + rpt.getTodate() + "' ";
            }
            if (!rpt.getCountername().contentEquals("")) {
                sql += "AND B.APPLICATIONCOUNTER='" + rpt.getCountername() + "'";
            }
            if (!rpt.getCountrtuser().contentEquals("")) {
                sql += "AND B.CREATEDUSER= '" + rpt.getCountrtuser() + "'";
            }
            if (!rpt.getStarttime().contentEquals("") && !rpt.getEndtime().contentEquals("")) {
                sql += "AND STARTTIME BETWEEN ? AND ? ";
            }
//            if (!rpt.getAcceptorreject().contentEquals("")) {
//                sql += " AND BV.STATUS='" + rpt.getAcceptorreject() + "'";
//            }
            sql += " group by APPLICATIONCOUNTER";
            statement = connection.prepareStatement(sql);
            if (!rpt.getStarttime().contentEquals("") && !rpt.getEndtime().contentEquals("")) {
                statement.setTimestamp(1, Common.getTimestampFromDateAndTime("yyyy-MM-dd hh:mm a", rpt.getFromdate() + " " + rpt.getStarttime()));
                statement.setTimestamp(2, Common.getTimestampFromDateAndTime("yyyy-MM-dd hh:mm a", rpt.getTodate() + " " + rpt.getEndtime()));
            }
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("APPLICANTCOUNT", resultSet.getString("APPLICANTCOUNT"));
                jsonObj.put("APPLICATIONCOUNTERID", resultSet.getString("APPLICATIONCOUNTERID"));
                jsonObj.put("DESCRIPTION", resultSet.getString("DESCRIPTION"));
                array.put(jsonObj);
            }

            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                String sql2 = "SELECT (SELECT COUNT(BV.BOOKINGVERIFICATIONDOCID) AS COUNTSTATUS "
                        + "        FROM   bookingverificationdocs BV INNER JOIN bookings B ON BV.BOOKINGID = B.BOOKINGID "
                        + "        WHERE   APPLICATIONCOUNTER=BO.APPLICATIONCOUNTER :date1) "
                        + "         AS ACCP "
                        + "FROM   bookings BO "
                        + "WHERE  1 = 1 AND BO.APPLICATIONCOUNTER = '" + obj.getString("APPLICATIONCOUNTERID") + "' "
                        + "GROUP BY BO.APPLICATIONCOUNTER";
                if (rpt.getFromdate() != null && rpt.getTodate() != null) {
                    sql2 = sql2.replace(":date1", " AND B.BOOKEDDATE BETWEEN '" + rpt.getFromdate() + "'  AND '" + rpt.getTodate() + "' ");

                } else {
                    sql2 = sql2.replace(":date1", "1=1");
                }

                statement = connection.prepareStatement(sql2);
                resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    CounterSummeryReport cussum = new CounterSummeryReport();
                    cussum.setCounterid(obj.getString("APPLICATIONCOUNTERID"));
                    cussum.setCountername(obj.getString("DESCRIPTION"));
                    cussum.setNumberOfApplicant(Integer.valueOf(obj.getString("APPLICANTCOUNT")));
                    cussum.setNoOfDocs(resultSet.getInt("ACCP"));
                    csr.add(cussum);
                }

            }

//            for (int i = 0; i < array.length(); i++) {
//                JSONObject obj = array.getJSONObject(i);
//                String sql2 = "SELECT (SELECT COUNT(BV.STATUS) AS COUNTSTATUS "
//                        + "        FROM   bookingverificationdocs BV INNER JOIN bookings B ON BV.BOOKINGID = B.BOOKINGID "
//                        + "        WHERE  BV.STATUS = 'ACCP' AND APPLICATIONCOUNTER=BO.APPLICATIONCOUNTER :date1) "
//                        + "         AS ACCP, "
//                        + "       (SELECT COUNT(BV.STATUS) AS COUNTSTATUS "
//                        + "        FROM   bookingverificationdocs BV INNER JOIN bookings B ON BV.BOOKINGID = B.BOOKINGID "
//                        + "        WHERE  BV.STATUS = 'REJT' AND APPLICATIONCOUNTER=BO.APPLICATIONCOUNTER :date2 ) "
//                        + "         AS REJT "
//                        + "FROM   bookings BO "
//                        + "WHERE  1 = 1 AND BO.APPLICATIONCOUNTER = '" + obj.getString("APPLICATIONCOUNTERID") + "' "
//                        + "GROUP BY BO.APPLICATIONCOUNTER";
//                if (rpt.getFromdate() != null && rpt.getTodate() != null) {
//                    sql2 = sql2.replace(":date1", " AND B.BOOKEDDATE BETWEEN '" + rpt.getFromdate() + "'  AND '" + rpt.getTodate() + "' ");
//                    
//                } else {
//                    sql2 = sql2.replace(":date1", "1=1");
//                }
//                
//                if (rpt.getFromdate() != null && rpt.getTodate() != null) {
//                    sql2 = sql2.replace(":date2", " AND B.BOOKEDDATE BETWEEN '" + rpt.getFromdate() + "'  AND '" + rpt.getTodate() + "' ");
//                    
//                } else {
//                    sql2 = sql2.replace(":date2", "1=1");
//                }
//                
//                if (rpt.getFromdate() != null && rpt.getTodate() != null) {
//                    sql2 += " AND BO.BOOKEDDATE BETWEEN '" + rpt.getFromdate() + "'  AND '" + rpt.getTodate() + "' ";
//                }
//                
//                statement = connection.prepareStatement(sql2);
//                resultSet = statement.executeQuery();
//                while (resultSet.next()) {
//                    CounterSummeryReport cussum = new CounterSummeryReport();
//                    cussum.setCounterid(obj.getString("APPLICATIONCOUNTERID"));
//                    cussum.setCountername(obj.getString("DESCRIPTION"));
//                    cussum.setNumberOfApplicant(Integer.valueOf(obj.getString("APPLICANTCOUNT")));
//                    cussum.setNoOfAcceptedDocs(resultSet.getInt("ACCP"));
//                    cussum.setNoOfRejectedDocs(resultSet.getInt("REJT"));
//                    csr.add(cussum);
//                }
//                
//            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return csr;
    }

}
