/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.dao.impl;

import com.queuemgr.modals.Display;
import com.queuemgr.util.LogFileCreator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 *
 * @author Maheshm
 */
public class DisplayManagerDaoImpl {

    private DataSource dataSource;
//    private Connection connection;
//    private PreparedStatement statement;
//    private ResultSet resultSet;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Display getToken() throws SQLException, Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Display display = null;
        try {
            String sql = "SELECT t.ID,t.TOKENNUMBER,s.DESCRIPTION FROM tokentodisplay t,servicedesks s where s.SDID = t.COUNTERNUMBER ORDER BY t.TOKENNUMBER ASC";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                display = new Display();
                display.setId(resultSet.getString("ID"));
                display.setTokenNo(resultSet.getString("TOKENNUMBER"));
                display.setCounterNo(resultSet.getString("DESCRIPTION"));
                break;
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } catch (Exception e) {
            LogFileCreator.writeErrorToLog(e);
            throw e;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return display;
    }

    public void deleteDocumentType(String recordNumber) throws SQLException, Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "DELETE FROM tokentodisplay WHERE ID = ?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, recordNumber);
            statement.executeUpdate();

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } catch (Exception e) {
            LogFileCreator.writeErrorToLog(e);
            throw e;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
    }

}
