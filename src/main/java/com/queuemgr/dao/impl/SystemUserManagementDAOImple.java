/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.dao.impl;

import com.queuemgr.modals.ServiceDesk;
import com.queuemgr.modals.SystemUser;
import com.queuemgr.modals.UserRole;
import com.queuemgr.util.LogFileCreator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author Kelum Madushan
 */
public class SystemUserManagementDAOImple {

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<SystemUser> getUserList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        SystemUser user = null;
        List<SystemUser> list = new ArrayList<>();
        try {
            String sql = "SELECT su.USERNAME,su.FIRSTNAME,su.USERROLE,ur.DESCRIPTION,su.LASTNAME,su.MOBILE,su.EMAIL,su.NIC FROM systemuser su,userrole ur WHERE su.USERROLE = ur.USERROLEID";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user = new SystemUser();
                user.setUsername(resultSet.getString("USERNAME"));
                user.setFirstname(resultSet.getString("FIRSTNAME"));
                user.setLastname(resultSet.getString("LASTNAME"));
                user.setUserrole(resultSet.getString("USERROLE"));
                user.setUserRoleDes(resultSet.getString("DESCRIPTION"));
                user.setMobile(resultSet.getString("MOBILE"));
                user.setEmail(resultSet.getString("EMAIL"));
                user.setNic(resultSet.getString("NIC"));
                list.add(user);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }

    public List<UserRole> getRoleList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        UserRole role = null;
        List<UserRole> list = null;
        try {
            list = new ArrayList<>();
            String sql = "SELECT USERROLEID,DESCRIPTION FROM userrole";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                role = new UserRole();
                role.setDescription(resultSet.getString("DESCRIPTION"));
                role.setUserroleid(resultSet.getString("USERROLEID"));
                list.add(role);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }

    public void addUser(SystemUser user) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "INSERT INTO systemuser(username,password,firstname,lastname,userrole,mobile,email,nic,createduser,createddatetime) VALUES( ?, ?, ?,?,?,?,?,?,?,CURRENT_TIMESTAMP)";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, user.getUsername().trim());
            statement.setString(2, user.getPassword().trim());
            statement.setString(3, user.getFirstname().trim());
            statement.setString(4, user.getLastname().trim());
            statement.setString(5, user.getUserrole().trim());
            statement.setString(6, user.getMobile().trim());
            statement.setString(7, user.getEmail().trim());
            statement.setString(8, user.getNic().trim());
            statement.setString(9, user.getCreateduser().trim());
            statement.execute();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
    }

    public List<SystemUser> getUser(String username) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        SystemUser user = null;
        List<SystemUser> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM systemuser WHERE username=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, username);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user = new SystemUser();
                user.setUsername(resultSet.getString("USERNAME"));
                user.setFirstname(resultSet.getString("FIRSTNAME"));
                user.setLastname(resultSet.getString("LASTNAME"));
                user.setUserrole(resultSet.getString("USERROLE"));
                user.setMobile(resultSet.getString("MOBILE"));
                user.setEmail(resultSet.getString("EMAIL"));
                user.setNic(resultSet.getString("NIC"));
                list.add(user);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }

    public void updateUser(SystemUser user) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "UPDATE systemuser SET username=?,firstname=?,lastname=?,userrole=?,mobile=?,email=?,nic=?,createduser=?,lastupdatedatetime=CURRENT_TIMESTAMP WHERE username=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, user.getUsername().trim());
            statement.setString(2, user.getFirstname().trim());
            statement.setString(3, user.getLastname().trim());
            statement.setString(4, user.getUserrole().trim());
            statement.setString(5, user.getMobile().trim());
            statement.setString(6, user.getEmail().trim());
            statement.setString(7, user.getNic().trim());
            statement.setString(8, user.getCreateduser().trim());
            statement.setString(9, user.getUsername().trim());
            statement.executeUpdate();
        } catch (SQLException | NumberFormatException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                }
            }
        }
    }
    
    public void deleteUser(String username) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "DELETE FROM systemuser WHERE username=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, username);
            statement.execute();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
    }

    public String checkuser(String username) throws Exception{
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String checkuser="";
        try {
            String sql = "SELECT USERNAME FROM `systemuser` WHERE USERNAME=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, username);
            statement.execute();
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                checkuser=resultSet.getString("USERNAME");
                
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return checkuser;
    }
    
    public String checkusernic(String nic) throws Exception{
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String checkuser="";
        try {
            String sql = "SELECT NIC FROM `systemuser` WHERE NIC=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, nic);
            statement.execute();
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                checkuser=resultSet.getString("NIC");
                
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return checkuser;
    }

}
