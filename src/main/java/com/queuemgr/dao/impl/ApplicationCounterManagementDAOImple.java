/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.dao.impl;

import com.queuemgr.modals.ApplicationCounter;
import com.queuemgr.modals.ServiceDesk;
import com.queuemgr.modals.Status;
import com.queuemgr.util.LogFileCreator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author Kelum Madushan
 */
public class ApplicationCounterManagementDAOImple {

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<ApplicationCounter> getCounterList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ApplicationCounter desk = null;
        List<ApplicationCounter> list = new ArrayList<>();
        try {
            String sql = "SELECT ac.APPLICATIONCOUNTERID,ac.DESCRIPTION,ac.STATUS,s.STATUSDESCRIPTION FROM applicationcounter ac,status s WHERE s.STATUSCODE = ac.STATUS";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                desk = new ApplicationCounter();
                desk.setApplicationcounterid(resultSet.getInt("APPLICATIONCOUNTERID"));
                desk.setDescription(resultSet.getString("DESCRIPTION"));
                desk.setStatus(resultSet.getString("STATUS"));
                desk.setStatusDes(resultSet.getString("STATUSDESCRIPTION"));
                list.add(desk);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }

    public List<Status> getStatus() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Status status = null;
        List<Status> list = null;
        try {
            list = new ArrayList<>();
            String sql = "SELECT STATUSCODE,STATUSDESCRIPTION FROM status JOIN statuscategory ON statuscategory.categoryid=status.categoryid WHERE status.categoryid=2";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                status = new Status();
                status.setStatusCode(resultSet.getString("STATUSCODE"));
                status.setStatusCode(resultSet.getString("STATUSDESCRIPTION"));
                list.add(status);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }

    public void addCounter(ApplicationCounter counter) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "INSERT INTO applicationcounter(description,status,createduser,createddatetime) VALUES( ?, ?, ?,CURRENT_TIMESTAMP)";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, counter.getDescription().trim());
            statement.setString(2, counter.getStatus().trim());
            statement.setString(3, counter.getCreateduser().trim());
            statement.execute();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
    }

    public List<ApplicationCounter> getCounter(int counterid) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ApplicationCounter desk = null;
        List<ApplicationCounter> list = new ArrayList<>();
        try {
            String sql = "SELECT APPLICATIONCOUNTERID,DESCRIPTION,STATUS FROM applicationcounter WHERE APPLICATIONCOUNTERID=" + counterid + "";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                desk = new ApplicationCounter();
                desk.setApplicationcounterid(resultSet.getInt("APPLICATIONCOUNTERID"));
                desk.setDescription(resultSet.getString("DESCRIPTION"));
                desk.setStatus(resultSet.getString("STATUS"));
                list.add(desk);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }

    public void updateCounter(ApplicationCounter counter) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "UPDATE applicationcounter SET DESCRIPTION=?,STATUS=?,CREATEDUSER=?,LASTUPDATEDATETIME=CURRENT_TIMESTAMP WHERE APPLICATIONCOUNTERID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, counter.getDescription());
            statement.setString(2, counter.getStatus());
            statement.setString(3, counter.getCreateduser());
            statement.setInt(4, counter.getApplicationcounterid());
            statement.executeUpdate();
        } catch (SQLException | NumberFormatException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                }
            }
        }
    }

    public void deleteCounter(int counterid) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "DELETE FROM applicationcounter WHERE APPLICATIONCOUNTERID=" + counterid + "";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.execute();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
    }
}
