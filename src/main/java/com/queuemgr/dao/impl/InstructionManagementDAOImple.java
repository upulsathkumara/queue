/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.dao.impl;

import com.queuemgr.modals.Instruction;
import com.queuemgr.modals.Status;
import com.queuemgr.util.LogFileCreator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author Kelum Madushan
 */
public class InstructionManagementDAOImple {

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Instruction> getInstructionList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Instruction instruction = null;
        List<Instruction> list = new ArrayList<>();
        try {
            String sql = "SELECT instructionid,status.statusdescription,english,tamil,sinhala FROM instruction JOIN status ON status.statuscode=instruction.status where status.categoryid=4";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                instruction = new Instruction();
                instruction.setInstructionid(resultSet.getInt("instructionid"));
                instruction.setStatus(resultSet.getString("statusdescription"));
                instruction.setEnglish(resultSet.getString("english"));
                instruction.setTamil(resultSet.getString("tamil"));
                instruction.setSinhala(resultSet.getString("sinhala"));
                list.add(instruction);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }

    public List<Status> getStatus() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Status status = null;
        List<Status> list = null;
        try {
            list = new ArrayList<>();
            String sql = "SELECT STATUSDESCRIPTION,STATUSCODE FROM status JOIN statuscategory ON statuscategory.categoryid=status.categoryid WHERE status.categoryid=4";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                status = new Status();
                status.setStatusDescription(resultSet.getString("STATUSDESCRIPTION"));
                status.setStatusCode(resultSet.getString("STATUSCODE"));
                list.add(status);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }

    public void addInstruction(Instruction instruction) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "INSERT INTO instruction(status,sinhala,english,tamil) VALUES( ?, ?, ?, ?)";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, instruction.getStatus().trim());
            statement.setString(2, instruction.getSinhala().trim());
            statement.setString(3, instruction.getEnglish().trim());
            statement.setString(4, instruction.getTamil().trim());
            statement.execute();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
    }
    
    public List<Instruction> getInstruction(int instructionid) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Instruction ins = null;
        List<Instruction> list = new ArrayList<>();
        try {
            String sql = "SELECT instructionid,status,sinhala,english,tamil FROM instruction WHERE instructionid="+instructionid+"";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ins = new Instruction();
                ins.setInstructionid(resultSet.getInt("instructionid"));
                ins.setStatus(resultSet.getString("status"));
                ins.setEnglish(resultSet.getString("english"));
                ins.setSinhala(resultSet.getString("sinhala"));
                ins.setTamil(resultSet.getString("tamil"));
                list.add(ins);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }
    
    public void deleteInstruction(int instructionid) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "DELETE FROM instruction WHERE instructionid="+instructionid+"";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.execute();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
    }
    public void updateInstruction(Instruction ins) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "UPDATE instruction set STATUS=?,SINHALA=?,ENGLISH=?,TAMIL=? WHERE INSTRUCTIONID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1,ins.getStatus().trim());
            statement.setString(2,ins.getSinhala().trim());
            statement.setString(3,ins.getEnglish().trim());
            statement.setString(4,ins.getTamil().trim());
            statement.setInt(5,ins.getInstructionid());
            statement.executeUpdate();
        } catch (SQLException | NumberFormatException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                }
            }
        }
    }
}
