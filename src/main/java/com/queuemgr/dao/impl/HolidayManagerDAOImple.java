/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.dao.impl;

import com.queuemgr.modals.ApplicationCounter;
import com.queuemgr.modals.Holiday;
import com.queuemgr.modals.Status;
import com.queuemgr.util.Common;
import com.queuemgr.util.LogFileCreator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author Kelum Madushan
 */
public class HolidayManagerDAOImple {

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Holiday> getHolidayList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Holiday day = null;
        List<Holiday> list = new ArrayList<>();
        try {
            String sql = "SELECT h.holidayid,h.holidaydescription,h.holidaydate,h.status,h.starttime,h.endtime,h.halfday,s.STATUSDESCRIPTION FROM holiday h,status s WHERE s.STATUSCODE = h.STATUS";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                day = new Holiday();
                day.setHolidayid(resultSet.getInt("holidayid"));
                day.setHolidaydescription(resultSet.getString("holidaydescription"));
                day.setHolidaydate(resultSet.getString("holidaydate"));
                day.setStatus(resultSet.getString("status"));
                if (resultSet.getString("starttime") != null) {
                    day.setStartTime("(" + resultSet.getString("starttime") + "-");
                    day.setEndTime(resultSet.getString("endtime") + ")");
                } else {
                    day.setStartTime(resultSet.getString("starttime"));
                    day.setEndTime(resultSet.getString("endtime"));
                }
                day.setHalfday(resultSet.getBoolean("halfday"));
                day.setStatusDes(resultSet.getString("STATUSDESCRIPTION"));
                list.add(day);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;

    }

    public List<Status> getStatus() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Status status = null;
        List<Status> list = null;
        try {
            list = new ArrayList<>();
            String sql = "SELECT STATUSCODE,STATUSDESCRIPTION FROM status JOIN statuscategory ON statuscategory.categoryid=status.categoryid WHERE status.categoryid=2";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                status = new Status();
                status.setStatusCode(resultSet.getString("STATUSCODE"));
                status.setStatusDescription(resultSet.getString("STATUSDESCRIPTION"));
                list.add(status);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }

    public void addHoliday(Holiday day) throws SQLException, ParseException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "INSERT INTO holiday(holidaydescription,holidaydate,status,createduser,starttime,endtime,halfday,createddatetime,LASTUPDATEDDATETIME) VALUES( ?,?,?,?,?,?,?,NOW(),NOW())";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, day.getHolidaydescription().trim());
            statement.setString(2, day.getHolidaydate().trim());
            statement.setString(3, day.getStatus().trim());
            statement.setString(4, day.getCreateduser().trim());
            if (day.getStartTime() != null) {
                statement.setString(5, Common.timeConcatanation(day.getStartTime()));
            }else{
                statement.setString(5, null);
            }
            if (day.getEndTime() != null) {
                statement.setString(6, Common.timeConcatanation(day.getEndTime()));
            }else{
                statement.setString(6, null);
            }
            statement.setBoolean(7, day.getHalfday());
            statement.execute();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
    }

    public List<Holiday> getHoliday(int dayid) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Holiday day = null;
        List<Holiday> list = new ArrayList<>();
        try {
            String sql = "SELECT HOLIDAYID,HOLIDAYDESCRIPTION,HOLIDAYDATE,STATUS,STARTTIME,ENDTIME,HALFDAY FROM holiday WHERE HOLIDAYID=" + dayid + "";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                day = new Holiday();
                day.setHolidayid(resultSet.getInt("HOLIDAYID"));
                day.setHolidaydate(resultSet.getString("HOLIDAYDATE"));
                day.setHolidaydescription(resultSet.getString("HOLIDAYDESCRIPTION"));
                day.setStatus(resultSet.getString("STATUS"));
                day.setStartTime(resultSet.getString("STARTTIME"));
                day.setEndTime(resultSet.getString("ENDTIME"));
                day.setHalfday(resultSet.getBoolean("HALFDAY"));
                list.add(day);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }

    public void updateHoliday(Holiday day) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "UPDATE holiday SET HOLIDAYDESCRIPTION=?,STATUS=?,CREATEDUSER=?,LASTUPDATEDDATETIME=now(),STARTTIME=?,ENDTIME=?,HALFDAY=? WHERE HOLIDAYID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
//            statement.setString(1,day.getHolidaydate().trim());
            statement.setString(1, day.getHolidaydescription().trim());
            statement.setString(2, day.getStatus().trim());
            statement.setString(3, day.getCreateduser().trim());
            if (day.getStartTime() != null) {
                statement.setString(4, Common.timeConcatanation(day.getStartTime()));
            }else{
                statement.setString(4, null);
            }
            if (day.getEndTime() != null) {
                statement.setString(5, Common.timeConcatanation(day.getEndTime()));
            }else{
                statement.setString(5, null);
            }
            statement.setBoolean(6, day.getHalfday());
            statement.setInt(7, day.getHolidayid());
            statement.executeUpdate();
        } catch (SQLException | NumberFormatException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                }
            }
        }
    }

    public void deleteHoliday(int dayid) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "DELETE FROM holiday WHERE HOLIDAYID=" + dayid + "";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.execute();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
    }

    public String checkdate(String holyday) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String checkdate = "";
        try {
            String sql = "SELECT `HOLIDAYDATE` FROM `holiday` WHERE `HOLIDAYDATE`=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, holyday);
            statement.execute();
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                checkdate = resultSet.getString("HOLIDAYDATE");

            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return checkdate;
    }
}
