/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.dao.impl;

import com.queuemgr.modals.ServiceDesk;
import com.queuemgr.modals.Status;
import com.queuemgr.util.LogFileCreator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author Kelum Madushan
 */
public class ServiceCounterManagementDAOImple {

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<ServiceDesk> getCounterList() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ServiceDesk desk = null;
        List<ServiceDesk> list = new ArrayList<>();
        try {
            String sql = "SELECT sd.SDID,sd.DESCRIPTION,sd.STATUS,s.STATUSDESCRIPTION FROM servicedesks sd,status s WHERE s.STATUSCODE = sd.STATUS";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                desk = new ServiceDesk();
                desk.setSdid(resultSet.getString("SDID"));
                desk.setDescription(resultSet.getString("DESCRIPTION"));
                desk.setStatus(resultSet.getString("STATUS"));
                desk.setStatusDes(resultSet.getString("STATUSDESCRIPTION"));
                list.add(desk);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }

    public List<Status> getStatus() throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Status status = null;
        List<Status> list = null;
        try {
            list = new ArrayList<>();
            String sql = "SELECT STATUSCODE,STATUSDESCRIPTION FROM status JOIN statuscategory ON statuscategory.categoryid=status.categoryid WHERE status.categoryid=2";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                status = new Status();
                status.setStatusCode(resultSet.getString("STATUSCODE"));
                status.setStatusDescription(resultSet.getString("STATUSDESCRIPTION"));
                list.add(status);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }
    
    public void addCounter(ServiceDesk counter) throws SQLException{
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "INSERT INTO servicedesks(description,status,createduser,createddatetime,LASTUPDATEDDATETIME,SDID) VALUES( ?, ?, ?,NOW(),NOW(),?)";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1,counter.getDescription().trim());
            statement.setString(2,counter.getStatus().trim());
            statement.setString(3,counter.getCreateduser().trim());
            statement.setString(4,counter.getSdid().trim());
            statement.execute();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
    }
    public List<ServiceDesk> getCounter(String sdid) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        ServiceDesk desk = null;
        List<ServiceDesk> list = new ArrayList<>();
        try {
            String sql = "SELECT SDID,DESCRIPTION,STATUS FROM servicedesks WHERE SDID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, sdid);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                desk = new ServiceDesk();
                desk.setSdid(resultSet.getString("SDID"));
                desk.setDescription(resultSet.getString("DESCRIPTION"));
                desk.setStatus(resultSet.getString("STATUS"));
                list.add(desk);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return list;
    }
    
    public void updateCounter(ServiceDesk counter) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "UPDATE servicedesks SET DESCRIPTION=?,STATUS=?,CREATEDUSER=?,LASTUPDATEDDATETIME=NOW() WHERE SDID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1,counter.getDescription().trim());
            statement.setString(2,counter.getStatus().trim());
            statement.setString(3,counter.getCreateduser().trim());
            statement.setString(4,counter.getSdid().trim());
            statement.executeUpdate();
        } catch (SQLException | NumberFormatException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                }
            }
        }
    }
    
    public void deleteCounter(String sdid) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "DELETE FROM servicedesks WHERE SDID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, sdid);
            statement.execute();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
    }
    
    public String checksdid(String sdid) throws Exception{
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String checksdid="";
        try {
            String sql = "SELECT SDID FROM `servicedesks` WHERE SDID=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, sdid);
            statement.execute();
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                checksdid=resultSet.getString("SDID");
                
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return checksdid;
    }
}
