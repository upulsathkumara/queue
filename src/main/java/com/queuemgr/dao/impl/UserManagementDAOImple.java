/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.dao.impl;

import com.queuemgr.modals.Customer;
import com.queuemgr.modals.ResponseToEDAS;
import com.queuemgr.modals.SystemUser;
import com.queuemgr.modals.WorkingTime;
import com.queuemgr.util.LogFileCreator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

public class UserManagementDAOImple {

    private DataSource dataSource;
//    private Connection connection;
//    private PreparedStatement statement;
//    private ResultSet resultSet;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    //return user object for authentication. only return users of status 'active'
    public SystemUser getAuthUserById(String username) throws Exception {
        SystemUser user = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT USERNAME, PASSWORD, USERROLE FROM systemuser WHERE USERNAME=?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, username);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user = new SystemUser();
                user.setUsername(resultSet.getString("USERNAME"));
                user.setPassword(resultSet.getString("PASSWORD"));
                user.setUserrole(resultSet.getString("USERROLE"));
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }

        return user;
    }

    public String getTime() throws Exception {
        String startTime = "";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT SERVICEDESKSTARTTIME FROM commonconfigurations";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);

            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                startTime = resultSet.getString("SERVICEDESKSTARTTIME");
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return startTime;
    }

    public String getEndTime() throws Exception {
        String endTime = "";
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT SERVICEDESKENDTIME FROM commonconfigurations";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);

            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                endTime = resultSet.getString("SERVICEDESKENDTIME");
            }

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

        return endTime;
    }

    public void saveWorkingtime(WorkingTime data) throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "UPDATE"
                    + "     commonconfigurations"
                    + "     SET "
                    + "     SERVICEDESKSTARTTIME = ?, "
                    + "     SERVICEDESKENDTIME = ?,"
                    + "     ONLINEPERCENTAGE = ?,"
                    + "     WALKINPERCENTAGE = ?,"
                    + "     ONLINECUTOFFTIME = ?,"
                    + "     TIMEGAPTOBOOK = ?,"
                    + "     TIMEGAPTOISSUETICKET = ?,"
                    + "     TIMEGAPTOEXPIRETICKET = ?,"
                    + "     TIMEINTERVALTOCALLNEXT = ? "
                    + "     WHERE "
                    + "     1 ";
            String sql1 = "UPDATE commonconfigurations SET SERVICEDESKSTARTTIME=?,SERVICEDESKENDTIME=? WHERE 1";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, data.getStartTime());
            statement.setString(2, data.getEndtime());
            statement.setString(3, data.getOnlinePercentage());
            statement.setString(4, data.getWorkingPercentage());
            statement.setString(5, data.getOnlineCutOffTime());
            statement.setString(6, data.getTimeGapToBook());
            statement.setString(7, data.getBeforeTimeToIssueTicket());
            statement.setString(8, data.getAfterTimeToIssueTicket());
            statement.setString(9, data.getCallNextInterval());
            statement.execute();
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }

    }

    public WorkingTime getCommonConfiguration() throws SQLException {
        WorkingTime workingtime;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT * FROM commonconfigurations";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);

            resultSet = statement.executeQuery();
            workingtime = new WorkingTime();
            while (resultSet.next()) {

                workingtime.setStartTime(resultSet.getString("SERVICEDESKSTARTTIME"));
                workingtime.setEndtime(resultSet.getString("SERVICEDESKENDTIME"));
                workingtime.setOnlinePercentage(resultSet.getString("ONLINEPERCENTAGE"));
                workingtime.setWorkingPercentage(resultSet.getString("WALKINPERCENTAGE"));
                workingtime.setOnlineCutOffTime(resultSet.getString("ONLINECUTOFFTIME"));
                workingtime.setTimeGapToBook(resultSet.getString("TIMEGAPTOBOOK"));
                workingtime.setBeforeTimeToIssueTicket(resultSet.getString("TIMEGAPTOISSUETICKET"));
                workingtime.setAfterTimeToIssueTicket(resultSet.getString("TIMEGAPTOEXPIRETICKET"));
                workingtime.setCallNextInterval(resultSet.getString("TIMEINTERVALTOCALLNEXT"));
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }
        }
        return workingtime;
    }
}
