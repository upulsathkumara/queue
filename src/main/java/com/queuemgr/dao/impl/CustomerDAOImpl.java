/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.dao.impl;

import com.queuemgr.modals.BookingVerification;
import com.queuemgr.modals.Customer;
import com.queuemgr.modals.DocumentType;
import com.queuemgr.util.Common;
import com.queuemgr.util.LogFileCreator;
import com.queuemgr.util.StatusVarList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author Rasika
 */
public class CustomerDAOImpl {

    private DataSource dataSource;
//    private Connection connection;
//    private PreparedStatement statement;
//    private ResultSet resultSet;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Customer> getCustomer(String identificationNumber) throws Exception {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Customer> customerList = null;

        try {
            String sql = "SELECT "
                    + "     FIRSTNAME,"
                    + "     LASTNAME,"
                    + "     EMAIL,"
                    + "     MOBILE,"
                    + "     IDENTIFICATIONNUMBER,"
                    + "     ADDRESSLINE1,"
                    + "     ADDRESSLINE2,"
                    + "     ADDRESSLINE3,"
                    + "     IDENTIFICATIONTYPECODE, "
                    + "     CUSTOMERID,"
                    + "     PREFFEREDLANG "
                    + " FROM "
                    + "     customer "
                    + " WHERE "
                    + "     IDENTIFICATIONNUMBER=?";

            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, identificationNumber);
            resultSet = statement.executeQuery();
            customerList = new ArrayList();
            Customer customer = new Customer();
            while (resultSet.next()) {
                customer.setFristName(resultSet.getString("FIRSTNAME"));
                customer.setLastName(resultSet.getString("LASTNAME"));
                customer.setIdentificationNumber(resultSet.getString("IDENTIFICATIONNUMBER"));
                customer.setEmail(resultSet.getString("EMAIL"));
                customer.setTelephoneMobile(resultSet.getString("MOBILE"));
                customer.setAddressLine1(resultSet.getString("ADDRESSLINE1"));
                customer.setAddressLine2(resultSet.getString("ADDRESSLINE2"));
                customer.setAddressLine3(resultSet.getString("ADDRESSLINE3"));
                customer.setCustomerId(resultSet.getString("CUSTOMERID"));
                customer.setIdentificatioDocumentType(resultSet.getString("IDENTIFICATIONTYPECODE"));
                customer.setPreferredLanguage(resultSet.getString("PREFFEREDLANG"));
                customerList.add(customer);
                break;
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return customerList;
    }

    public List<Customer> getCustomerVIP(String bookingId) throws Exception {
        List<Customer> customerList = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT "
                    + "     FIRSTNAME,"
                    + "     LASTNAME,"
                    + "     IDENTIFICATIONNUMBER,"
                    + "     C.CUSTOMERID,"
                    + "     IDENTIFICATIONTYPECODE "
                    + " FROM "
                    + "     customer C "
                    + " INNER JOIN "
                    + "     bookings B "
                    + " ON "
                    + "     C.CUSTOMERID = B.CUSTOMERID  "
                    + " WHERE "
                    + "     B.BOOKINGID=? "
                    + " AND "
                    + "     B.BOOKINGSTATUS='INIT' "
                    + " AND "
                    + "     ISVIP=1";

            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, Integer.parseInt(bookingId));
            resultSet = statement.executeQuery();
            customerList = new ArrayList();
            Customer customer = new Customer();
            while (resultSet.next()) {
                customer.setCustomerId(resultSet.getString("C.CUSTOMERID"));
                customer.setFristName(resultSet.getString("FIRSTNAME"));
                customer.setLastName(resultSet.getString("LASTNAME"));
                customer.setIdentificationNumber(resultSet.getString("IDENTIFICATIONNUMBER"));
                customer.setIdentificatioDocumentType(resultSet.getString("IDENTIFICATIONTYPECODE"));
                customerList.add(customer);
                break;
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return customerList;
    }

    public List<Customer> getOnlineCustomer(int bookingId) throws Exception {
        List<Customer> customerList = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {

            String sql = "SELECT "
                    + "     IDENTIFICATIONTYPECODE,"
                    + "     IDENTIFICATIONNUMBER,"
                    + "     APPLICATIONTYPEID,"
                    + "     FIRSTNAME,"
                    + "     LASTNAME,"
                    + "     ADDRESSLINE1,"
                    + "     ADDRESSLINE2,"
                    + "     ADDRESSLINE3,"
                    + "     EMAIL,"
                    + "     MOBILE,"
                    + "     PREFFEREDLANG,"
                    + "     STARTTIME,"
                    + "     ENDTIME"
                    + " FROM "
                    + "     bookings "
                    + " INNER JOIN "
                    + "     customer "
                    + " ON "
                    + "     customer.CUSTOMERID=bookings.CUSTOMERID"
                    + " WHERE "
                    + "     bookings.BOOKINGID=? AND bookings.ISONLINE=1 AND bookings.BOOKINGSTATUS='INIT'";

            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            resultSet = statement.executeQuery();
            customerList = new ArrayList();
            Customer customer = new Customer();
            while (resultSet.next()) {
                customer.setIdentificatioDocumentType(resultSet.getString("IDENTIFICATIONTYPECODE"));
                customer.setIdentificationNumber(resultSet.getString("IDENTIFICATIONNUMBER"));
                customer.setApplicationType(resultSet.getString("APPLICATIONTYPEID"));
                customer.setFristName(resultSet.getString("FIRSTNAME"));
                customer.setLastName(resultSet.getString("LASTNAME"));
                customer.setAddressLine1(resultSet.getString("ADDRESSLINE1"));
                customer.setAddressLine2(resultSet.getString("ADDRESSLINE2"));
                customer.setAddressLine3(resultSet.getString("ADDRESSLINE3"));
                customer.setEmail(resultSet.getString("EMAIL"));
                customer.setTelephoneMobile(resultSet.getString("MOBILE"));
                customer.setPreferredLanguage(resultSet.getString("PREFFEREDLANG"));
                customer.setStartTimeStr(Common.getTimeFromTimestamp(resultSet.getTimestamp("STARTTIME")));
                customer.setEndtimeStr(Common.getTimeFromTimestamp(resultSet.getTimestamp("ENDTIME")));
                customer.setBookingId(Integer.toString(bookingId));
                customerList.add(customer);
                break;
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return customerList;
    }
    
    public List<Customer> getImmediateCustomer(int bookingId) throws Exception {
        List<Customer> customerList = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {

            String sql = "SELECT "
                    + "     IDENTIFICATIONTYPECODE,"
                    + "     IDENTIFICATIONNUMBER,"
                    + "     APPLICATIONTYPEID,"
                    + "     FIRSTNAME,"
                    + "     LASTNAME,"
                    + "     ADDRESSLINE1,"
                    + "     ADDRESSLINE2,"
                    + "     ADDRESSLINE3,"
                    + "     EMAIL,"
                    + "     MOBILE,"
                    + "     PREFFEREDLANG,"
                    + "     STARTTIME,"
                    + "     ENDTIME"
                    + " FROM "
                    + "     bookings "
                    + " INNER JOIN "
                    + "     customer "
                    + " ON "
                    + "     customer.CUSTOMERID=bookings.CUSTOMERID"
                    + " WHERE "
                    + "     bookings.BOOKINGID=? AND bookings.BOOKINGSERVICESTATUS=?";

            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, bookingId);
            statement.setString(2, StatusVarList.SENT_TO_HELP_DESK);
            resultSet = statement.executeQuery();
            customerList = new ArrayList();
            Customer customer = new Customer();
            while (resultSet.next()) {
                customer.setIdentificatioDocumentType(resultSet.getString("IDENTIFICATIONTYPECODE"));
                customer.setIdentificationNumber(resultSet.getString("IDENTIFICATIONNUMBER"));
                customer.setApplicationType(resultSet.getString("APPLICATIONTYPEID"));
                customer.setFristName(resultSet.getString("FIRSTNAME"));
                customer.setLastName(resultSet.getString("LASTNAME"));
                customer.setAddressLine1(resultSet.getString("ADDRESSLINE1"));
                customer.setAddressLine2(resultSet.getString("ADDRESSLINE2"));
                customer.setAddressLine3(resultSet.getString("ADDRESSLINE3"));
                customer.setEmail(resultSet.getString("EMAIL"));
                customer.setTelephoneMobile(resultSet.getString("MOBILE"));
                customer.setPreferredLanguage(resultSet.getString("PREFFEREDLANG"));
                customer.setStartTimeStr(Common.getTimeFromTimestamp(resultSet.getTimestamp("STARTTIME")));
                customer.setEndtimeStr(Common.getTimeFromTimestamp(resultSet.getTimestamp("ENDTIME")));
                customer.setBookingId(Integer.toString(bookingId));
                customerList.add(customer);
                break;
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return customerList;
    }

}
