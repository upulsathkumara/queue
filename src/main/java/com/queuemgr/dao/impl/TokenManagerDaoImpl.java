/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.dao.impl;

import com.queuemgr.modals.ApplicantObject;
import com.queuemgr.modals.DocumentObject;
import com.queuemgr.modals.ResponseToEDAS;
import com.queuemgr.modals.Token;
import com.queuemgr.util.LogFileCreator;
import com.queuemgr.util.StatusVarList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author Maheshm
 */
public class TokenManagerDaoImpl {

    private DataSource dataSource;
//    private Connection connection;
//    private PreparedStatement statement;
//    private ResultSet resultSet;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Token getAvailableToken() throws SQLException, Exception {
        Token token = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT TOKEN,BOOKINGID FROM bookings WHERE BOOKINGSTATUS=? AND BOOKINGSERVICESTATUS=? AND BOOKEDDATE = CURDATE()";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, StatusVarList.BOOKING_CONFIRMED);
            statement.setString(2, StatusVarList.READY_TO_QUEUE);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                // This will pick a one record randomly if there is more results
                token = new Token();
                token.setTokenNumber(resultSet.getString("TOKEN"));
                token.setServiceId(resultSet.getString("BOOKINGID"));
//                
//                if(StatusVarList.ONLINE_CUSTOMER.equals(resultSet.getString("ISONLINE"))){
//                    token.setBookingType("ONLINE");
//                }else if(StatusVarList.WALKING_CUSTOMER.equals(resultSet.getString("ISONLINE"))){
//                    token.setBookingType("WALKING");
//                }else{
//                    // this is not possible. But added this else clause for application safety purpose. Like If some one change the DB :)
//                }
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } catch (Exception e) {
            throw e;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return token;
    }

    public ApplicantObject getCustomerObjectByServiceId(String bookingId) throws SQLException, Exception {
        ApplicantObject obj = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT C.FIRSTNAME,C.LASTNAME,BI.DOCTYPE,BI.DOCUMENTREFERENCENO,C.EMAIL,C.ADDRESSLINE1,C.ADDRESSLINE2,C.ADDRESSLINE3,C.MOBILE,C.TELEPHONERESIDENCE,B.ISONLINE FROM customer C,bookings B,bookingidentificationdocument BI WHERE C.CUSTOMERID = B.CUSTOMERID AND BI.BOOKINGID = B.BOOKINGID AND B.BOOKINGID =?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, bookingId);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                // This will pick a one record randomly if there is more results
                obj = new ApplicantObject();
                obj.setName(resultSet.getString("FIRSTNAME") + " " + resultSet.getString("LASTNAME"));
                obj.setCitizenIdType(resultSet.getString("DOCTYPE"));
                obj.setCitizenId(resultSet.getString("DOCUMENTREFERENCENO"));
                obj.setEmail(resultSet.getString("EMAIL"));
                obj.setAddress(resultSet.getString("ADDRESSLINE1") + "," + resultSet.getString("ADDRESSLINE2") + "," + resultSet.getString("ADDRESSLINE3"));
                obj.setMobileNo(resultSet.getString("MOBILE"));
                obj.setLandPhoneNo(resultSet.getString("TELEPHONERESIDENCE"));
                if (StatusVarList.ONLINE_CUSTOMER.equals(resultSet.getString("ISONLINE"))) {
                    obj.setAppointmentType("ONLINE");
                } else if (StatusVarList.WALKING_CUSTOMER.equals(resultSet.getString("ISONLINE"))) {
                    obj.setAppointmentType("WALK-IN");
                }
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } catch (Exception e) {
            throw e;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return obj;
    }

    public List<DocumentObject> getDocumentObListByServiceId(String bookingId) throws SQLException, Exception {
        List<DocumentObject> objList = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT DOCUMENTTYPE,REFERENCE,STATUS FROM bookingverificationdocs WHERE BOOKINGID = ?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, bookingId);
            resultSet = statement.executeQuery();

            objList = new ArrayList<>();
            DocumentObject obj = null;

            while (resultSet.next()) {
                obj = new DocumentObject();

                obj.setDocumentType(resultSet.getString("DOCUMENTTYPE"));
                obj.setRemark(resultSet.getString("REFERENCE"));
                obj.setDocStatus(resultSet.getString("STATUS"));

                objList.add(obj);
            }
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } catch (Exception e) {
            throw e;
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return objList;
    }

    public Boolean updateStatusBeforeSendToken(String scId, String serviceId) throws SQLException, Exception {
        boolean status = false;
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "UPDATE bookings SET SERVEDSDID = ? ,BOOKINGSERVICESTATUS = ?,LASTUPDATEDDATETIME = NOW() WHERE BOOKINGID = ?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, scId);
            statement.setString(2, StatusVarList.SENT_TO_QUEUE);
            statement.setString(3, serviceId);
            statement.executeUpdate();
            status = true;

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } catch (Exception e) {
            throw e;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return status;
    }

    public ResponseToEDAS updateBookingServiceStatus(String serviceId, String status) throws SQLException, Exception {
        ResponseToEDAS response = null;
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            String sql = "UPDATE bookings SET BOOKINGSERVICESTATUS = ?,LASTUPDATEDDATETIME = NOW(), LASTUPDATEDUSER=? WHERE BOOKINGID = ?";
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(sql);
            statement.setString(1, status);
            statement.setString(2, StatusVarList.EDAS_USER);
            statement.setString(3, serviceId);
            statement.executeUpdate();

            response = new ResponseToEDAS();
            response.setMessage(StatusVarList.RESPONSE_UPATE_SUCCESS);
            response.setResponseCode(StatusVarList.RESPONSE_CODE_UPATE_SUCCESS);

        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            throw ex;
        } catch (Exception e) {
            throw e;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ex) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ex) {
                }
            }

        }
        return response;
    }

}
