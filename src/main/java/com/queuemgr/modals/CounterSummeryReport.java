/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 *
 * @author user
 */
public class CounterSummeryReport {

    private int numberOfApplicant;
    private int noOfAcceptedDocs;
    private int noOfRejectedDocs;
    private String counteruser;
    private String countername;
    private String counterid;
    private String datetime;
    private int noOfDocs;
    private String timeslout;

    

    /**
     * @return the counteruser
     */
    public String getCounteruser() {
        return counteruser;
    }

    /**
     * @param counteruser the counteruser to set
     */
    public void setCounteruser(String counteruser) {
        this.counteruser = counteruser;
    }

    /**
     * @return the countername
     */
    public String getCountername() {
        return countername;
    }

    /**
     * @param countername the countername to set
     */
    public void setCountername(String countername) {
        this.countername = countername;
    }

    /**
     * @return the counterid
     */
    public String getCounterid() {
        return counterid;
    }

    /**
     * @param counterid the counterid to set
     */
    public void setCounterid(String counterid) {
        this.counterid = counterid;
    }

    /**
     * @return the datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    /**
     * @return the numberOfApplicant
     */
    public int getNumberOfApplicant() {
        return numberOfApplicant;
    }

    /**
     * @param numberOfApplicant the numberOfApplicant to set
     */
    public void setNumberOfApplicant(int numberOfApplicant) {
        this.numberOfApplicant = numberOfApplicant;
    }

    /**
     * @return the noOfAcceptedDocs
     */
    public int getNoOfAcceptedDocs() {
        return noOfAcceptedDocs;
    }

    /**
     * @param noOfAcceptedDocs the noOfAcceptedDocs to set
     */
    public void setNoOfAcceptedDocs(int noOfAcceptedDocs) {
        this.noOfAcceptedDocs = noOfAcceptedDocs;
    }

    /**
     * @return the noOfRejectedDocs
     */
    public int getNoOfRejectedDocs() {
        return noOfRejectedDocs;
    }

    /**
     * @param noOfRejectedDocs the noOfRejectedDocs to set
     */
    public void setNoOfRejectedDocs(int noOfRejectedDocs) {
        this.noOfRejectedDocs = noOfRejectedDocs;
    }

    /**
     * @return the noOfDocs
     */
    public int getNoOfDocs() {
        return noOfDocs;
    }

    /**
     * @param noOfDocs the noOfDocs to set
     */
    public void setNoOfDocs(int noOfDocs) {
        this.noOfDocs = noOfDocs;
    }

    /**
     * @return the timeslout
     */
    public String getTimeslout() {
        return timeslout;
    }

    /**
     * @param timeslout the timeslout to set
     */
    public void setTimeslout(String timeslout) {
        this.timeslout = timeslout;
    }

}
