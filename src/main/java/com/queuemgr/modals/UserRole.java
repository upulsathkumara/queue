/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 *
 * @author Kelum Madushan
 */
public class UserRole {
    private String userroleid;
    private String description;

    /**
     * @return the userroleid
     */
    public String getUserroleid() {
        return userroleid;
    }

    /**
     * @param userroleid the userroleid to set
     */
    public void setUserroleid(String userroleid) {
        this.userroleid = userroleid;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
}
