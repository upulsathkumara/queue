/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

import java.sql.Timestamp;

/**
 *
 * @author Maheshm
 */
public class AllBookings {
    private Timestamp startTime;
    private Timestamp endTime;
    private String counterCode;
    private String serviseId;
    private String customerName;
    private String bookingDate;
    
    private String stringStartTime;
    private String stringEndTime;

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public String getCounterCode() {
        return counterCode;
    }

    public void setCounterCode(String counterCode) {
        this.counterCode = counterCode;
    }

    /**
     * @return the serviseId
     */
    public String getServiseId() {
        return serviseId;
    }

    /**
     * @param serviseId the serviseId to set
     */
    public void setServiseId(String serviseId) {
        this.serviseId = serviseId;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the bookingDate
     */
    public String getBookingDate() {
        return bookingDate;
    }

    /**
     * @param bookingDate the bookingDate to set
     */
    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    /**
     * @return the sstartTime
     */
    public String getStringStartTime() {
        return stringStartTime;
    }

    /**
     * @param stringStartTime the sstartTime to set
     */
    public void setStringStartTime(String stringStartTime) {
        this.stringStartTime = stringStartTime;
    }

    /**
     * @return the sendTime
     */
    public String getStringEndTime() {
        return stringEndTime;
    }

    /**
     * @param stringEndTime the sendTime to set
     */
    public void setStringEndTime(String stringEndTime) {
        this.stringEndTime = stringEndTime;
    }
}
