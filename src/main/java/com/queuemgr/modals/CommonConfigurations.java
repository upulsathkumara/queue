/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 *
 * @author user
 */
public class CommonConfigurations {
    private String serviceDeskStartTime;
    private String serviceDeskEndTime;
    private String middleTime;

    /**
     * @return the serviceDeskStartTime
     */
    public String getServiceDeskStartTime() {
        return serviceDeskStartTime;
    }

    /**
     * @param serviceDeskStartTime the serviceDeskStartTime to set
     */
    public void setServiceDeskStartTime(String serviceDeskStartTime) {
        this.serviceDeskStartTime = serviceDeskStartTime;
    }

    /**
     * @return the serviceDeskEndTime
     */
    public String getServiceDeskEndTime() {
        return serviceDeskEndTime;
    }

    /**
     * @param serviceDeskEndTime the serviceDeskEndTime to set
     */
    public void setServiceDeskEndTime(String serviceDeskEndTime) {
        this.serviceDeskEndTime = serviceDeskEndTime;
    }

    /**
     * @return the middleTime
     */
    public String getMiddleTime() {
        return middleTime;
    }

    /**
     * @param middleTime the middleTime to set
     */
    public void setMiddleTime(String middleTime) {
        this.middleTime = middleTime;
    }
    
}
