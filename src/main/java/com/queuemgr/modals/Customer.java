/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

import java.sql.Timestamp;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Rasika
 */
public class Customer {

    private String fristName;
    private String lastName;
    private String bookingId;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String email;
    private String telephoneMobile;
    private String identificationNumber;
    private String customerId;
    private String bookeDsdId;
    private Timestamp startTime;
    private Timestamp endtime;
    private String documentTypeList;
    private String documentVarifictionList;
    private String identificatioDocumentType;
    private String applicationType;
    private String preferredLanguage;
    private String bookingdate;
    private int timeDuration;
    private int sumTimeDuration;
    private List<MultipartFile> files;

    private String startTimeStr;
    private String endtimeStr;

    private int availabelSlot;
    
    private String exceed;
    private String halfday;
    private  String appoinmentType;
    
     private String reason;
     private String reschduleType;
     private String helpdeskreason;

    public String getHelpdeskreason() {
        return helpdeskreason;
    }

    public void setHelpdeskreason(String helpdeskreason) {
        this.helpdeskreason = helpdeskreason;
    }

    public String getAppoinmentType() {
        return appoinmentType;
    }

    public void setAppoinmentType(String appoinmentType) {
        this.appoinmentType = appoinmentType;
    }

    public String getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(String preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    /**
     * @return the fristName
     */
    public String getFristName() {
        return fristName;
    }

    /**
     * @param fristName the fristName to set
     */
    public void setFristName(String fristName) {
        this.fristName = fristName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the addressLine1
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     * @param addressLine1 the addressLine1 to set
     */
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    /**
     * @return the addressLine2
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     * @param addressLine2 the addressLine2 to set
     */
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    /**
     * @return the addressLine3
     */
    public String getAddressLine3() {
        return addressLine3;
    }

    /**
     * @param addressLine3 the addressLine3 to set
     */
    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telephoneMobile
     */
    public String getTelephoneMobile() {
        return telephoneMobile;
    }

    /**
     * @param telephoneMobile the telephoneMobile to set
     */
    public void setTelephoneMobile(String telephoneMobile) {
        this.telephoneMobile = telephoneMobile;
    }

    /**
     * @return the bookeDsdId
     */
    public String getBookeDsdId() {
        return bookeDsdId;
    }

    /**
     * @param bookeDsdId the bookeDsdId to set
     */
    public void setBookeDsdId(String bookeDsdId) {
        this.bookeDsdId = bookeDsdId;
    }

    /**
     * @return the startTime
     */
    public Timestamp getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endtime
     */
    public Timestamp getEndtime() {
        return endtime;
    }

    /**
     * @param endtime the endtime to set
     */
    public void setEndtime(Timestamp endtime) {
        this.endtime = endtime;
    }

    /**
     * @return the documentTypeList
     */
    public String getDocumentTypeList() {
        return documentTypeList;
    }

    /**
     * @param documentTypeList the documentTypeList to set
     */
    public void setDocumentTypeList(String documentTypeList) {
        this.documentTypeList = documentTypeList;
    }

    /**
     * @return the documentVarifictionList
     */
    public String getDocumentVarifictionList() {
        return documentVarifictionList;
    }

    /**
     * @param documentVarifictionList the documentVarifictionList to set
     */
    public void setDocumentVarifictionList(String documentVarifictionList) {
        this.documentVarifictionList = documentVarifictionList;
    }

    /**
     * @return the identificatioDocumentType
     */
    public String getIdentificatioDocumentType() {
        return identificatioDocumentType;
    }

    /**
     * @param identificatioDocumentType the identificatioDocumentType to set
     */
    public void setIdentificatioDocumentType(String identificatioDocumentType) {
        this.identificatioDocumentType = identificatioDocumentType;
    }

    /**
     * @return the identificationNumber
     */
    public String getIdentificationNumber() {
        return identificationNumber;
    }

    /**
     * @param identificationNumber the identificationNumber to set
     */
    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    /**
     * @return the applicationType
     */
    public String getApplicationType() {
        return applicationType;
    }

    /**
     * @param applicationType the applicationType to set
     */
    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    /**
     * @return the files
     */
    public List<MultipartFile> getFiles() {
        return files;
    }

    /**
     * @param files the files to set
     */
    public void setFiles(List<MultipartFile> files) {
        this.files = files;
    }

    /**
     * @return the bookingId
     */
    public String getBookingId() {
        return bookingId;
    }

    /**
     * @param bookingId the bookingId to set
     */
    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    /**
     * @return the customerId
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId the customerId to set
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    /**
     * @return the startTimeStr
     */
    public String getStartTimeStr() {
        return startTimeStr;
    }

    /**
     * @param startTimeStr the startTimeStr to set
     */
    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    /**
     * @return the endtimeStr
     */
    public String getEndtimeStr() {
        return endtimeStr;
    }

    /**
     * @param endtimeStr the endtimeStr to set
     */
    public void setEndtimeStr(String endtimeStr) {
        this.endtimeStr = endtimeStr;
    }

    /**
     * @return the bookingdate
     */
    public String getBookingdate() {
        return bookingdate;
    }

    /**
     * @param bookingdate the bookingdate to set
     */
    public void setBookingdate(String bookingdate) {
        this.bookingdate = bookingdate;
    }

    /**
     * @return the timeDuration
     */
    public int getTimeDuration() {
        return timeDuration;
    }

    /**
     * @param timeDuration the timeDuration to set
     */
    public void setTimeDuration(int timeDuration) {
        this.timeDuration = timeDuration;
    }

    /**
     * @return the sumTimeDuration
     */
    public int getSumTimeDuration() {
        return sumTimeDuration;
    }

    /**
     * @param sumTimeDuration the sumTimeDuration to set
     */
    public void setSumTimeDuration(int sumTimeDuration) {
        this.sumTimeDuration = sumTimeDuration;
    }

    /**
     * @return the availabelSlot
     */
    public int getAvailabelSlot() {
        return availabelSlot;
    }

    /**
     * @param availabelSlot the availabelSlot to set
     */
    public void setAvailabelSlot(int availabelSlot) {
        this.availabelSlot = availabelSlot;
    }

    /**
     * @return the exceed
     */
    public String getExceed() {
        return exceed;
    }

    /**
     * @param exceed the exceed to set
     */
    public void setExceed(String exceed) {
        this.exceed = exceed;
    }

    /**
     * @return the halfday
     */
    public String getHalfday() {
        return halfday;
    }

    /**
     * @param halfday the halfday to set
     */
    public void setHalfday(String halfday) {
        this.halfday = halfday;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * @return the reschduleType
     */
    public String getReschduleType() {
        return reschduleType;
    }

    /**
     * @param reschduleType the reschduleType to set
     */
    public void setReschduleType(String reschduleType) {
        this.reschduleType = reschduleType;
    }

   

}
