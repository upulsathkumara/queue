/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 *
 * @author Kelum Madushan
 */
public class Holiday {
    private int holidayid;
    private String holidaydescription;
    private String holidaydate;
    private String status;
    private String createduser;
    private String createddatetime;
    private String lastupdateddatetime;
    private Boolean halfday;
    private String startTime;
    private String endTime;
    private int holidayCount;
    private String statusDes;

    public String getStatusDes() {
        return statusDes;
    }

    public void setStatusDes(String statusDes) {
        this.statusDes = statusDes;
    }

    /**
     * @return the holidayid
     */
    public int getHolidayid() {
        return holidayid;
    }

    /**
     * @param holidayid the holidayid to set
     */
    public void setHolidayid(int holidayid) {
        this.holidayid = holidayid;
    }

    /**
     * @return the holidaydescription
     */
    public String getHolidaydescription() {
        return holidaydescription;
    }

    /**
     * @param holidaydescription the holidaydescription to set
     */
    public void setHolidaydescription(String holidaydescription) {
        this.holidaydescription = holidaydescription;
    }

    /**
     * @return the holidaydate
     */
    public String getHolidaydate() {
        return holidaydate;
    }

    /**
     * @param holidaydate the holidaydate to set
     */
    public void setHolidaydate(String holidaydate) {
        this.holidaydate = holidaydate;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the createduser
     */
    public String getCreateduser() {
        return createduser;
    }

    /**
     * @param createduser the createduser to set
     */
    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    /**
     * @return the createddatetime
     */
    public String getCreateddatetime() {
        return createddatetime;
    }

    /**
     * @param createddatetime the createddatetime to set
     */
    public void setCreateddatetime(String createddatetime) {
        this.createddatetime = createddatetime;
    }

    /**
     * @return the lastupdateddatetime
     */
    public String getLastupdateddatetime() {
        return lastupdateddatetime;
    }

    /**
     * @param lastupdateddatetime the lastupdateddatetime to set
     */
    public void setLastupdateddatetime(String lastupdateddatetime) {
        this.lastupdateddatetime = lastupdateddatetime;
    }

    /**
     * @return the halfday
     */
    public Boolean getHalfday() {
        return halfday;
    }

    /**
     * @param halfday the halfday to set
     */
    public void setHalfday(Boolean halfday) {
        this.halfday = halfday;
    }

    /**
     * @return the startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the holidayCount
     */
    public int getHolidayCount() {
        return holidayCount;
    }

    /**
     * @param holidayCount the holidayCount to set
     */
    public void setHolidayCount(int holidayCount) {
        this.holidayCount = holidayCount;
    }
    
}
