/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 *
 * @author Kelum Madushan
 */
public class Instruction {
    private int instructionid;
    private String status;
    private String sinhala;
    private String english;
    private String tamil;
    private String lang;

    /**
     * @return the instructionid
     */
    public int getInstructionid() {
        return instructionid;
    }

    /**
     * @param instructionid the instructionid to set
     */
    public void setInstructionid(int instructionid) {
        this.instructionid = instructionid;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the sinhala
     */
    public String getSinhala() {
        return sinhala;
    }

    /**
     * @param sinhala the sinhala to set
     */
    public void setSinhala(String sinhala) {
        this.sinhala = sinhala;
    }

    /**
     * @return the english
     */
    public String getEnglish() {
        return english;
    }

    /**
     * @param english the english to set
     */
    public void setEnglish(String english) {
        this.english = english;
    }

    /**
     * @return the tamil
     */
    public String getTamil() {
        return tamil;
    }

    /**
     * @param tamil the tamil to set
     */
    public void setTamil(String tamil) {
        this.tamil = tamil;
    }

    /**
     * @return the lang
     */
    public String getLang() {
        return lang;
    }

    /**
     * @param lang the lang to set
     */
    public void setLang(String lang) {
        this.lang = lang;
    }
}
