/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 *
 * @author Kelum Madushan
 */
public class DocumentCharges {

    private String documentChargeCode;
    private String description;
    private String fee;
    private String status;
    private String createdUser;
    private String createdDatetime;
    private String lastupdatedatetime;

    /**
     * @return the documentChargeCode
     */
    public String getDocumentChargeCode() {
        return documentChargeCode;
    }

    /**
     * @param documentChargeCode the documentChargeCode to set
     */
    public void setDocumentChargeCode(String documentChargeCode) {
        this.documentChargeCode = documentChargeCode;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the fee
     */
    public String getFee() {
        return fee;
    }

    /**
     * @param fee the fee to set
     */
    public void setFee(String fee) {
        this.fee = fee;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the createdUser
     */
    public String getCreatedUser() {
        return createdUser;
    }

    /**
     * @param createdUser the createdUser to set
     */
    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    /**
     * @return the createdDatetime
     */
    public String getCreatedDatetime() {
        return createdDatetime;
    }

    /**
     * @param createdDatetime the createdDatetime to set
     */
    public void setCreatedDatetime(String createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    /**
     * @return the lastupdatedatetime
     */
    public String getLastupdatedatetime() {
        return lastupdatedatetime;
    }

    /**
     * @param lastupdatedatetime the lastupdatedatetime to set
     */
    public void setLastupdatedatetime(String lastupdatedatetime) {
        this.lastupdatedatetime = lastupdatedatetime;
    }

}
