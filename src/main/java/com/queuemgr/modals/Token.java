/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

import java.util.List;

/**
 *
 * @author Maheshm
 */
public class Token {
    private String tokenNumber;
    private String serviceId;
    private ApplicantObject applicantObject;
    private List<DocumentObject> documentObjects;

    public List<DocumentObject> getDocumentObjects() {
        return documentObjects;
    }

    public void setDocumentObjects(List<DocumentObject> documentObjects) {
        this.documentObjects = documentObjects;
    }

    public ApplicantObject getApplicantObject() {
        return applicantObject;
    }

    public void setApplicantObject(ApplicantObject applicantObject) {
        this.applicantObject = applicantObject;
    }


    public String getTokenNumber() {
        return tokenNumber;
    }

    public void setTokenNumber(String tokenNumber) {
        this.tokenNumber = tokenNumber;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
