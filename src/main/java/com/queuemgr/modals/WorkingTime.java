/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.queuemgr.modals;

/**
 *
 * @author SameeraPJ
 */
public class WorkingTime {
    private String startTime;
    private String endtime;
    private String onlinePercentage;
    private String workingPercentage;
    private String onlineCutOffTime;
    private String timeGapToBook;
    private String beforeTimeToIssueTicket;
    private String afterTimeToIssueTicket;
    private String callNextInterval;

    public String getCallNextInterval() {
        return callNextInterval;
    }

    public void setCallNextInterval(String callNextInterval) {
        this.callNextInterval = callNextInterval;
    }

    public String getBeforeTimeToIssueTicket() {
        return beforeTimeToIssueTicket;
    }

    public void setBeforeTimeToIssueTicket(String beforeTimeToIssueTicket) {
        this.beforeTimeToIssueTicket = beforeTimeToIssueTicket;
    }

    public String getAfterTimeToIssueTicket() {
        return afterTimeToIssueTicket;
    }

    public void setAfterTimeToIssueTicket(String afterTimeToIssueTicket) {
        this.afterTimeToIssueTicket = afterTimeToIssueTicket;
    }

    /**
     * @return the startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endtime
     */
    public String getEndtime() {
        return endtime;
    }

    /**
     * @param endtime the endtime to set
     */
    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    /**
     * @return the onlinePercentage
     */
    public String getOnlinePercentage() {
        return onlinePercentage;
    }

    /**
     * @param onlinePercentage the onlinePercentage to set
     */
    public void setOnlinePercentage(String onlinePercentage) {
        this.onlinePercentage = onlinePercentage;
    }

    /**
     * @return the workingPercentage
     */
    public String getWorkingPercentage() {
        return workingPercentage;
    }

    /**
     * @param workingPercentage the workingPercentage to set
     */
    public void setWorkingPercentage(String workingPercentage) {
        this.workingPercentage = workingPercentage;
    }

    /**
     * @return the onlineCutOffTime
     */
    public String getOnlineCutOffTime() {
        return onlineCutOffTime;
    }

    /**
     * @param onlineCutOffTime the onlineCutOffTime to set
     */
    public void setOnlineCutOffTime(String onlineCutOffTime) {
        this.onlineCutOffTime = onlineCutOffTime;
    }

    /**
     * @return the timeGapToBook
     */
    public String getTimeGapToBook() {
        return timeGapToBook;
    }

    /**
     * @param timeGapToBook the timeGapToBook to set
     */
    public void setTimeGapToBook(String timeGapToBook) {
        this.timeGapToBook = timeGapToBook;
    }
}
