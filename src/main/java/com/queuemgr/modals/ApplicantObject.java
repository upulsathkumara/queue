/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 *
 * @author Maheshm
 */
public class ApplicantObject {
    private String name;
    private String citizenIdType;
    private String citizenId;
    private String email;
    private String address;
    private String mobileNo;
    private String landPhoneNo;
    private String appointmentType;
    private String advancePayment;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCitizenIdType() {
        return citizenIdType;
    }

    public void setCitizenIdType(String citizenIdType) {
        this.citizenIdType = citizenIdType;
    }

    public String getCitizenId() {
        return citizenId;
    }

    public void setCitizenId(String citizenId) {
        this.citizenId = citizenId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getLandPhoneNo() {
        return landPhoneNo;
    }

    public void setLandPhoneNo(String landPhoneNo) {
        this.landPhoneNo = landPhoneNo;
    }

    public String getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        this.appointmentType = appointmentType;
    }

    public String getAdvancePayment() {
        return advancePayment;
    }

    public void setAdvancePayment(String advancePayment) {
        this.advancePayment = advancePayment;
    }
}
