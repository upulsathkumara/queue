/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

import java.sql.Timestamp;

/**
 *
 * @author Maheshm
 */
public class Bookings {
    private Timestamp startDate;
    private Timestamp endDate;
    private String bookeDsdId;

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the bookeDsdId
     */
    public String getBookeDsdId() {
        return bookeDsdId;
    }

    /**
     * @param bookeDsdId the bookeDsdId to set
     */
    public void setBookeDsdId(String bookeDsdId) {
        this.bookeDsdId = bookeDsdId;
    }
}
