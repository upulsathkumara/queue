/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 *
 * @author Rasika
 */
public class DocumentType {

    private String documentTypeId;
    private String quantity;
    private String varificationTime;
    private String description;

    /**
     * @return the documentTypeId
     */
    public String getDocumentTypeId() {
        return documentTypeId;
    }

    /**
     * @param documentTypeId the documentTypeId to set
     */
    public void setDocumentTypeId(String documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    /**
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the varificationTime
     */
    public String getVarificationTime() {
        return varificationTime;
    }

    /**
     * @param varificationTime the varificationTime to set
     */
    public void setVarificationTime(String varificationTime) {
        this.varificationTime = varificationTime;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

}
