/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 *
 * @author maheshl
 */
public class TmeGaps {
    private Double gapBefore;
    private Double gapAfter;

    public Double getGapBefore() {
        return gapBefore;
    }

    public void setGapBefore(Double gapBefore) {
        this.gapBefore = gapBefore;
    }

    public Double getGapAfter() {
        return gapAfter;
    }

    public void setGapAfter(Double gapAfter) {
        this.gapAfter = gapAfter;
    }
}
