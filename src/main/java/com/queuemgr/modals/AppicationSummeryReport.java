/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 * @Author : User
 * @Document : AppicationSummeryReport
 * @Created on : Nov 27, 2016, 10:14:38 AM
 */
public class AppicationSummeryReport {

    private String customerid;
    private String customername;
    private String mobilenumber;
    private String doucmentype;
    private String status;
    private String bookingid;
    private String applicationtype;
    private String acceptdoc;
    private String rejptdoc;
    private String bookingdate;
    private String identyficationnumber;
    private String documentcount;
    private String countername;
    private String starttime;
    private String endtime;
    private String  mobile;
    private String creatdate;

    /**
     * @return the customerid
     */
    public String getCustomerid() {
        return customerid;
    }

    /**
     * @param customerid the customerid to set
     */
    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    /**
     * @return the customername
     */
    public String getCustomername() {
        return customername;
    }

    /**
     * @param customername the customername to set
     */
    public void setCustomername(String customername) {
        this.customername = customername;
    }

    /**
     * @return the mobilenumber
     */
    public String getMobilenumber() {
        return mobilenumber;
    }

    /**
     * @param mobilenumber the mobilenumber to set
     */
    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    /**
     * @return the doucmentype
     */
    public String getDoucmentype() {
        return doucmentype;
    }

    /**
     * @param doucmentype the doucmentype to set
     */
    public void setDoucmentype(String doucmentype) {
        this.doucmentype = doucmentype;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the bookingid
     */
    public String getBookingid() {
        return bookingid;
    }

    /**
     * @param bookingid the bookingid to set
     */
    public void setBookingid(String bookingid) {
        this.bookingid = bookingid;
    }

    /**
     * @return the applicationtype
     */
    public String getApplicationtype() {
        return applicationtype;
    }

    /**
     * @param applicationtype the applicationtype to set
     */
    public void setApplicationtype(String applicationtype) {
        this.applicationtype = applicationtype;
    }

    /**
     * @return the acceptdoc
     */
    public String getAcceptdoc() {
        return acceptdoc;
    }

    /**
     * @param acceptdoc the acceptdoc to set
     */
    public void setAcceptdoc(String acceptdoc) {
        this.acceptdoc = acceptdoc;
    }

    /**
     * @return the rejptdoc
     */
    public String getRejptdoc() {
        return rejptdoc;
    }

    /**
     * @param rejptdoc the rejptdoc to set
     */
    public void setRejptdoc(String rejptdoc) {
        this.rejptdoc = rejptdoc;
    }

    /**
     * @return the bookingdate
     */
    public String getBookingdate() {
        return bookingdate;
    }

    /**
     * @param bookingdate the bookingdate to set
     */
    public void setBookingdate(String bookingdate) {
        this.bookingdate = bookingdate;
    }

    /**
     * @return the identyficationnumber
     */
    public String getIdentyficationnumber() {
        return identyficationnumber;
    }

    /**
     * @param identyficationnumber the identyficationnumber to set
     */
    public void setIdentyficationnumber(String identyficationnumber) {
        this.identyficationnumber = identyficationnumber;
    }

    /**
     * @return the documentcount
     */
    public String getDocumentcount() {
        return documentcount;
    }

    /**
     * @param documentcount the documentcount to set
     */
    public void setDocumentcount(String documentcount) {
        this.documentcount = documentcount;
    }

    /**
     * @return the countername
     */
    public String getCountername() {
        return countername;
    }

    /**
     * @param countername the countername to set
     */
    public void setCountername(String countername) {
        this.countername = countername;
    }

    /**
     * @return the starttime
     */
    public String getStarttime() {
        return starttime;
    }

    /**
     * @param starttime the starttime to set
     */
    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    /**
     * @return the endtime
     */
    public String getEndtime() {
        return endtime;
    }

    /**
     * @param endtime the endtime to set
     */
    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the creatdate
     */
    public String getCreatdate() {
        return creatdate;
    }

    /**
     * @param creatdate the creatdate to set
     */
    public void setCreatdate(String creatdate) {
        this.creatdate = creatdate;
    }

}
