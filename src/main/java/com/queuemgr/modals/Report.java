/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 * @Author : USER
 * @Document : Report
 * @Created on : Nov 26, 2016, 7:52:08 AM
 */
public class Report {

    private String fromdate;
    private String todate;
    private String countername;
    private String customername;
    private String foringorloacal;
    private String acceptorreject;
    private String applicationtype;
    private  int reporttype;
    private  String countrtuser;
    private  String starttime;
    private  String endtime;
    private String  identyficationnumber;

    /**
     * @return the fromdate
     */
    public String getFromdate() {
        return fromdate;
    }

    /**
     * @param fromdate the fromdate to set
     */
    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    /**
     * @return the todate
     */
    public String getTodate() {
        return todate;
    }

    /**
     * @param todate the todate to set
     */
    public void setTodate(String todate) {
        this.todate = todate;
    }

    /**
     * @return the countername
     */
    public String getCountername() {
        return countername;
    }

    /**
     * @param countername the countername to set
     */
    public void setCountername(String countername) {
        this.countername = countername;
    }

    /**
     * @return the customername
     */
    public String getCustomername() {
        return customername;
    }

    /**
     * @param customername the customername to set
     */
    public void setCustomername(String customername) {
        this.customername = customername;
    }

    /**
     * @return the foringorloacal
     */
    public String getForingorloacal() {
        return foringorloacal;
    }

    /**
     * @param foringorloacal the foringorloacal to set
     */
    public void setForingorloacal(String foringorloacal) {
        this.foringorloacal = foringorloacal;
    }

    /**
     * @return the acceptorreject
     */
    public String getAcceptorreject() {
        return acceptorreject;
    }

    /**
     * @param acceptorreject the acceptorreject to set
     */
    public void setAcceptorreject(String acceptorreject) {
        this.acceptorreject = acceptorreject;
    }

    /**
     * @return the applicationtype
     */
    public String getApplicationtype() {
        return applicationtype;
    }

    /**
     * @param applicationtype the applicationtype to set
     */
    public void setApplicationtype(String applicationtype) {
        this.applicationtype = applicationtype;
    }

    /**
     * @return the reporttype
     */
    public int getReporttype() {
        return reporttype;
    }

    /**
     * @param reporttype the reporttype to set
     */
    public void setReporttype(int reporttype) {
        this.reporttype = reporttype;
    }



    /**
     * @return the starttime
     */
    public String getStarttime() {
        return starttime;
    }

    /**
     * @param starttime the starttime to set
     */
    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    /**
     * @return the endtime
     */
    public String getEndtime() {
        return endtime;
    }

    /**
     * @param endtime the endtime to set
     */
    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    /**
     * @return the countrtuser
     */
    public String getCountrtuser() {
        return countrtuser;
    }

    /**
     * @param countrtuser the countrtuser to set
     */
    public void setCountrtuser(String countrtuser) {
        this.countrtuser = countrtuser;
    }

    /**
     * @return the identyficationnumber
     */
    public String getIdentyficationnumber() {
        return identyficationnumber;
    }

    /**
     * @param identyficationnumber the identyficationnumber to set
     */
    public void setIdentyficationnumber(String identyficationnumber) {
        this.identyficationnumber = identyficationnumber;
    }

  

}
