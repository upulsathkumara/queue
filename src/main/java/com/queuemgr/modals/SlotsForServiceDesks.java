/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

import java.util.List;

/**
 *
 * @author Maheshm
 */
public class SlotsForServiceDesks {
    String deskNo;
    List<AvailableSlots> slots;

    public String getDeskNo() {
        return deskNo;
    }

    public void setDeskNo(String deskNo) {
        this.deskNo = deskNo;
    }

    public List<AvailableSlots> getSlots() {
        return slots;
    }

    public void setSlots(List<AvailableSlots> slots) {
        this.slots = slots;
    }
}
