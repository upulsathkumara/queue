/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 *
 * @author user
 */
public class IdentificationDocument {
    private String documenttype;
    private String path1;
    private String path2;
    private String referenceNumber;

    /**
     * @return the documenttype
     */
    public String getDocumenttype() {
        return documenttype;
    }

    /**
     * @param documenttype the documenttype to set
     */
    public void setDocumenttype(String documenttype) {
        this.documenttype = documenttype;
    }

    /**
     * @return the path1
     */
    public String getPath1() {
        return path1;
    }

    /**
     * @param path1 the path1 to set
     */
    public void setPath1(String path1) {
        this.path1 = path1;
    }

    /**
     * @return the path2
     */
    public String getPath2() {
        return path2;
    }

    /**
     * @param path2 the path2 to set
     */
    public void setPath2(String path2) {
        this.path2 = path2;
    }

    /**
     * @return the referenceNumber
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * @param referenceNumber the referenceNumber to set
     */
    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    
}
