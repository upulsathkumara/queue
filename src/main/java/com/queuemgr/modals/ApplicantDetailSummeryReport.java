/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 * @Author : User
 * @Document : ApplicationDetailSummeryReport
 * @Created on : Nov 28, 2016, 8:10:20 PM
 */
public class ApplicantDetailSummeryReport {

    private String customerid;
    private String customername;
    private String identyficationnumber;
    private String email;
    private String mobile;
    private String address;
    private String bookingid;
    private String bookingdate;
    private String counterid;
    private String countername;
    private String starttime;
    private String endtime;
    private String documentdetails;
    private String createddatetime;

    /**
     * @return the customername
     */
    public String getCustomername() {
        return customername;
    }

    /**
     * @param customername the customername to set
     */
    public void setCustomername(String customername) {
        this.customername = customername;
    }

    /**
     * @return the identyficationnumber
     */
    public String getIdentyficationnumber() {
        return identyficationnumber;
    }

    /**
     * @param identyficationnumber the identyficationnumber to set
     */
    public void setIdentyficationnumber(String identyficationnumber) {
        this.identyficationnumber = identyficationnumber;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the customerid
     */
    public String getCustomerid() {
        return customerid;
    }

    /**
     * @param customerid the customerid to set
     */
    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    /**
     * @return the bookingid
     */
    public String getBookingid() {
        return bookingid;
    }

    /**
     * @param bookingid the bookingid to set
     */
    public void setBookingid(String bookingid) {
        this.bookingid = bookingid;
    }

    /**
     * @return the bookingdate
     */
    public String getBookingdate() {
        return bookingdate;
    }

    /**
     * @param bookingdate the bookingdate to set
     */
    public void setBookingdate(String bookingdate) {
        this.bookingdate = bookingdate;
    }

    /**
     * @return the counterid
     */
    public String getCounterid() {
        return counterid;
    }

    /**
     * @param counterid the counterid to set
     */
    public void setCounterid(String counterid) {
        this.counterid = counterid;
    }

    /**
     * @return the countername
     */
    public String getCountername() {
        return countername;
    }

    /**
     * @param countername the countername to set
     */
    public void setCountername(String countername) {
        this.countername = countername;
    }

    /**
     * @return the starttime
     */
    public String getStarttime() {
        return starttime;
    }

    /**
     * @param starttime the starttime to set
     */
    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    /**
     * @return the endtime
     */
    public String getEndtime() {
        return endtime;
    }

    /**
     * @param endtime the endtime to set
     */
    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    /**
     * @return the documentdetails
     */
    public String getDocumentdetails() {
        return documentdetails;
    }

    /**
     * @param documentdetails the documentdetails to set
     */
    public void setDocumentdetails(String documentdetails) {
        this.documentdetails = documentdetails;
    }

    /**
     * @return the createddatetime
     */
    public String getCreateddatetime() {
        return createddatetime;
    }

    /**
     * @param createddatetime the createddatetime to set
     */
    public void setCreateddatetime(String createddatetime) {
        this.createddatetime = createddatetime;
    }

}
