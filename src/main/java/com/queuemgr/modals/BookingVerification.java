/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.modals;

/**
 *
 * @author Rasika
 */

//test
public class BookingVerification {
    private String documentTypeId;
    private String reference;
    private String status;
    private String charge;
    private int bookingVerificationId;
    private String description;

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }


    /**
     * @return the documentTypeId
     */
    public String getDocumentTypeId() {
        return documentTypeId;
    }

    /**
     * @param documentTypeId the documentTypeId to set
     */
    public void setDocumentTypeId(String documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the bookingVerificationId
     */
    public int getBookingVerificationId() {
        return bookingVerificationId;
    }

    /**
     * @param bookingVerificationId the bookingVerificationId to set
     */
    public void setBookingVerificationId(int bookingVerificationId) {
        this.bookingVerificationId = bookingVerificationId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

  
    
}
