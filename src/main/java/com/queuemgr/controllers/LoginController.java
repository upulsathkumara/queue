package com.queuemgr.controllers;

import com.queuemgr.dao.impl.ReservationDaoImpl;
import com.queuemgr.util.LogFileCreator;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    @Autowired
    ReservationDaoImpl reservationDaoImpl;

    @RequestMapping(value = "/", method = RequestMethod.GET)

    public String index(@RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout, ModelMap model,HttpServletRequest request) {
        try {
            if (error != null) {
                System.out.println("Rasika Madushanka");
                model.addAttribute("error", "Invalid username and password!");

            }

            if (logout != null) {
                HttpSession session = request.getSession();
                session.invalidate();
                model.addAttribute("msg", "You've been logged out successfully.");
            }
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
        }
        return "login";

    }
    

    @RequestMapping(value = "/sessionout", method = RequestMethod.GET)
    public String sessionout(ModelMap model,HttpServletRequest request) {
        model.addAttribute("msg", "The session is expired!");
        HttpSession session = request.getSession();
        session.invalidate();
        return "login";
    }
    
    @RequestMapping(value = "/unotherized", method = RequestMethod.GET)
    public String unotherized(ModelMap model,HttpServletRequest request) {
        model.addAttribute("msg", "You don't have authorization to access this page!");
        HttpSession session = request.getSession();
        session.invalidate();
        return "login";
    }
    
    
    
    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String printWelcome(ModelMap model , HttpServletRequest request) {
        try {

            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            int notificationsCount = reservationDaoImpl.getVipNotificationCount();
            
            HttpSession session = request.getSession();
            session.setAttribute("notifications", notificationsCount);
            if (authentication.getAuthorities().toString().contains("USER")) {
                

                return "userdashboard";

            }else if (authentication.getAuthorities().toString().contains("ADMIN")){
                return "admindashboard";
            }

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
        }
        return null;
    }

}
