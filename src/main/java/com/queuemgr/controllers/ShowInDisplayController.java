/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Nash
 */
@Controller
public class ShowInDisplayController {

    @RequestMapping(value = "/getDisplay", method = RequestMethod.GET)
    public String index() {
        
        return "display/showindisplay";

    }
    
    @RequestMapping(value = "/getConfirmation", method = RequestMethod.GET)
    public String OnlineConfirmation() {
        
        return "display/detailsverificationwindow";

    }
    @RequestMapping(value = "/getOnlineConfirmation", method = RequestMethod.GET)
    public String confirmation() {
        
        return "display/detailsverificationwindowonline";

    }
    @RequestMapping(value = "/getPriorityConfirmation", method = RequestMethod.GET)
    public String priorityConfirmation() {
        
        return "display/detailsverificationwindowpriority";

    }
}
