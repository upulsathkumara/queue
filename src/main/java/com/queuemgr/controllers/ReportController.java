/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.queuemgr.dao.impl.ReportDaoImpl;
import com.queuemgr.modals.AppicationSummeryReport;
import com.queuemgr.modals.ApplicantDetailSummeryReport;
import com.queuemgr.modals.CounterSummeryReport;
import com.queuemgr.modals.Report;
import com.queuemgr.util.LogFileCreator;
import com.queuemgr.util.StatusVarList;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author user
 */
@Controller
public class ReportController {

    @Autowired
    ServletContext context;
    @Autowired
    ReportDaoImpl reportDaoImpl;

    @RequestMapping(value = "/admin/report/sample", method = RequestMethod.GET)
    public String loadCounterReportPage(@ModelAttribute("insertForm") Report data, ModelMap model) {
        try {
            this.setPageViewComponenets(model, 1);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "report/countersummery";
    }
    
      @RequestMapping(value = "/admin/report/countersummarybyhours", method = RequestMethod.GET)
    public String loadCounterByHoursReportPage(@ModelAttribute("insertForm") Report data, ModelMap model) {
        try {
            this.setPageViewComponenets(model, 1);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "report/newcountersummary";
    }


    @RequestMapping(value = "/admin/report/applicationsummery", method = RequestMethod.GET)
    public String loadAppicantSummeryReportPage(@ModelAttribute("insertForm") Report data, ModelMap model) {
        try {
            this.setPageViewComponenets(model, 2);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "report/applicantsummery";
    }

    @RequestMapping(value = "/admin/report/applicantdetails", method = RequestMethod.GET)
    public String loadAppicantDetailsReportPage(@ModelAttribute("insertForm") Report data, ModelMap model) {
        try {
            this.setPageViewComponenets(model, 3);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "report/applicantdetails";
    }

    @RequestMapping(value = "/admin/report/userdurationreports", method = RequestMethod.GET)
    public String loadUserDurationReportPage(@ModelAttribute("insertForm") Report data, ModelMap model) {
        try {
            this.setPageViewComponenets(model, 4);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "report/userdurationreports";
    }

    @RequestMapping(value = "/counterreport/pdf", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<byte[]> getCountetReport(@ModelAttribute("insertForm") Report rptdata, Map<String, Object> model, HttpServletResponse response, HttpServletRequest request, HttpSession session) throws Exception {

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap<String, Object> parameterMap;
        ResponseEntity<byte[]> outPutFile = null;
        rptdata.setReporttype(1);
        parameterMap = this.getDisplayParameterMap(rptdata, username);
        HttpHeaders headers = new HttpHeaders();

        ByteArrayOutputStream outputStream;
        byte[] outputFile = null;
        String filename = "Counter Summary Report.pdf";
        outputFile = this.getPdfReport(username, parameterMap);

        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        ResponseEntity<byte[]> respons = new ResponseEntity<byte[]>(outputFile, headers, HttpStatus.OK);

        return respons;
    }

     @RequestMapping(value = "/counterreportbysummary/pdf", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<byte[]> getCountetByHoursReport(@ModelAttribute("insertForm") Report rptdata, Map<String, Object> model, HttpServletResponse response, HttpServletRequest request, HttpSession session) throws Exception {

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap<String, Object> parameterMap;
        ResponseEntity<byte[]> outPutFile = null;
        rptdata.setReporttype(5);
        parameterMap = this.getDisplayParameterMap(rptdata, username);
        HttpHeaders headers = new HttpHeaders();

        ByteArrayOutputStream outputStream;
        byte[] outputFile = null;
        String filename = "Counter Summary By Hours Report.pdf";
        outputFile = this.getPdfReport(username, parameterMap);

        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        ResponseEntity<byte[]> respons = new ResponseEntity<byte[]>(outputFile, headers, HttpStatus.OK);

        return respons;
    }
    
    
    
    
    @RequestMapping(value = "/applicationsumeery/pdf", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<byte[]> getApplicantSummaryReport(@ModelAttribute("insertForm") Report rptdata, Map<String, Object> model, HttpServletResponse response, HttpServletRequest request, HttpSession session) throws Exception {

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap<String, Object> parameterMap;
        ResponseEntity<byte[]> outPutFile = null;
        rptdata.setReporttype(2);
        parameterMap = this.getDisplayParameterMap(rptdata, username);
        HttpHeaders headers = new HttpHeaders();

        ByteArrayOutputStream outputStream;
        byte[] outputFile = null;
        String filename = "Application Summary Report.pdf";
        outputFile = this.getPdfReport(username, parameterMap);

        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        ResponseEntity<byte[]> respons = new ResponseEntity<byte[]>(outputFile, headers, HttpStatus.OK);

        return respons;
    }

    @RequestMapping(value = "/applicantdetails/pdf", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<byte[]> getAppicantDetailReport(@ModelAttribute("insertForm") Report rptdata, Map<String, Object> model, HttpServletResponse response, HttpServletRequest request, HttpSession session) throws Exception {

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap<String, Object> parameterMap;
        ResponseEntity<byte[]> outPutFile = null;
        rptdata.setReporttype(3);
        parameterMap = this.getDisplayParameterMap(rptdata, username);
        HttpHeaders headers = new HttpHeaders();

        ByteArrayOutputStream outputStream;
        byte[] outputFile = null;
        String filename = "Applicant Detail Report.pdf";
        outputFile = this.getPdfReport(username, parameterMap);

        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        ResponseEntity<byte[]> respons = new ResponseEntity<byte[]>(outputFile, headers, HttpStatus.OK);

        return respons;
    }

    @RequestMapping(value = "/userdurationreports/pdf", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<byte[]> getUserDurationDetailReport(@ModelAttribute("insertForm") Report rptdata, Map<String, Object> model, HttpServletResponse response, HttpServletRequest request, HttpSession session) throws Exception {

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        HashMap<String, Object> parameterMap;
        ResponseEntity<byte[]> outPutFile = null;
        rptdata.setReporttype(4);
        parameterMap = this.getDisplayParameterMap(rptdata, username);
        HttpHeaders headers = new HttpHeaders();

        ByteArrayOutputStream outputStream;
        byte[] outputFile = null;
        String filename = "User Duration Detail Report.pdf";
        outputFile = this.getPdfReport(username, parameterMap);

        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        ResponseEntity<byte[]> respons = new ResponseEntity<byte[]>(outputFile, headers, HttpStatus.OK);

        return respons;
    }

    private HashMap<String, Object> getDisplayParameterMap(Report rpt, String user) throws Exception {
        HashMap<String, Object> parameterMap = new HashMap();
        List<CounterSummeryReport> cList = new ArrayList<>();
        List<AppicationSummeryReport> aList = new ArrayList<>();
        List<ApplicantDetailSummeryReport> adList = new ArrayList<>();

        try {
            if (rpt.getReporttype() == 1) {
                cList = reportDaoImpl.CounterSummeryReport(rpt);
                parameterMap.put("COUNTSUMMERYREPORTTABLE", new JRBeanCollectionDataSource(cList));
                parameterMap.put("COUNTER", rpt.getCountername().contentEquals("") ? null : rpt.getCountername());

                parameterMap.put("RPT_TYPE", 1);
            } else if (rpt.getReporttype() == 2) {
                aList = reportDaoImpl.ApplicantSummery(rpt);
                parameterMap.put("COUNTSUMMERYREPORTTABLE", new JRBeanCollectionDataSource(aList));
                parameterMap.put("RPT_TYPE", 2);
                parameterMap.put("NAME", rpt.getCustomername());
                parameterMap.put("IDENTY", rpt.getIdentyficationnumber());
//                parameterMap.put("STATUS", rpt.getAcceptorreject().contentEquals("") ? null : rpt.getAcceptorreject().contentEquals("REJT") ? "Rejecte" : "Accept");
//                parameterMap.put("DOCCHARGE", rpt.getForingorloacal().contentEquals("") ? null : rpt.getForingorloacal());
                parameterMap.put("APPLICATIONTYPE", rpt.getApplicationtype().contentEquals("") ? null : rpt.getApplicationtype().contentEquals("1") ? "Personal" : rpt.getApplicationtype().contentEquals("2") ? "Business" : rpt.getApplicationtype().contentEquals("3") ? "Corporate" : "Priority");
            } else if (rpt.getReporttype() == 3) {
                adList = reportDaoImpl.ApplicantDetailReport(rpt);
                parameterMap.put("COUNTSUMMERYREPORTTABLE", new JRBeanCollectionDataSource(adList));
                parameterMap.put("RPT_TYPE", 3);
                parameterMap.put("NAME", rpt.getCustomername());
                parameterMap.put("IDENTY", rpt.getIdentyficationnumber());
                parameterMap.put("APPLICATIONTYPE", rpt.getApplicationtype().contentEquals("") ? null : rpt.getApplicationtype().contentEquals("1") ? "Personal" : rpt.getApplicationtype().contentEquals("2") ? "Business" : rpt.getApplicationtype().contentEquals("3") ? "Corporate" : "Priority");
            } else if (rpt.getReporttype() == 4) {
                cList = reportDaoImpl.UserDeurationReport(rpt);
                parameterMap.put("COUNTSUMMERYREPORTTABLE", new JRBeanCollectionDataSource(cList));
                parameterMap.put("COUNTER", rpt.getCountername().contentEquals("") ? null : rpt.getCountername());
                parameterMap.put("NAME", rpt.getCountrtuser().contentEquals("") ? null : rpt.getCountrtuser());
                parameterMap.put("STARTTIME", rpt.getStarttime().contentEquals("") ? null : rpt.getStarttime());
                parameterMap.put("ENDTIME", rpt.getEndtime().contentEquals("") ? null : rpt.getEndtime());
                parameterMap.put("RPT_TYPE", 4);

            }else if (rpt.getReporttype() == 5) {
                cList = reportDaoImpl.CounterSummeryByHoursReport(rpt);
                parameterMap.put("COUNTSUMMERYREPORTTABLE", new JRBeanCollectionDataSource(cList));
                parameterMap.put("COUNTER", rpt.getCountername().contentEquals("") ? null : rpt.getCountername());

                parameterMap.put("RPT_TYPE", 5);
            }
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
        }
        parameterMap.put("FROM_DATE", rpt.getFromdate());
        parameterMap.put("TO_DATE", rpt.getTodate());
        parameterMap.put("URL", context.getRealPath("") + File.separator + StatusVarList.REPORT_LOG_LOCATION);
        return parameterMap;
    }

    private byte[] getPdfReport(String user, HashMap<String, Object> parameterMap) throws Exception {
        byte[] outputFile;
        Connection connection = null;
        JRSwapFileVirtualizer virtualizer = null;
        String reportLocation = "";
        try {
            String reportBuildPath = "report";
            if (parameterMap.get("RPT_TYPE").toString().contentEquals("1")) {
                reportLocation = context.getRealPath("") + File.separator + StatusVarList.REPORT_LOCATION + StatusVarList.REPORT_NAME_COUNTSUMMERYREPORT;
            } else if (parameterMap.get("RPT_TYPE").toString().contentEquals("2")) {
                reportLocation = context.getRealPath("") + File.separator + StatusVarList.REPORT_LOCATION + StatusVarList.REPORT_NAME_APPLICANTSUMMERYREPORT;
            } else if (parameterMap.get("RPT_TYPE").toString().contentEquals("3")) {
                reportLocation = context.getRealPath("") + File.separator + StatusVarList.REPORT_LOCATION + StatusVarList.REPORT_NAME_APPLICANTDETAILSREPORT;
            } else if (parameterMap.get("RPT_TYPE").toString().contentEquals("4")) {
                reportLocation = context.getRealPath("") + File.separator + StatusVarList.REPORT_LOCATION + StatusVarList.REPORT_NAME_USER_DURATIONREPORT;
            } else if (parameterMap.get("RPT_TYPE").toString().contentEquals("5")) {
                reportLocation = context.getRealPath("") + File.separator + StatusVarList.REPORT_LOCATION + StatusVarList.REPORT_NAME_COUNTSUMMERYREPORTBYHOURS;
            }
            reportBuildPath += File.separator + user + File.separator + "PDF";
            File file = new File(reportBuildPath);
            if (!file.exists()) {
                file.mkdirs();

            } else {
                for (File temp : file.listFiles()) {
                    temp.delete();
                }
            }
            int numberOfPdfPagesInMemory = Integer.valueOf("50");
            JRSwapFile swapFile = new JRSwapFile(reportBuildPath, 1024, 100);
            virtualizer = new JRSwapFileVirtualizer(numberOfPdfPagesInMemory, swapFile, Boolean.TRUE);
            parameterMap.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);

            JasperPrint jasperPrint = JasperFillManager.fillReport(reportLocation, parameterMap, new JREmptyDataSource());

            outputFile = JasperExportManager.exportReportToPdf(jasperPrint);
        } catch (Exception e) {
            LogFileCreator.writeErrorToLog(e);
            throw e;
        } finally {
            if (virtualizer != null) {
                virtualizer.cleanup();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return outputFile;
    }

    private void setPageViewComponenets(ModelMap model, int parm) throws Exception {
        if (parm == 1) {
            model.addAttribute("counterList", reportDaoImpl.getCounteList());
        } else if (parm == 2) {
            model.addAttribute("applcationList", reportDaoImpl.getApplicationTypeDropdownList());
            model.addAttribute("statusList", reportDaoImpl.getAcceptedOrRejectStatusDropdownList());
            model.addAttribute("foriegnorlocaList", reportDaoImpl.getDocChargesDropdownList());
        } else if (parm == 3) {
            model.addAttribute("applcationList", reportDaoImpl.getApplicationTypeDropdownList());
//            model.addAttribute("documentTypeList", reportDaoImpl.getDocumentTypeDropdownList());
            model.addAttribute("statusList", reportDaoImpl.getAcceptedOrRejectStatusDropdownList());
        } else if (parm == 4) {
            model.addAttribute("applicationcounterList", reportDaoImpl.getCounteList());
            model.addAttribute("userList", reportDaoImpl.getUserList());
            model.addAttribute("statusList", reportDaoImpl.getAcceptedOrRejectStatusDropdownList());
        }
    }

}
