/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.queuemgr.dao.impl.ReservationDaoImpl;
import com.queuemgr.dao.impl.UserManagementDAOImple;
import com.queuemgr.modals.WorkingTime;
import com.queuemgr.util.Common;
import com.queuemgr.util.StatusVarList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Maheshm
 */
@Controller
public class LoginAuthController {

    @Autowired
    ReservationDaoImpl reservationDaoImpl;

    @Autowired
    UserManagementDAOImple UserManagementDAOImple;

    @RequestMapping(value = "/loginAuth", method = RequestMethod.POST)

    public String printWelcome(@ModelAttribute("workingHourForm") WorkingTime data, ModelMap model, HttpServletRequest request) {
        String returnUrl = null;
        try {
            String username = request.getParameter("username");
            String password = Common.getMD5(request.getParameter("password"));

//            String ip = request.getParameter("ip");
//            Object obj = EPIC_LOGIN_AUTH.loginAuth(username, password, ip, StatusVarList.ACCESS_TOKEN);
//            JSONObject jsonObj = new JSONObject(obj.toString());
//            String status = jsonObj.get("statusCode").toString();
//            if ("100".equals(status)) {
//                String jsonInside = jsonObj.get("userDetails").toString();
//                JSONObject insideObj = new JSONObject(jsonInside);
//                String userRole = (String) insideObj.get("userRole");
//                if ("SEE".equals(userRole)) {
//                    int notificationsCount = reservationDaoImpl.getVipNotificationCount();
//                    HttpSession session = request.getSession();
//                    session.setAttribute("notifications", notificationsCount);
//                    session.setAttribute("loggedUser", username);
//                    session.setAttribute("role", "USER");
//                    session.setAttribute("loggedCounter", (String) insideObj.get("counterCode"));
//                    returnUrl = "redirect:/reservation/create";
//                } else if ("WAD".equals(userRole)) {
//                    HttpSession session = request.getSession();
//                    session.setAttribute("loggedUser", username);
//                    session.setAttribute("role", "ADMIN");
////                    session.setAttribute("loggedCounter", (String) insideObj.get("counterCode"));
//                    String startTime = UserManagementDAOImple.getTime();
//                    String endTime = UserManagementDAOImple.getEndTime();
//                    WorkingTime commonConfig = UserManagementDAOImple.getCommonConfiguration();
//                    model.put("STARTTIME", commonConfig.getStartTime());
//                    model.put("ENDTIME", commonConfig.getEndtime());
//                    model.put("ONLINEPERCENTAGE", commonConfig.getOnlinePercentage());
//                    model.put("WORKINGPERCENTAGE", commonConfig.getWorkingPercentage());
//                    model.put("ONLINECUTOFFTIME", commonConfig.getOnlineCutOffTime());
//                    model.put("TIMEGAPTOBOOK", commonConfig.getTimeGapToBook());
//                    model.put("TIMEGAPBEFORE", commonConfig.getBeforeTimeToIssueTicket());
//                    model.put("TIMEGAPAFTER", commonConfig.getAfterTimeToIssueTicket());
//                    model.put("CALLNEXT", commonConfig.getCallNextInterval());
//
//                    returnUrl = "working_hour_management/working_hour";
//                    returnUrl = "admindashboard";
//                }
//            } else {
//                String error = jsonObj.get("errCode").toString();
//                String jsonInside = null;
//                JSONObject insideObj = null;
//                if ("200".equals(error)) {
//                    model.addAttribute("error", "Username or password incorrect!");
//                } else if ("201".equals(error)) {
//                    model.addAttribute("error", "User is deactivated by admin!");
//                } else if ("202".equals(error)) {
//                    model.addAttribute("error", "User role is deactivated by admin!");
//                } else if ("203".equals(error)) {
//                    model.addAttribute("error", "Employee is deactivated!");
//                } else if ("204".equals(error)) {
//                    model.addAttribute("error", "User access denid!");
//                } else if ("205".equals(error)) {
//                    model.addAttribute("error", "Counter not assigned!");
//                } else if ("206".equals(error)) {
//                    jsonInside = jsonObj.get("userDetails").toString();
//                    insideObj = new JSONObject(jsonInside);
//                    String counterName = (String) insideObj.get("counterName");
//                    model.addAttribute("error", "Sorry you have assigned to " + counterName + "!");
//                } else {
//                    model.addAttribute("error", "Login Error #" + error + "!");
//                }
//                returnUrl = "login";
//            }
            if (username.equals("apc") && password.equals("0cc175b9c0f1b6a831c399e269772661")) {
                int notificationsCount = reservationDaoImpl.getVipNotificationCount();
                HttpSession session = request.getSession();
                session.setAttribute("notifications", notificationsCount);
                session.setAttribute("loggedUser", username);
                session.setAttribute("role", "USER");
                session.setAttribute("loggedCounter", "APC01");

                returnUrl = "redirect:/reservation/create";
            } else if (username.equals("qad") && password.equals("0cc175b9c0f1b6a831c399e269772661")) {
                HttpSession session = request.getSession();
                session.setAttribute("loggedUser", username);
                session.setAttribute("role", "ADMIN");

                String startTime = UserManagementDAOImple.getTime();
                String endTime = UserManagementDAOImple.getEndTime();
                WorkingTime commonConfig = UserManagementDAOImple.getCommonConfiguration();
                model.put("STARTTIME", commonConfig.getStartTime());
                model.put("ENDTIME", commonConfig.getEndtime());
                model.put("ONLINEPERCENTAGE", commonConfig.getOnlinePercentage());
                model.put("WORKINGPERCENTAGE", commonConfig.getWorkingPercentage());
                model.put("ONLINECUTOFFTIME", commonConfig.getOnlineCutOffTime());
                model.put("TIMEGAPTOBOOK", commonConfig.getTimeGapToBook());
                model.put("TIMEGAPBEFORE", commonConfig.getBeforeTimeToIssueTicket());
                model.put("TIMEGAPAFTER", commonConfig.getAfterTimeToIssueTicket());
                model.put("CALLNEXT", commonConfig.getCallNextInterval());

                returnUrl = "working_hour_management/working_hour";
            } else {
                model.addAttribute("error", "Invalid username and password!");
                returnUrl = "login";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return returnUrl;
    }
}
