/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.queuemgr.controllers;

import com.queuemgr.dao.impl.ReservationDaoImpl;
import com.queuemgr.dao.impl.UserManagementDAOImple;
import com.queuemgr.modals.Customer;
import com.queuemgr.modals.WorkingTime;
import com.queuemgr.util.LogFileCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author SameeraPJ
 */
@Controller
@RequestMapping(value = "/admin")
public class WorkingHourMgrController {
    
    @Autowired
    UserManagementDAOImple UserManagementDAOImple;
    @Autowired
    ReservationDaoImpl reservationDaoImpl;
    
    @RequestMapping(value = "/workinghour/create", method = RequestMethod.GET)
    public String loadWorkingHourMgrPage(@ModelAttribute("workingHourForm") WorkingTime data, ModelMap model){
        try {
        String startTime=UserManagementDAOImple.getTime();
        String endTime=UserManagementDAOImple.getEndTime();
        WorkingTime commonConfig=UserManagementDAOImple.getCommonConfiguration();
        model.put("STARTTIME",commonConfig.getStartTime() );
        model.put("ENDTIME",commonConfig.getEndtime());
        model.put("ONLINEPERCENTAGE", commonConfig.getOnlinePercentage());
        model.put("WORKINGPERCENTAGE", commonConfig.getWorkingPercentage());
        model.put("ONLINECUTOFFTIME", commonConfig.getOnlineCutOffTime());
        model.put("TIMEGAPTOBOOK", commonConfig.getTimeGapToBook());
        model.put("TIMEGAPBEFORE", commonConfig.getBeforeTimeToIssueTicket());
        model.put("TIMEGAPAFTER", commonConfig.getAfterTimeToIssueTicket());
        model.put("CALLNEXT", commonConfig.getCallNextInterval());
        
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "working_hour_management/working_hour";
    }
    
    @RequestMapping(value = "/workinghour/save", method = RequestMethod.POST)
    public String saveWorkingHourMgrPage(@ModelAttribute("workingHourForm") WorkingTime data, ModelMap model){
        try {
        UserManagementDAOImple.saveWorkingtime(data);
        model.addAttribute("succMsg", "Updated Successfully");
        
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
            model.addAttribute("errMsg", "An error occured while updating the record.");
        }
        return "working_hour_management/working_hour";
    }
    
}
