/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.queuemgr.dao.impl.ApplicationCounterManagementDAOImple;
import com.queuemgr.dao.impl.ServiceCounterManagementDAOImple;
import com.queuemgr.modals.ApplicationCounter;
import com.queuemgr.modals.ServiceDesk;
import com.queuemgr.util.LogFileCreator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Kelum Madushan
 */
@Controller
@RequestMapping(value = "/admin")
public class ApplicationCounterManagementController {

    @Autowired
    ApplicationCounterManagementDAOImple countermanag;

    @RequestMapping(value = "/applicationcountermanagement/getcounterlist", method = RequestMethod.GET)
    public String getApplicationCounterList(ModelMap modelmap) {
        try {
            modelmap.put("counterList", countermanag.getCounterList());
        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            Logger.getLogger(ApplicationCounterManagementController.class.getName()).log(Level.SEVERE, null, exception);
        }

        return "application_counter_management/counter_list";
    }

    @RequestMapping(value = "/applicationcountermanagement/addcounter", method = RequestMethod.GET)
    public String addCounter(@ModelAttribute("insertForm") ApplicationCounter counter, ModelMap modelmap) {
        try {
            modelmap.put("status", countermanag.getStatus());
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(ApplicationCounterManagementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "application_counter_management/counter_create";
    }

    @RequestMapping(value = "/applicationcountermanagement/addcounter", method = RequestMethod.POST)
    public ModelAndView saveCounter(@ModelAttribute("insertForm") ApplicationCounter counter, HttpServletRequest request) {
        try {

            String name = request.getSession().getAttribute("loggedUser").toString();

            counter.setCreateduser(name);
            countermanag.addCounter(counter);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(ApplicationCounterManagementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ModelAndView("redirect:/admin/applicationcountermanagement/getcounterlist");
    }

    @RequestMapping(value = "/applicationcountermanagement/editcounter", method = RequestMethod.POST)
    public String editCounter(@ModelAttribute("editForm") ApplicationCounter counter, ModelMap modelmap) {
        try {
            modelmap.put("status", countermanag.getStatus());
            modelmap.put("counter", countermanag.getCounter(counter.getApplicationcounterid()));
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(ApplicationCounterManagementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "application_counter_management/counter_edit";

    }

    @RequestMapping(value = "/applicationcountermanagement/editedcounter", method = RequestMethod.POST)
    public ModelAndView editedCounter(@ModelAttribute("editForm") ApplicationCounter counter, HttpServletRequest request) {
        try {
            String name = request.getSession().getAttribute("loggedUser").toString();
            
            counter.setCreateduser(name);
            countermanag.updateCounter(counter);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(ApplicationCounterManagementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ModelAndView("redirect:/admin/applicationcountermanagement/getcounterlist");
    }

    @RequestMapping(value = "/applicationcountermanagement/deletecounter", method = RequestMethod.POST)
    public ModelAndView removeCounter(@ModelAttribute("deleteForm") ApplicationCounter counter, ModelMap modelmap) {
        try {
            countermanag.deleteCounter(counter.getApplicationcounterid());
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(ApplicationCounterManagementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ModelAndView("redirect:/admin/applicationcountermanagement/getcounterlist");
    }
}
