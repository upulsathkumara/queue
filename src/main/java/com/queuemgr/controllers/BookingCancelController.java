/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.queuemgr.dao.impl.ReservationDaoImpl;
import com.queuemgr.util.LogFileCreator;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Kelum Madushan
 */
@Controller
@RequestMapping(value = "/admin")
public class BookingCancelController {
    
    @Autowired
    ReservationDaoImpl reservationDaoImpl;

    @RequestMapping(value = "/bookinglist/getlist", method = RequestMethod.GET)
    public String getBookingList(ModelMap modelmap) {
        try {
            modelmap.put("bookingList",reservationDaoImpl.getAllBookings());
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(BookingCancelController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "booking_cancel/booking_cancel";
    }
    
    @RequestMapping(value = "/bookingdelete", method = RequestMethod.POST)
    public String deleteBooking(ModelMap modelmap,@RequestParam(value = "bookingId")int bookingId) {
        try {
            reservationDaoImpl.deleteBooking(bookingId);
            modelmap.put("bookingList",reservationDaoImpl.getAllBookings());
        } catch (SQLException ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(BookingCancelController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(BookingCancelController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "booking_cancel/booking_cancel";
    }
}
