/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.queuemgr.dao.impl.ReservationDaoImpl;
import com.queuemgr.util.Common;
import com.queuemgr.util.LogFileCreator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author maheshl
 */
@Controller
public class ServiceDeskAuthenticator {
    @Autowired
    ReservationDaoImpl reservationDaoImpl;
    @RequestMapping(value = "/loginToQms", method = RequestMethod.POST)
    public String printWelcome(ModelMap model, HttpServletRequest request) {
        String returnUrl = null;
        try {
            String username = request.getParameter("username");
            String token = request.getParameter("token");
            String role = request.getParameter("userrole");
            String counterno = request.getParameter("counterno");
           
            if(reservationDaoImpl.validateToken(username, token)){
                int notificationsCount = reservationDaoImpl.getVipNotificationCount();
                HttpSession session = request.getSession();
                session.setAttribute("notifications", notificationsCount);
                session.setAttribute("loggedUser", username);
                session.setAttribute("token", token);
                session.setAttribute("loggedCounter", counterno);
                session.setAttribute("loogedViaEdas", "1");
                session.setAttribute("role", role);
                
                returnUrl = "redirect:/reservation/create";
            }else{
                model.addAttribute("error", "Authentication Failed!");
                returnUrl = "errorqmslogin";
            }
            

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return returnUrl;
    }
    
    
     @RequestMapping(value = "/loginToServiceDesk", method = RequestMethod.GET)
    public String loginToServiceDesk(ModelMap model, HttpServletRequest request) {
        String returnUrl = null;
        try {
            String username = request.getParameter("username");
            String token = request.getParameter("token");
           
            if(reservationDaoImpl.validateToken(username, token)){
                int notificationsCount = reservationDaoImpl.getVipNotificationCount();
                HttpSession session = request.getSession();
                session.setAttribute("notifications", notificationsCount);
                session.setAttribute("loggedUser", username);
                session.setAttribute("loggedCounter", "APC01");
                session.setAttribute("loogedViaEdas", "1");
                
                returnUrl = "redirect:/reservation/create";
            }else{
                model.addAttribute("error", "Invalid username and password!");
                returnUrl = "errorqmslogin";
            }
            

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return returnUrl;
    }
}
