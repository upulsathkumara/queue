/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.queuemgr.dao.impl.ReservationDaoImpl;
import com.queuemgr.modals.BookingTimes;
import com.queuemgr.modals.TmeGaps;
import com.queuemgr.util.LogFileCreator;
import com.queuemgr.util.StatusVarList;
import java.io.ByteArrayOutputStream;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author SameeraPJ
 */
@Controller
public class IssueTicketController {

    @Autowired
    ReservationDaoImpl reservationDaoImpl;

    @RequestMapping(value = "/issuetickets/create", method = RequestMethod.GET)
    public String loadReservationCreatePage() {

        return "issue_tickets/ticketCreate";
    }

    @RequestMapping(value = "/issuetickets/updateissue", method = RequestMethod.POST)
    public @ResponseBody
    String updateissue(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String jsoncontent = request.getParameter("data");
        JSONObject rsp = new JSONObject(jsoncontent);
        String myJson;
        JSONObject customerJson = new JSONObject();
        try {
            int serviceId = rsp.getInt("serviceId");

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            QRCodeWriter writer = new QRCodeWriter();
            BitMatrix matrix = writer.encode(Integer.toString(serviceId), BarcodeFormat.QR_CODE, 125, 125);
            MatrixToImageWriter.writeToStream(matrix, "png", output);

            String loggedUser = request.getSession().getAttribute("loggedUser").toString();
            String role = request.getSession().getAttribute("role").toString();

                if (role.equals("USER")) {
                    if (StatusVarList.FRONT_DESK_NORMAL.equals(reservationDaoImpl.getServiceIdStatus(Integer.toString(serviceId)))) {
                        if (reservationDaoImpl.isToday(serviceId)) {
                            String token = reservationDaoImpl.bookingStatusAndTokenUpdate(serviceId, loggedUser);
                            customerJson.put("SUCCESS", "SUCCESS");
                            customerJson.put("serviceId", "Service ID: " + serviceId);
                            customerJson.put("token", token);
                            customerJson.put("tokenlabel", "Token Number");
                        } else {
                            customerJson.put("SUCCESS", "SUCCESS");
                            customerJson.put("serviceId", "Sorry, Cannot generate a queue ticket number for past or future date bookings");
                            customerJson.put("token", "");
                            customerJson.put("tokenlabel", "");
                        }
                    } else {
                        if (reservationDaoImpl.isInCorrectStatus(serviceId)) {
                            if (reservationDaoImpl.isToday(serviceId)) {
                                BookingTimes bt = reservationDaoImpl.getBookingTime(serviceId);
                                String bookingTime = bt.getStartTime();
                                String bookingEndTime = bt.getEndTime();

                                TmeGaps tg = reservationDaoImpl.getTimeGapIssueTicket();
                                double fixedGap = tg.getGapBefore();
                                double fixedGapAfter = tg.getGapAfter();

                                SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                                Date date = new Date();
                                String sysTime = dateFormat.format(date);
                                Date dateSysTime = dateFormat.parse(sysTime);
                                Date dateBooking = dateFormat.parse(bookingTime);
                                double difference = (dateBooking.getTime() / 60000 - dateSysTime.getTime() / 60000);

                                Date dateBookingEnd = dateFormat.parse(bookingEndTime);
                                double differenceAfter = (dateSysTime.getTime() / 60000 - dateBookingEnd.getTime() / 60000);
                                System.out.println(">>>>>>> " + differenceAfter);
                                if ((difference < fixedGap) && (differenceAfter < fixedGapAfter)) {
                                    String token = reservationDaoImpl.bookingStatusAndTokenUpdate(serviceId, loggedUser);
                                    customerJson.put("SUCCESS", "SUCCESS");
                                    customerJson.put("serviceId", "Service ID: " + serviceId);
                                    customerJson.put("token", token);
                                    customerJson.put("tokenlabel", "Token Number");
                                } else {
                                    customerJson.put("SUCCESS", "SUCCESS");
                                    customerJson.put("serviceId", "Sorry, Cannot create the queue ticket at this moment");
                                    customerJson.put("token", "");
                                    customerJson.put("tokenlabel", "");
                                }
                            } else {
                                customerJson.put("SUCCESS", "SUCCESS");
                                customerJson.put("serviceId", "Sorry, Cannot generate a queue ticket number for past or future date bookings");
                                customerJson.put("token", "");
                                customerJson.put("tokenlabel", "");
                            }
                        } else {
                            customerJson.put("SUCCESS", "SUCCESS");
                            customerJson.put("serviceId", "Sorry, Cannot generate a queue ticket number. Please visit an application counter to complete your process.");
                            customerJson.put("token", "");
                            customerJson.put("tokenlabel", "");
                        }
                    }

                } else {
                    customerJson.put("SUCCESS", "SUCCESS");
                    customerJson.put("serviceId", "Sorry, Cannot generate a queue ticket number. Please try again.");
                    customerJson.put("token", "");
                    customerJson.put("tokenlabel", "");
                }
            

        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            customerJson.put("SUCCESS", "SUCCESS");
            customerJson.put("serviceId", "Sorry, Cannot generate a queue ticket number. Please try again.");
            customerJson.put("token", "");
            customerJson.put("tokenlabel", "");
        }
        myJson = customerJson.toString();
        return myJson;
    }

    @RequestMapping(value = "/issuetickets/reprint", method = RequestMethod.POST)
    public @ResponseBody
    String reprintRecipt(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String jsoncontent = request.getParameter("data");
        JSONObject rsp = new JSONObject(jsoncontent);
        String myJson;
        JSONObject customerJson = new JSONObject();
        try {
            int serviceId = rsp.getInt("serviceId");

            String loggedUser = request.getSession().getAttribute("loggedUser").toString();
            String role = request.getSession().getAttribute("role").toString();
            if (role.equals("USER")) {
                if (reservationDaoImpl.isInCorrectStatusReprint(serviceId)) {
                    if (reservationDaoImpl.isToday(serviceId)) {
                        String token = reservationDaoImpl.getTokenReprint(serviceId);
                        customerJson.put("SUCCESS", "SUCCESS");
                        customerJson.put("serviceId", "Service ID: " + serviceId);
                        customerJson.put("token", "Token Number: " + token);
                    } else {
                        customerJson.put("SUCCESS", "SUCCESS");
                        customerJson.put("serviceId", "Sorry, Cannot generate a queue ticket number for past or future date bookings");
                        customerJson.put("token", "");
                    }
                } else {
                    customerJson.put("SUCCESS", "SUCCESS");
                    customerJson.put("serviceId", "Sorry, Cannot generate a queue ticket number. Please visit an application counter to complete your process.");
                    customerJson.put("token", "");
                }

            } else {
                customerJson.put("ERROR", "ERROR");
            }

        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            customerJson.put("ERROR", "ERROR");
        }
        myJson = customerJson.toString();
        return myJson;
    }

    @RequestMapping(value = "/issuetickets/reissue", method = RequestMethod.GET)
    public String reissueLoad() {

        return "issue_tickets/reissue";
    }

}
