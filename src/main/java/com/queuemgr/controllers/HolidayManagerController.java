/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.queuemgr.dao.impl.HolidayManagerDAOImple;
import com.queuemgr.modals.ApplicationCounter;
import com.queuemgr.modals.Holiday;
import com.queuemgr.util.LogFileCreator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Kelum Madushan
 */
@Controller
@RequestMapping(value = "/admin")
public class HolidayManagerController {
    
    @Autowired
    HolidayManagerDAOImple holidaydao;
    
    @RequestMapping(value = "/holidaymanagement/getholidaylist", method = RequestMethod.GET)
    public String getHolidayList(ModelMap modelmap,HttpServletRequest request) {
        try {
            modelmap.put("holidayList", holidaydao.getHolidayList());
//            modelmap.addAttribute("msg", request.getParameter("msg"));
        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            Logger.getLogger(HolidayManagerController.class.getName()).log(Level.SEVERE, null, exception);
        }
        return "holiday_management/holiday_list";
    }
    
    @RequestMapping(value = "/holidaymanagement/addholiday", method = RequestMethod.GET)
    public String addHoliday(@ModelAttribute("insertForm") Holiday day, ModelMap modelmap) {
        try {
            modelmap.put("status", holidaydao.getStatus());
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(HolidayManagerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "holiday_management/holiday_create";
    }
    
    @RequestMapping(value = "/holidaymanagement/addholiday", method = RequestMethod.POST)
    public ModelAndView saveCounter(@ModelAttribute("insertForm") Holiday day, RedirectAttributes redir, HttpServletRequest request) {

        try {
//            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//            String name = auth.getName(); //get logged in username
            String name = request.getSession().getAttribute("loggedUser").toString();
            
            day.setCreateduser(name);
            holidaydao.addHoliday(day);
            redir.addFlashAttribute("succMsg", "Record added successfully.");
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(HolidayManagerController.class.getName()).log(Level.SEVERE, null, ex);
            redir.addFlashAttribute("errMsg", "Record not added successfully.");
        }
        return new ModelAndView("redirect:/admin/holidaymanagement/getholidaylist");
    }
    
    @RequestMapping(value = "/holidaymanagement/editholiday", method = RequestMethod.POST)
    public String editCounter(@ModelAttribute("editForm") Holiday day,ModelMap modelmap) {
        try {
            modelmap.put("status", holidaydao.getStatus());
            modelmap.put("holiday",holidaydao.getHoliday(day.getHolidayid()));
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(HolidayManagerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "holiday_management/holiday_edit";
    
    }
    
    @RequestMapping(value = "/holidaymanagement/editedholiday", method = RequestMethod.POST)
    public ModelAndView editedCounter(@ModelAttribute("editForm") Holiday day,RedirectAttributes redir, HttpServletRequest request) {
        try {
//            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//            String name = auth.getName(); //get logged in username
            String name = request.getSession().getAttribute("loggedUser").toString();
            day.setCreateduser(name);
            holidaydao.updateHoliday(day);
            redir.addFlashAttribute("succMsg", "Record updated successfully.");
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(HolidayManagerController.class.getName()).log(Level.SEVERE, null, ex);
            redir.addFlashAttribute("errMsg", "Record not updated successfully.");
        }
        return new ModelAndView("redirect:/admin/holidaymanagement/getholidaylist");
    }
    
    @RequestMapping(value = "/holidaymanagement/deleteholiday", method = RequestMethod.POST)
    public ModelAndView removeCounter(@ModelAttribute("deleteForm")Holiday day, ModelMap modelmap,RedirectAttributes redir) {
        try {
            holidaydao.deleteHoliday(day.getHolidayid());
            redir.addFlashAttribute("succMsg", "Record deleted successfully.");
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(HolidayManagerController.class.getName()).log(Level.SEVERE, null, ex);
            redir.addFlashAttribute("errMsg", "Record not deleted successfully.");
        }
        return new ModelAndView("redirect:/admin/holidaymanagement/getholidaylist");
    }
    
    @RequestMapping(value = "/holidaymanagement/exsist", method = RequestMethod.POST)
    @ResponseBody
    public Boolean existdate(@RequestParam String datetimepicker) {
        String status = "";
        try {
            status = holidaydao.checkdate(datetimepicker);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
        }
        if (status == "") {
            return true;
        } else {
            return false;
        }
    }
}
