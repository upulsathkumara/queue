/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.queuemgr.dao.impl.DisplayManagerDaoImpl;
import com.queuemgr.modals.Display;
import com.queuemgr.util.LogFileCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Maheshm
 */
@Controller
@RequestMapping("/getToken")
public class DisplayManagerController {

    @Autowired
    DisplayManagerDaoImpl displayDaoImpl;

    @RequestMapping(value = "{recordNo}", method = RequestMethod.GET)
    public @ResponseBody
    Display getToken(@PathVariable String recordNo) {
        Display display = null;
        try {
            if (!"0".equals(recordNo)) {
                displayDaoImpl.deleteDocumentType(recordNo);
            }
            display = displayDaoImpl.getToken();
        } catch (Exception e) {
            LogFileCreator.writeErrorToLog(e);
            display = null;
        }
        return display;
    }
}
