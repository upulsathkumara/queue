/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.queuemgr.dao.impl.CustomerDAOImpl;
import com.queuemgr.dao.impl.DocumentTypeDAOImpl;
import com.queuemgr.dao.impl.ReservationDaoImpl;
import com.queuemgr.modals.BookingVerification;
import com.queuemgr.modals.Customer;
import com.queuemgr.modals.DocumentCharges;
import com.queuemgr.modals.DocumentType;
import com.queuemgr.modals.Instruction;
import com.queuemgr.util.Common;
import com.queuemgr.util.LogFileCreator;
import com.queuemgr.util.SMSSender;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author user
 */
@Controller
public class VipCustomer {

    @Autowired
    DocumentTypeDAOImpl documentTypeDAOImpl;

    @Autowired
    CustomerDAOImpl customerDAOImpl;

    @Autowired
    ReservationDaoImpl reservationDaoImpl;

    @RequestMapping(value = "/reservation/vip/create", method = RequestMethod.GET)
    public String loadReservationCreatePage(@ModelAttribute("reservationForm") Customer data, ModelMap model, HttpServletRequest request) {
        try {

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date dateNow = new Date();

            List<DocumentType> documentTypeList = documentTypeDAOImpl.getDocumentTypeBeanList();
            JSONObject customerJson = new JSONObject();
            customerJson.put("TYPE", documentTypeList);
            model.put("documentType", documentTypeDAOImpl.getDocumentTypeList());
            model.put("applicationType", documentTypeDAOImpl.getVipApplicationTypeList(1));
            model.put("Type", customerJson.toString());
            model.put("SERVICEID", 0);
            model.put("dateNow", dateFormat.format(dateNow));

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "reservation/vipcustomer";
    }

    @RequestMapping(value = "/reservation/vip/created", method = RequestMethod.POST)
    public String createBookingPage(@ModelAttribute("reservationForm") Customer data, ModelMap model, HttpServletRequest request) {
        int bookingId = 0;
        try {
            String loggedUser = request.getSession().getAttribute("loggedUser").toString();
            String loggedCounter = request.getSession().getAttribute("loggedCounter").toString();

            Gson gson = new Gson();
            List<DocumentType> documentType = gson.fromJson(data.getDocumentTypeList(), new TypeToken<List<DocumentType>>() {
            }.getType());
            List<BookingVerification> bookingVerification = gson.fromJson(data.getDocumentVarifictionList(), new TypeToken<List<BookingVerification>>() {
            }.getType());

            bookingId = reservationDaoImpl.createBooking_vip(data, bookingVerification, documentType, loggedUser, null, loggedCounter);

            int notificationsCount = reservationDaoImpl.getVipNotificationCount();

            HttpSession session = request.getSession();
            session.setAttribute("notifications", notificationsCount);
            String message = reservationDaoImpl.getWalkInSMSMessage("VIP");
            message = message.replace("<<SERVICEID>>", Integer.toString(bookingId));
            message = message.replace("<<BOOKINGDATE>>", data.getBookingdate());
            SMSSender.send(message, data.getTelephoneMobile());
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
            try {
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date dateNow = new Date();

                List<DocumentType> documentTypeList = documentTypeDAOImpl.getDocumentTypeBeanList();
                JSONObject customerJson = new JSONObject();
                customerJson.put("TYPE", documentTypeList);
                model.put("documentType", documentTypeDAOImpl.getDocumentTypeList());
                model.put("applicationType", documentTypeDAOImpl.getVipApplicationTypeList(1));
                model.put("Type", customerJson.toString());
                model.put("SERVICEID", 0);
                model.put("dateNow", dateFormat.format(dateNow));
                model.put("errorMsg", "Something went wrong. Please try again.");
            } catch (Exception e) {
                e.printStackTrace();
                LogFileCreator.writeErrorToLog(e);
                model.put("errorMsg", "Something went wrong. Please try again.");
                return "reservation/vipcustomer";
            }
            return "reservation/vipcustomer";
        }
        return "redirect:/reservation/create/vip/success/" + bookingId;
    }

    @RequestMapping(value = "/reservation/create/vip/success/{bookingid}", method = RequestMethod.GET)
    public String loadReservationCreateSuccessPage(ModelMap model, @PathVariable("bookingid") int bookingId) {
        try {
            Customer data = reservationDaoImpl.getRecevationVipCustomerSuccesData(bookingId);
            Instruction instruction = reservationDaoImpl.getInstructionwalk(2, data.getPreferredLanguage());
            List<BookingVerification> verification = reservationDaoImpl.getverificationDoc(bookingId);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            QRCodeWriter writer = new QRCodeWriter();
            BitMatrix matrix = writer.encode(Integer.toString(bookingId), BarcodeFormat.QR_CODE, 125, 125);
            MatrixToImageWriter.writeToStream(matrix, "png", output);

            String imageDataString = Base64.encodeBase64URLSafeString(output.toByteArray()).replace("_", "/").replace("-", "+");
            DateFormat df = new SimpleDateFormat("HH:mm:ss");
            Date dateobj = new Date();

            model.put("NAME", data.getFristName());
            model.put("MOBILE", data.getTelephoneMobile());
            model.put("EMAIL", data.getEmail());
            model.put("SERVICEID", bookingId);
            model.put("QRIMAGEPATH", imageDataString);
            model.put("IDENTIFICATION", data.getIdentificatioDocumentType() + ":" + data.getIdentificationNumber());

            model.put("INSTRUCTION", instruction.getLang());
            model.put("BOOKINGREFERENCE", verification);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
            model.put("ERR", 1);
            model.put("errorMsg", "Reservation created. But something went wrong while retrieving data. Please reload the page.");
        }
        return "reservation/vipsuccesspage";
    }

    @RequestMapping(value = "/reservation/vip/triger/create", method = RequestMethod.GET)
    public String loadReservationVipTrigerPage(@ModelAttribute("reservationForm") Customer data, ModelMap model) {
        try {

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "reservation/vip_triggering";
    }

    @RequestMapping(value = "/reservation/vip/triger/created", method = RequestMethod.POST)
    public String createVipTrigerBookingPage(@ModelAttribute("reservationForm") Customer data, HttpServletRequest request, ModelMap model) {
        int bookingId = 0;
        try {
            String loggedUser = request.getSession().getAttribute("loggedUser").toString();

            bookingId = reservationDaoImpl.createBooking_vip_triger(data, loggedUser);
            int notificationsCount = reservationDaoImpl.getVipNotificationCount();

            HttpSession session = request.getSession();
            session.setAttribute("notifications", notificationsCount);

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "redirect:/reservation/create/vip/trigger/success/" + bookingId;
    }

    @RequestMapping(value = "/reservation/create/vip/trigger/success/{bookingid}", method = RequestMethod.GET)
    public String loadTrigerCreateSuccessPage(ModelMap model, @PathVariable("bookingid") int bookingId) {
        try {
            Customer data = reservationDaoImpl.getRecevationVipTriggerSuccesData(bookingId);
            Instruction instruction = reservationDaoImpl.getInstructionwalk(2, "ENGLISH");
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            QRCodeWriter writer = new QRCodeWriter();
            BitMatrix matrix = writer.encode(Integer.toString(bookingId), BarcodeFormat.QR_CODE, 125, 125);
            MatrixToImageWriter.writeToStream(matrix, "png", output);

            String imageDataString = Base64.encodeBase64URLSafeString(output.toByteArray()).replace("_", "/").replace("-", "+");
            DateFormat df = new SimpleDateFormat("HH:mm:ss");
            Date dateobj = new Date();

            model.put("SERVICEID", bookingId);
            model.put("NAME", data.getFristName());
            model.put("DATE", Common.getDateString());
            model.put("QRIMAGEPATH", imageDataString);
            model.put("CREATEDTIME", df.format(dateobj));
            model.put("INSTRUCTION", instruction);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();

            return "reservation/vip_triggering";
        }
        return "reservation/viptriggerSuccess";
    }

    @RequestMapping(value = "/notification/list", method = RequestMethod.POST)
    public @ResponseBody
    String notificationList(@RequestParam String data) throws Exception {
        String strJson = "";
        try {
            JSONObject Json = new JSONObject();
            List<String> object = reservationDaoImpl.getVipNotificationList();
            Json.put("object", object);
            strJson = Json.toString();

        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, exception);
        }
        return strJson;
    }

    @RequestMapping(value = "/notification/serviceId", method = RequestMethod.POST)
    public @ResponseBody
    String addSessionServiceId(@RequestParam int data, HttpServletRequest request) throws Exception {
        String strJson = "";
        JSONObject Json = new JSONObject();
        try {
            HttpSession session = request.getSession();
            session.setAttribute("vipnotifictionserviceid", data);

            Json.put("STATUS", "SUCCESS");
        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            Json.put("STATUS", "ERROR");
            Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, exception);
        }
        strJson = Json.toString();
        return strJson;
    }

    @RequestMapping(value = "/reservation/vip/create/notifications", method = RequestMethod.GET)
    public String loadReservationCreatePagenotifications(@ModelAttribute("reservationForm") Customer data, ModelMap model, HttpServletRequest request) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date dateNow = new Date();
            List<DocumentType> documentTypeList = documentTypeDAOImpl.getDocumentTypeBeanList();
            JSONObject customerJson = new JSONObject();
            customerJson.put("TYPE", documentTypeList);
            model.put("documentType", documentTypeDAOImpl.getDocumentTypeList());
            model.put("applicationType", documentTypeDAOImpl.getApplicationTypeList(1));
            model.put("Type", customerJson.toString());
            model.put("dateNow", dateFormat.format(dateNow));

            HttpSession session = request.getSession();
            int serviceId = (int) session.getAttribute("vipnotifictionserviceid");
            model.put("SERVICEID", serviceId);
            System.out.println(serviceId);

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "reservation/vipcustomer";
    }

}
