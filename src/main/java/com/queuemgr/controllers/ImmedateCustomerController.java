/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.queuemgr.dao.impl.CustomerDAOImpl;
import com.queuemgr.dao.impl.DocumentTypeDAOImpl;
import com.queuemgr.dao.impl.OnlineBookingDAOImpl;
import com.queuemgr.dao.impl.ReservationDaoImpl;
import com.queuemgr.modals.BookingVerification;
import com.queuemgr.modals.Customer;
import com.queuemgr.modals.DocumentCharges;
import com.queuemgr.modals.DocumentType;
import com.queuemgr.util.LogFileCreator;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author user
 */
@Controller
public class ImmedateCustomerController {
    @Autowired
    DocumentTypeDAOImpl documentTypeDAOImpl;

    @Autowired
    CustomerDAOImpl customerDAOImpl;

    @Autowired
    ReservationDaoImpl reservationDaoImpl;
    
    @Autowired
    OnlineBookingDAOImpl onlineDaoImpl;
    
     @RequestMapping(value = "/reservation/immediate", method = RequestMethod.GET)
    public String loadReservationCreatePage(@ModelAttribute("reservationForm") Customer data, ModelMap model) {
        try {

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date dateNow = new Date();

            List<DocumentType> documentTypeList = documentTypeDAOImpl.getDocumentTypeBeanList();
            JSONObject customerJson = new JSONObject();
            customerJson.put("TYPE", documentTypeList);
            model.put("documentType", documentTypeDAOImpl.getDocumentTypeList());
            model.put("applicationType", documentTypeDAOImpl.getApplicationTypeList(0));
            model.put("Type", customerJson.toString());
            model.put("dateNow", dateFormat.format(dateNow));

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "reservation/immediateCustomer";
    }
    
     @RequestMapping(value = "/reservation/immediate/customer", method = RequestMethod.POST)
    public @ResponseBody
    String createAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String jsoncontent = request.getParameter("data");
        JSONObject rsp = new JSONObject(jsoncontent);
        List<Customer> customerList;
        List<DocumentType> documentTypeList;
        List<DocumentType> documentTypeList1;
        String myJson;
        JSONObject customerJson = new JSONObject();
        try {

            customerList = customerDAOImpl.getImmediateCustomer(Integer.parseInt(rsp.getString("serachValue").trim()));
            customerJson.put("CUSTOMER", customerList);
            if (customerList.size() > 0) {
                documentTypeList = onlineDaoImpl.getOnlineBookinDocument(Integer.parseInt(rsp.getString("serachValue").trim()));
                documentTypeList1 = onlineDaoImpl.getOnlineBookinDocuments(rsp.getInt("serachValue"));
                List<BookingVerification> verificationtList = onlineDaoImpl.getOnlineBookinDocumentVerification(Integer.parseInt(rsp.getString("serachValue").trim()));
                customerJson.put("DOCUMENTDESCRIPTION", documentTypeList);
                customerJson.put("DOCUMENT", documentTypeList1);
                customerJson.put("VERFICATIONLIST", verificationtList);
            }

        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            customerJson.put("CODE", "ERROR");
            Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, exception);
        }
        myJson = customerJson.toString();
        return myJson;
    }
    
}
