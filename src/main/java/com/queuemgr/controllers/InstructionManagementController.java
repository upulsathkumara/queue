
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.queuemgr.dao.impl.InstructionManagementDAOImple;
import com.queuemgr.modals.Instruction;
import com.queuemgr.util.LogFileCreator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringEscapeUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Kelum Madushan
 */
@Controller
@RequestMapping(value = "/admin")
public class InstructionManagementController {

    @Autowired
    InstructionManagementDAOImple instruction;

    @RequestMapping(value = "/instructionmanagement/getinsttructionlist", method = RequestMethod.GET)
    public String getInstructionList(ModelMap modelmap, HttpServletRequest request) {
        try {
            modelmap.put("instructionList", instruction.getInstructionList());
//            modelmap.addAttribute("msg", request.getParameter("msg"));
        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            Logger.getLogger(InstructionManagementController.class.getName()).log(Level.SEVERE, null, exception);
        }

        return "instruction_management/instruction_list";
    }

    @RequestMapping(value = "/instructionmanagement/addinstruction", method = RequestMethod.GET)
    public String addInstruction(@ModelAttribute("insertForm") Instruction ins, ModelMap modelmap) {
        try {
            modelmap.put("status", instruction.getStatus());
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(InstructionManagementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "instruction_management/instruction_create";
    }

    @RequestMapping(value = "/instructionmanagement/addinstruction", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public ModelAndView saveInstruction(@ModelAttribute("insertForm") Instruction ins, RedirectAttributes redir) {
        try {

            String text = ins.getSinhala();
            text = StringEscapeUtils.unescapeJava(text);
            System.out.println("text " + text);

            String string = "abc\u0dbb" + "uodc3";
            byte[] utf8 = string.getBytes("UTF-8");

            string = new String(utf8, "UTF-8");
            System.out.println(string);

            instruction.addInstruction(ins);
            redir.addAttribute("msg", "Record added successfully.");
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(SystemUserManagementController.class.getName()).log(Level.SEVERE, null, ex);
            redir.addAttribute("msg", "Record not added successfully.");
        }
        return new ModelAndView("redirect:/admin/instructionmanagement/getinsttructionlist");
    }

    @RequestMapping(value = "/instructionmanagement/editinstruction", method = RequestMethod.POST)
    public String editInstruction(@ModelAttribute("editForm") Instruction ins, ModelMap modelmap) {
        try {
            modelmap.put("status", instruction.getStatus());
            modelmap.put("instructionList", instruction.getInstruction(ins.getInstructionid()));
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(SystemUserManagementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "instruction_management/instruction_edit";
    }

    @RequestMapping(value = "/instructionmanagement/edittedinstruction", method = RequestMethod.POST)

    public ModelAndView editedCounter(@ModelAttribute("editForm") Instruction ins,
            @RequestParam(value = "sinhala", required = true) String sinhala, 
            @RequestParam(value = "english", required = true) String english, 
            @RequestParam(value = "tamil", required = true) String tamil, RedirectAttributes redir) {
        try {
            ins.setEnglish(english);
            ins.setSinhala(sinhala);
            ins.setTamil(tamil);
            instruction.updateInstruction(ins);
            redir.addFlashAttribute("succMsg", "Record updated successfully.");
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(ApplicationCounterManagementController.class.getName()).log(Level.SEVERE, null, ex);
            redir.addFlashAttribute("errMsg", "Record not updated successfully.");
        }
        return new ModelAndView("redirect:/admin/instructionmanagement/getinsttructionlist");
    }

    @RequestMapping(value = "/instructionmanagement/deleteinstruction", method = RequestMethod.POST)
    public ModelAndView removeInstruction(@ModelAttribute("deleteForm") Instruction ins, ModelMap modelmap, RedirectAttributes redir) {
        try {
            instruction.deleteInstruction(ins.getInstructionid());
            redir.addAttribute("msg", "Record deleted successfully.");
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(SystemUserManagementController.class.getName()).log(Level.SEVERE, null, ex);
            redir.addAttribute("msg", "Record not deleted successfully.");
        }
        return new ModelAndView("redirect:/admin/instructionmanagement/getinsttructionlist");
    }
}
