/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.queuemgr.dao.impl.SystemUserManagementDAOImple;
import com.queuemgr.modals.SystemUser;
import com.queuemgr.util.LogFileCreator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Kelum Madushan
 */
@Controller
@RequestMapping("/admin")
public class SystemUserManagementController {

    @Autowired
    SystemUserManagementDAOImple systemuser;

    @RequestMapping(value = "/systemuser/getsystemuserlist", method = RequestMethod.GET)
    public String getApplicationCounterList(ModelMap modelmap, HttpServletRequest request) {
        try {
            modelmap.put("userList", systemuser.getUserList());
            modelmap.addAttribute("msg", request.getParameter("msg"));
        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            Logger.getLogger(SystemUserManagementController.class.getName()).log(Level.SEVERE, null, exception);
        }
        return "system_user/user_list";
    }

    @RequestMapping(value = "/systemuser/adduser", method = RequestMethod.GET)
    public String addCounter(@ModelAttribute("insertForm") SystemUser user, ModelMap modelmap) {
        try {
            modelmap.put("role", systemuser.getRoleList());
        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            Logger.getLogger(SystemUserManagementController.class.getName()).log(Level.SEVERE, null, exception);
        }
        return "system_user/user_create";
    }

    @RequestMapping(value = "/systemuser/adduser", method = RequestMethod.POST)
    public ModelAndView saveCounter(@ModelAttribute("insertForm") SystemUser user, RedirectAttributes redir) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String name = auth.getName(); //get logged in username
            user.setCreateduser(name);
            systemuser.addUser(user);
            redir.addAttribute("msg", "Record added successfully.");
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(SystemUserManagementController.class.getName()).log(Level.SEVERE, null, ex);
            redir.addAttribute("msg", "Record not added successfully.");
        }
        return new ModelAndView("redirect:/admin/systemuser/getsystemuserlist");
    }

    @RequestMapping(value = "/systemuser/edituser", method = RequestMethod.POST)
    public String editCounter(@ModelAttribute("editForm") SystemUser user, ModelMap modelmap) {
        try {
            modelmap.put("role", systemuser.getRoleList());
            modelmap.put("user", systemuser.getUser(user.getUsername()));
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(SystemUserManagementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "system_user/user_edit";

    }

    @RequestMapping(value = "/systemuser/editeduser", method = RequestMethod.POST)
    public ModelAndView editedCounter(@ModelAttribute("editForm") SystemUser user, RedirectAttributes redir) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String name = auth.getName(); //get logged in username
            user.setCreateduser(name);
            systemuser.updateUser(user);
            redir.addAttribute("msg", "Record updated successfully.");
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(SystemUserManagementController.class.getName()).log(Level.SEVERE, null, ex);
            redir.addAttribute("msg", "Record not updated successfully.");
        }
        return new ModelAndView("redirect:/admin/systemuser/getsystemuserlist");
    }

    @RequestMapping(value = "/systemuser/deletecounter", method = RequestMethod.POST)
    public ModelAndView removeCounter(@ModelAttribute("deleteForm") SystemUser user, ModelMap modelmap, RedirectAttributes redir) {
        try {
            systemuser.deleteUser(user.getUsername());
            redir.addAttribute("msg", "Record deleted successfully.");
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(ServiceCounterManagementController.class.getName()).log(Level.SEVERE, null, ex);
            redir.addAttribute("msg", "Record not deleted successfully.");
        }
        return new ModelAndView("redirect:/admin/systemuser/getsystemuserlist");
    }

    @RequestMapping(value = "/systemuser/exsist", method = RequestMethod.POST)
    @ResponseBody
    public Boolean existuser(@RequestParam String username) {
        String status = "";
        try {
            status = systemuser.checkuser(username);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
        }
        if (status == "") {
            return true;
        } else {
            return false;
        }
    }

    @RequestMapping(value = "/systemuser/exsistnic", method = RequestMethod.POST)
    @ResponseBody
    public Boolean existusernic(@RequestParam String nic) {
        String status = "";
        try {
            status = systemuser.checkusernic(nic);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
        }
        if (status == "") {
            return true;
        } else {
            return false;
        }
    }
}
