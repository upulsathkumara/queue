/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.queuemgr.dao.impl.ServiceCounterManagementDAOImple;
import com.queuemgr.modals.ServiceDesk;
import com.queuemgr.util.LogFileCreator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Kelum Madushan
 */
@Controller
@RequestMapping(value = "/admin")
public class ServiceCounterManagementController {

    @Autowired
    ServiceCounterManagementDAOImple countermanag;

    @RequestMapping(value = "/servicecountermanagement/getcounterlist", method = RequestMethod.GET)
    public String getServiceCounterList(ModelMap modelmap, HttpServletRequest request) {
        try {
            modelmap.put("counterList", countermanag.getCounterList());
            modelmap.addAttribute("msg", request.getParameter("msg"));
        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            Logger.getLogger(ServiceCounterManagementController.class.getName()).log(Level.SEVERE, null, exception);
        }

        return "service_counter_management/counter_list";
    }

    @RequestMapping(value = "/servicecountermanagement/addcounter", method = RequestMethod.GET)
    public String addCounter(@ModelAttribute("insertForm") ServiceDesk counter, ModelMap modelmap) {
        try {
            modelmap.put("status", countermanag.getStatus());
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(ServiceCounterManagementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "service_counter_management/counter_create";
    }

    @RequestMapping(value = "/servicecountermanagement/addcounter", method = RequestMethod.POST)
    public ModelAndView saveCounter(@ModelAttribute("insertForm") ServiceDesk counter, RedirectAttributes redir, HttpServletRequest request) {
        try {
//            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//            String name = auth.getName(); //get logged in username
            String name = request.getSession().getAttribute("loggedUser").toString();
            System.out.println(">>>>>>>>>>>>>> " + name);

            counter.setCreateduser(name);
            countermanag.addCounter(counter);
            redir.addAttribute("msg", "Record added successfully.");
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(ServiceCounterManagementController.class.getName()).log(Level.SEVERE, null, ex);
            redir.addAttribute("msg", "Record not added successfully.");
        }
        return new ModelAndView("redirect:/admin/servicecountermanagement/getcounterlist");
    }

    @RequestMapping(value = "/servicecountermanagement/editcounter", method = RequestMethod.POST)
    public String editCounter(@ModelAttribute("editForm") ServiceDesk counter, ModelMap modelmap) {
        try {
            modelmap.put("status", countermanag.getStatus());
            modelmap.put("counter", countermanag.getCounter(counter.getSdid()));
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(ServiceCounterManagementController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "service_counter_management/counter_edit";

    }

    @RequestMapping(value = "/servicecountermanagement/editedcounter", method = RequestMethod.POST)
    public ModelAndView editedCounter(@ModelAttribute("editForm") ServiceDesk counter, RedirectAttributes redir, HttpServletRequest request) {
        try {
//            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//            String name = auth.getName(); //get logged in username

            String name = request.getSession().getAttribute("loggedUser").toString();
            System.out.println(">>>>>>>>>>>>>> " + name);
            counter.setCreateduser(name);
            countermanag.updateCounter(counter);
            redir.addAttribute("msg", "Record updated successfully.");
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(ServiceCounterManagementController.class.getName()).log(Level.SEVERE, null, ex);
            redir.addAttribute("msg", "Record not updated successfully.");
        }
        return new ModelAndView("redirect:/admin/servicecountermanagement/getcounterlist");
    }

    @RequestMapping(value = "/servicecountermanagement/deletecounter", method = RequestMethod.POST)
    public ModelAndView removeCounter(@ModelAttribute("deleteForm") ServiceDesk counter, ModelMap modelmap, RedirectAttributes redir) {
        try {
            countermanag.deleteCounter(counter.getSdid());
            redir.addAttribute("msg", "Record deleted successfully.");
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            Logger.getLogger(ServiceCounterManagementController.class.getName()).log(Level.SEVERE, null, ex);
            redir.addAttribute("msg", "Record not deleted successfully.");
        }
        return new ModelAndView("redirect:/admin/servicecountermanagement/getcounterlist");
    }

    @RequestMapping(value = "/servicecountermanagement/checksdid", method = RequestMethod.POST)
    @ResponseBody
    public Boolean existusernic(@RequestParam String sdid) {
        String status = "";
        try {
            status = countermanag.checksdid(sdid);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
        }
        if (status == "") {
            return true;
        } else {
            return false;
        }
    }
}
