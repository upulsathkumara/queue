
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.queuemgr.dao.impl.CustomerDAOImpl;
import com.queuemgr.dao.impl.DocumentTypeDAOImpl;
import com.queuemgr.dao.impl.ReservationDaoImpl;
import com.queuemgr.modals.BookingVerification;
import com.queuemgr.modals.CommonConfigurations;
import com.queuemgr.modals.Customer;
import com.queuemgr.modals.DocumentCharges;
import com.queuemgr.modals.DocumentType;
import com.queuemgr.modals.Holiday;
import com.queuemgr.modals.Instruction;
import com.queuemgr.modals.ReservationFullObject;
import com.queuemgr.util.Common;
import com.queuemgr.util.LogFileCreator;
import com.queuemgr.util.SMSSender;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.apache.commons.codec.binary.Base64;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author Rasika
 */
@Controller
public class ReservationController {

    @Autowired
    DocumentTypeDAOImpl documentTypeDAOImpl;

    @Autowired
    CustomerDAOImpl customerDAOImpl;

    @Autowired
    ReservationDaoImpl reservationDaoImpl;

    @RequestMapping(value = "/reservation/create", method = RequestMethod.GET)
    public String loadReservationCreatePage(@ModelAttribute("reservationForm") Customer data, ModelMap model) {
        try {

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date dateNow = new Date();

            List<DocumentType> documentTypeList = documentTypeDAOImpl.getDocumentTypeBeanList();
            JSONObject customerJson = new JSONObject();
            customerJson.put("TYPE", documentTypeList);
            model.put("documentType", documentTypeDAOImpl.getDocumentTypeList());
            model.put("applicationType", documentTypeDAOImpl.getApplicationTypeList(0));
            model.put("Type", customerJson.toString());
            model.put("dateNow", dateFormat.format(dateNow));

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "reservation/reservationCreate";
    }

    @RequestMapping(value = "/reservation/created", method = RequestMethod.POST)
    public String createBookingPage(@ModelAttribute("reservationForm") Customer data, ModelMap model, HttpServletRequest request) {
        int bookingId = 0;
        try {
            String loggedUser = request.getSession().getAttribute("loggedUser").toString();
            String loggedCounter = request.getSession().getAttribute("loggedCounter").toString();
            Gson gson = new Gson();
            List<DocumentType> documentType = gson.fromJson(data.getDocumentTypeList(), new TypeToken<List<DocumentType>>() {
            }.getType());
            List<BookingVerification> bookingVerification = gson.fromJson(data.getDocumentVarifictionList(), new TypeToken<List<BookingVerification>>() {
            }.getType());
            Timestamp startTime = Common.getTimestampFromDateAndTime(data.getBookingdate() + " " + data.getStartTimeStr());
            Timestamp endTime = Common.getTimestampFromDateAndTime(data.getBookingdate() + " " + data.getEndtimeStr());
            boolean isTrueTimeSloatTaken = this.getTimeSlotAvailabel(data);
            boolean isTrueTwoBookingSameTimeSloat = reservationDaoImpl.thisTimeSloatSameCustomer(startTime, endTime, data.getIdentificationNumber());
            if (!isTrueTwoBookingSameTimeSloat) {
                if (isTrueTimeSloatTaken) {
                    bookingId = reservationDaoImpl.createBooking(data, bookingVerification, documentType, loggedUser, null, loggedCounter);
                    String message = reservationDaoImpl.getWalkInSMSMessage("WALKIN");
                    message = message.replace("<<SERVICEID>>", Integer.toString(bookingId));
                    message = message.replace("<<BOOKINGDATE>>", data.getBookingdate());
                    message = message.replace("<<BOOKINGTIME>>", Common.getTimeFromTimestampAMPM(startTime));
                    message = message.replace("<<ENDTIME>>", Common.getTimeFromTimestampAMPM(endTime));
                    message = message.replace("<<TIMEGAPTOISSUETICKET>>", Integer.toString(reservationDaoImpl.getTimeGapToIssueTicket()));
                    SMSSender.send(message, data.getTelephoneMobile());
                } else {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date dateNow = new Date();

                    List<DocumentType> documentTypeList = documentTypeDAOImpl.getDocumentTypeBeanList();
                    JSONObject customerJson = new JSONObject();
                    customerJson.put("TYPE", documentTypeList);
                    model.put("documenttypeoldlist", documentTypeDAOImpl.getReminderDocumentTypeBeanListAddDescription(documentType));
                    Map<String, String> documentTypeDtabseList = documentTypeDAOImpl.getDocumentTypeOldList(this.setOldDocumentTypeListWhere(documentType));

                    model.put("documentType", documentTypeDtabseList);
                    model.put("applicationType", documentTypeDAOImpl.getApplicationTypeList(0));
                    model.put("Type", customerJson.toString());
                    model.put("dateNow", dateFormat.format(dateNow));
                    model.put("timeduration", data.getTimeDuration());
                    model.put("olddocumentverificationlist", data.getDocumentVarifictionList());
                    model.put("errorMsg", "Sorry another customer has been taken this time slot. Please try again with a different time slot");

                    return "reservation/reservationCreate";
                }

            } else {
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date dateNow = new Date();

                List<DocumentType> documentTypeList = documentTypeDAOImpl.getDocumentTypeBeanList();
                JSONObject customerJson = new JSONObject();
                customerJson.put("TYPE", documentTypeList);
                model.put("documenttypeoldlist", documentTypeDAOImpl.getReminderDocumentTypeBeanListAddDescription(documentType));
                Map<String, String> documentTypeDtabseList = documentTypeDAOImpl.getDocumentTypeOldList(this.setOldDocumentTypeListWhere(documentType));

                model.put("documentType", documentTypeDtabseList);
                model.put("applicationType", documentTypeDAOImpl.getApplicationTypeList(0));
                model.put("Type", customerJson.toString());
                model.put("dateNow", dateFormat.format(dateNow));
                model.put("timeduration", data.getTimeDuration());
                model.put("olddocumentverificationlist", data.getDocumentVarifictionList());
                model.put("errorMsg", "Already this customer has a reservation at this time");

                return "reservation/reservationCreate";

            }

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            try {
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date dateNow = new Date();

                Gson gson = new Gson();
                List<DocumentType> documentType = gson.fromJson(data.getDocumentTypeList(), new TypeToken<List<DocumentType>>() {
                }.getType());
                List<BookingVerification> bookingVerification = gson.fromJson(data.getDocumentVarifictionList(), new TypeToken<List<BookingVerification>>() {
                }.getType());

                List<DocumentType> documentTypeList = documentTypeDAOImpl.getDocumentTypeBeanList();
                JSONObject customerJson = new JSONObject();
                customerJson.put("TYPE", documentTypeList);
                model.put("documenttypeoldlist", documentTypeDAOImpl.getReminderDocumentTypeBeanListAddDescription(documentType));
                Map<String, String> documentTypeDtabseList = documentTypeDAOImpl.getDocumentTypeOldList(this.setOldDocumentTypeListWhere(documentType));

                model.put("documentType", documentTypeDtabseList);
                model.put("applicationType", documentTypeDAOImpl.getApplicationTypeList(0));
                model.put("Type", customerJson.toString());
                model.put("dateNow", dateFormat.format(dateNow));
                model.put("timeduration", data.getTimeDuration());
                model.put("olddocumentverificationlist", data.getDocumentVarifictionList());
                model.put("errorMsg", "Something went wrong. Please try again.");

            } catch (Exception e) {
                LogFileCreator.writeErrorToLog(e);
                e.printStackTrace();
                model.put("errorMsg", "Something went wrong. Please try again.");
            }
            return "reservation/reservationCreate";

        }
        return "redirect:/reservation/create/success/" + bookingId;
    }

    @RequestMapping(value = "/reservation/create/success/{bookingid}", method = RequestMethod.GET)
    public String loadReservationCreateSuccessPage(ModelMap model, @PathVariable("bookingid") int bookingId) {
        try {
            System.out.println("Rasika:" + bookingId);

            Customer data = reservationDaoImpl.getRecevationSuccesData(bookingId);
            Instruction instruction = reservationDaoImpl.getInstructionwalk(0, data.getPreferredLanguage());
            List<BookingVerification> verification = reservationDaoImpl.getverificationDoc(bookingId);
            String startTime = Common.getTimeFromTimestampAMPM(Common.getTimestampFromDateAndTime(data.getBookingdate() + " " + data.getStartTimeStr()));
            String endTime = Common.getTimeFromTimestampAMPM(Common.getTimestampFromDateAndTime(data.getBookingdate() + " " + data.getEndtimeStr()));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            QRCodeWriter writer = new QRCodeWriter();
            BitMatrix matrix = writer.encode(Integer.toString(bookingId), BarcodeFormat.QR_CODE, 125, 125);
            MatrixToImageWriter.writeToStream(matrix, "png", output);

            String imageDataString = Base64.encodeBase64URLSafeString(output.toByteArray()).replace("_", "/").replace("-", "+");
            DateFormat df = new SimpleDateFormat("HH:mm:ss");
            Date dateobj = new Date();
            model.put("STARTTIME", data.getStartTimeStr());
            model.put("ENDTIME", data.getEndtimeStr());
            model.put("DATE", Common.dateforamtChange(data.getBookingdate()));
            model.put("NAME", data.getFristName() + data.getLastName());
            model.put("MOBILE", data.getTelephoneMobile());
            model.put("EMAIL", data.getEmail());
            model.put("SERVICEID", bookingId);
            model.put("QRIMAGEPATH", imageDataString);
            model.put("CREATEDTIME", startTime + " - " + endTime);
            model.put("IDENTIFICATION", data.getIdentificatioDocumentType() + ":" + data.getIdentificationNumber());

            model.put("INSTRUCTION", instruction.getLang());
            model.put("BOOKINGREFERENCE", verification);

            LogFileCreator.writeInfoToLog("Walk-in reservation created. Service ID: " + bookingId);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
            model.put("ERR",1);
            model.put("errorMsg", "Reservation created. But something went wrong while retrieving data. Please reload the page.");
        }
        return "reservation/succcesfull";
    }

    @RequestMapping(value = "/reservation/succesfull", method = RequestMethod.GET)
    public String loadReservationSuccessfull(ModelMap model, HttpServletRequest request) {
        try {

            Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
            if (flashMap != null) {

                System.out.println("Book Name:" + flashMap.get("bookingId"));
            }
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
        }
        return "reservation/succcesfull";
    }

    @RequestMapping(value = "/reservation/search", method = RequestMethod.POST)
    public @ResponseBody
    String searchReservation(@RequestParam String data) throws Exception {
        String strJson = "";
        List<Customer> cus = new ArrayList();
        Customer customer = new Customer();
        try {
            JSONObject rsp = new JSONObject(data);
            JSONObject Json = new JSONObject();
            List<ReservationFullObject> object = reservationDaoImpl.getCustomerFulldetails(rsp.getString("serachValue"));
//            for(int i=0;i<object.size();i++){
//                
//            }
            Json.put("object", object);
            strJson = Json.toString();

        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, exception);
        }
        return strJson;
    }

    @RequestMapping(value = "/reservation/exit/customer", method = RequestMethod.POST)
    public @ResponseBody
    String createAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String jsoncontent = request.getParameter("data");
        JSONObject rsp = new JSONObject(jsoncontent);
        List<Customer> customerList;
        String myJson;
        JSONObject customerJson = new JSONObject();
        try {

            customerList = customerDAOImpl.getCustomer(rsp.getString("serachValue"));
            customerJson.put("CUSTOMER", customerList);

        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            customerJson.put("CODE", "ERROR");
            Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, exception);
        }
        myJson = customerJson.toString();
        return myJson;
    }

    @RequestMapping(value = "/reservation/exit/customer/vip", method = RequestMethod.POST)
    public @ResponseBody
    String exitCustomerVip(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String jsoncontent = request.getParameter("data");
        JSONObject rsp = new JSONObject(jsoncontent);
        List<Customer> customerList;
        String myJson;
        JSONObject customerJson = new JSONObject();
        try {
            customerList = customerDAOImpl.getCustomerVIP(rsp.getString("serachValue"));
            customerJson.put("CUSTOMER", customerList);

        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            customerJson.put("CODE", "ERROR");
            Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, exception);
        }
        myJson = customerJson.toString();
        return myJson;
    }

    @RequestMapping(value = "/reservation/created/QRCODE/getimage", method = RequestMethod.POST)
    public @ResponseBody
    void getImage(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        try {
            String image = request.getParameter("image");
            System.out.println(image);
            String absolutePath = new File("").getAbsolutePath();
            String fileStrnew = absolutePath + "/resources/img/rasika/23.PNG";
            System.out.println(absolutePath);
            File file = new File(fileStrnew);
            byte[] fileBytes = null;
            fileBytes = Files.readAllBytes(file.toPath());
            response.setContentType("image/jpeg");
            response.setContentLength(fileBytes.length);

            response.getOutputStream().write(fileBytes);
        } catch (Exception e) {
            LogFileCreator.writeErrorToLog(e);
            e.printStackTrace();
        }

    }

    @RequestMapping(value = "/reservation/avilable/slot", method = RequestMethod.POST)
    public @ResponseBody
    String durationTime(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String jsoncontent = request.getParameter("data");
        String myJson = null;
        double totalTimeDuration;
        String date;

        try {
            JSONObject rsp = new JSONObject(jsoncontent);
            JSONObject customerJson = new JSONObject();

            totalTimeDuration = rsp.getDouble("timeDuration");
            date = rsp.getString("bookingDate");

            CommonConfigurations common;

            boolean isFullHoliday = reservationDaoImpl.isFullHoliday(date);

            if (isFullHoliday) {
                customerJson.put("HOLIDAY", "YES");
            } else {
                customerJson.put("HOLIDAY", "NO");

                common = reservationDaoImpl.getTime();
                int walkInPercentage = reservationDaoImpl.getWalkInPercentage();
                String cuttoffTimestr = reservationDaoImpl.getOnlineCutoffTime();
                LocalDate localDate = LocalDate.now();
                String localdate = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(localDate);
                Timestamp cutoffTime = Common.getTimestampFromDateAndTime(localdate + " " + cuttoffTimestr);

                List<Customer> customerList = new ArrayList<>();

                List<Holiday> holidays = reservationDaoImpl.getHolidayTime(date);
                if (holidays.size() > 2) {
                    customerList = this.setHafDayCustomerList(date, holidays);
                } else {
                    Customer cus;
                    String startTime = common.getServiceDeskStartTime();
                    String endTime = common.getServiceDeskEndTime();
                    String[] tokens;
                    tokens = startTime.split(":");
                    if (!"00".equals(tokens[1])) {
                        int addHour = Integer.parseInt(tokens[0]);
                        addHour = addHour + 1;
                        String newStartTime = Common.timeConcatanation(String.valueOf(addHour) + ":00");
                        startTime = Common.timeConcatanation(startTime);
                        int d1 = Common.getTimeDifferenceInMinutes(Common.getTimestampFromDateAndTime(date + " " + startTime), Common.getTimestampFromDateAndTime(date + " " + endTime));
                        int d2 = Common.getTimeDifferenceInMinutes(Common.getTimestampFromDateAndTime(date + " " + startTime), Common.getTimestampFromDateAndTime(date + " " + newStartTime));
                        if (d1 > d2) {
                            cus = new Customer();
                            cus.setStartTimeStr(startTime);
                            cus.setEndtimeStr(newStartTime);
                            customerList.add(cus);
                            startTime = newStartTime;
                        }
                    }
                    Timestamp dayStartTime = Common.getTimestampFromDateAndTime(date + " " + startTime);
                    Timestamp dayEndTime = Common.getTimestampFromDateAndTime(date + " " + endTime);
                    int timeDiffrence = Common.getTimeDifferenceInMinutes(dayStartTime, dayEndTime);
                    int hour = timeDiffrence / 60;
                    int minute = 60;
                    int modulestime = timeDiffrence % 60;

                    for (int i = 0; i < hour; i++) {
                        cus = new Customer();
                        if (i == 0) {
                            cus.setStartTimeStr(Common.getTimeFromTimestamp(dayStartTime));
                            cus.setEndtimeStr(Common.AddMinutesToTime(dayStartTime, 60));
                        } else {
                            cus.setStartTimeStr(Common.AddMinutesToTime(dayStartTime, minute));
                            cus.setEndtimeStr(Common.AddMinutesToTime(dayStartTime, minute + 60));
                            minute = minute + 60;
                        }
                        customerList.add(cus);
                    }
                    if (modulestime > 0) {
                        cus = new Customer();
                        cus.setStartTimeStr(Common.AddMinutesToTime(dayStartTime, minute));
                        cus.setEndtimeStr(Common.AddMinutesToTime(dayStartTime, minute + modulestime));
                        customerList.add(cus);
                    }
                }

                int serviceDeskCount = reservationDaoImpl.getCountServiceDesk();

                List<Customer> cutoffTimeNotExceed = reservationDaoImpl.getSumDuration(date, "AND ISONLINE=0");
                List<Customer> cutoffTimeExceed = reservationDaoImpl.getSumDuration(date, "");

                for (int i = 0; i < customerList.size(); i++) {
                    int sumTimeDuration = 0;
                    Timestamp startTime = Common.getTimestampFromDateAndTime(date + " " + customerList.get(i).getStartTimeStr());
                    Timestamp endTime = Common.getTimestampFromDateAndTime(date + " " + customerList.get(i).getEndtimeStr());
                    Timestamp nowTime = Common.getCurrentTimeStamp();
                    if (!"YES".equals(customerList.get(i).getHalfday())) {
                        if (localdate.equals(date)) {

                            if (cutoffTime.after(nowTime)) {
                                if (nowTime.before(startTime)) {
                                    int timeDiffrencein = Common.getTimeDifferenceInMinutes(startTime, endTime);
                                    for (int j = 0; j < cutoffTimeNotExceed.size(); j++) {
                                        if (customerList.get(i).getStartTimeStr().equals(cutoffTimeNotExceed.get(j).getStartTimeStr())) {
                                            sumTimeDuration = cutoffTimeNotExceed.get(j).getSumTimeDuration();
                                        }
                                    }
                                    int sloat;
                                    sloat = (int) (((timeDiffrencein * serviceDeskCount * walkInPercentage) / 100 - sumTimeDuration) / totalTimeDuration);
                                    customerList.get(i).setAvailabelSlot((int) Math.round(sloat));
                                    customerList.get(i).setExceed("NO");
                                } else if (nowTime.after(endTime)) {
                                    int timeDiffrencein = Common.getTimeDifferenceInMinutes(startTime, endTime);
                                    for (int j = 0; j < cutoffTimeNotExceed.size(); j++) {
                                        if (customerList.get(i).getStartTimeStr().equals(cutoffTimeNotExceed.get(j).getStartTimeStr())) {
                                            sumTimeDuration = cutoffTimeNotExceed.get(j).getSumTimeDuration();
                                        }
                                    }
                                    int sloat;
                                    sloat = (int) (((timeDiffrencein * serviceDeskCount * walkInPercentage) / 100 - sumTimeDuration) / totalTimeDuration);
                                    customerList.get(i).setAvailabelSlot((int) Math.round(sloat));
                                    customerList.get(i).setExceed("YES");
                                } else if (nowTime.after(startTime) && nowTime.before(endTime)) {
                                    int timeDiffrencein = Common.getTimeDifferenceInMinutes(nowTime, endTime);
                                    int timeDiffrencebefore = Common.getTimeDifferenceInMinutes(startTime, nowTime);
                                    for (int j = 0; j < cutoffTimeNotExceed.size(); j++) {
                                        if (customerList.get(i).getStartTimeStr().equals(cutoffTimeNotExceed.get(j).getStartTimeStr())) {
                                            sumTimeDuration = cutoffTimeNotExceed.get(j).getSumTimeDuration();
                                        }
                                    }
                                    if (sumTimeDuration < (timeDiffrencebefore * serviceDeskCount * walkInPercentage) / 100) {
                                        sumTimeDuration = 0;
                                    } else {
                                        sumTimeDuration = sumTimeDuration - (timeDiffrencebefore * serviceDeskCount * walkInPercentage) / 100;
                                    }
                                    int sloat;
                                    sloat = (int) (((timeDiffrencein * serviceDeskCount * walkInPercentage) / 100 - sumTimeDuration) / totalTimeDuration);
                                    customerList.get(i).setAvailabelSlot((int) Math.round(sloat));
                                    customerList.get(i).setExceed("NO");
                                }
                            } else if (nowTime.before(startTime)) {
                                int timeDiffrencein = Common.getTimeDifferenceInMinutes(startTime, endTime);
                                for (int j = 0; j < cutoffTimeExceed.size(); j++) {
                                    if (customerList.get(i).getStartTimeStr().equals(cutoffTimeExceed.get(j).getStartTimeStr())) {
                                        sumTimeDuration = cutoffTimeExceed.get(j).getSumTimeDuration();
                                    }
                                }
                                int sloat;
                                sloat = (int) (((timeDiffrencein * serviceDeskCount) - sumTimeDuration) / totalTimeDuration);
                                customerList.get(i).setAvailabelSlot((int) Math.round(sloat));
                                customerList.get(i).setExceed("NO");
                            } else if (nowTime.after(endTime)) {
                                int timeDiffrencein = Common.getTimeDifferenceInMinutes(startTime, endTime);
                                for (int j = 0; j < cutoffTimeExceed.size(); j++) {
                                    if (customerList.get(i).getStartTimeStr().equals(cutoffTimeExceed.get(j).getStartTimeStr())) {
                                        sumTimeDuration = cutoffTimeExceed.get(j).getSumTimeDuration();
                                    }
                                }
                                int sloat;
                                sloat = (int) (((timeDiffrencein * serviceDeskCount) - sumTimeDuration) / totalTimeDuration);
                                customerList.get(i).setAvailabelSlot((int) Math.round(sloat));
                                customerList.get(i).setExceed("YES");
                            } else if (nowTime.after(startTime) && nowTime.before(endTime)) {
                                int timeDiffrencein = Common.getTimeDifferenceInMinutes(nowTime, endTime);
                                int timeDiffrencebefore = Common.getTimeDifferenceInMinutes(startTime, nowTime);
                                for (int j = 0; j < cutoffTimeExceed.size(); j++) {
                                    if (customerList.get(i).getStartTimeStr().equals(cutoffTimeExceed.get(j).getStartTimeStr())) {
                                        sumTimeDuration = cutoffTimeExceed.get(j).getSumTimeDuration();
                                    }
                                }
                                if (sumTimeDuration < timeDiffrencebefore * serviceDeskCount) {
                                    sumTimeDuration = 0;
                                } else {
                                    sumTimeDuration = sumTimeDuration - timeDiffrencebefore * serviceDeskCount;
                                }
                                int sloat;
                                sloat = (int) (((timeDiffrencein * serviceDeskCount) - sumTimeDuration) / totalTimeDuration);
                                customerList.get(i).setAvailabelSlot((int) Math.round(sloat));
                                customerList.get(i).setExceed("NO");
                            }
                        } else {

                            int timeDiffrencein = Common.getTimeDifferenceInMinutes(startTime, endTime);
                            for (int j = 0; j < cutoffTimeNotExceed.size(); j++) {
                                if (customerList.get(i).getStartTimeStr().equals(cutoffTimeNotExceed.get(j).getStartTimeStr())) {
                                    sumTimeDuration = cutoffTimeNotExceed.get(j).getSumTimeDuration();
                                }
                            }
                            int sloat;
                            sloat = (int) (((timeDiffrencein * serviceDeskCount * walkInPercentage) / 100 - sumTimeDuration) / totalTimeDuration);
                            customerList.get(i).setAvailabelSlot((int) Math.round(sloat));
                            customerList.get(i).setExceed("NO");
                        }
                    }

//                    System.out.println(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(localDate));
//                    if (nowTime.after(startTime) && nowTime.before(totime)) {
//                    }
                }
                customerJson.put("TIMESLOAT", customerList);
            }

            myJson = customerJson.toString();

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }

        return myJson;
    }

    public boolean getTimeSlotAvailabel(Customer data) throws Exception {
        boolean isTrue = false;

        try {
            int sloat = 0;
            int sumTimeDuration = 0;
            int walkInPercentage = reservationDaoImpl.getWalkInPercentage();
            String cuttoffTimestr = reservationDaoImpl.getOnlineCutoffTime();
            int totalTimeDuration = data.getTimeDuration();
            LocalDate localDate = LocalDate.now();
            String localdate = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(localDate);
            Timestamp cutoffTime = Common.getTimestampFromDateAndTime(localdate + " " + cuttoffTimestr);
            String date = data.getBookingdate();

            Timestamp startTime = Common.getTimestampFromDateAndTime(date + " " + data.getStartTimeStr());
            Timestamp endTime = Common.getTimestampFromDateAndTime(date + " " + data.getEndtimeStr());
            Timestamp nowTime = Common.getCurrentTimeStamp();

            int serviceDeskCount = reservationDaoImpl.getCountServiceDesk();
            int timeDiffrencein = Common.getTimeDifferenceInMinutes(startTime, endTime);
            int cutoffTimeNotExceed = reservationDaoImpl.getSumDurationTimeBlock(startTime, "AND ISONLINE=0");
            int cutoffTimeExceed = reservationDaoImpl.getSumDurationTimeBlock(startTime, "");

            if (localdate.equals(date)) {

                if (cutoffTime.after(nowTime)) {

                    if (nowTime.before(startTime)) {

                        sloat = (int) (((timeDiffrencein * serviceDeskCount * walkInPercentage) / 100 - cutoffTimeNotExceed) / totalTimeDuration);

                    } else if (nowTime.after(endTime)) {

                        sloat = (int) (((timeDiffrencein * serviceDeskCount * walkInPercentage) / 100 - cutoffTimeNotExceed) / totalTimeDuration);

                    } else if (nowTime.after(startTime) && nowTime.before(endTime)) {

                        timeDiffrencein = Common.getTimeDifferenceInMinutes(nowTime, endTime);
                        int timeDiffrencebefore = Common.getTimeDifferenceInMinutes(startTime, nowTime);

                        if (cutoffTimeNotExceed < (timeDiffrencebefore * serviceDeskCount * walkInPercentage) / 100) {
                            cutoffTimeNotExceed = 0;
                        } else {
                            cutoffTimeNotExceed = cutoffTimeNotExceed - (timeDiffrencebefore * serviceDeskCount * walkInPercentage) / 100;
                        }
                        sloat = (int) (((timeDiffrencein * serviceDeskCount * walkInPercentage) / 100 - cutoffTimeNotExceed) / totalTimeDuration);

                    }
                } else if (nowTime.before(startTime)) {

                    sloat = (int) (((timeDiffrencein * serviceDeskCount) - cutoffTimeExceed) / totalTimeDuration);

                } else if (nowTime.after(endTime)) {

                    sloat = (int) (((timeDiffrencein * serviceDeskCount) - cutoffTimeExceed) / totalTimeDuration);

                } else if (nowTime.after(startTime) && nowTime.before(endTime)) {

                    timeDiffrencein = Common.getTimeDifferenceInMinutes(nowTime, endTime);
                    int timeDiffrencebefore = Common.getTimeDifferenceInMinutes(startTime, nowTime);

                    if (cutoffTimeExceed < (timeDiffrencebefore * serviceDeskCount * walkInPercentage) / 100) {
                        cutoffTimeExceed = 0;
                    } else {
                        cutoffTimeExceed = cutoffTimeExceed - (timeDiffrencebefore * serviceDeskCount * walkInPercentage) / 100;
                    }
                    sloat = (int) ((timeDiffrencein * serviceDeskCount - cutoffTimeExceed) / totalTimeDuration);

                }
//                        
            } else {

                sloat = (int) (((timeDiffrencein * serviceDeskCount * walkInPercentage) / 100 - cutoffTimeNotExceed) / totalTimeDuration);

            }

            if (sloat > 0) {
                isTrue = true;
            }
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }

        return isTrue;
    }

    public List<Customer> setHafDayCustomerList(String date, List<Holiday> holidays) throws Exception {
        List<Customer> customerList = new ArrayList<>();
        Customer cus;
        for (int i = 0; i < holidays.size() - 1; i++) {

            Timestamp holidayStartTime = Common.getTimestampFromDateAndTime(date + " " + holidays.get(i).getStartTime());
            Timestamp holidayEndTime = Common.getTimestampFromDateAndTime(date + " " + holidays.get(i).getEndTime());
            Timestamp holidayNextStartTime = Common.getTimestampFromDateAndTime(date + " " + holidays.get(i + 1).getStartTime());

            int timegraphalfday = Common.getTimeDifferenceInMinutes(holidayStartTime, holidayEndTime);
            int timegrapworking = Common.getTimeDifferenceInMinutes(holidayEndTime, holidayNextStartTime);
            if (timegraphalfday > 0) {
                cus = new Customer();
                cus.setStartTimeStr(holidays.get(i).getStartTime());
                cus.setEndtimeStr(holidays.get(i).getEndTime());
                cus.setHalfday("YES");
                customerList.add(cus);
                timegraphalfday = 0;
            }
            if (timegrapworking > 0) {
                String startTime = holidays.get(i).getEndTime();
                String endTime = holidays.get(i + 1).getStartTime();
                String[] tokens;
                tokens = startTime.split(":");
                if (!"00".equals(tokens[1])) {
                    int addHour = Integer.parseInt(tokens[0]);
                    addHour = addHour + 1;
                    String newStartTime = Common.timeConcatanation(String.valueOf(addHour) + ":00");

                    int d1 = Common.getTimeDifferenceInMinutes(Common.getTimestampFromDateAndTime(date + " " + startTime), Common.getTimestampFromDateAndTime(date + " " + endTime));
                    int d2 = Common.getTimeDifferenceInMinutes(Common.getTimestampFromDateAndTime(date + " " + startTime), Common.getTimestampFromDateAndTime(date + " " + newStartTime));
                    if (d1 > d2) {
                        cus = new Customer();
                        cus.setStartTimeStr(startTime);
                        cus.setEndtimeStr(newStartTime);
                        customerList.add(cus);
                        startTime = newStartTime;
                    }
                }
                Timestamp dayStartTime = Common.getTimestampFromDateAndTime(date + " " + startTime);
                Timestamp dayEndTime = Common.getTimestampFromDateAndTime(date + " " + endTime);
                int timeDiffrence = Common.getTimeDifferenceInMinutes(dayStartTime, dayEndTime);
                int hour = timeDiffrence / 60;
                int minute = 60;
                int modulestime = timeDiffrence % 60;

                for (int k = 0; k < hour; k++) {
                    cus = new Customer();
                    if (k == 0) {
                        cus.setStartTimeStr(Common.getTimeFromTimestamp(dayStartTime));
                        cus.setEndtimeStr(Common.AddMinutesToTime(dayStartTime, 60));
                    } else {
                        cus.setStartTimeStr(Common.AddMinutesToTime(dayStartTime, minute));
                        cus.setEndtimeStr(Common.AddMinutesToTime(dayStartTime, minute + 60));
                        minute = minute + 60;
                    }
                    customerList.add(cus);
                }
                if (modulestime > 0) {
                    if (hour > 0) {
                        cus = new Customer();
                        cus.setStartTimeStr(Common.AddMinutesToTime(dayStartTime, minute));
                        cus.setEndtimeStr(Common.AddMinutesToTime(dayStartTime, minute + modulestime));
                        customerList.add(cus);
                    } else {
                        cus = new Customer();
                        cus.setStartTimeStr(Common.AddMinutesToTime(dayStartTime, 0));
                        cus.setEndtimeStr(Common.AddMinutesToTime(dayStartTime, 0 + modulestime));
                        customerList.add(cus);
                    }

                }
                timegrapworking = 0;

            }
            System.out.println("sssss");
        }

        return customerList;
    }

    public String setOldDocumentTypeListWhere(List<DocumentType> list) throws Exception {
        String where = "";
        try {
            for (int i = 0; i < list.size(); i++) {
                if (i == 0) {
                    where = where + "WHERE DOCUMENTTYPEID<>'" + list.get(i).getDocumentTypeId() + "'";
                } else {
                    where = where + "AND DOCUMENTTYPEID<>'" + list.get(i).getDocumentTypeId() + "'";
                }
            }
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return where;
    }

}
