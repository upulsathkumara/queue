/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.queuemgr.dao.impl.CustomerDAOImpl;
import com.queuemgr.dao.impl.DocumentTypeDAOImpl;
import com.queuemgr.dao.impl.OnlineBookingDAOImpl;
import com.queuemgr.dao.impl.ReschduleDAOImpl;
import com.queuemgr.dao.impl.ReservationDaoImpl;
import com.queuemgr.modals.BookingVerification;
import com.queuemgr.modals.Customer;
import com.queuemgr.modals.DocumentCharges;
import com.queuemgr.modals.DocumentType;
import com.queuemgr.modals.Instruction;
import com.queuemgr.util.Common;
import com.queuemgr.util.LogFileCreator;
import com.queuemgr.util.StatusVarList;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author user
 */
@Controller
public class ReschduleApprovalController {

    @Autowired
    DocumentTypeDAOImpl documentTypeDAOImpl;

    @Autowired
    CustomerDAOImpl customerDAOImpl;

    @Autowired
    ReservationDaoImpl reservationDaoImpl;

    @Autowired
    OnlineBookingDAOImpl onlineDaoImpl;

    @Autowired
    ReschduleDAOImpl reschduleDaoimpl;

    @RequestMapping(value = "/reservation/reschdule/approval", method = RequestMethod.GET)
    public String loadReservationCreatePage(@ModelAttribute("reservationForm") Customer data, ModelMap model) {
        try {

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date dateNow = new Date();

            List<DocumentType> documentTypeList = documentTypeDAOImpl.getDocumentTypeBeanList();
            JSONObject customerJson = new JSONObject();
            customerJson.put("TYPE", documentTypeList);
            model.put("documentType", documentTypeDAOImpl.getDocumentTypeList());
            model.put("applicationType", documentTypeDAOImpl.getApplicationTypeList(0));
            model.put("Type", customerJson.toString());
            model.put("dateNow", dateFormat.format(dateNow));

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "reservation/reschduleAdmin";
    }

    @RequestMapping(value = "/reservation/reschdule/customer", method = RequestMethod.POST)
    public @ResponseBody
    String createAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String jsoncontent = request.getParameter("data");
        JSONObject rsp = new JSONObject(jsoncontent);
        List<Customer> customerList;
        List<DocumentType> documentTypeList;
        List<DocumentType> documentTypeList1;
        String myJson;
        JSONObject customerJson = new JSONObject();
        try {

            customerList = reschduleDaoimpl.getReschduleCustomer(Integer.parseInt(rsp.getString("serachValue").trim()));
            customerJson.put("CUSTOMER", customerList);
            if (customerList.size() > 0) {
                String reasons = reschduleDaoimpl.getReasons(Integer.parseInt(rsp.getString("serachValue").trim()));
                customerJson.put("REASON", reasons);
                documentTypeList = onlineDaoImpl.getOnlineBookinDocument(Integer.parseInt(rsp.getString("serachValue").trim()));
                documentTypeList1 = onlineDaoImpl.getOnlineBookinDocuments(rsp.getInt("serachValue"));
                List<BookingVerification> verificationtList = onlineDaoImpl.getOnlineBookinDocumentVerification(Integer.parseInt(rsp.getString("serachValue").trim()));
                customerJson.put("DOCUMENTDESCRIPTION", documentTypeList);
                customerJson.put("DOCUMENT", documentTypeList1);
                customerJson.put("VERFICATIONLIST", verificationtList);
            }

        } catch (Exception exception) {
            LogFileCreator.writeErrorToLog(exception);
            customerJson.put("CODE", "ERROR");
            Logger.getLogger(ReservationController.class.getName()).log(Level.SEVERE, null, exception);
        }
        myJson = customerJson.toString();
        return myJson;
    }

    @RequestMapping(value = "/reservation/reschdule/updated", method = RequestMethod.POST)
    public String createBookingPage(@ModelAttribute("reservationForm") Customer data, ModelMap model, HttpServletRequest request) {
        try {

            Gson gson = new Gson();
            String loggedUser = request.getSession().getAttribute("loggedUser").toString();

            List<BookingVerification> bookingVerification = gson.fromJson(data.getDocumentVarifictionList(), new TypeToken<List<BookingVerification>>() {
            }.getType());
            reschduleDaoimpl.reschduleCustomerUpdate(data, loggedUser, bookingVerification, Integer.parseInt(data.getBookingId()));
            if (StatusVarList.APPOINMENT_RESCHDULE_REJECT.equals(data.getReschduleType())) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date dateNow = new Date();

                List<DocumentType> documentTypeList = documentTypeDAOImpl.getDocumentTypeBeanList();
                JSONObject customerJson = new JSONObject();
                customerJson.put("TYPE", documentTypeList);
                model.put("documentType", documentTypeDAOImpl.getDocumentTypeList());
                model.put("applicationType", documentTypeDAOImpl.getApplicationTypeList(0));
                model.put("Type", customerJson.toString());
                model.put("dateNow", dateFormat.format(dateNow));
                model.put("errorMsg", "Successfully updated the record");
                return "reservation/reschduleAdmin";
            } else if (StatusVarList.APPOINMENT_RESCHDULE_APPROVAL.equals(data.getReschduleType())) {
                return "redirect:/reservation/reschdule/update/success/" + data.getBookingId();
            }

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "reservation/reschduleAdmin";
    }

    @RequestMapping(value = "/reservation/reschdule/update/success/{bookingid}", method = RequestMethod.GET)
    public String loadReservationCreateSuccessPage(ModelMap model, @PathVariable("bookingid") int bookingId) {
        try {
            Customer data = reservationDaoImpl.getRecevationSuccesData(bookingId);

            Instruction instruction = reservationDaoImpl.getInstructionwalk(reservationDaoImpl.getApoinmentType(bookingId), data.getPreferredLanguage());
            List<BookingVerification> verification = reservationDaoImpl.getverificationDoc(bookingId);
            String startTime = Common.getTimeFromTimestampAMPM(Common.getTimestampFromDateAndTime(data.getBookingdate() + " " + data.getStartTimeStr()));
            String endTime = Common.getTimeFromTimestampAMPM(Common.getTimestampFromDateAndTime(data.getBookingdate() + " " + data.getEndtimeStr()));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            QRCodeWriter writer = new QRCodeWriter();
            BitMatrix matrix = writer.encode(Integer.toString(bookingId), BarcodeFormat.QR_CODE, 125, 125);
            MatrixToImageWriter.writeToStream(matrix, "png", output);

            String imageDataString = Base64.encodeBase64URLSafeString(output.toByteArray()).replace("_", "/").replace("-", "+");
            DateFormat df = new SimpleDateFormat("HH:mm:ss");
            Date dateobj = new Date();
            model.put("STARTTIME", data.getStartTimeStr());
            model.put("ENDTIME", data.getEndtimeStr());
            model.put("DATE", Common.dateforamtChange(data.getBookingdate()));
            model.put("NAME", data.getFristName() + data.getLastName());
            model.put("MOBILE", data.getTelephoneMobile());
            model.put("EMAIL", data.getEmail());
            model.put("SERVICEID", bookingId);
            model.put("QRIMAGEPATH", imageDataString);
            model.put("CREATEDTIME", startTime + " - " + endTime);
            model.put("IDENTIFICATION", data.getIdentificatioDocumentType() + ":" + data.getIdentificationNumber());

            model.put("INSTRUCTION", instruction.getLang());
            model.put("BOOKINGREFERENCE", verification);
        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return "reservation/reschduleSucccesfull";
    }

}
