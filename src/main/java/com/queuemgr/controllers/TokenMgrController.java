/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.controllers;

import com.queuemgr.dao.impl.TokenManagerDaoImpl;
import com.queuemgr.modals.ApplicantObject;
import com.queuemgr.modals.DocumentObject;
import com.queuemgr.modals.ResponseToEDAS;
import com.queuemgr.modals.Token;
import com.queuemgr.util.LogFileCreator;
import com.queuemgr.util.StatusVarList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Maheshm
 */
@Controller
@RequestMapping("/callNext")
public class TokenMgrController {

    @Autowired
    TokenManagerDaoImpl tokenMgrDaoImpl;

    @RequestMapping(value = "{sdId}", method = RequestMethod.GET)
    public @ResponseBody
    Token getNextToken(@PathVariable String sdId) {
        Token token = null;
        try {
            token = tokenMgrDaoImpl.getAvailableToken();
            
            ApplicantObject applicantObj = tokenMgrDaoImpl.getCustomerObjectByServiceId(token.getServiceId());
            token.setApplicantObject(applicantObj);
            
            List<DocumentObject> docObj = tokenMgrDaoImpl.getDocumentObListByServiceId(token.getServiceId());
            token.setDocumentObjects(docObj);
            
            if (tokenMgrDaoImpl.updateStatusBeforeSendToken(sdId, token.getServiceId())) {

            } else {
                token = new Token();
                token.setTokenNumber(StatusVarList.NO_AVAILABLE_TOKEN);
            }

        } catch (Exception e) {
            LogFileCreator.writeErrorToLog(e);
            e.printStackTrace();
            token = new Token();
            token.setTokenNumber(StatusVarList.NO_AVAILABLE_TOKEN);
        }
        return token;
    }

    
    @RequestMapping(value="/updateQueue", method = RequestMethod.GET)
    public @ResponseBody
    ResponseToEDAS updateQueue(@RequestParam("serId") String serId,@RequestParam("status") String status) {
        ResponseToEDAS response = null;
        try {
            response = tokenMgrDaoImpl.updateBookingServiceStatus(serId, status);
        } catch (Exception e) {
            LogFileCreator.writeErrorToLog(e);
            response = new ResponseToEDAS();
            response.setMessage(StatusVarList.NO_AVAILABLE_TOKEN);
        }
        return response;
    }
    
}
