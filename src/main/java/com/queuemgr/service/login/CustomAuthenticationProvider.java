/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.service.login;


import com.queuemgr.dao.impl.UserManagementDAOImple;
import com.queuemgr.modals.SystemUser;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 *
 * @author Hirantha
 */
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private UserManagementDAOImple userDAO;

    public void setUserDAO(UserManagementDAOImple userDAO) {
        this.userDAO = userDAO;
    }

    public UserManagementDAOImple getUserDAO() {
        return userDAO;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName().trim();
        String password = authentication.getCredentials().toString().trim();
        SystemUser sysUser = null;
        List<GrantedAuthority> grantedAuths;
        try {
            sysUser = userDAO.getAuthUserById(name);
        } catch (Exception ex) {
            Logger.getLogger(CustomAuthenticationProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (sysUser == null || sysUser.getUsername().equals("")) {
            return null;
        }
        if (password.equals(sysUser.getPassword())) {
            grantedAuths = new ArrayList();
            grantedAuths.add(new SimpleGrantedAuthority((String) sysUser.getUserrole()));
        } else {
            return null;
        }

        Authentication auth = new UsernamePasswordAuthenticationToken(name, password, grantedAuths);
        return auth;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
