/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.util;

//import epic_login_auth.EPIC_LOGIN_AUTH;
//import epic_login_auth.JSONException_Exception;
//import epic_login_auth.LoginAuth;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

/**
 *
 * @author Maheshm
 */
public class Common {

    public static int getTimeDifferenceInMinutes(Timestamp startTime, Timestamp endTime) {
        int diff = (int) (endTime.getTime() - startTime.getTime());

        return diff / (60 * 1000);
    }

//    public static double getTimeDifferenceInMinutesDouble(Timestamp startTime, Timestamp endTime) {
//        double diff = (int) (endTime.getTime() - startTime.getTime());
//
//        return diff / (60 * 1000);
//    }
    public static String getDateString() {
        String date = new SimpleDateFormat("yyyy-MM-dd").format(System.currentTimeMillis());
        return date;
    }

    public static String getCurrentDateTime() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateobj = new Date();
        return df.format(dateobj);
    }

    public static String AddMinutesToTime(Timestamp date, int minutesToAdd) throws ParseException {
        long t = date.getTime();
        long m = minutesToAdd * 60 * 1000;
        Timestamp time = new Timestamp(t + m);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String timeConvert = sdf.format(time.getTime());

        return timeConvert;
    }

    public static Timestamp getTimestampFromDateAndTime(String date) throws ParseException {
        //2014-08-12 11:23
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        java.util.Date consDate = sdf.parse(date);

        return new Timestamp(consDate.getTime());
    }

    public static java.sql.Timestamp getCurrentTimeStamp() {
        return new java.sql.Timestamp(System.currentTimeMillis());
    }

    public static String getToken(Timestamp startTime, int deskNo) {
        String[] schoolbag = new String[26];
        schoolbag[0] = "A";
        schoolbag[1] = "B";
        schoolbag[2] = "C";
        schoolbag[3] = "D";
        schoolbag[4] = "E";
        schoolbag[5] = "F";
        schoolbag[6] = "G";
        schoolbag[7] = "H";
        schoolbag[8] = "I";
        schoolbag[9] = "J";
        schoolbag[10] = "K";
        schoolbag[11] = "L";
        schoolbag[12] = "M";
        schoolbag[13] = "N";
        schoolbag[14] = "O";
        schoolbag[15] = "P";
        schoolbag[16] = "Q";
        schoolbag[17] = "R";
        schoolbag[18] = "S";
        schoolbag[19] = "T";
        schoolbag[20] = "U";
        schoolbag[21] = "V";
        schoolbag[22] = "W";
        schoolbag[23] = "X";
        schoolbag[24] = "Y";
        schoolbag[25] = "Z";

        String token = "W";
        for (int i = 0; i < schoolbag.length; i++) {
            if (deskNo - 1 == i) {
                token = token + schoolbag[i];
                break;
            }
        }
        String time = getTimeFromTimestamp(startTime);
        time = time.replace(":", "");
        token = token + time;

        return token;
    }

    public static String getTimeFromTimestamp(Timestamp gettime) {
        String time = new SimpleDateFormat("HH:mm").format(gettime);
        return time;
    }
    public static String getTimeFromTimestampAMPM(Timestamp gettime) {
        String time = new SimpleDateFormat("HH:mm a").format(gettime);
        return time;
    }

    public static String getToday() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static String generateQrCode(String serviceId, String path) {
//        String path = "src\\main\\webapp\\resources\\img\\QR\\" + serviceId + ".JPG";
        try {
            ByteArrayOutputStream out = QRCode.from(serviceId).to(ImageType.PNG).stream();
            FileOutputStream fout = new FileOutputStream(new File(path));

            fout.write(out.toByteArray());

            fout.flush();
            fout.close();
        } catch (Exception e) {
            LogFileCreator.writeErrorToLog(e);
            e.printStackTrace();
        }
        return path;
    }

    public static String generateQrCodeSuccess(String serviceId) {
        String path = "resources/img/QR/" + serviceId + ".JPG";
//        String path = "src\\main\\webapp\\resources\\img\\QR\\" + serviceId + ".JPG";
        try {
            ByteArrayOutputStream out = QRCode.from(serviceId).to(ImageType.PNG).stream();
            FileOutputStream fout = new FileOutputStream(new File(path));

            fout.write(out.toByteArray());

            fout.flush();
            fout.close();
        } catch (Exception e) {
            LogFileCreator.writeErrorToLog(e);
        }
        return path;
    }

    public static void deleteFile(String path) {
        try {
//            String path = "src\\main\\webapp\\resources\\img\\QR\\" + fieName;
            File file = new File(path);

            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
                System.out.println("Delete operation is failed.");
            }

        } catch (Exception e) {
            LogFileCreator.writeErrorToLog(e);
            e.printStackTrace();

        }

    }

    public static String getMD5(String password) {
        String md5String = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());

            byte byteData[] = md.digest();

            //convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            md5String = sb.toString();
        } catch (Exception e) {
            LogFileCreator.writeErrorToLog(e);
            e.printStackTrace();
        }

        return md5String;
    }

    public static Timestamp getTimestampFromDateAndTime(String format, String datetime) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date consDate = sdf.parse(datetime);
        return new Timestamp(consDate.getTime());
    }

    public void sendEmail(String reciever, String sender, String content, String subject) throws MessagingException {

        final String username = "amjjrathnayake@gmail.com";
        final String password = "Sameera@0716534707";

//                Properties props = new Properties();
        java.util.Properties props = System.getProperties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("aspjayasinghe@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(reciever));
            message.setSubject(subject);
            message.setText(content);
            Transport.send(message);
        } catch (MessagingException e) {
            LogFileCreator.writeErrorToLog(e);
            throw new RuntimeException(e);
        }

    }

    public static String getStringToTimestamp(Timestamp time) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String timeConvert = sdf.format(time.getTime());

        return timeConvert;
    }

    public static String formatDay(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public static String parseDate(String date, String format) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.parse(date).toString();
    }

    public static String timeConcatanation(String time) throws ParseException {
        if (time.length() < 5) {
            time = "0" + time;
        }
        System.out.println("hhhh");
        return time;
    }

    public static String dateforamtChange(String date1) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = (Date) formatter.parse(date1);
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy/MM/dd");
        String finalString = newFormat.format(date);

        return finalString;
    }
}
