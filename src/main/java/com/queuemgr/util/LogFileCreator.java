/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.util;

/**
 *
 * @author Maheshm
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class LogFileCreator {

    // ---------------------------------------------------------------------------------------------------------------------------------------
    /**
     * This method writes given message to an information log file in given
     * path.
     */
    public static void writeInfoToLog(String msg) {

        String line = "\n_______________________________________________________________________________________\n";
        String filename = getFileName() + "_QMS";
        BufferedWriter bw = null;

        if (StatusVarList.FILE_PATH_LOG_FILE == null) {
            File file = new File("Init/Infor");
            if (!file.exists()) {
                file.mkdirs();
            }
            filename = "Init/Infor/" + filename + "_Infor.txt";
        } else {
            File file = new File(StatusVarList.FILE_PATH_LOG_FILE + "/Infor");
            if (!file.exists()) {
                file.mkdirs();
            }
            filename = StatusVarList.FILE_PATH_LOG_FILE + "/Infor/" + filename + "_Infor.txt";
        }

        msg = line + getTime() + "\n" + msg;

        try {
            bw = new BufferedWriter(new FileWriter(filename, true));
            bw.write(msg);
            bw.newLine();
            bw.flush();
        } catch (Exception ioe) {
            System.out.println("Problem when writing to log file in LogFileCreator.writInforTologs()");
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------
    /**
     * This method writes given exceptions to an error log file in given path.
     */
    public static void writeErrorToLog(Throwable aThrowable) {

        String line = "\n_______________________________________________________________________________________\n";
        String filename = getFileName() + "_QMS";
        BufferedWriter bw = null;
        String msg = "";

        if (StatusVarList.FILE_PATH_LOG_FILE == null) {
            File file = new File("Init/Error");
            if (!file.exists()) {
                file.mkdirs();
            }
            filename = "Init/Error/" + filename + "_Error.txt";
        } else {
            File file = new File(StatusVarList.FILE_PATH_LOG_FILE + "/Error");
            if (!file.exists()) {
                file.mkdirs();
            }
            filename = StatusVarList.FILE_PATH_LOG_FILE + "/Error/" + filename + "_Error.txt";
        }

        msg = line + getTime() + ":    " + getStackTrace(aThrowable);

        try {
            bw = new BufferedWriter(new FileWriter(filename, true));
            bw.write(msg);
            bw.newLine();
            bw.flush();
        } catch (Exception ioe) {
            System.out.println("Problem when writing to log file in LogFileCreator.writErrorToLog()" + ioe);
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException ioe2) {
                    System.out.println("Problem when writing to log file in LogFileCreator.writErrorToLog()" + ioe2);
                }
            }
        }
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------	
    /**
     * This method is used to get the file name including the date
     */
    private static String getFileName() {

        GregorianCalendar now = new GregorianCalendar();
        now.getTime();
        int intCurrentYear = now.get(Calendar.YEAR);
        int intCurrentMonth = now.get(Calendar.MONTH) + 1;
        int intCurrentDay = now.get(Calendar.DAY_OF_MONTH);

        String m = "";
        String d = "";

        m = intCurrentMonth + "";
        d = intCurrentDay + "";

        if (intCurrentMonth < 10) {
            m = "0" + intCurrentMonth + "";
        }
        if (intCurrentDay < 10) {
            d = "0" + intCurrentDay + "";
        }

        String file = intCurrentYear + "" + m + d;
        return file;
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------
    /**
     * This method is used to derive time
     */
    private static String getTime() {

        GregorianCalendar now = new GregorianCalendar();
        return now.getTime().toString();
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------
    /**
     * This method is used to get the stack trace of error
     */
    private static String getStackTrace(Throwable aThrowable) {
        String re = "";
        Writer result = null;
        PrintWriter printWriter = null;
        try {
            result = new StringWriter();
            printWriter = new PrintWriter(result);

            aThrowable.printStackTrace(printWriter);
            re = result.toString();
            result.close();
            printWriter.close();

        } catch (Exception e) {
            System.out.println("Error when getting error description getStackTrace()" + e);
        } finally {

            try {
                if (result != null) {
                    result.close();
                }
                if (printWriter != null) {
                    printWriter.close();
                }
            } catch (IOException e) {
                System.out.println("Error when getting error description getStackTrace()" + e);
            }
        }
        return re;
    }

}
