/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.util;

import java.rmi.RemoteException;
import lk.icta.schemas.xsd.kannel.handler.v1.GovSMSAuthenticationFaultException1;
import lk.icta.schemas.xsd.kannel.handler.v1.GovSMSInvalidDepartmentFaultException2;
import lk.icta.schemas.xsd.kannel.handler.v1.GovSMSRoutingFaultException0;
import lk.icta.schemas.xsd.kannel.handler.v1.MTSmsHandlerServiceStub;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;

/**
 *
 * @author user
 */
public class SMSSender {

    public static void send(String message, String mobile) {
        try {
            mobile = SMSSender.plusRemoveMobileNumber(mobile);

            String epr = "http://lankagate.gov.lk:9080/services/GovSMSMTHandlerProxy.GovSMSMTHandlerProxyHttpSoap11Endpoint";

            MTSmsHandlerServiceStub stub = new MTSmsHandlerServiceStub(epr);

            Options options = stub._getServiceClient().getOptions();
            options.setProperty(HTTPConstants.CHUNKED, Boolean.FALSE);

            ServiceClient serviceClient = stub._getServiceClient();
            OMFactory factory = OMAbstractFactory.getOMFactory();
            OMNamespace namespace = factory.createOMNamespace(
                    "http://govsms.icta.lk/", "govsms");
            OMElement authData = factory.createOMElement("authData", namespace);

            OMElement user = factory.createOMElement("user", namespace);
            user.setText("icta");

            OMElement key = factory.createOMElement("key", namespace);
            key.setText("g0v5ms123");

            authData.addChild(user);
            authData.addChild(key);
            serviceClient.addHeader(authData);

            MTSmsHandlerServiceStub.SMSRequestData requestData = new MTSmsHandlerServiceStub.SMSRequestData();
            requestData.setDepCode("MFA");
//            requestData.setDepCode("TSHDA");
            requestData.setOutSms(message);
            requestData.setRecepient(mobile);
            requestData.setSmscId("");
            requestData.setBillable("");

            MTSmsHandlerServiceStub.SMSRequest sMSRequest = new MTSmsHandlerServiceStub.SMSRequest();

            MTSmsHandlerServiceStub.SMSRequestData requests[] = new MTSmsHandlerServiceStub.SMSRequestData[1];

            requests[0] = requestData;

            sMSRequest.setRequestData(requests);

            MTSmsHandlerServiceStub.SMSAck ack = stub.sendSms(sMSRequest);
        } catch (RemoteException | GovSMSRoutingFaultException0 | GovSMSAuthenticationFaultException1 | GovSMSInvalidDepartmentFaultException2 ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
    }

    public static String plusRemoveMobileNumber(String mobile) {
        String replace = "";
        try {
            char ch1 = mobile.charAt(0);
            String word = Character.toString(ch1);
            if (("+").equals(word)) {
                replace = mobile.substring(1);
            } else {
                replace = mobile;
            }

        } catch (Exception ex) {
            LogFileCreator.writeErrorToLog(ex);
            ex.printStackTrace();
        }
        return replace;
    }
}
