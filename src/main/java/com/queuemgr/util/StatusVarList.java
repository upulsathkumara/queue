/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.queuemgr.util;

import java.io.File;

/**
 *
 * @author Maheshm
 */
public class StatusVarList {

    public static String BOOKING_CONFIRMED = "CONF";
    public static String READY_TO_QUEUE = "RDTQ";
    public static String SENT_TO_QUEUE = "SETQ";
    public static String PENDING = "PEND";
    public static String BOOKING_INITIAL = "INIT";
    public static String QUEUE_ABSENT = "QABS";
    public static String SENT_TO_HELP_DESK = "SEHD";
    public static String FRONT_DESK_IMMEDIATE = "FDIM";
    public static String FRONT_DESK_AWAITING_APPROVAL = "FDAA";
    public static String FRONT_DESK_NORMAL = "FDNM";
    public static String APPOINMENT_TYPE_IMMEDIATE = "IMMEDIATE";
    public static String APPOINMENT_TYPE_NORMAL = "NORMAL";
    public static String APPOINMENT_RESCHDULE_REJECT = "REJECT";
    public static String APPOINMENT_RESCHDULE_APPROVAL = "APPROVAL";
    public static String APPOINMENT_WALKIN = "WALKIN";
    public static String APPOINMENT_WALKIN_SEND_TO_HELP_DESK = "WALKINSEHD";
    public static String APPOINMENT_VIP = "VIP";
    public static String APPOINMENT_VIP_SEND_TO_HELP_DESK = "VIPSEHD";

    public static String ONLINE_CUSTOMER = "1";
    public static String WALKING_CUSTOMER = "0";

    public static String NO_AVAILABLE_TOKEN = "NOT_AVAILABLE";

    public static String EDAS_USER = "EDAS";

    //response messages and response codes
    public static String RESPONSE_UPATEFAILED = "Update Failed";
    public static String RESPONSE_CODE_UPATEFAILED = "FAILED";
    public static String RESPONSE_UPATE_SUCCESS = "Successfully Updated";
    public static String RESPONSE_CODE_UPATE_SUCCESS = "SUCCESS";

    public static String ACTIVE = "ACTV";
    public static String QM_CODE_STATUS_REJECT = "REJT";
    public static String QM_CODE_STATUS_ACCEPT = "ACCP";
    public static final int QM_CODE_STATUS_CATEGORY_DOCUMENT_VERIFICATION = 5;

    public static final String REPORT_LOG_LOCATION = File.separator + "resources" + File.separator + "img" + File.separator + "Emblem_of_Sri_Lanka.png";
    public static final String REPORT_LOCATION = File.separator + "WEB-INF" + File.separator + "views" + File.separator + "jsp" + File.separator + "reports";
    public static final String REPORT_NAME_COUNTSUMMERYREPORT = File.separator + "jasper" + File.separator + "countersummary.jasper";
    public static final String REPORT_NAME_COUNTSUMMERYREPORTBYHOURS = File.separator + "jasper" + File.separator + "newcountersummary.jasper";
    public static final String REPORT_NAME_APPLICANTSUMMERYREPORT = File.separator + "jasper" + File.separator + "appsummary.jasper";
    public static final String REPORT_NAME_APPLICANTDETAILSREPORT = File.separator + "jasper" + File.separator + "applicantdetail_2.jasper";
    public static final String REPORT_NAME_USER_DURATIONREPORT = File.separator + "jasper" + File.separator + "userdureation.jasper";

    public static String ACCESS_TOKEN = "30176dcbccfd523e7f42c7c0e003392c";
    
//    public static String FILE_PATH_LOG_FILE = "/usr/local/QMS_LOGS";
     public static String FILE_PATH_LOG_FILE = "C:\\QMSLOGS";
}
